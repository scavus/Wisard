# About
Wisard is a GPU-based renderer I've written to learn physically based rendering and GPU programming.

## Features
  * CUDA and OpenCL backends.
  * BVH2 accelerator with a traversal method described in [1].
  * Nvidia OptiX Prime accelerator for CUDA backend.
  * AMD Firerays accelerator for OpenCL backend.
  * [KIntegratorProgressiveUdpt](src/Kernels/KIntegratorProgressiveUdpt.c): Simple progressive path tracer with path regeneration [2].
  * [KIntegratorProgressiveDeferredUdpt](src/Kernels/KIntegratorProgressiveDeferredUdpt.c): Wavefront path tracer based on [3].
  * [KIntegratorBranchedDeferredUdpt](src/Kernels/KIntegratorBranchedDeferredUdpt.c): Wavefront path tracer like above, but shoots multiple rays per primary hit point similar to Blender Cycles and Arnold renderer. This was an experimental effort which turned out to be quite messy.

## References
[1] Aila, Timo, and Samuli Laine. "Understanding the efficiency of ray traversal on GPUs." *Proceedings of the conference on high performance graphics 2009*. ACM, 2009.

[2] Novák, Jan, Vlastimil Havran, and Carsten Dachsbacher. "Path regeneration for interactive path tracing." *Eurographics (Short Papers)*. 2010.

[3] Laine, Samuli, Tero Karras, and Timo Aila. "Megakernels considered harmful: wavefront path tracing on GPUs." *Proceedings of the 5th High-Performance Graphics Conference*. ACM, 2013.
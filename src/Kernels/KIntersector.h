/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"

#define WR_INTERSECTOR(name, geom_type, accelerator_type)\
    WR_KERNEL void name(WR_GMEM_PARAM SRay* rays,\
        WR_GMEM_PARAM U32* active_ray_stream,\
        WR_GMEM_PARAM SStreamInfo* active_ray_stream_info,\
        WR_GMEM_PARAM const geom_type* WR_RESTRICT geom_buf,\
        WR_GMEM_PARAM const accelerator_type* WR_RESTRICT accelerator_buf,\
        WR_GMEM_PARAM SIntersectionData* intersection_datas)

#define WR_INTERSECTOR_START()
#define WR_INTERSECTOR_END()

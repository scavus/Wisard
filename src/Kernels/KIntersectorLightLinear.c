/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KCommonTypes.h"
#include "KGeometry.h"
#include "KDeferredIntegratorCommon.h"
#include "KLight.h"
#include "KSceneMetadata.h"

WR_PROGRAM_BEGIN

WR_KERNEL void IntersectionLightLinear(
    WR_GMEM_PARAM SRay* g_rays,
    WR_GMEM_PARAM U32* g_active_ray_stream,
    WR_GMEM_PARAM SStreamInfo* g_active_ray_stream_info,
    WR_GMEM_PARAM SAreaLight* g_area_lights,
    U32 g_area_light_count,
	WR_GMEM_PARAM SIntersectionData* g_intersection_datas)
{
    U32 thread_idx = GetGlobalIdX();

    if (thread_idx >= g_active_ray_stream_info->counter)
    {
        return;
    }

    U32 work_id = g_active_ray_stream[thread_idx];
    SRay ray = g_rays[work_id];
    SIntersectionData intersection_data = g_intersection_datas[work_id];

	for (U32 area_light_idx = 0; area_light_idx < g_area_light_count; area_light_idx++)
	{
		SAreaLight area_light = g_area_lights[area_light_idx];
		bool b_intersects = false;

		switch (area_light.type)
		{
			case LIGHT_TYPE_QUAD:
			{
				b_intersects = IntersectQuad(&ray, &area_light.shape.quad, &intersection_data);
				break;
			}
			case LIGHT_TYPE_DISK:
			{
				b_intersects = IntersectDisk(&ray, &area_light.shape.disk, &intersection_data);
				break;
			}
			case LIGHT_TYPE_SPHERE:
			{
				b_intersects = IntersectSphere(&ray, &area_light.shape.sphere, &intersection_data);
				break;
			}
			case LIGHT_TYPE_TUBE:
			{
				break;
			}
		}

        if (b_intersects)
        {
            F32 hit_distance = Distance(ray.origin, intersection_data.pos);

            if (hit_distance < ray.t_max)
            {
                intersection_data.intersection_type = INTERSECTION_TYPE_LIGHT_SURFACE;
                intersection_data.pos = intersection_data.pos;
                intersection_data.occluder_id = area_light_idx;

                ray.t_max = hit_distance;
            }
        }

	}

    g_rays[work_id] = ray;
    g_intersection_datas[work_id] = intersection_data;
}

WR_PROGRAM_END

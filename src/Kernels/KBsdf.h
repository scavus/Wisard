/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KMath.h"
#include "KShading.h"
#include "KSampling.h"

WR_PROGRAM_BEGIN

WR_DEVICE_FN F32 BsdfLambertEval()
{
	return 1.0f / PI;
}

WR_DEVICE_FN F32 BsdfLambertSample(F32 u0, F32 u1, SSampleInfo* sample_ptr)
{
	sample_ptr->direction = SampleHemisphereCosineWeighted(u0, u1);
	sample_ptr->pdf = PdfHemisphereCosineWeighted(dot(sample_ptr->direction, MakeF32x3(0.0f, 0.0f, 1.0f)));

	return BsdfLambertEval();
}

WR_DEVICE_FN F32 FresnelSchlick(F32 ior, F32 u)
{
	F32 f0 = pown( (ior - 1.0f) / (ior + 1.0f), 2);

	return f0 + (1.0f - f0) * pown(1.0f - u, 5);
}

WR_DEVICE_FN F32 DistributionGgx(F32 alpha, F32 cos_theta_m)
{
	F32 cos_theta_sqr = Sqr(cos_theta_m);
	F32 tan_theta_sqr = (1.0f - cos_theta_sqr) / cos_theta_sqr;

	return INV_PI * Sqr(alpha / (cos_theta_sqr * (alpha * alpha + tan_theta_sqr)));
}

WR_DEVICE_FN F32 GeometryGgx(F32 alpha, F32 cos_theta)
{
	F32 alpha_sqr = Sqr(alpha);
	return (2.0f * cos_theta) / (cos_theta + sqrt(alpha_sqr + (1.0f - alpha_sqr) * Sqr(cos_theta)));
}

WR_DEVICE_FN F32 GeometrySmithGgx(F32 alpha, F32 n_dot_l, F32 n_dot_v)
{
	// G1(l, h) * G1(v, h)
	return (GeometryGgx(alpha, n_dot_l) * GeometryGgx(alpha, n_dot_v));
}

WR_DEVICE_FN F32 GeometryVCavity(F32x3 normal, F32 n_dot_h, F32 n_dot_v, F32 n_dot_l, F32 v_dot_h)
{
	F32 coeff = 2.0f * n_dot_h / v_dot_h;
	return min(1.0f, min(coeff * n_dot_v, coeff * n_dot_l));
}

WR_DEVICE_FN F32 PdfDistributionGgx(F32 alpha, F32 n_dot_h, F32 v_dot_h)
{
	F32 pdf_microfacet = DistributionGgx(alpha, n_dot_h) * n_dot_h;
	F32 jacobian = 1.0f / (4.0f * v_dot_h);

	return pdf_microfacet * jacobian;
}

WR_DEVICE_FN F32x3 SampleDistributionGgx(F32 u0, F32 u1, F32 alpha)
{
	F32 theta = acos( sqrt( (1.0f - u1) / ( (alpha*alpha - 1.0f) * u1 + 1.0f ) ) );
	F32 phi = 2.0f * PI * u0;

	return SphericalToCartesianUnit(theta, phi);
}

WR_DEVICE_FN F32 BsdfMicrofacetGgxEval(F32x3 light_dir, F32x3 view_dir, F32x3 normal, F32 alpha, F32 ior)
{
	F32x3 half_dir = normalize(view_dir + light_dir);

	F32 n_dot_l = max(dot(normal, light_dir), 1e-3f);
	F32 n_dot_v = max(dot(normal, view_dir), 1e-3f);
	F32 n_dot_h = max(dot(normal, half_dir), 1e-3f);
	F32 l_dot_h = max(dot(light_dir, half_dir), 1e-3f);
	F32 v_dot_h = max(dot(view_dir, half_dir), 1e-3f);

	F32 f = FresnelSchlick(ior, l_dot_h);
	F32 d = DistributionGgx(alpha, n_dot_h);
	F32 g = GeometrySmithGgx(alpha, n_dot_l, n_dot_v);
	F32 denominator = 4.0f * n_dot_l * n_dot_v;

	return ((f * d * g) / denominator);
}

WR_DEVICE_FN F32 BsdfMicrofacetGgxSample(SShaderGlobals* sg, F32 u0, F32 u1, F32 alpha, F32 ior, SSampleInfo* sample_info_ptr)
{
	F32x3 m = SampleDistributionGgx(u0, u1, alpha);
	m = Transform(&sg->tangent_to_world_mtx, make_float4(m, 0.0f));
	F32x3 light_dir = Reflect(sg->wo, m);

	F32 n_dot_h = max(dot(sg->normal, m), 1e-3f);
	F32 l_dot_h = max(dot(light_dir, m), 1e-3f);
	F32 v_dot_h = max(dot(sg->wo, m), 1e-3f);

	sample_info_ptr->direction = light_dir;
	sample_info_ptr->pdf = PdfDistributionGgx(alpha, n_dot_h, v_dot_h);

	return BsdfMicrofacetGgxEval(sample_info_ptr->direction, sg->wo, sg->normal, alpha, ior);
}

WR_DEVICE_FN F32 DistributionBeckmann(F32 alpha, F32 cos_theta_m)
{
	F32 cos_theta_m_2 = Sqr(cos_theta_m);
	F32 cos_theta_m_4 = Sqr(cos_theta_m_2);
	F32 alpha_2 = Sqr(alpha);

	return ( 1.0f / (PI * alpha_2 * cos_theta_m_4) ) * exp( (cos_theta_m_2 - 1.0f) / (alpha_2 * cos_theta_m_2 ) );
}

WR_DEVICE_FN F32 GeometryBeckmann(F32 alpha, F32 cos_theta)
{
	F32 cos_theta_2 = Sqr(cos_theta);
	F32 c = cos_theta * (1.0f / alpha) * rsqrt(1.0f - cos_theta_2);
	F32 c_2 = Sqr(c);

	if (c < 1.6f)
	{
		return ( (3.535f * c) + (2.181f * c_2) ) / ( 1.0f + (2.276f * c) + (2.577f * c_2) );
	}
	else if (c >= 1.6f)
	{
		return 1.0f;
	}
}

WR_DEVICE_FN F32 GeometrySmithBeckmann(F32 alpha, F32 n_dot_l, F32 n_dot_v)
{
	return GeometryBeckmann(alpha, n_dot_l) * GeometryBeckmann(alpha, n_dot_v);
}

WR_DEVICE_FN F32 PdfDistributionBeckmann(F32 alpha, F32 n_dot_h, F32 v_dot_h)
{
	F32 pdf_microfacet = DistributionBeckmann(alpha, n_dot_h) * n_dot_h;
	F32 jacobian = 1.0f / (4.0f * v_dot_h);

	return pdf_microfacet * jacobian;
}

WR_DEVICE_FN F32x3 SampleDistributionBeckmann(F32 u0, F32 u1, F32 alpha)
{
	F32 theta = atan(sqrt(-alpha*alpha * log(1.0f - u0)));
	F32 phi = (2.0f * PI) * u1;

	return SphericalToCartesianUnit(theta, phi);
}

WR_DEVICE_FN F32 BsdfMicrofacetBeckmannEval(F32x3 light_dir, F32x3 view_dir, F32x3 normal, F32 alpha, F32 ior)
{
	F32x3 half_dir = normalize(view_dir + light_dir);

	F32 n_dot_l = max(dot(normal, light_dir), 1e-3f);
	F32 n_dot_v = max(dot(normal, view_dir), 1e-3f);
	F32 n_dot_h = max(dot(normal, half_dir), 1e-3f);
	F32 l_dot_h = max(dot(light_dir, half_dir), 1e-3f);
	F32 v_dot_h = max(dot(view_dir, half_dir), 1e-3f);

	F32 f = FresnelSchlick(ior, l_dot_h);
	F32 d = DistributionBeckmann(alpha, n_dot_h);
	F32 g = GeometrySmithBeckmann(alpha, n_dot_l, n_dot_v);
	F32 denominator = 4.0f * n_dot_l * n_dot_v;

	return ((f * d * g) / denominator);
}

WR_DEVICE_FN F32 BsdfMicrofacetBeckmannSample(SShaderGlobals* sg, F32 u0, F32 u1, F32 alpha, F32 ior, SSampleInfo* sample_info_ptr)
{
	F32x3 m = SampleDistributionBeckmann(u0, u1, alpha);
	m = Transform(&sg->tangent_to_world_mtx, make_float4(m, 0.0f));
	F32x3 light_dir = Reflect(sg->wo, m);

	F32 n_dot_h = max(dot(sg->normal, m), 1e-3f);
	F32 l_dot_h = max(dot(light_dir, m), 1e-3f);
	F32 v_dot_h = max(dot(sg->wo, m), 1e-3f);

	sample_info_ptr->direction = light_dir;
	sample_info_ptr->pdf = PdfDistributionBeckmann(alpha, n_dot_h, v_dot_h);

	return BsdfMicrofacetBeckmannEval(sample_info_ptr->direction, sg->wo, sg->normal, alpha, ior);
}

WR_PROGRAM_END

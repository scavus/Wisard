/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KIntegratorProgressiveUdpt.h"

extern "C"
{


	__global__ void Initialize
	(
		SPathState* path_states,
		F32x4*        accumulation_buffer
	)
	{
		U32 thread_id = GetGlobalIdX();

		path_states[thread_id].state = THREAD_STATE_IDLE;
		path_states[thread_id].path_count = 0;

		SRandomNumberGenerator rng;
		rng.state = thread_id;

		path_states[thread_id].rng = rng;

		accumulation_buffer[thread_id] = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
	}


	__global__ void GeneratePaths
	(
		SPathState*    path_states,
		SCamera*         g_camera,
		SRenderSettings* g_render_settings
	)
	{

		/*
		U32x2 PixelIdx = make_uint2(GetGlobalIdX(), GetGlobalIdY());
		U32 thread_id = PixelIdx.y * GetGlobalSizeX() + PixelIdx.x;
		*/

		U32 thread_id = GetGlobalIdX();
		U32x2 PixelIdx = make_uint2(thread_id % 512, thread_id / 512);

		/*
		SPathState path_state = path_states[thread_id];
		*/
		EThreadState state = path_states[thread_id].state;
		SRandomNumberGenerator rng = path_states[thread_id].rng;


		if (state == THREAD_STATE_IDLE)
		{
			// Generate new path from camera

			SMatrix4x4 camera_to_world_mtx = g_camera->camera_to_world_mtx;

			F32x2 camera_sample;
			camera_sample.x = GenerateF32(&rng);
			camera_sample.y = GenerateF32(&rng);

			camera_sample = convert_float2(PixelIdx) + camera_sample;

			F32 fov = tan(g_camera->fov * 0.5f * PI / 180.0f);
			F32 aspect_width = (F32)(g_render_settings->render_width);
			F32 aspect_height = (F32)(g_render_settings->render_height);

			camera_sample.x = (2.0f * (camera_sample.x) / aspect_width - 1.0f) * fov * (aspect_width / aspect_height);
			camera_sample.y = (1.0f - 2.0f * (camera_sample.y) / aspect_height) * fov;

			SRay ray;
			ray.origin = Transform(&camera_to_world_mtx, make_float4(0.0f, 0.0f, 0.0f, 1.0f));
			ray.direction = normalize(Transform(&camera_to_world_mtx, make_float4(camera_sample.x, camera_sample.y, -1.0f, 0.0f)));

			/*
			path_state.state = THREAD_STATE_ACTIVE;
			path_state.path_count++;
			path_state.path.ray = ray;
			path_state.path.throughput = make_float4(1.0f, 1.0f, 1.0f, 0.0f);
			path_state.path.radiance = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
			path_state.path.bounce_count = 0;

			path_states[thread_id] = path_state;
			*/

			path_states[thread_id].state = THREAD_STATE_ACTIVE;
			path_states[thread_id].rng = rng;
			path_states[thread_id].path_count++;
			path_states[thread_id].path.ray = ray;
			path_states[thread_id].path.throughput = make_float4(1.0f, 1.0f, 1.0f, 0.0f);
			path_states[thread_id].path.radiance = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
			path_states[thread_id].path.bounce_count = 0;
		}
	}


	__global__ void ExtendPaths
	(
		SPathState*        path_states,
		SRenderSettings*     render_settings,
		SSceneMetadata*      scene_metadata_ptr,
		SMesh*               meshes,
		const STriangle* __restrict__           triangles,
		const SBvhNode* __restrict__            bvh_nodes,
		SAreaLight*          area_lights,
		SQuad*               area_light_quads,
		SDisk*               area_light_disks,
		SSphere*             area_light_spheres,
		SMaterial*           materials,
		SDirectIllumData*    direct_illum_datas
	)
	{
		U32 thread_id = GetGlobalIdX();
		// U32 thread_id = GetGlobalIdY() * GetGlobalSizeX() + GetGlobalIdX();

		// TODO: No need to load all elements, "sg" is not needed here
		SPathState path_state = path_states[thread_id];

		SSceneMetadata scene_metadata = *scene_metadata_ptr;

		SIntersectionData intersection_data;
		IntersectScene(&path_state.path.ray, &scene_metadata, meshes, triangles, bvh_nodes, area_lights, area_light_disks, area_light_spheres, area_light_quads, &intersection_data);

		if (intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
		{
			path_state.state = THREAD_STATE_IDLE;

			path_states[thread_id] = path_state;

			return;
		}
		else if (intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
		{
			// Get emission and update radiance
			SAreaLight light = area_lights[intersection_data.occluder_id];
			F32x4 L = light.color * light.intensity * pow(2.0f, light.exposure);

			path_state.path.radiance += path_state.path.throughput * L;

			path_state.state = THREAD_STATE_IDLE;

			path_states[thread_id] = path_state;

			return;

		}

		if (path_state.path.bounce_count > render_settings->max_indirect_bounce_count)
		{
			path_state.state = THREAD_STATE_IDLE;

			path_states[thread_id] = path_state;

			return;
		}

		F32 cont_prob = fmin(0.5f, path_state.path.throughput.y);

		if(GenerateF32(&path_state.rng) > cont_prob)
		{
			path_state.state = THREAD_STATE_IDLE;

			path_states[thread_id] = path_state;

			return;
		}

		path_state.path.throughput /= cont_prob;

		SShaderGlobals sg;
		sg.pos = intersection_data.pos;
		sg.normal = intersection_data.normal;
		sg.texcoord = intersection_data.texcoord;
		sg.wo = path_state.path.ray.direction;
		/*
		sg.tangent_to_world_mtx = BuildMatrix4x4((F32x4)(intersection_data.tangent, 0.0f),
															   (F32x4)(intersection_data.binormal, 0.0f),
															   (F32x4)(intersection_data.normal, 0.0f),
															   (F32x4)(0.0f, 0.0f, 0.0f, 1.0f));
		*/

		sg.tangent_to_world_mtx = BuildMatrix4x4(make_float4(intersection_data.tangent, 0.0f),
			make_float4(intersection_data.binormal, 0.0f),
			make_float4(intersection_data.normal, 0.0f),
			make_float4(0.0f, 0.0f, 0.0f, 1.0f));

		sg.mesh_id = intersection_data.occluder_id;
		sg.material_id = meshes[sg.mesh_id].material_id;

		SMaterial Material = materials[sg.material_id];

		SSampleInfo DirectIllumBsdfSample;
		SampleShader(&path_state.rng, &sg, &Material, &DirectIllumBsdfSample);

		SSampleInfo DirectIllumLightSample;
		SampleLight(&path_state.rng, &sg, &scene_metadata, area_lights, area_light_disks, area_light_spheres, area_light_quads, &DirectIllumLightSample);

		SDirectIllumData direct_illum_data;
		direct_illum_data.bsdf_sample = DirectIllumBsdfSample;
		direct_illum_data.light_sample = DirectIllumLightSample;
		direct_illum_data.throughput = path_state.path.throughput;

		SSampleInfo indirect_illum_sample;
		SampleShader(&path_state.rng, &sg, &Material, &indirect_illum_sample);
		F32x4 shading_result = EvaluateShader(&sg, &Material, indirect_illum_sample.direction);
		F32 cos_theta = Saturate(dot(sg.normal, indirect_illum_sample.direction));
		path_state.path.throughput *= shading_result * cos_theta / indirect_illum_sample.pdf;
		path_state.path.ray.origin = sg.pos;
		path_state.path.ray.direction = indirect_illum_sample.direction;
		path_state.sg = sg;
		path_state.path.bounce_count++;

		path_states[thread_id] = path_state;
		direct_illum_datas[thread_id] = direct_illum_data;
	}


	__global__ void EvaluateDirectIllumination
	(
		SPathState*        path_states,
		const SDirectIllumData* __restrict__    direct_illum_datas,
		SSceneMetadata*      scene_metadata_ptr,
		SMesh*               meshes,
		const STriangle* __restrict__           triangles,
		const SBvhNode* __restrict__            bvh_nodes,
		SAreaLight*          area_lights,
		SQuad*               area_light_quads,
		SDisk*               area_light_disks,
		SSphere*             area_light_spheres,
		SMaterial*           materials
	)
	{
		U32 thread_id = GetGlobalIdX();

		SPathState path_state = path_states[thread_id];

		SSceneMetadata scene_metadata = *scene_metadata_ptr;

		if (path_state.state == THREAD_STATE_IDLE)
		{
			return;
		}

		// SDirectIllumData direct_illum_data = direct_illum_datas[thread_id];

		SRay bsdf_ray;
		SRay light_ray;

		F32 bsdf_sample_pdf;
		F32 light_sample_pdf;
		// SDirectIllumData direct_illum_data;

		bsdf_ray.direction.x = __ldg(&direct_illum_datas[thread_id].bsdf_sample.direction.x);
		bsdf_ray.direction.y = __ldg(&direct_illum_datas[thread_id].bsdf_sample.direction.y);
		bsdf_ray.direction.z = __ldg(&direct_illum_datas[thread_id].bsdf_sample.direction.z);
		bsdf_sample_pdf = __ldg(&direct_illum_datas[thread_id].bsdf_sample.pdf);

		light_ray.direction.x = __ldg(&direct_illum_datas[thread_id].light_sample.direction.x);
		light_ray.direction.y = __ldg(&direct_illum_datas[thread_id].light_sample.direction.y);
		light_ray.direction.z = __ldg(&direct_illum_datas[thread_id].light_sample.direction.z);
		light_sample_pdf = __ldg(&direct_illum_datas[thread_id].light_sample.pdf);

		F32x4 throughput = direct_illum_datas[thread_id].throughput;

		bsdf_ray.origin = path_state.sg.pos;
		light_ray.origin = path_state.sg.pos;
		/*
		SRay bsdf_ray;
		bsdf_ray.origin = path_state.sg.pos;
		bsdf_ray.direction = direct_illum_data.bsdf_sample.direction;

		SRay light_ray;
		light_ray.origin = path_state.sg.pos;
		light_ray.direction = direct_illum_data.light_sample.direction;
		*/


		SMaterial Material = materials[path_state.sg.material_id];

		// Cast bsdf sample dir
		SIntersectionData intersection_data;

		IntersectScene(&bsdf_ray, &scene_metadata, meshes, triangles, bvh_nodes, area_lights, area_light_disks, area_light_spheres, area_light_quads, &intersection_data);

		if (intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
		{
			/*
			SAreaLight light = area_lights[intersection_data.occluder_id];
			F32x4 L = light.color * light.intensity * pow(2.0f, light.exposure);
			F32x4 shading_result = EvaluateShader(&path_state.sg, &Material, direct_illum_data.bsdf_sample.direction);
			F32 cos_theta = Saturate(dot(path_state.sg.normal, direct_illum_data.bsdf_sample.direction));
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, direct_illum_data.bsdf_sample.pdf, 1, direct_illum_data.light_sample.pdf);
			path_state.path.radiance += direct_illum_data.throughput * mis_weight * shading_result * L * cos_theta / direct_illum_data.bsdf_sample.pdf;
			*/
			SAreaLight light = area_lights[intersection_data.occluder_id];
			F32x4 L = light.color * light.intensity * pow(2.0f, light.exposure);
			F32x4 shading_result = EvaluateShader(&path_state.sg, &Material, bsdf_ray.direction);
			F32 cos_theta = Saturate(dot(path_state.sg.normal, bsdf_ray.direction));
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, bsdf_sample_pdf, 1, light_sample_pdf);
			path_state.path.radiance += throughput * mis_weight * shading_result * L * cos_theta / bsdf_sample_pdf;
			/*
			*/
		}

		// Cast light sample dir
		IntersectScene(&light_ray, &scene_metadata, meshes, triangles, bvh_nodes, area_lights, area_light_disks, area_light_spheres, area_light_quads, &intersection_data);

		if (intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
		{
			/*
			SAreaLight light = area_lights[intersection_data.occluder_id];
			F32x4 L = light.color * light.intensity * pow(2.0f, light.exposure);
			F32x4 shading_result = EvaluateShader(&path_state.sg, &Material, direct_illum_data.light_sample.direction);
			F32 cos_theta = Saturate(dot(path_state.sg.normal, direct_illum_data.light_sample.direction));
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, direct_illum_data.light_sample.pdf, 1, direct_illum_data.bsdf_sample.pdf);
			path_state.path.radiance += direct_illum_data.throughput * mis_weight * shading_result * L * cos_theta / direct_illum_data.light_sample.pdf;
			*/

			SAreaLight light = area_lights[intersection_data.occluder_id];
			F32x4 L = light.color * light.intensity * pow(2.0f, light.exposure);
			F32x4 shading_result = EvaluateShader(&path_state.sg, &Material, light_ray.direction);
			F32 cos_theta = Saturate(dot(path_state.sg.normal, light_ray.direction));
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, light_sample_pdf, 1, bsdf_sample_pdf);
			path_state.path.radiance += throughput * mis_weight * shading_result * L * cos_theta / light_sample_pdf;
		}

		path_states[thread_id] = path_state;
	}


	__global__ void AccumulatePixelValues
	(
		SPathState*                path_states,
		F32x4*                       accumulation_buffer,
		F32x4*                       framebuffer
	)
	{
		U32 thread_id = GetGlobalIdX();

		// U32x2 PixelIdx = (U32x3)(get_global_id(0), get_global_id(1));

		SPathState path_state = path_states[thread_id];

		F32x4 pixel_color = accumulation_buffer[thread_id];

		pixel_color += clamp(Pow(path_state.path.radiance, 1.0f / 2.2f), make_float4(0.0f, 0.0f, 0.0f, 0.0f), make_float4(1.0f, 1.0f, 1.0f, 1.0f));

		accumulation_buffer[thread_id] = pixel_color;

		pixel_color /= path_state.path_count;
		pixel_color = Pow(pixel_color, 1.0f / 2.2f);

		framebuffer[thread_id] = clamp(pixel_color, make_float4(0.0f, 0.0f, 0.0f, 0.0f), make_float4(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

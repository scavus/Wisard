/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"

WR_PROGRAM_BEGIN

/*
typedef struct SRandomNumberGenerator
{
	S32 state;
} SRandomNumberGenerator;
*/

typedef struct SRngXorShift
{
	U32 state;
} SRngXorShift;

typedef struct SRngLcg
{
	U32 state;
} SRngLcg;

typedef SRngXorShift SRandomNumberGenerator;

WR_DEVICE_FN U32 HashFnWang(U32 seed)
{
	seed = (seed ^ 61) ^ (seed >> 16);
	seed *= 9;
	seed = seed ^ (seed >> 4);
	seed *= 0x27d4eb2d;
	seed = seed ^ (seed >> 15);
	return seed;
}

WR_DEVICE_FN void SeedRng(SRngXorShift* rng_ptr, U32 seed)
{
	rng_ptr->state = HashFnWang(seed);
}

WR_DEVICE_FN U32 GenerateU32(SRngXorShift* rng_ptr)
{
	U32 state = rng_ptr->state;
	state ^= (state << 13);
	state ^= (state >> 17);
	state ^= (state << 5);
	rng_ptr->state = state;
	return state;
}

WR_DEVICE_FN F32 GenerateF32(SRngXorShift* rng_ptr)
{
	U32 state = rng_ptr->state;
	state ^= (state << 13);
	state ^= (state >> 17);
	state ^= (state << 5);
	rng_ptr->state = state;
	return (float)state / (float)(UINT_MAX);
}


/**
* Generates a uniformly distributed number in the range of [0, 1) using the Linear Congruential Generator (LCG)
* @param seed The provided seed array.
* @param index The index within the seed array that will be modified by this function.
* @return A float in the range of [0,1). This float is based on the modified seed value at index in the seed array.
* @see http://en.wikipedia.org/wiki/Linear_congruential_generator
* @note GCC's values (M, A, C) for the LCG are used
*/
/*
WR_DEVICE_FN float GenerateF32(SRandomNumberGenerator* rng_ptr)
{
	int M = INT_MAX;
	int A = 1103515245;
	int C = 12345;
	int num = A * rng_ptr->state + C;
	rng_ptr->state = num % M;
	num = rng_ptr->state;
	return fabs(num / ((float)M));
}

WR_DEVICE_FN U32 GenerateU32(SRandomNumberGenerator* rng_ptr)
{
	int M = INT_MAX;
	int A = 1103515245;
	int C = 12345;
	int num = A * rng_ptr->state + C;
	rng_ptr->state = num % M;
	num = rng_ptr->state;
	return abs(num);
}
*/

WR_PROGRAM_END

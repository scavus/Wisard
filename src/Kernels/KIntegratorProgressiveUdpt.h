/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KMath.h"
#include "KGeometry.h"
#include "KRandom.h"
#include "KSampling.h"
#include "KCamera.h"
#include "KScene.h"
#include "KSceneMetadata.h"
#include "KMesh.h"
#include "KLight.h"
#include "KMaterial.h"
#include "KShading.h"

typedef struct SRenderSettings
{
	U32 render_width;
	U32 render_height;
	U32 max_indirect_bounce_count;
	U32 iteration_count;
	bool use_mis;
} SRenderSettings;

typedef struct SDirectIllumData
{
	SSampleInfo bsdf_sample;
	SSampleInfo light_sample;
	F32x4 throughput;

} SDirectIllumData;

typedef struct SPath
{
	F32x4 throughput;
	F32x4 radiance;
	SRay ray;
	U8 bounce_count;

} SPath;

typedef enum EThreadState
{
	THREAD_STATE_IDLE,
	THREAD_STATE_ACTIVE

} EThreadState;

typedef struct SPathState
{
	EThreadState state;
	U32 path_count;
	SPath path;
	SRandomNumberGenerator rng;
	SShaderGlobals sg;

} SPathState;

/*
struct SThreadStateSOA
{
	EThreadState* States;
	U32* PathCounts;
	// Paths
	F32x4* Throughputs;
	F32x4* Radiances;

	// ray
	F32x3* Origins;
	F32x3* Directions;
	F32* TMins;
	F32* t_max;

	U8* BounceCounts;

	SRandomNumberGenerator* RngStates;

	// ShadingPointInfos
	F32x3* Positions;
	F32x3* Normals;
	F32x2* TexCoords;

	F32x3* IncomingDirs;

	SMatrix4x4* TangentToWorldMatrixs;

	U32* MeshIDs;
	U32* MaterialIDs;
};
*/

#define INTERNAL_KERNEL_PARAMS \
	SPathState* ThreadStates, \
	SDirectIllumData* DirectIllumDatas, \
	F32x4* FrameBuffer, \
	F32x4* AccumulationBuffer

#define GFX_KERNEL_PARAMS \
	SCamera* Camera, \
	SMesh* Meshes, \
	STriangle* MeshTriangles, \
	SBvhNode* BvhNodes, \
	SAreaLight* AreaLights, \
	SQuad* AreaLightQuads, \
	SDisk* AreaLightDisks, \
	SSphere* AreaLightSpheres, \
	SMaterial* Materials

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KCommonTypes.cuh"

WR_DEVICE_FN void QueryIntersectionQuadLinear(SRay* ray_ptr,
    WR_GMEM_PARAM const SQuad* WR_RESTRICT geom_buf,
    SIntersectionData* intersection_data_ptr)
{
    for (U32 i = 0; i < geom_count; i++)
    {
        SQuad quad = geom_buf[i];
        SIntersectionData temp_intersection_data;
        bool b_intersects = IntersectQuad(ray_ptr, &quad, &temp_intersection_data, &temp_hit_distance);

        if (b_intersects && temp_hit_distance < ray_ptr->t_max)
        {
            ray_ptr->t_max = temp_hit_distance;
            *intersection_data_ptr = temp_intersection_data;
        }
    }
}

WR_KERNEL void IntersectionQuadLinear(WR_GMEM_PARAM SRay* rays,
    WR_GMEM_PARAM U32* active_ray_stream,
    WR_GMEM_PARAM SStreamInfo* active_ray_stream_info,
    WR_GMEM_PARAM const SQuad* WR_RESTRICT geom_buf,
    WR_GMEM_PARAM SIntersectionData* intersection_datas)
{
    U32 thread_idx = GetGlobalIdX();

	if (thread_idx >= active_ray_stream_info->counter)
	{
        return;
	}

	U32 work_id = active_ray_stream[thread_idx];
    SRay ray = rays[work_id];
    SIntersectionData intersection_data = intersection_datas[work_id];
    QueryIntersectionQuadLinear(&ray, geom_buf, &intersection_data);
    intersection_datas[work_id] = intersection_data;
    rays[work_id] = ray;
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KCommonTypes.h"
#include "KMath.h"
#include "KGeometry.h"

WR_PROGRAM_BEGIN

typedef struct SRayFireRays
{
    float4 o;
    float4 d;
    int2 extra;
    int2 padding;

} SRayFireRays;

typedef struct SHitFireRays
{
    // UV parametrization
   float4 uvwt;
   // Shape ID
   int shapeid;
   // Primitve ID
   int primid;

   int padding0;
   int padding1;

} SHitFireRays;

WR_KERNEL void ConvertRays(
    WR_PMEM_PARAM U32 g_active_ray_count,
    WR_GMEM_PARAM SRay* g_rays,
    WR_GMEM_PARAM U32* g_active_ray_stream,
    WR_GMEM_PARAM SRayFireRays* g_api_rays)
{
    // Get active rays and convert them

    U32 thread_id = GetGlobalIdX();

    if (thread_id >= g_active_ray_count)
    {
        return;
    }

    U32 ray_id = g_active_ray_stream[thread_id];

    SRay ray = g_rays[ray_id];

    SRayFireRays api_ray;
    api_ray.o.x = ray.origin.x;
    api_ray.o.y = ray.origin.y;
    api_ray.o.z = ray.origin.z;

    api_ray.d.x = ray.direction.x;
    api_ray.d.y = ray.direction.y;
    api_ray.d.z = ray.direction.z;

    api_ray.o.w = ray.t_max;

    g_api_rays[thread_id] = api_ray;
}

WR_KERNEL void ConvertGeometry(
    WR_PMEM_PARAM U32 g_triangle_count,
    WR_GMEM_PARAM STriangle* g_triangles,
    WR_GMEM_PARAM F32* g_api_vertices,
    WR_GMEM_PARAM int* g_api_indices)
{
    // Extract vertices from triangles

    U32 thread_id = GetGlobalIdX();

    if (thread_id >= g_triangle_count)
    {
        return;
    }

    SVertex vtx[3];
    vtx[0] = g_triangles[thread_id].vtx[0];
    vtx[1] = g_triangles[thread_id].vtx[1];
    vtx[2] = g_triangles[thread_id].vtx[2];

    U32 vtx_offset = thread_id * 3 * 3;
    g_api_vertices[vtx_offset + 0] = vtx[0].pos.x;
    g_api_vertices[vtx_offset + 1] = vtx[0].pos.y;
    g_api_vertices[vtx_offset + 2] = vtx[0].pos.z;

    g_api_vertices[vtx_offset + 3] = vtx[1].pos.x;
    g_api_vertices[vtx_offset + 4] = vtx[1].pos.y;
    g_api_vertices[vtx_offset + 5] = vtx[1].pos.z;

    g_api_vertices[vtx_offset + 6] = vtx[2].pos.x;
    g_api_vertices[vtx_offset + 7] = vtx[2].pos.y;
    g_api_vertices[vtx_offset + 8] = vtx[2].pos.z;

    U32 index_offset = thread_id * 3;
    g_api_indices[index_offset + 0] = index_offset + 0;
    g_api_indices[index_offset + 1] = index_offset + 1;
    g_api_indices[index_offset + 2] = index_offset + 2;
}

WR_KERNEL void ConvertQueryResult(
    U32 g_active_ray_count,
    WR_GMEM_PARAM SRayFireRays* g_api_rays,
    WR_GMEM_PARAM SHitFireRays* g_api_hits,
    WR_GMEM_PARAM STriangle* g_triangles,
    WR_GMEM_PARAM SRay* g_rays,
    WR_GMEM_PARAM U32* g_active_ray_stream,
    WR_GMEM_PARAM SIntersectionData* g_intersection_datas)
{
    // Convert the firerays query result to wisard query result

    U32 thread_id = GetGlobalIdX();

    if (thread_id >= g_active_ray_count)
    {
        return;
    }

    U32 ray_id = g_active_ray_stream[thread_id];
    SRay ray = g_rays[ray_id];

    SHitFireRays api_hit = g_api_hits[thread_id];

    SIntersectionData intersection_data;

    if (api_hit.shapeid == -1)
    {
        // TODO:
        intersection_data.intersection_type = INTERSECTION_TYPE_BACKGROUND;
    }
    else
    {
        STriangle tri = g_triangles[api_hit.primid];

        F32 u = api_hit.uvwt.x;
        F32 v = api_hit.uvwt.y;
        F32 w = 1.0f - u - v;

        F32x3 isect_point = ray.origin + (ray.direction * (api_hit.uvwt.w - 1e-3f));
        g_rays[ray_id].t_max = api_hit.uvwt.w - 1e-3f;

        intersection_data.intersection_type = INTERSECTION_TYPE_MESH_SURFACE;
        intersection_data.pos = isect_point;
        intersection_data.normal = w * tri.vtx[0].normal + u * tri.vtx[1].normal + v * tri.vtx[2].normal;
        intersection_data.tangent = w * tri.vtx[0].tangent + u * tri.vtx[1].tangent + v * tri.vtx[2].tangent;
        intersection_data.binormal = w * tri.vtx[0].binormal + u * tri.vtx[1].binormal + v * tri.vtx[2].binormal;
        intersection_data.texcoord = w * tri.vtx[0].texcoord + u * tri.vtx[1].texcoord + v * tri.vtx[2].texcoord;
        intersection_data.occluder_id = tri.mesh_id;
    }

    g_intersection_datas[ray_id] = intersection_data;
}

WR_PROGRAM_END

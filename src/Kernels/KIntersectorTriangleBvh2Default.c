/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KCommonTypes.h"
#include "KGeometry.h"
#include "KDeferredIntegratorCommon.h"
#include "KLight.h"
#include "KSceneMetadata.h"
#include "KAcceleratorBvh2.h"

WR_PROGRAM_BEGIN

WR_DEVICE_FN void QueryIntersectionTriangleBvh2Default(SRay* ray_ptr,
	WR_GMEM_PARAM const STriangle* WR_RESTRICT triangles,
	WR_GMEM_PARAM const SBvhNode* WR_RESTRICT nodes,
	SIntersectionData* intersection_data_ptr)
{
	U32 traversal_stack[64];
	const S32 stack_head_idx_null = -1;
	S32 stack_head_idx = -1;

	SIntersectionData intersection_data;

	F32x3 InvDir = MakeF32x3(1.0f / ray_ptr->direction.x, 1.0f / ray_ptr->direction.y, 1.0f / ray_ptr->direction.z);
	// bool DirIsNeg[3] = {InvDir.x < 0, InvDir.y < 0, InvDir.z < 0};
	// FIXME: MSVC gives "Constant too big" error

	bool bTerminated = false;
	U32 current_node_id = 0;
	U32 PostponedNodeId = 0;
	SBvhNode current_node = nodes[current_node_id];

	while (!bTerminated)
	{
		while (current_node.prim_count == 0)
		{
			SAabb aabb0;
			SAabb aabb1;

#ifndef WR_BACKEND_CUDA
			aabb0 = nodes[current_node_id + 1].aabb;
			aabb1 = nodes[current_node.offset].aabb;
#endif

#ifdef WR_BACKEND_CUDA
			aabb0.bound_max.x = __ldg(&nodes[current_node_id + 1].aabb.bound_max.x);
			aabb0.bound_max.y = __ldg(&nodes[current_node_id + 1].aabb.bound_max.y);
			aabb0.bound_max.z = __ldg(&nodes[current_node_id + 1].aabb.bound_max.z);

			aabb0.bound_min.x = __ldg(&nodes[current_node_id + 1].aabb.bound_min.x);
			aabb0.bound_min.y = __ldg(&nodes[current_node_id + 1].aabb.bound_min.y);
			aabb0.bound_min.z = __ldg(&nodes[current_node_id + 1].aabb.bound_min.z);

			aabb1.bound_max.x = __ldg(&nodes[current_node.offset].aabb.bound_max.x);
			aabb1.bound_max.y = __ldg(&nodes[current_node.offset].aabb.bound_max.y);
			aabb1.bound_max.z = __ldg(&nodes[current_node.offset].aabb.bound_max.z);

			aabb1.bound_min.x = __ldg(&nodes[current_node.offset].aabb.bound_min.x);
			aabb1.bound_min.y = __ldg(&nodes[current_node.offset].aabb.bound_min.y);
			aabb1.bound_min.z = __ldg(&nodes[current_node.offset].aabb.bound_min.z);
#endif

			bool bIntersectsChildNode0 = IntersectAabbFast2(ray_ptr, &aabb0, InvDir);
			bool bIntersectsChildNode1 = IntersectAabbFast2(ray_ptr, &aabb1, InvDir);

			if (!bIntersectsChildNode0 && !bIntersectsChildNode1)
			{
				if (stack_head_idx != stack_head_idx_null)
				{
					current_node_id = traversal_stack[stack_head_idx];

#ifndef WR_BACKEND_CUDA
					current_node = nodes[current_node_id];
#endif

#ifdef WR_BACKEND_CUDA
					current_node.aabb.bound_max.x = __ldg(&nodes[current_node_id].aabb.bound_max.x);
					current_node.aabb.bound_max.y = __ldg(&nodes[current_node_id].aabb.bound_max.y);
					current_node.aabb.bound_max.z = __ldg(&nodes[current_node_id].aabb.bound_max.z);

					current_node.aabb.bound_min.x = __ldg(&nodes[current_node_id].aabb.bound_min.x);
					current_node.aabb.bound_min.y = __ldg(&nodes[current_node_id].aabb.bound_min.y);
					current_node.aabb.bound_min.z = __ldg(&nodes[current_node_id].aabb.bound_min.z);

					current_node.offset = __ldg(&nodes[current_node_id].offset);
					current_node.prim_count = __ldg(&nodes[current_node_id].prim_count);
#endif

					stack_head_idx--;
				}
				else
				{
					bTerminated = true;
					break;
				}
			}
			else if (bIntersectsChildNode0 && bIntersectsChildNode1)
			{
				// Push the farthest node
				/*
				if (DirIsNeg[current_node.axis])
				{
					stack_head_idx++;
					traversal_stack[stack_head_idx] = current_node_id + 1;

					current_node_id = current_node.offset;
					current_node = nodes[current_node_id];
				}
				else
				{
					stack_head_idx++;
					traversal_stack[stack_head_idx] = current_node.offset;

					current_node_id = current_node_id + 1;
					current_node = nodes[current_node_id];
				}
				*/

				stack_head_idx++;
				traversal_stack[stack_head_idx] = current_node.offset;

				current_node_id = current_node_id + 1;

#ifndef WR_BACKEND_CUDA
				current_node = nodes[current_node_id];
#endif

#ifdef WR_BACKEND_CUDA
				current_node.aabb.bound_max.x = __ldg(&nodes[current_node_id].aabb.bound_max.x);
				current_node.aabb.bound_max.y = __ldg(&nodes[current_node_id].aabb.bound_max.y);
				current_node.aabb.bound_max.z = __ldg(&nodes[current_node_id].aabb.bound_max.z);

				current_node.aabb.bound_min.x = __ldg(&nodes[current_node_id].aabb.bound_min.x);
				current_node.aabb.bound_min.y = __ldg(&nodes[current_node_id].aabb.bound_min.y);
				current_node.aabb.bound_min.z = __ldg(&nodes[current_node_id].aabb.bound_min.z);

				current_node.offset = __ldg(&nodes[current_node_id].offset);
				current_node.prim_count = __ldg(&nodes[current_node_id].prim_count);
#endif

			}
			else if (bIntersectsChildNode0 && !bIntersectsChildNode1)
			{
				current_node_id = current_node_id + 1;

#ifndef WR_BACKEND_CUDA
				current_node = nodes[current_node_id];
#endif

#ifdef WR_BACKEND_CUDA
				current_node.aabb.bound_max.x = __ldg(&nodes[current_node_id].aabb.bound_max.x);
				current_node.aabb.bound_max.y = __ldg(&nodes[current_node_id].aabb.bound_max.y);
				current_node.aabb.bound_max.z = __ldg(&nodes[current_node_id].aabb.bound_max.z);

				current_node.aabb.bound_min.x = __ldg(&nodes[current_node_id].aabb.bound_min.x);
				current_node.aabb.bound_min.y = __ldg(&nodes[current_node_id].aabb.bound_min.y);
				current_node.aabb.bound_min.z = __ldg(&nodes[current_node_id].aabb.bound_min.z);

				current_node.offset = __ldg(&nodes[current_node_id].offset);
				current_node.prim_count = __ldg(&nodes[current_node_id].prim_count);
#endif
			}
			else
			{
				current_node_id = current_node.offset;

#ifndef WR_BACKEND_CUDA

				current_node = nodes[current_node_id];
#endif

#ifdef WR_BACKEND_CUDA
				current_node.aabb.bound_max.x = __ldg(&nodes[current_node_id].aabb.bound_max.x);
				current_node.aabb.bound_max.y = __ldg(&nodes[current_node_id].aabb.bound_max.y);
				current_node.aabb.bound_max.z = __ldg(&nodes[current_node_id].aabb.bound_max.z);

				current_node.aabb.bound_min.x = __ldg(&nodes[current_node_id].aabb.bound_min.x);
				current_node.aabb.bound_min.y = __ldg(&nodes[current_node_id].aabb.bound_min.y);
				current_node.aabb.bound_min.z = __ldg(&nodes[current_node_id].aabb.bound_min.z);

				current_node.offset = __ldg(&nodes[current_node_id].offset);
				current_node.prim_count = __ldg(&nodes[current_node_id].prim_count);
#endif
			}
		}

		while (current_node.prim_count > 0)
		{

			for (U32 prim_idx = 0; prim_idx < current_node.prim_count; prim_idx++)
			{
				STriangle triangle = triangles[current_node.offset + prim_idx];

				bool bIntersects;
				bIntersects = IntersectTriangle(ray_ptr, &triangle, &intersection_data);

				if (bIntersects)
				{
					F32 hit_distance = Distance(ray_ptr->origin, intersection_data.pos);

					if (hit_distance < ray_ptr->t_max)
					{
						intersection_data_ptr->intersection_type = INTERSECTION_TYPE_MESH_SURFACE;
						intersection_data_ptr->pos = intersection_data.pos;
						intersection_data_ptr->normal = intersection_data.normal;
						intersection_data_ptr->binormal = intersection_data.binormal;
						intersection_data_ptr->tangent = intersection_data.tangent;
						intersection_data_ptr->texcoord = intersection_data.texcoord;
						intersection_data_ptr->occluder_id = triangle.mesh_id;

						ray_ptr->t_max = hit_distance;
					}
				}
			}

			if (stack_head_idx == stack_head_idx_null)
			{
				bTerminated = true;
				break;
			}
			else
			{
				current_node_id = traversal_stack[stack_head_idx];

#ifndef WR_BACKEND_CUDA
				current_node = nodes[current_node_id];
#endif

#ifdef WR_BACKEND_CUDA
				current_node.aabb.bound_max.x = __ldg(&nodes[current_node_id].aabb.bound_max.x);
				current_node.aabb.bound_max.y = __ldg(&nodes[current_node_id].aabb.bound_max.y);
				current_node.aabb.bound_max.z = __ldg(&nodes[current_node_id].aabb.bound_max.z);

				current_node.aabb.bound_min.x = __ldg(&nodes[current_node_id].aabb.bound_min.x);
				current_node.aabb.bound_min.y = __ldg(&nodes[current_node_id].aabb.bound_min.y);
				current_node.aabb.bound_min.z = __ldg(&nodes[current_node_id].aabb.bound_min.z);

				current_node.offset = __ldg(&nodes[current_node_id].offset);
				current_node.prim_count = __ldg(&nodes[current_node_id].prim_count);
#endif
				stack_head_idx--;
			}
		}
	}
}

WR_KERNEL void IntersectionTriangleBvh2Default(
	WR_GMEM_PARAM SRay* rays,
    WR_GMEM_PARAM U32* active_ray_stream,
    WR_GMEM_PARAM SStreamInfo* active_ray_stream_info,
    WR_GMEM_PARAM const STriangle* WR_RESTRICT geom_buf,
    WR_GMEM_PARAM const SBvhNode* WR_RESTRICT accelerator_buf,
    WR_GMEM_PARAM SIntersectionData* intersection_datas)
{
    U32 thread_idx = GetGlobalIdX();

	if (thread_idx >= active_ray_stream_info->counter)
	{
        return;
	}

	U32 work_id = active_ray_stream[thread_idx];
    SRay ray = rays[work_id];
    SIntersectionData intersection_data = intersection_datas[work_id];
    QueryIntersectionTriangleBvh2Default(&ray, geom_buf, accelerator_buf, &intersection_data);
	rays[work_id] = ray;
    intersection_datas[work_id] = intersection_data;
}

WR_PROGRAM_END

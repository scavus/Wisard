/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KMath.h"
#include "KGeometry.h"
#include "KRandom.h"
#include "KSampling.h"
#include "KShading.h"
#include "KSceneMetadata.h"

WR_PROGRAM_BEGIN

typedef enum ELightType
{
	LIGHT_TYPE_QUAD,
	LIGHT_TYPE_DISK,
	LIGHT_TYPE_SPHERE,
	LIGHT_TYPE_TUBE

} ELightType;

typedef struct SAreaLight
{
	ELightType type;

	SMatrix4x4 obj_to_world_mtx;
	SMatrix4x4 world_to_obj_mtx;

	F32 intensity;
	F32 exposure;
	F32x4 color;

	union UShape
	{
		SQuad quad;
		SDisk disk;
		SSphere sphere;

	} shape;

} SAreaLight;

typedef struct SDirectionalLight
{
	F32x3 direction;

	F32x4 color;
	F32 intensity;
	F32 angle;

} SDirectionalLight;

typedef struct SEnvLight
{
	SMatrix4x4 obj_to_world_mtx;
	SMatrix4x4 world_to_obj_mtx;

	U32 width;
	U32 height;

} SEnvLight;

WR_DEVICE_FN F32 Luminance(F32x4 color)
{
	return 0.2126f * color.x + 0.7152f * color.y + 0.0722f * color.z;
}

WR_DEVICE_FN void SampleLightSphere(SAreaLight* light_ptr, SShaderGlobals* shading_point_info_ptr, F32 u0, F32 u1, SSampleInfo* light_sample_ptr)
{


}

WR_DEVICE_FN void SampleLightQuad(SAreaLight* light_ptr, SShaderGlobals* shading_point_info_ptr, F32 u0, F32 u1, SSampleInfo* light_sample_ptr)
{
	F32x3 sample_pos;
	F32x3 sample_dir;

	SQuad quad = light_ptr->shape.quad;

	sample_pos = MakeF32x3(2 * u0 - 1.0f, 2 * u1 - 1.0f, 0.0f);

	sample_pos = Transform(&light_ptr->obj_to_world_mtx, make_float4(sample_pos, 1.0f));
	sample_dir = normalize(sample_pos - shading_point_info_ptr->pos);

	F32 distance_sqr = DistanceSq(shading_point_info_ptr->pos, sample_pos);

	F32 cos_theta_prime = fabs(dot(quad.normal, -sample_dir));

	F32 sx = length(quad.u);
	F32 sy = length(quad.v);

	light_sample_ptr->direction = sample_dir;
	light_sample_ptr->pdf = distance_sqr / (sx * sy * cos_theta_prime);
}

WR_DEVICE_FN void SampleLightDisk(SAreaLight* light_ptr, SShaderGlobals* shading_point_info_ptr, F32 u0, F32 u1, SSampleInfo* light_sample_ptr)
{
	F32x3 sample_pos;
	F32x3 sample_dir;

	SDisk disk = light_ptr->shape.disk;

	sample_pos = MakeF32x3(SampleDiskConcentric(u0, u1), 0.0f);

	sample_pos = Transform(&light_ptr->obj_to_world_mtx, make_float4(sample_pos, 1.0f));
	sample_dir = normalize(sample_pos - shading_point_info_ptr->pos);

	F32 distance_sqr = DistanceSq(shading_point_info_ptr->pos, sample_pos);

	F32 cos_theta_prime = fabs(dot(disk.normal, -sample_dir));

	light_sample_ptr->direction = sample_dir;
	light_sample_ptr->pdf = distance_sqr / (ComputeAreaDisk(&disk) * cos_theta_prime);
}

WR_DEVICE_FN void SampleEnvLight(SEnvLight* light_ptr, WR_GMEM_PARAM F32* g_conditional_cdfs, WR_GMEM_PARAM F32* g_marginal_cdf,
	F32 u0, F32 u1, SSampleInfo* light_sample_ptr)
{
	#define LowerBound(data, first, last, value, out) 	\
	{													\
		U32 count = last - first; 						\
		while (count > 0) 								\
		{ 												\
			U32 step = count / 2; 						\
			U32 middle = first + step;					\
			if (data[middle] < value)					\
			{											\
				first = middle + 1;						\
				count -= step + 1;						\
			}											\
			else										\
			{											\
				count = step;							\
			}											\
			out = first;								\
		}												\
	}

	U32 width = light_ptr->width;
	U32 height = light_ptr->height;

	U32 ui = 0;
	U32 first = 0;
	LowerBound(g_marginal_cdf, first, width, u0 * g_marginal_cdf[width - 1], ui);

	WR_GMEM F32* g_conditional_cdf = &g_conditional_cdfs[height * ui];

	U32 vi = 0;
	first = 0;
	LowerBound(g_conditional_cdf, first, height, u1 * g_conditional_cdf[height - 1], vi);

	F32 u = (F32)(ui) / (F32)(width);
	F32 v = (F32)(vi) / (F32)(height);

	F32 theta = v * PI;
	F32 phi = u * 2.0f * PI;

	F32x3 sample_dir = SphericalToCartesianUnit(theta, phi);
	// sample_dir = normalize(Transform(&light_ptr->obj_to_world_mtx, make_float4(sample_dir, 0.0f)));

	light_sample_ptr->direction = sample_dir;

	// NOTE: This is already computed in SphericalToCartesianUnit(...)
	F32 sin_theta = sin(theta);

	F32 pdf_u;
	F32 pdf_v;

	if (ui == 0)
	{
		pdf_u = g_marginal_cdf[0];
	}
	else
	{
		pdf_u = g_marginal_cdf[ui] - g_marginal_cdf[ui - 1];
	}

	pdf_u /= g_marginal_cdf[width - 1];

	if (vi == 0)
	{
		pdf_v = g_conditional_cdf[0];
	}
	else
	{
		pdf_v = g_conditional_cdf[vi] - g_conditional_cdf[vi - 1];
	}

	pdf_v /= g_conditional_cdf[height - 1];

	F32 pdf = (pdf_u * pdf_v) / (2.0f * PI * PI * sin_theta);

	if (sin_theta == 0.0f)
	{
		pdf = 0.0f;
	}

	// TODO: Multiply with 1 / Area
	light_sample_ptr->pdf = pdf;

	#undef LowerBound
}

WR_DEVICE_FN U32 SampleLightIdUniform(SRandomNumberGenerator* rng_ptr, SSceneMetadata* scene_metadata_ptr)
{
	U32 light_count = scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count;

	if (light_count > 1)
	{
		return GenerateU32(rng_ptr) % light_count;
	}
	else
	{
		return 0;
	}
}

WR_DEVICE_FN void SampleSelectedLight(
	U32 light_id,
	F32x2 uv,
	SShaderGlobals* shading_point_info_ptr,
	SSceneMetadata* scene_metadata_ptr,
	WR_GMEM_PARAM SAreaLight* area_lights,
	WR_GMEM_PARAM SEnvLight* g_env_light,
	WR_READONLY_TEXTURE2D g_env_map,
	WR_GMEM_PARAM F32* g_env_light_conditional_cdfs,
	WR_GMEM_PARAM F32* g_env_light_marginal_cdf,
	SSampleInfo* light_sample_ptr)
{

	if (scene_metadata_ptr->env_light_count > 0
		&&
		light_id == (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count - 1))
	{
		SEnvLight env_light = *g_env_light;
		SampleEnvLight(&env_light, g_env_light_conditional_cdfs, g_env_light_marginal_cdf, uv.x, uv.y, light_sample_ptr);
		return;
	}

	SAreaLight light = area_lights[light_id];

	switch (light.type)
	{
		case LIGHT_TYPE_QUAD:
		{
			SampleLightQuad(&light, shading_point_info_ptr, uv.x, uv.y, light_sample_ptr);
			break;
		}
		case LIGHT_TYPE_DISK:
		{
			SampleLightDisk(&light, shading_point_info_ptr, uv.x, uv.y, light_sample_ptr);
			break;
		}
		case LIGHT_TYPE_SPHERE:
		{
			break;
		}
		case LIGHT_TYPE_TUBE:
		{
			break;
		}
	}
}

WR_DEVICE_FN void SampleLight(SRandomNumberGenerator* rng_ptr, SShaderGlobals* shading_point_info_ptr, SSceneMetadata* scene_metadata_ptr,
	WR_GMEM_PARAM SAreaLight* area_lights,
	WR_GMEM_PARAM SEnvLight* g_env_light,
	WR_READONLY_TEXTURE2D g_env_map,
	WR_GMEM_PARAM F32* g_env_light_conditional_cdfs,
	WR_GMEM_PARAM F32* g_env_light_marginal_cdf,
	SSampleInfo* light_sample_ptr)
{
	U32 selected_light_id = SampleLightIdUniform(rng_ptr, scene_metadata_ptr);

	if (scene_metadata_ptr->env_light_count > 0
		&&
		selected_light_id == (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count - 1))
	{
		SEnvLight env_light = *g_env_light;
		F32 u0 = GenerateF32(rng_ptr);
		F32 u1 = GenerateF32(rng_ptr);
		SampleEnvLight(&env_light, g_env_light_conditional_cdfs, g_env_light_marginal_cdf, u0, u1, light_sample_ptr);
		light_sample_ptr->pdf *= 1.0f / (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count);
		return;
	}

	SAreaLight light = area_lights[selected_light_id];

	switch (light.type)
	{
		case LIGHT_TYPE_QUAD:
		{
			F32 u0 = GenerateF32(rng_ptr);
			F32 u1 = GenerateF32(rng_ptr);
			SampleLightQuad(&light, shading_point_info_ptr, u0, u1, light_sample_ptr);
			light_sample_ptr->pdf *= 1.0f / (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count);
			break;
		}

		case LIGHT_TYPE_DISK:
		{
			F32 u0 = GenerateF32(rng_ptr);
			F32 u1 = GenerateF32(rng_ptr);
			SampleLightDisk(&light, shading_point_info_ptr, u0, u1, light_sample_ptr);
			light_sample_ptr->pdf *= 1.0f / (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count);
			break;
		}

		case LIGHT_TYPE_SPHERE:
		{

			break;
		}

		case LIGHT_TYPE_TUBE:
		{

			break;
		}
	}
}

WR_PROGRAM_END

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

// #define WR_BACKEND_DEBUG

#ifdef __NVCC__
#define WR_BACKEND_CUDA
#endif

#ifdef WR_BACKEND_CUDA
#include "helper_math.h"
#define FLT_MAX 3.40282347E+38F
#define WR_KERNEL __global__
#define WR_DEVICE_FN __forceinline__ __device__
#define WR_CONSTANT __constant__
#define WR_GMEM_PARAM
#define WR_PMEM_PARAM
#define WR_READONLY_TEXTURE2D cudaTextureObject_t
#define WR_GMEM
#define WR_SMEM __shared__
#define WR_RESTRICT __restrict__
#define GetGlobalIdX() blockIdx.x * blockDim.x + threadIdx.x
#define GetGlobalIdY() blockIdx.y * blockDim.y + threadIdx.y
#define GetGlobalIdZ() blockIdx.z * blockDim.z + threadIdx.z
#define GetGlobalSizeX() blockDim.x * gridDim.x
#define GetGlobalSizeY() blockDim.y * gridDim.y
#define GetGlobalSizeZ() blockDim.z * gridDim.z
#define atomic_inc(val) atomicInc(val, UINT_MAX)
#define rsqrt(val) rsqrtf(val)
#define pown(x, y) pow(x, y)

#define ReadTexture2dU8x4(tex, s, t) tex2D<uchar4>(tex, s, t)
#define NormalizedU8x4(c) make_float4( (F32)(c.x) / (F32)(255), (F32)(c.y) / (F32)(255), (F32)(c.z) / (F32)(255), (F32)(c.w) / (F32)(255) )
#define ReadTexture2dU8x4Norm(tex, s, t) NormalizedU8x4(tex2D<uchar4>(tex, s, t))
#define ReadTexture2dF32x4(tex, s, t) tex2D<float4>(tex, s, t)
#define ReadTexture2dS32x4(tex, s, t) tex2D<int4>(tex, s, t)
#define ReadTexture2dU32x4(tex, s, t) tex2D<uint4>(tex, s, t)

WR_DEVICE_FN float2 convert_float2(uint2 v2){return make_float2(v2);}

#define ExitKernel() return

#define WR_PROGRAM_BEGIN extern "C"{
#define WR_PROGRAM_END }

#endif

#ifdef WR_BACKEND_OCL
#define WR_KERNEL kernel
#define WR_DEVICE_FN
#define WR_CONSTANT constant
#define WR_GMEM_PARAM global
#define WR_PMEM_PARAM private
#define WR_READONLY_TEXTURE2D read_only image2d_t
#define WR_GMEM global
#define WR_SMEM local
#define WR_RESTRICT restrict
#define GetGlobalIdX() get_global_id(0)
#define GetGlobalIdY() get_global_id(1)
#define GetGlobalIdZ() get_global_id(2)
#define GetGlobalSizeX() get_global_size(0)
#define GetGlobalSizeY() get_global_size(1)
#define GetGlobalSizeZ() get_global_size(2)

#define make_float4 (float4)
#define MakeF32x3 (float3)
#define make_float2 (float2)
typedef float3 	F32x3;
#define make_uint2 (uint2)

constant sampler_t WR_DEFAULT_SAMPLER = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;
#define ReadTexture2dF32x4(tex, s, t) read_imagef(tex, WR_DEFAULT_SAMPLER, (float2)(s, t))
#define ReadTexture2dS32x4(tex, s, t) read_imagei(tex, WR_DEFAULT_SAMPLER, (float2)(s, t))
#define ReadTexture2dU32x4(tex, s, t) read_imageui(tex, WR_DEFAULT_SAMPLER, (float2)(s, t))

#define NormalizedU8x4(c) make_float4( (F32)(c.x) / (F32)(255), (F32)(c.y) / (F32)(255), (F32)(c.z) / (F32)(255), (F32)(c.w) / (F32)(255) )
#define ReadTexture2dU8x4Norm(tex, s, t) read_imagef(tex, WR_DEFAULT_SAMPLER, (float2)(s, t))

#define WR_PROGRAM_BEGIN
#define WR_PROGRAM_END

#define ExitKernel() return

#endif

#ifdef WR_BACKEND_DEBUG
#include "helper_math.h"
#include <cfloat>
#include <atomic>
#define WR_KERNEL
#define WR_DEVICE_FN inline
#define WR_CONSTANT
#define WR_GMEM_PARAM
#define WR_GMEM
#define WR_SMEM
#define WR_RESTRICT
#define GetGlobalIdX() It
#define GetGlobalIdY()
#define GetGlobalIdZ()
#define GetGlobalSizeX()
#define GetGlobalSizeY()
#define GetGlobalSizeZ()
// #define FLT_MAX 3.40282347E+38F
// HACK:
inline unsigned atomic_inc(unsigned* val){ return reinterpret_cast< std::atomic<unsigned>* >(val)->fetch_add(1); /*return (*val)++;*/ }
#define rsqrt(val) rsqrtf(val)
#define pown(x, y) pow(x, y)
WR_DEVICE_FN float2 convert_float2(uint2 v2){ return make_float2(v2); }

#define ExitKernel() continue

#define WR_PROGRAM_BEGIN namespace LH{
#define WR_PROGRAM_END }

#endif

WR_PROGRAM_BEGIN

typedef unsigned char   U8;

typedef ushort			U16;

typedef uint   			U32;
typedef uint2  			U32x2;
typedef uint3  			U32x3;
typedef uint4  			U32x4;

typedef unsigned long	U64;

typedef short   S16;
typedef int   	S32;
typedef int2  	S32x2;
typedef int3  	S32x3;
typedef int4  	S32x4;
typedef long	S64;


typedef float 	F32;
typedef float2 	F32x2;
// typedef float3 	F32x3;
typedef float4 	F32x4;

typedef bool Bool;

struct SWorkDimension
{
	U32 x;
	U32 y;
	U32 z;
};

WR_PROGRAM_END

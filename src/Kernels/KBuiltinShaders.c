/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KCommonTypes.h"
#include "KRandom.h"
#include "KShading.h"
#include "KBsdf.h"
#include "KSampling.h"
#include "KDeferredIntegratorCommon.h"
#include "KMaterial.h"
#include "KBuiltinShaders.h"

#define WR_SHADER_TEX_SLOTS \
	WR_READONLY_TEXTURE2D t_0, \
	WR_READONLY_TEXTURE2D t_1, \
	WR_READONLY_TEXTURE2D t_2, \
	WR_READONLY_TEXTURE2D t_3, \
	WR_READONLY_TEXTURE2D t_4, \
	WR_READONLY_TEXTURE2D t_5, \
	WR_READONLY_TEXTURE2D t_6, \
	WR_READONLY_TEXTURE2D t_7

WR_PROGRAM_BEGIN

typedef struct SLambertShaderUniformBuffer
{
	F32x4 albedo_color;
	S32 albedo_color_tex_slot;

} SLambertShaderUniformBuffer;

typedef struct SBlinnOnlyShaderUniformBufferDesc
{
	F32x4 specular_color;

	F32 ior;
	F32 roughness;

} SBlinnOnlyShaderUniformBufferDesc;

typedef struct SGgxShaderUniformBufferDesc
{
	F32x4 specular_color;
	S32 specular_color_tex_slot;

	F32 ior;
	S32 ior_tex_slot;

	F32 alpha;
	S32 alpha_tex_slot;

} SGgxShaderUniformBufferDesc;

typedef SGgxShaderUniformBufferDesc SBeckmannShaderUniformBufferDesc;

typedef struct SMirrorShaderUniformBufferDesc
{
	F32x4 pad;
} SMirrorShaderUniformBufferDesc;

WR_KERNEL void LambertShaderEval(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SLambertShaderUniformBuffer* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	F32x4 albedo_color;

	S32 tex_slot = g_uniform_buffer->albedo_color_tex_slot;

	switch (tex_slot)
	{
		case -1: albedo_color = g_uniform_buffer->albedo_color; break;
		case  0: albedo_color = ReadTexture2dU8x4Norm(t_0, sg.texcoord.x, sg.texcoord.y); break;
		// case  1: albedo_color = fetch_tex2d(ctx_tex1, sg.u, sg.v); break;
		// case  2: albedo_color = fetch_tex2d(ctx_tex2, sg.u, sg.v); break;
		// case  3: albedo_color = fetch_tex2d(ctx_tex3, sg.u, sg.v); break;
	}

	SSampleInfo sample_info = g_sample_infos[work_id];

	F32x4 color = albedo_color * BsdfLambertEval() * fabs(dot(sample_info.direction, sg.normal));
	g_sample_infos[work_id].throughput = color / sample_info.pdf;
}

WR_KERNEL void LambertShaderSample(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM F32x2* g_rand_samples,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SLambertShaderUniformBuffer* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	F32x4 albedo_color;

	S32 tex_slot = g_uniform_buffer->albedo_color_tex_slot;

	switch (tex_slot)
	{
		case -1: albedo_color = g_uniform_buffer->albedo_color; break;
		case  0: albedo_color = ReadTexture2dU8x4Norm(t_0, sg.texcoord.x, sg.texcoord.y); break;
		// case  1: albedo_color = fetch_tex2d(ctx_tex1, sg.u, sg.v); break;
		// case  2: albedo_color = fetch_tex2d(ctx_tex2, sg.u, sg.v); break;
		// case  3: albedo_color = fetch_tex2d(ctx_tex3, sg.u, sg.v); break;
	}

	SSampleInfo sample_info;
	F32x2 uv = g_rand_samples[work_id];
	F32 fr = BsdfLambertSample(uv.x, uv.y, &sample_info);
	sample_info.direction = Transform(&sg.tangent_to_world_mtx, make_float4(sample_info.direction, 0.0f));

	g_sample_infos[work_id].direction  = sample_info.direction;
	g_sample_infos[work_id].throughput = albedo_color * fr * fabs(dot(sample_info.direction, sg.normal)) / sample_info.pdf;
	g_sample_infos[work_id].pdf 	   = sample_info.pdf;
}

WR_KERNEL void GgxShaderEval(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SGgxShaderUniformBufferDesc* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	F32x4 specular_color;

	SGgxShaderUniformBufferDesc ub = *g_uniform_buffer;
	S32 tex_slot = ub.specular_color_tex_slot;

	switch (tex_slot)
	{
		case -1: specular_color = ub.specular_color; break;
	}

	SSampleInfo sample_info = g_sample_infos[work_id];

	F32 fr = BsdfMicrofacetGgxEval(sample_info.direction, sg.wo, sg.normal, ub.alpha, ub.ior);
	F32x4 color = specular_color * fr * max(dot(sample_info.direction, sg.normal), 1e-3f);

	g_sample_infos[work_id].throughput = color / sample_info.pdf;
}

WR_KERNEL void GgxShaderSample(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM F32x2* g_rand_samples,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SGgxShaderUniformBufferDesc* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	F32x4 specular_color;

	SGgxShaderUniformBufferDesc ub = *g_uniform_buffer;
	S32 tex_slot = ub.specular_color_tex_slot;

	switch (tex_slot)
	{
		case -1: specular_color = ub.specular_color; break;
	}

	SSampleInfo sample_info;
	F32x2 uv = g_rand_samples[work_id];
	F32 fr = BsdfMicrofacetGgxSample(&sg, uv.x, uv.y, ub.alpha, ub.ior, &sample_info);

	g_sample_infos[work_id].direction  = sample_info.direction;
	g_sample_infos[work_id].throughput = specular_color * fr * max(dot(sample_info.direction, sg.normal), 1e-3f) / sample_info.pdf;
	g_sample_infos[work_id].pdf 	   = sample_info.pdf;
}

WR_KERNEL void MirrorShaderEval(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SMirrorShaderUniformBufferDesc* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	SSampleInfo sample_info = g_sample_infos[work_id];

	F32x3 reflected_dir = normalize(Reflect(sg.wo, sg.normal));

	F32 cos_theta = dot(reflected_dir, sample_info.direction);
	F32 fr = 0.0f;

	if (cos_theta == 1.0f)
	{
		fr = 1.0f;
	}

	F32x4 color = make_float4(1.0f, 1.0f, 1.0f, 1.0f) * fr;

	g_sample_infos[work_id].throughput = color / sample_info.pdf;
}

WR_KERNEL void MirrorShaderSample(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM F32x2* g_rand_samples,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SMirrorShaderUniformBufferDesc* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	F32x3 reflected_dir = normalize(Reflect(sg.wo, sg.normal));
	F32 fr = 1.0f;

	g_sample_infos[work_id].direction  = reflected_dir;
	g_sample_infos[work_id].throughput = make_float4(1.0f, 1.0f, 1.0f, 1.0f) * fr;
	g_sample_infos[work_id].pdf 	   = 1.0f;
}

WR_KERNEL void BeckmannShaderEval(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SBeckmannShaderUniformBufferDesc* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	F32x4 specular_color;

	SBeckmannShaderUniformBufferDesc ub = *g_uniform_buffer;
	S32 tex_slot = ub.specular_color_tex_slot;

	switch (tex_slot)
	{
		case -1: specular_color = ub.specular_color; break;
	}

	SSampleInfo sample_info = g_sample_infos[work_id];

	F32 fr = BsdfMicrofacetBeckmannEval(sample_info.direction, sg.wo, sg.normal, ub.alpha, ub.ior);
	F32x4 color = specular_color * fr * max(dot(sample_info.direction, sg.normal), 1e-3f);

	g_sample_infos[work_id].throughput = color / sample_info.pdf;
}

WR_KERNEL void BeckmannShaderSample(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM F32x2* g_rand_samples,
	WR_GMEM_PARAM SSampleInfo* g_sample_infos,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_PMEM_PARAM U32 g_substream_counter,
	WR_PMEM_PARAM U32 g_substream_offset,
	WR_GMEM_PARAM SBeckmannShaderUniformBufferDesc* g_uniform_buffer,
	WR_SHADER_TEX_SLOTS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_substream_counter)
	{
		return;
	}

	U32 work_id = g_shading_stream[g_substream_offset + thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	F32x4 specular_color;

	SBeckmannShaderUniformBufferDesc ub = *g_uniform_buffer;
	S32 tex_slot = ub.specular_color_tex_slot;

	switch (tex_slot)
	{
		case -1: specular_color = ub.specular_color; break;
	}

	SSampleInfo sample_info;
	F32x2 uv = g_rand_samples[work_id];
	F32 fr = BsdfMicrofacetBeckmannSample(&sg, uv.x, uv.y, ub.alpha, ub.ior, &sample_info);

	g_sample_infos[work_id].direction  = sample_info.direction;
	g_sample_infos[work_id].throughput = specular_color * fr * max(dot(sample_info.direction, sg.normal), 1e-3f) / sample_info.pdf;
	g_sample_infos[work_id].pdf 	   = sample_info.pdf;
}


WR_PROGRAM_END

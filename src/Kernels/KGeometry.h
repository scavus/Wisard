/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KMath.h"

WR_PROGRAM_BEGIN

typedef enum EIntersectionType
{
	INTERSECTION_TYPE_NULL,
	INTERSECTION_TYPE_BACKGROUND,
	INTERSECTION_TYPE_MESH_SURFACE,
	INTERSECTION_TYPE_LIGHT_SURFACE,
	INTERSECTION_TYPE_MESH_VOLUME,
	INTERSECTION_TYPE_LIGHT_VOLUME

} EIntersectionType;

typedef struct SIntersectionData
{
	F32x3 pos;
	F32x3 normal;
	F32x3 tangent;
	F32x3 binormal;
	F32x2 texcoord;

	EIntersectionType intersection_type;
	U32 occluder_id;

} SIntersectionData;

typedef struct SRay
{
	F32x3 origin;
	F32x3 direction;

	F32 t_min;
	F32 t_max;

} SRay;

typedef struct SVertex
{
	F32x3 pos;
	F32x3 normal;
	F32x3 tangent;
	F32x3 binormal;
	F32x2 texcoord;

} SVertex;

typedef struct STriangle
{
	SVertex vtx[3];

	U32 mesh_id;

} STriangle;

typedef struct SQuad
{
	F32x3 base;
	F32x3 normal;
	F32x3 u;
	F32x3 v;
	F32 inv_length_sqr_u;
	F32 inv_length_sqr_v;

} SQuad;

typedef struct SDisk
{
	F32x3 center;
	F32x3 normal;
	F32 radius;

} SDisk;

typedef struct SSphere
{
	F32x3 center;
	F32 radius;

} SSphere;

typedef struct SAabb
{
	F32x3 bound_min;
	F32x3 bound_max;

} SAabb;

/*
typedef struct FOcclusionData
{
EOcclusionType OcclusionType;
U32 occluder_id;

} FOcclusionData;
*/

WR_DEVICE_FN F32 ComputeAreaDisk(const SDisk* disk_ptr)
{
	F32 radius = disk_ptr->radius;

	return PI * radius * radius;
}

WR_DEVICE_FN F32 ComputeAreaSphere(const SSphere* sphere_ptr)
{
	F32 radius = sphere_ptr->radius;

	return (4.0f / 3.0f) * PI * radius * radius;
}

WR_DEVICE_FN F32 ComputeAreaTriangle(const STriangle* triangle_ptr)
{
	F32x3 edge0 = triangle_ptr->vtx[1].pos - triangle_ptr->vtx[0].pos;
	F32x3 edge1 = triangle_ptr->vtx[2].pos - triangle_ptr->vtx[0].pos;

	return length(cross(edge0, edge1)) * 0.5f;
}

WR_DEVICE_FN bool IntersectAabbFast2(SRay* ray_ptr, SAabb* aabb_ptr, F32x3 inv_dir)
{
	F32 t_min;
	F32 t_max;

	F32 Tx1 = (aabb_ptr->bound_min.x - ray_ptr->origin.x) * inv_dir.x;
	F32 Tx2 = (aabb_ptr->bound_max.x - ray_ptr->origin.x) * inv_dir.x;

	t_min = fmin(Tx1, Tx2);
	t_max = fmax(Tx1, Tx2);

	F32 Ty1 = (aabb_ptr->bound_min.y - ray_ptr->origin.y) * inv_dir.y;
	F32 Ty2 = (aabb_ptr->bound_max.y - ray_ptr->origin.y) * inv_dir.y;

	t_min = fmax(t_min, fmin(Ty1, Ty2));
	t_max = fmin(t_max, fmax(Ty1, Ty2));

	F32 Tz1 = (aabb_ptr->bound_min.z - ray_ptr->origin.z) * inv_dir.z;
	F32 Tz2 = (aabb_ptr->bound_max.z - ray_ptr->origin.z) * inv_dir.z;

	t_min = fmax(t_min, fmin(Tz1, Tz2));
	t_max = fmin(t_max, fmax(Tz1, Tz2));

	return t_max >= fmax(0.0f, t_min);
}

// CAUTION: Self-Intersection Problems due to Epsilon
// Moller-Trumbore triangle intersection
WR_DEVICE_FN bool IntersectTriangle(SRay* ray_ptr, STriangle* triangle_ptr, SIntersectionData* intersection_data_ptr)
{
	F32x3 e1 = triangle_ptr->vtx[1].pos - triangle_ptr->vtx[0].pos;
	F32x3 e2 = triangle_ptr->vtx[2].pos - triangle_ptr->vtx[0].pos;
	F32x3 vt = ray_ptr->origin - triangle_ptr->vtx[0].pos;
	F32x3 P = cross(ray_ptr->direction, e2);

	F32 det = dot(e1, P);

	if (det > -1e-6f && det < 1e-6f) return false;

	F32 inv_det = 1.0f / det;
	F32 u = dot(vt, P) * inv_det;

	if (u < 0.0f || u > 1.0f) return false;

	F32x3 q = cross(vt, e1);
	F32 v = dot(ray_ptr->direction, q) * inv_det;

	if (v < 0.0f || u + v > 1.0f) return false;

	F32 w = (1.0f - u - v);
	F32 t = dot(e2, q) * inv_det;

	if (t < 1e-3f) return false;

	ray_ptr->t_min = t;

	// Barycentric interpolation
	intersection_data_ptr->pos = ray_ptr->origin + (ray_ptr->direction * t);
	// intersection_data_ptr->pos =  w * triangle_ptr->vtx[0].pos + u * triangle_ptr->vtx[1].pos + v * triangle_ptr->vtx[2].pos;

	intersection_data_ptr->normal = w * triangle_ptr->vtx[0].normal + u * triangle_ptr->vtx[1].normal + v * triangle_ptr->vtx[2].normal;
	intersection_data_ptr->tangent = w * triangle_ptr->vtx[0].tangent + u * triangle_ptr->vtx[1].tangent + v * triangle_ptr->vtx[2].tangent;
	intersection_data_ptr->binormal = w * triangle_ptr->vtx[0].binormal + u * triangle_ptr->vtx[1].binormal + v * triangle_ptr->vtx[2].binormal;
	intersection_data_ptr->texcoord = w * triangle_ptr->vtx[0].texcoord + u * triangle_ptr->vtx[1].texcoord + v * triangle_ptr->vtx[2].texcoord;

	return true;
}

/*
bool IntersectTriangleWoop(SRay* ray_ptr, STriangle* triangle_ptr, SIntersectionData* intersection_data_ptr)
{

}
*/

WR_DEVICE_FN bool IntersectQuad(SRay* ray_ptr, SQuad* quad_ptr, SIntersectionData* intersection_data_ptr)
{
	F32 n_dot_d = dot(quad_ptr->normal, ray_ptr->direction);

	if (n_dot_d < 1e-6f)
	{
		return false;
	}

	F32 t = dot(quad_ptr->normal, quad_ptr->base - ray_ptr->origin) / n_dot_d;

	if (t <= 0.0f)
	{
		return false;
	}

	F32x3 point_in_plane = ray_ptr->origin + ray_ptr->direction * t;
	F32x3 Vec = point_in_plane - quad_ptr->base;
	F32 u = dot(Vec, quad_ptr->u) * quad_ptr->inv_length_sqr_u;
	F32 v = dot(Vec, quad_ptr->v) * quad_ptr->inv_length_sqr_v;

	if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
	{
		return false;
	}

	intersection_data_ptr->pos = point_in_plane;
	intersection_data_ptr->normal = quad_ptr->normal;
	intersection_data_ptr->texcoord = make_float2(u, 1.0f - v);

	return true;
}

WR_DEVICE_FN bool IntersectDisk(SRay* ray_ptr, SDisk* disk_ptr, SIntersectionData* intersection_data_ptr)
{
	F32 n_dot_d = dot(disk_ptr->normal, ray_ptr->direction);

	if (n_dot_d >= 0.0f)
	{
		return false;
	}

	F32 t = dot(disk_ptr->normal, disk_ptr->center - ray_ptr->origin) / n_dot_d;

	if (t < 0.0f)
	{
		return false;
	}

	F32x3 point_in_plane = ray_ptr->origin + ray_ptr->direction * t;
	F32 distance_sqr = DistanceSq(point_in_plane, disk_ptr->center);

	if (distance_sqr > disk_ptr->radius * disk_ptr->radius)
	{
		return false;
	}

	intersection_data_ptr->pos = point_in_plane;
	intersection_data_ptr->normal = disk_ptr->normal;

	return true;
}

WR_DEVICE_FN bool IntersectSphere(SRay* ray_ptr, SSphere* sphere_ptr, SIntersectionData* intersection_data_ptr)
{

	return true;
}

WR_PROGRAM_END

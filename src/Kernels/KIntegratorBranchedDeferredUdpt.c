/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KIntegratorBranchedDeferredUdpt.h"

WR_PROGRAM_BEGIN

WR_KERNEL void Initialize(
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SRenderWorkInfo* g_render_work_info,
	WR_GMEM_PARAM SStreamInfo* g_active_path_stream_info,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM SStreamInfo* g_shadow_stream_info,
	WR_GMEM_PARAM SStreamInfo* g_extension_stream_info,
	WR_GMEM_PARAM SSceneMetadata* g_scene_metadata,
	WR_GMEM_PARAM SSubstreamInfo* g_shading_substream_infos)
{
	U32 thread_id = GetGlobalIdX();
	SRenderWorkInfo work_info = *g_render_work_info;

	if (thread_id >= work_info.tile_width * work_info.tile_height)
	{
		return;
	}

	SRandomNumberGenerator rng;
	SeedRng(&rng, thread_id);

	g_rngs[thread_id] = rng;

	if (thread_id == 0)
	{
		g_active_path_stream_info->counter = 0;
		g_shadow_stream_info->counter = 0;
		g_extension_stream_info->counter = 0;

		U32 aa_sample_count = g_render_settings->aa_sample_count;
		U32 total_aa_sample_count = Sqr(aa_sample_count);
		U32 mtl_count = g_scene_metadata->material_count;

		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			g_shading_substream_infos[mtl_idx].counter = 0;
			g_shading_substream_infos[mtl_idx].offset = mtl_idx * work_info.tile_width * work_info.tile_height * total_aa_sample_count;
		}
	}
}

WR_KERNEL void PrepareDirectIllumBranchedSamples(
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SRenderWorkInfo* g_render_work_info,
	WR_GMEM_PARAM SSceneMetadata* g_scene_metadata,
	WR_GMEM_PARAM SPathState* g_path_states,
	WR_GMEM_PARAM U32* g_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_active_path_stream_info,
	WR_GMEM_PARAM SRandomNumberGenerator* g_base_rngs,
	WR_GMEM_PARAM SRandomNumberGenerator* g_branched_rngs,
	WR_GMEM_PARAM SShaderGlobals* g_base_shader_globals,
	WR_GMEM_PARAM U32* g_base_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_base_shading_substream_infos,
	WR_GMEM_PARAM SShaderGlobals* g_branched_shader_globals,
	WR_GMEM_PARAM U32* g_branched_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_branched_shading_substream_infos,
	WR_GMEM_PARAM F32x2* g_direct_illum_rand_samples,
	WR_GMEM_PARAM SSampleInfo* g_bsdf_sample_infos,
	WR_GMEM_PARAM SSampleInfo* g_light_sample_infos,
	WR_GMEM_PARAM U32* g_shadow_stream,
	WR_GMEM_PARAM SStreamInfo* g_shadow_stream_info)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_active_path_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_active_path_stream[thread_id];

	SRandomNumberGenerator rng = g_base_rngs[work_id];
	SShaderGlobals sg = g_base_shader_globals[work_id];

	U32 mtl_count = g_scene_metadata->material_count;

	U32 di_sample_count = g_render_settings->direct_illum_sample_count;
	U32 total_di_sample_count = Sqr(di_sample_count);

	U32 aa_sample_count = g_render_settings->aa_sample_count;
	U32 total_aa_sample_count = Sqr(aa_sample_count);

	U32 base_path_count = g_render_work_info->tile_width * g_render_work_info->tile_height * total_aa_sample_count;
	U32 branched_path_count = base_path_count * total_di_sample_count;

	U32 sample_offset = work_id * total_di_sample_count;
	SampleCorrelatedMultiJittered2d(&rng, di_sample_count, di_sample_count, g_direct_illum_rand_samples + sample_offset);

	SSampleInfo sample_info;
	sample_info.throughput = make_float4(1.0f, 1.0f, 1.0f, 1.0f);
	sample_info.pdf = 1.0f;

	for (U32 sample_idx = 0; sample_idx < total_di_sample_count; sample_idx++)
	{
		g_branched_shader_globals[sample_offset + sample_idx] = sg;
		g_branched_rngs[sample_offset + sample_idx] = rng;
		g_bsdf_sample_infos[sample_offset + sample_idx] = sample_info;
		g_light_sample_infos[sample_offset + sample_idx] = sample_info;

		g_shadow_stream[thread_id * total_di_sample_count + sample_idx] = sample_offset + sample_idx;

		/*
		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			U32 mtl_offset = base_path_count * mtl_idx;

			U32 shading_work_id = g_base_shading_stream[mtl_offset + thread_id];
			U32 branched_path_shading_id = shading_work_id * total_di_sample_count + sample_idx;
			U32 shading_sample_offset = shading_work_id * total_di_sample_count;
			g_branched_shading_stream[(shading_sample_offset + sample_idx) + (branched_path_count * mtl_idx)] = branched_path_shading_id;
		}
		*/
	}

	if (thread_id == 0)
	{
		g_shadow_stream_info->counter = g_active_path_stream_info->counter * total_di_sample_count;

		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			g_branched_shading_substream_infos[mtl_idx].counter = g_base_shading_substream_infos[mtl_idx].counter * total_di_sample_count;
			g_branched_shading_substream_infos[mtl_idx].offset = mtl_idx * branched_path_count;
		}
	}
}

WR_KERNEL void PrepareIndirectIllumBranchedSamples(
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SRenderWorkInfo* g_render_work_info,
	WR_GMEM_PARAM SSceneMetadata* g_scene_metadata,
	WR_GMEM_PARAM SPathState* g_base_path_states,
	WR_GMEM_PARAM U32* g_base_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_base_active_path_stream_info,
	WR_GMEM_PARAM SRandomNumberGenerator* g_base_rngs,
	WR_GMEM_PARAM SShaderGlobals* g_base_shader_globals,
	WR_GMEM_PARAM U32* g_base_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_base_shading_substream_infos,

	WR_GMEM_PARAM SPathState* g_branched_path_states,
	WR_GMEM_PARAM U32* g_branched_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_branched_active_path_stream_info,
	WR_GMEM_PARAM SRandomNumberGenerator* g_branched_rngs,
	WR_GMEM_PARAM SShaderGlobals* g_branched_shader_globals,
	WR_GMEM_PARAM U32* g_branched_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_branched_shading_substream_infos,

	WR_GMEM_PARAM U32* g_extension_stream,
	WR_GMEM_PARAM SStreamInfo* g_extension_stream_info,
	WR_GMEM_PARAM SSampleInfo* g_extension_sample_infos,
	WR_GMEM_PARAM F32x2* g_indirect_illum_rand_samples)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_base_active_path_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_base_active_path_stream[thread_id];

	SPathState path = g_base_path_states[work_id];
	SRandomNumberGenerator rng = g_base_rngs[work_id];
	SShaderGlobals sg = g_base_shader_globals[work_id];

	U32 mtl_count = g_scene_metadata->material_count;

	U32 ii_sample_count = g_render_settings->indirect_illum_sample_count;
	U32 total_ii_sample_count = Sqr(ii_sample_count);

	U32 aa_sample_count = g_render_settings->aa_sample_count;
	U32 total_aa_sample_count = Sqr(aa_sample_count);

	U32 base_path_count = g_render_work_info->tile_width * g_render_work_info->tile_height * total_aa_sample_count;
	U32 branched_path_count = base_path_count * total_ii_sample_count;

	U32 sample_offset = work_id * total_ii_sample_count;
	SampleCorrelatedMultiJittered2d(&rng, ii_sample_count, ii_sample_count, g_indirect_illum_rand_samples + sample_offset);

	SSampleInfo sample_info;
	sample_info.throughput = make_float4(1.0f, 1.0f, 1.0f, 1.0f);
	sample_info.pdf = 1.0f;

	for (U32 sample_idx = 0; sample_idx < total_ii_sample_count; sample_idx++)
	{
		U32 branched_path_id = sample_offset + sample_idx;

		g_branched_rngs[branched_path_id] = rng;
		g_extension_sample_infos[branched_path_id] = sample_info;
		g_branched_shader_globals[branched_path_id] = sg;
		g_branched_path_states[branched_path_id] = path;

		// g_branched_active_path_stream[sample_offset + sample_idx] = branched_path_id;
		// g_extension_stream[sample_offset + sample_idx] = branched_path_id;
		g_branched_active_path_stream[thread_id * total_ii_sample_count + sample_idx] = branched_path_id;
		g_extension_stream[thread_id * total_ii_sample_count + sample_idx] = branched_path_id;

		/*
		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			U32 mtl_offset = base_path_count * mtl_idx;

			U32 shading_work_id = g_base_shading_stream[mtl_offset + thread_id];
			U32 branched_path_shading_id = shading_work_id * total_ii_sample_count + sample_idx;
			g_branched_shading_stream[(sample_offset + sample_idx) + (branched_path_count * mtl_idx)] = branched_path_shading_id;
		}
		*/
	}

	if (thread_id == 0)
	{
		U32 c = g_base_active_path_stream_info->counter * total_ii_sample_count;
		g_branched_active_path_stream_info->counter = c;
		g_extension_stream_info->counter = c;

		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			g_branched_shading_substream_infos[mtl_idx].counter = g_base_shading_substream_infos[mtl_idx].counter * total_ii_sample_count;
			g_branched_shading_substream_infos[mtl_idx].offset = mtl_idx * branched_path_count;
		}
	}
}

WR_KERNEL void PrepareDirectIllumBranchedShadingStream(
	WR_GMEM_PARAM SRenderWorkInfo* g_render_work_info,
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SSceneMetadata* g_scene_metadata,
	WR_GMEM_PARAM U32* g_base_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_base_shading_substream_infos,
	WR_GMEM_PARAM U32* g_branched_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_branched_shading_substream_infos)
{
	U32 thread_id = GetGlobalIdX();

	U32 aa_sample_count = g_render_settings->aa_sample_count;
	U32 direct_sample_count = g_render_settings->direct_illum_sample_count;
	U32 total_aa_sample_count = Sqr(aa_sample_count);
	U32 total_direct_illum_sample_count = Sqr(direct_sample_count);

	U32 base_path_count = g_render_work_info->tile_width * g_render_work_info->tile_height * total_aa_sample_count;
	U32 branched_path_count = base_path_count * total_direct_illum_sample_count;

	if (thread_id >= branched_path_count)
	{
		return;
	}

	U32 sample_offset = thread_id / total_direct_illum_sample_count;
	U32 work_offset = thread_id % total_direct_illum_sample_count;

	U32 mtl_count = g_scene_metadata->material_count;

	for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
	{
		U32 mtl_offset = base_path_count * mtl_idx;
		U32 branched_mtl_offset = branched_path_count * mtl_idx;

		U32 work_id = g_base_shading_stream[mtl_offset + sample_offset];
		U32 branched_work_id = work_id * total_direct_illum_sample_count + work_offset;

		g_branched_shading_stream[branched_mtl_offset + thread_id] = branched_work_id;
	}

	if (thread_id == 0)
	{
		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			g_branched_shading_substream_infos[mtl_idx].counter = g_base_shading_substream_infos[mtl_idx].counter * total_direct_illum_sample_count;
			g_branched_shading_substream_infos[mtl_idx].offset = mtl_idx * branched_path_count;
		}
	}
}

WR_KERNEL void PrepareIndirectIllumBranchedShadingStream(
	WR_GMEM_PARAM SRenderWorkInfo* g_render_work_info,
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SSceneMetadata* g_scene_metadata,
	WR_GMEM_PARAM U32* g_base_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_base_shading_substream_infos,
	WR_GMEM_PARAM U32* g_branched_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_branched_shading_substream_infos)
{
	U32 thread_id = GetGlobalIdX();

	U32 aa_sample_count = g_render_settings->aa_sample_count;
	U32 ii_sample_count = g_render_settings->indirect_illum_sample_count;
	U32 total_aa_sample_count = Sqr(aa_sample_count);
	U32 total_ii_sample_count = Sqr(ii_sample_count);

	U32 base_path_count = g_render_work_info->tile_width * g_render_work_info->tile_height * total_aa_sample_count;
	U32 branched_path_count = base_path_count * total_ii_sample_count;

	if (thread_id >= branched_path_count)
	{
		return;
	}

	U32 sample_offset = thread_id / total_ii_sample_count;
	U32 work_offset = thread_id % total_ii_sample_count;

	U32 mtl_count = g_scene_metadata->material_count;

	for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
	{
		U32 mtl_offset = base_path_count * mtl_idx;
		U32 branched_mtl_offset = branched_path_count * mtl_idx;

		U32 work_id = g_base_shading_stream[mtl_offset + sample_offset];
		U32 branched_work_id = work_id * total_ii_sample_count + work_offset;

		g_branched_shading_stream[branched_mtl_offset + thread_id] = branched_work_id;
	}

	if (thread_id == 0)
	{
		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			g_branched_shading_substream_infos[mtl_idx].counter = g_base_shading_substream_infos[mtl_idx].counter * total_ii_sample_count;
			g_branched_shading_substream_infos[mtl_idx].offset = mtl_idx * branched_path_count;
		}
	}
}

WR_KERNEL void PrepareGenericIllumSamples(
	WR_GMEM_PARAM SPathState* g_path_states,
	WR_GMEM_PARAM U32* g_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_active_path_stream_info,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM F32x2* g_direct_illum_rand_samples,
	WR_GMEM_PARAM F32x2* g_indirect_illum_rand_samples,
	WR_GMEM_PARAM SSampleInfo* g_extension_sample_infos,
	WR_GMEM_PARAM SSampleInfo* g_bsdf_sample_infos,
	WR_GMEM_PARAM SSampleInfo* g_light_sample_infos,
	WR_GMEM_PARAM U32* g_shadow_stream,
	WR_GMEM_PARAM SStreamInfo* g_shadow_stream_info,
	WR_GMEM_PARAM U32* g_extension_stream,
	WR_GMEM_PARAM SStreamInfo* g_extension_stream_info)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_active_path_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_active_path_stream[thread_id];

	SRandomNumberGenerator rng = g_rngs[work_id];

	SSampleInfo sample_info;
	sample_info.throughput = make_float4(1.0f, 1.0f, 1.0f, 1.0f);
	sample_info.pdf = 1.0f;

	g_indirect_illum_rand_samples[work_id] = make_float2(GenerateF32(&rng), GenerateF32(&rng));
	g_direct_illum_rand_samples[work_id] = make_float2(GenerateF32(&rng), GenerateF32(&rng));
	g_rngs[work_id] = rng;
	g_bsdf_sample_infos[work_id] = sample_info;
	g_light_sample_infos[work_id] = sample_info;
	g_extension_sample_infos[work_id] = sample_info;

	g_shadow_stream[thread_id] = work_id;
	g_extension_stream[thread_id] = work_id;

	if (thread_id == 0)
	{
		U32 c = g_active_path_stream_info->counter;
		g_shadow_stream_info->counter = c;
		g_extension_stream_info->counter = c;
	}
}

WR_KERNEL void GeneratePaths(
	WR_GMEM_PARAM SCamera* g_camera,
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SRenderWorkInfo* g_render_work_info,
	WR_GMEM_PARAM SRandomNumberGenerator* g_base_rngs,
	WR_GMEM_PARAM SRandomNumberGenerator* g_branched_rngs,
	WR_GMEM_PARAM F32x2* g_aa_rand_samples,
	WR_GMEM_PARAM SRay* g_extension_rays,
	WR_GMEM_PARAM SIntersectionData* g_extension_ray_intersection_datas,
	WR_GMEM_PARAM U32* g_extension_stream,
	WR_GMEM_PARAM SStreamInfo* g_extension_stream_info,
	WR_GMEM_PARAM SSampleInfo* g_extension_sample_infos,
	WR_GMEM_PARAM SPathState* g_path_states,
	WR_GMEM_PARAM U32* g_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_active_path_stream_info)
{
	U32 thread_id = GetGlobalIdX();
	SRenderWorkInfo work_info = *g_render_work_info;

	if (thread_id >= work_info.tile_width * work_info.tile_height)
	{
		return;
	}

	U32 aa_sample_count = g_render_settings->aa_sample_count;
	U32 total_sample_count = Sqr(aa_sample_count);
	U32 sample_offset = thread_id * total_sample_count;

	SRandomNumberGenerator rng = g_base_rngs[thread_id];

	U32x2 pixel_coord = make_uint2((thread_id % work_info.tile_width) + work_info.tile_offset_x,
		(thread_id / work_info.tile_width) + work_info.tile_offset_y);
	U32 pixel_id = g_render_settings->common.render_width * pixel_coord.y + pixel_coord.x;
	U32 pixel_offset = pixel_id * total_sample_count;

	SMatrix4x4 camera_to_world_mtx = g_camera->camera_to_world_mtx;

	SampleCorrelatedMultiJittered2d(&rng, aa_sample_count, aa_sample_count, g_aa_rand_samples + sample_offset);

	for (U32 sample_idx = 0; sample_idx < total_sample_count; sample_idx++)
	{
		F32x2 uv = make_float2(0.0f, 0.0f);
		uv = g_aa_rand_samples[sample_offset + sample_idx];

		F32x2 camera_sample = convert_float2(pixel_coord) + uv;

		F32 fov;

		if (g_camera->type == PINHOLE)
		{
			fov = tan(g_camera->properties.pinhole.fov * 0.5f * PI / 180.0f);
		}
		else if (g_camera->type == THINLENS)
		{
			fov = tan(g_camera->properties.thinlens.fov * 0.5f * PI / 180.0f);
		}

		F32 aspect_width = (F32)(g_render_settings->common.render_width);
		F32 aspect_height = (F32)(g_render_settings->common.render_height);

		camera_sample.x = (2.0f * (camera_sample.x) / aspect_width - 1.0f) * fov * (aspect_width / aspect_height);
		camera_sample.y = (1.0f - 2.0f * (camera_sample.y) / aspect_height) * fov;

		SRay ray;

		if (g_camera->type == PINHOLE)
		{
			ray.origin = Transform(&camera_to_world_mtx, make_float4(0.0f, 0.0f, 0.0f, 1.0f));
			ray.direction = normalize(Transform(&camera_to_world_mtx, make_float4(camera_sample.x, camera_sample.y, -1.0f, 0.0f)));
			ray.t_max = FLT_MAX;
		}
		else if (g_camera->type == THINLENS)
		{
			F32x2 lens_uv = SampleDiskConcentric(uv.x, uv.y);
			lens_uv *= g_camera->properties.thinlens.aperture_size;
			ray.origin = Transform(&camera_to_world_mtx, make_float4(0.0f, 0.0f, 0.0f, 1.0f));
			ray.direction = normalize(Transform(&camera_to_world_mtx, make_float4(camera_sample.x, camera_sample.y, -1.0f, 0.0f)));
			F32x3 focal_point = ray.origin + (ray.direction * g_camera->properties.thinlens.focal_length);

			ray.origin = Transform(&camera_to_world_mtx, make_float4(MakeF32x3(lens_uv.x, lens_uv.y, 0.0f), 1.0f));
			ray.direction = normalize(focal_point - ray.origin);
			ray.t_max = FLT_MAX;
		}

		SSampleInfo sample_info;
		sample_info.direction = ray.direction;
		sample_info.throughput = make_float4(1.0f, 1.0f, 1.0f, 1.0f);
		sample_info.pdf = 1.0f;

		U32 work_id = sample_offset + sample_idx;

		// To reduce correlation
		SRandomNumberGenerator new_rng;
		SeedRng(&new_rng, pixel_offset + sample_idx);
		g_branched_rngs[work_id] = new_rng;

		g_path_states[work_id].throughput = make_float4(1.0f, 1.0f, 1.0f, 1.0f);
		g_path_states[work_id].radiance = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
		g_path_states[work_id].bounce_count = 0;

		g_extension_rays[work_id] = ray;
		g_extension_ray_intersection_datas[work_id].intersection_type = INTERSECTION_TYPE_BACKGROUND;
		g_extension_stream[work_id] = work_id;
		g_active_path_stream[work_id] = work_id;

		g_extension_sample_infos[work_id] = sample_info;
	}

	if (thread_id == 0)
	{
		U32 work_size = g_render_work_info->tile_width * g_render_work_info->tile_height * total_sample_count;

		g_extension_stream_info->counter = work_size;
		g_active_path_stream_info->counter = work_size;
	}
}

WR_KERNEL void ContribDirectIllumBranched(
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SPathState* g_path_states,
	WR_GMEM_PARAM U32* g_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_active_path_stream_info,
	WR_GMEM_PARAM SIntersectionData* g_bsdf_ray_intersection_datas,
	WR_GMEM_PARAM SIntersectionData* g_light_ray_intersection_datas,
	WR_GMEM_PARAM SSampleInfo* g_bsdf_sample_infos,
	WR_GMEM_PARAM SSampleInfo* g_light_sample_infos,
	WR_GMEM_PARAM SAreaLight* g_area_lights,
	WR_GMEM_PARAM SEnvLight* g_env_light,
	WR_READONLY_TEXTURE2D g_env_map)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_active_path_stream_info->counter)
	{
		return;
	}

	U32 path_id = g_active_path_stream[thread_id];
	SPathState path = g_path_states[path_id];

	U32 total_sample_count = Sqr(g_render_settings->direct_illum_sample_count);
	U32 light_sample_count = total_sample_count;
	U32 bsdf_sample_count = total_sample_count;
	U32 sample_offset = path_id * total_sample_count;

	F32 weight = 1.0f / (F32)(total_sample_count);

	for (U32 sample_idx = 0; sample_idx < total_sample_count; sample_idx++)
	{
		SIntersectionData bsdf_ray_intersection_data = g_bsdf_ray_intersection_datas[sample_offset + sample_idx];
		SIntersectionData light_ray_intersection_data = g_light_ray_intersection_datas[sample_offset + sample_idx];

		SSampleInfo bsdf_sample_info = g_bsdf_sample_infos[sample_offset + sample_idx];
		SSampleInfo light_sample_info = g_light_sample_infos[sample_offset + sample_idx];

		if (bsdf_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
		{
			if (g_env_light)
			{
				F32x4 throughput = path.throughput * bsdf_sample_info.throughput;

				SEnvLight light = *g_env_light;
				// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(bsdf_sample_info.direction, 0.0f)));
				F32x3 w = bsdf_sample_info.direction;
				F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
				F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
				F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);
				F32 mis_weight = ComputeMisWeightBalanceHeuristic(bsdf_sample_count, bsdf_sample_info.pdf, light_sample_count, light_sample_info.pdf);
				path.radiance += throughput * mis_weight * l * weight;
			}
		}
		else if (bsdf_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
		{
			F32x4 throughput = path.throughput * bsdf_sample_info.throughput;
			SAreaLight light = g_area_lights[bsdf_ray_intersection_data.occluder_id];
			F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(bsdf_sample_count, bsdf_sample_info.pdf, light_sample_count, light_sample_info.pdf);
			path.radiance += throughput * mis_weight * l * weight;

			g_bsdf_ray_intersection_datas[sample_offset + sample_idx].intersection_type = INTERSECTION_TYPE_NULL;
		}

		if (light_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
		{
			if (g_env_light)
			{
				F32x4 throughput = path.throughput * light_sample_info.throughput;

				SEnvLight light = *g_env_light;
				// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(light_sample_info.direction, 0.0f)));
				F32x3 w = light_sample_info.direction;
				F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
				F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
				F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);
				F32 mis_weight = ComputeMisWeightBalanceHeuristic(light_sample_count, light_sample_info.pdf, bsdf_sample_count, bsdf_sample_info.pdf);
				path.radiance += throughput * mis_weight * l;
			}
		}
		else if (light_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
		{
			F32x4 throughput = path.throughput * light_sample_info.throughput;
			SAreaLight light = g_area_lights[light_ray_intersection_data.occluder_id];
			F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(light_sample_count, light_sample_info.pdf, bsdf_sample_count, bsdf_sample_info.pdf);
			path.radiance += throughput * mis_weight * l * weight;

			g_light_ray_intersection_datas[sample_offset + sample_idx].intersection_type = INTERSECTION_TYPE_NULL;
		}
	}

	g_path_states[path_id] = path;
}

WR_KERNEL void ContribDirectIllumSingle(
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SPathState* g_path_states,
	WR_GMEM_PARAM U32* g_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_active_path_stream_info,
	WR_GMEM_PARAM SIntersectionData* g_bsdf_ray_intersection_datas,
	WR_GMEM_PARAM SIntersectionData* g_light_ray_intersection_datas,
	WR_GMEM_PARAM SSampleInfo* g_bsdf_sample_infos,
	WR_GMEM_PARAM SSampleInfo* g_light_sample_infos,
	WR_GMEM_PARAM SAreaLight* g_area_lights,
	WR_GMEM_PARAM SEnvLight* g_env_light,
	WR_READONLY_TEXTURE2D g_env_map)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_active_path_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_active_path_stream[thread_id];

	SPathState path = g_path_states[work_id];

	SIntersectionData bsdf_ray_intersection_data = g_bsdf_ray_intersection_datas[work_id];
	SIntersectionData light_ray_intersection_data = g_light_ray_intersection_datas[work_id];

	SSampleInfo bsdf_sample_info = g_bsdf_sample_infos[work_id];
	SSampleInfo light_sample_info = g_light_sample_infos[work_id];

	if (bsdf_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
	{
		if (g_env_light)
		{
			F32x4 throughput = path.throughput * bsdf_sample_info.throughput;

			SEnvLight light = *g_env_light;
			// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(bsdf_sample_info.direction, 0.0f)));
			F32x3 w = bsdf_sample_info.direction;
			F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
			F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
			F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, bsdf_sample_info.pdf, 1, light_sample_info.pdf);
			path.radiance += throughput * mis_weight * l;
		}
	}
	else if (bsdf_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
	{
		F32x4 throughput = path.throughput * bsdf_sample_info.throughput;
		SAreaLight light = g_area_lights[bsdf_ray_intersection_data.occluder_id];
		F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);
		F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, bsdf_sample_info.pdf, 1, light_sample_info.pdf);
		path.radiance += throughput * mis_weight * l;

		g_bsdf_ray_intersection_datas[work_id].intersection_type = INTERSECTION_TYPE_NULL;
	}

	if (light_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
	{
		if (g_env_light)
		{
			F32x4 throughput = path.throughput * light_sample_info.throughput;

			SEnvLight light = *g_env_light;
			// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(light_sample_info.direction, 0.0f)));
			F32x3 w = light_sample_info.direction;
			F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
			F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
			F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);
			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, light_sample_info.pdf, 1, bsdf_sample_info.pdf);
			path.radiance += throughput * mis_weight * l;
		}
	}
	else if (light_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
	{
		F32x4 throughput = path.throughput * light_sample_info.throughput;
		SAreaLight light = g_area_lights[light_ray_intersection_data.occluder_id];
		F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);
		F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, light_sample_info.pdf, 1, bsdf_sample_info.pdf);
		path.radiance += throughput * mis_weight * l;

		g_light_ray_intersection_datas[work_id].intersection_type = INTERSECTION_TYPE_NULL;
	}

	g_path_states[work_id] = path;
}

WR_KERNEL void ContribIndirectIllum(
	WR_GMEM_PARAM SPathState* g_base_path_states,
	WR_GMEM_PARAM U32* g_base_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_base_active_path_stream_info,
	WR_GMEM_PARAM SPathState* g_branched_path_states,
	WR_GMEM_PARAM SRenderSettings* g_render_settings)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_base_active_path_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_base_active_path_stream[thread_id];
	SPathState base_path = g_base_path_states[work_id];

	U32 indirect_illum_sample_count = g_render_settings->indirect_illum_sample_count;
	U32 total_sample_count = Sqr(indirect_illum_sample_count);

	U32 sample_offset = work_id * total_sample_count;

	F32 weight = 1.0f / (F32)(total_sample_count);

	for (U32 sample_idx = 0; sample_idx < total_sample_count; sample_idx++)
	{
		base_path.radiance += g_branched_path_states[sample_offset + sample_idx].radiance * weight;
	}

	g_base_path_states[work_id] = base_path;
}

WR_KERNEL void Controller(
	WR_PMEM_PARAM U32 g_active_path_count,
	WR_PMEM_PARAM U32 g_total_path_count,
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM SPathState* g_path_states,
	WR_GMEM_PARAM U32* g_active_path_stream,
	WR_GMEM_PARAM SStreamInfo* g_active_path_stream_info,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM SSampleInfo* g_extension_sample_infos,
	WR_GMEM_PARAM SIntersectionData* g_extension_ray_intersection_datas,
	WR_GMEM_PARAM SMesh* g_meshes,
	WR_GMEM_PARAM SAreaLight* g_area_lights,
	WR_GMEM_PARAM SEnvLight* g_env_light,
	WR_READONLY_TEXTURE2D g_env_map,
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM U32* g_shading_stream,
	WR_GMEM_PARAM SSubstreamInfo* g_shading_substream_infos)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_active_path_count)
	{
		return;
	}

	U32 work_id = g_active_path_stream[thread_id];

	SPathState path = g_path_states[work_id];
	SRandomNumberGenerator rng = g_rngs[work_id];
	SSampleInfo extension_sample_info = g_extension_sample_infos[work_id];
	SIntersectionData extension_ray_intersection_data = g_extension_ray_intersection_datas[work_id];

	if (extension_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
	{
		if (g_env_light)
		{
			F32x4 throughput = path.throughput * extension_sample_info.throughput;

			SEnvLight light = *g_env_light;
			// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(extension_sample_info.direction, 0.0f)));
			F32x3 w = extension_sample_info.direction;
			F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
			F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
			F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);

			path.radiance += throughput * l;
		}

		g_path_states[work_id] = path;

		return;
	}
	else if (extension_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
	{
		// Get emission and update radiance
		F32x4 throughput = path.throughput * extension_sample_info.throughput;
		SAreaLight light = g_area_lights[extension_ray_intersection_data.occluder_id];
		F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);

		path.radiance += throughput * l;

		g_path_states[work_id] = path;

		return;
	}

	path.throughput *= extension_sample_info.throughput;

	if (path.bounce_count > g_render_settings->max_indirect_bounce_count)
	{
		g_path_states[work_id] = path;

		return;
	}

	/*
	if (g_render_settings->enable_rr)
	{
		F32 cont_prob = fmin(0.5f, Luminance(path.throughput));

		if (GenerateF32(&rng) > cont_prob)
		{
			g_path_states[work_id] = path;

			return;
		}

		path.throughput /= cont_prob;
	}
	*/

	path.bounce_count++;

	SShaderGlobals sg;
	sg.pos = extension_ray_intersection_data.pos;
	sg.normal = extension_ray_intersection_data.normal;
	sg.texcoord = extension_ray_intersection_data.texcoord;
	sg.wo = -extension_sample_info.direction;
	sg.tangent_to_world_mtx = BuildMatrix4x4(
		make_float4(extension_ray_intersection_data.tangent, 0.0f),
		make_float4(extension_ray_intersection_data.binormal, 0.0f),
		make_float4(extension_ray_intersection_data.normal, 0.0f),
		make_float4(0.0f, 0.0f, 0.0f, 1.0f));
	sg.tangent_to_world_mtx = TransposeMatrix4x4(&sg.tangent_to_world_mtx);
	sg.mesh_id = extension_ray_intersection_data.occluder_id;
	sg.material_id = g_meshes[sg.mesh_id].material_id;

	g_shader_globals[work_id] = sg;
	g_path_states[work_id] = path;
	g_rngs[work_id] = rng;

	U32 shading_work_id = atomic_inc(&g_shading_substream_infos[sg.material_id].counter);
	shading_work_id += sg.material_id * g_total_path_count;

	g_shading_stream[shading_work_id] = work_id;

	U32 active_work_id = atomic_inc(&g_active_path_stream_info->counter);
	g_active_path_stream[active_work_id] = work_id;
}

WR_KERNEL void SampleLights(
	WR_GMEM_PARAM U32* g_shadow_stream,
	WR_GMEM_PARAM SStreamInfo* g_shadow_stream_info,
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRandomNumberGenerator* g_rngs,
	WR_GMEM_PARAM F32x2* g_direct_illum_rand_samples,
	WR_GMEM_PARAM SSampleInfo* g_light_sample_infos,
	WR_GMEM_PARAM SSceneMetadata* g_scene_metadata,
	WR_GMEM_PARAM SAreaLight* g_area_lights,
	WR_GMEM_PARAM SEnvLight* g_env_light,
	WR_READONLY_TEXTURE2D g_env_map,
	WR_GMEM_PARAM F32* g_env_light_conditional_cdfs,
	WR_GMEM_PARAM F32* g_env_light_marginal_cdf)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_shadow_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_shadow_stream[thread_id];

	SSceneMetadata scene_metadata = *g_scene_metadata;
	SRandomNumberGenerator rng = g_rngs[work_id];
	SShaderGlobals sg = g_shader_globals[work_id];
	F32x2 uv = g_direct_illum_rand_samples[work_id];

	U32 light_id = SampleLightIdUniform(&rng, &scene_metadata);

	SSampleInfo light_sample_info;

	SampleSelectedLight(light_id, uv, &sg,
		&scene_metadata,
		g_area_lights, g_env_light, g_env_map, g_env_light_conditional_cdfs, g_env_light_marginal_cdf,
		&light_sample_info);

	light_sample_info.pdf *= 1.0f / (scene_metadata.area_light_count + scene_metadata.env_light_count);

	g_light_sample_infos[work_id] = light_sample_info;
}

WR_KERNEL void PrepareShadowRays(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRay* g_bsdf_rays,
	WR_GMEM_PARAM SRay* g_light_rays,
	WR_GMEM_PARAM SIntersectionData* g_bsdf_ray_intersection_datas,
	WR_GMEM_PARAM SIntersectionData* g_light_ray_intersection_datas,
	WR_GMEM_PARAM SSampleInfo* g_bsdf_sample_infos,
	WR_GMEM_PARAM SSampleInfo* g_light_sample_infos,
	WR_GMEM_PARAM U32* g_shadow_stream,
	WR_GMEM_PARAM SStreamInfo* g_shadow_stream_info)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_shadow_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_shadow_stream[thread_id];

	F32x3 pos = g_shader_globals[work_id].pos;

	SSampleInfo bsdf_sample_info = g_bsdf_sample_infos[work_id];
	SSampleInfo light_sample_info = g_light_sample_infos[work_id];

	SRay bsdf_ray;
	bsdf_ray.origin = pos;
	bsdf_ray.direction = bsdf_sample_info.direction;
	bsdf_ray.t_max = FLT_MAX;

	SRay light_ray;
	light_ray.origin = pos;
	light_ray.direction = light_sample_info.direction;
	light_ray.t_max = FLT_MAX;

	g_bsdf_rays[work_id] = bsdf_ray;
	g_light_rays[work_id] = light_ray;

	g_bsdf_ray_intersection_datas[work_id].intersection_type = INTERSECTION_TYPE_BACKGROUND;
	g_light_ray_intersection_datas[work_id].intersection_type = INTERSECTION_TYPE_BACKGROUND;
}

WR_KERNEL void PrepareExtensionRays(
	WR_GMEM_PARAM SShaderGlobals* g_shader_globals,
	WR_GMEM_PARAM SRay* g_extension_rays,
	WR_GMEM_PARAM SIntersectionData* g_extension_ray_intersection_datas,
	WR_GMEM_PARAM SSampleInfo* g_extension_sample_infos,
	WR_GMEM_PARAM U32* g_extension_stream,
	WR_GMEM_PARAM SStreamInfo* g_extension_stream_info)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_extension_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_extension_stream[thread_id];

	F32x3 pos = g_shader_globals[work_id].pos;

	SSampleInfo extension_sample_info = g_extension_sample_infos[work_id];

	SRay extension_ray;
	extension_ray.origin = pos;
	extension_ray.direction = extension_sample_info.direction;
	extension_ray.t_max = FLT_MAX;

	g_extension_rays[work_id] = extension_ray;
	g_extension_ray_intersection_datas[work_id].intersection_type = INTERSECTION_TYPE_BACKGROUND;
}

WR_KERNEL void AccumulatePixelValues(
	WR_GMEM_PARAM SPathState* g_path_states,
	WR_GMEM_PARAM SRenderWorkInfo* g_render_work_info,
	WR_GMEM_PARAM SRenderSettings* g_render_settings,
	WR_GMEM_PARAM F32x4* g_framebuffer
)
{
	U32 thread_id = GetGlobalIdX();
	SRenderWorkInfo work_info = *g_render_work_info;

	if (thread_id >= work_info.tile_width * work_info.tile_height)
	{
		return;
	}

	F32x4 pixel_color = make_float4(0.0f, 0.0f, 0.0f, 0.0f);

	U32 aa_sample_count = g_render_settings->aa_sample_count;
	U32 total_sample_count = Sqr(aa_sample_count);

	for (U32 sample_idx = 0; sample_idx < total_sample_count; sample_idx++)
	{
		U32 path_id = thread_id * total_sample_count + sample_idx;
		SPathState path = g_path_states[path_id];

		pixel_color += clamp(Pow(path.radiance, 1.0f / 2.2f), make_float4(0.0f, 0.0f, 0.0f, 0.0f), make_float4(1.0f, 1.0f, 1.0f, 1.0f));
		// normal_color += make_float4((sg.normal + MakeF32x3(1.0f, 1.0f, 1.0f)) / 2.0f,  1.0f);
		// texcoord_color += make_float4(sg.texcoord.x, idata.texcoord.y, 1.0f, 1.0f);
	}

	pixel_color /= total_sample_count;
	pixel_color = Pow(pixel_color, 1.0f / 2.2f);

	g_framebuffer[thread_id] = clamp(pixel_color, make_float4(0.0f, 0.0f, 0.0f, 0.0f), make_float4(1.0f, 1.0f, 1.0f, 1.0f));
}

WR_PROGRAM_END

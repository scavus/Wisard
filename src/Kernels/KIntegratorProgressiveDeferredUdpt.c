/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KIntegratorProgressiveDeferredUdpt.h"

WR_PROGRAM_BEGIN

WR_KERNEL void Initialize(INTEGRATOR_CTX_PARAMS, SCENE_CTX_PARAMS)
{
	U32 thread_id = GetGlobalIdX();
	SRenderWorkInfo work_info = *g_render_work_info;

	if (thread_id >= work_info.sample_count)
	{
		return;
	}

	U32x2 pixel_coord = make_uint2((thread_id + work_info.sample_offset) % g_render_settings->common.render_width,
		(thread_id + work_info.sample_offset) / g_render_settings->common.render_width);

	SRandomNumberGenerator rng;
	SeedRng(&rng, thread_id);

	g_rngs[thread_id] = rng;
	g_path_states[thread_id].pixel_coord = pixel_coord;
	g_path_states[thread_id].path_count = 0;
	g_path_gen_stream[thread_id] = thread_id;
	g_accumulation_buffer[thread_id] = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
	g_extension_ray_stream[thread_id] = thread_id;

	if (thread_id == 0)
	{
		g_path_gen_stream_info->counter = work_info.sample_count;
		g_extension_ray_stream_info->counter = work_info.sample_count;
		g_shadow_ray_stream_info->counter = 0;

		U32 mtl_count = g_scene_metadata->material_count;

		for (U32 mtl_idx = 0; mtl_idx < mtl_count; mtl_idx++)
		{
			g_shading_substream_infos[mtl_idx].counter = 0;
			g_shading_substream_infos[mtl_idx].offset = mtl_idx * work_info.sample_count;
		}
	}
}

WR_KERNEL void GeneratePaths(INTEGRATOR_CTX_PARAMS, SCENE_CTX_PARAMS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_path_gen_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_path_gen_stream[thread_id];

	SRandomNumberGenerator rng = g_rngs[work_id];
	U32x2 pixel_coord = g_path_states[work_id].pixel_coord;

	// generate new path from camera

	SMatrix4x4 camera_to_world_mtx = g_camera->camera_to_world_mtx;

	F32x2 uv;
	uv.x = GenerateF32(&rng);
	uv.y = GenerateF32(&rng);

	F32x2 camera_sample;
	camera_sample = convert_float2(pixel_coord) + uv;

	F32 fov;

	if (g_camera->type == PINHOLE)
	{
		fov = tan(g_camera->properties.pinhole.fov * 0.5f * PI / 180.0f);
	}
	else if (g_camera->type == THINLENS)
	{
		fov = tan(g_camera->properties.thinlens.fov * 0.5f * PI / 180.0f);
	}

	F32 aspect_width = (F32)(g_render_settings->common.render_width);
	F32 aspect_height = (F32)(g_render_settings->common.render_height);

	camera_sample.x = (2.0f * (camera_sample.x) / aspect_width - 1.0f) * fov * (aspect_width / aspect_height);
	camera_sample.y = (1.0f - 2.0f * (camera_sample.y) / aspect_height) * fov;

	SRay ray;

	if (g_camera->type == PINHOLE)
	{
		ray.origin = Transform(&camera_to_world_mtx, make_float4(0.0f, 0.0f, 0.0f, 1.0f));
		ray.direction = normalize(Transform(&camera_to_world_mtx, make_float4(camera_sample.x, camera_sample.y, -1.0f, 0.0f)));
		ray.t_max = FLT_MAX;
	}
	else if (g_camera->type == THINLENS)
	{
		F32x2 lens_uv = SampleDiskConcentric(uv.x, uv.y);
		lens_uv *= g_camera->properties.thinlens.aperture_size;
		ray.origin = Transform(&camera_to_world_mtx, make_float4(0.0f, 0.0f, 0.0f, 1.0f));
		ray.direction = normalize(Transform(&camera_to_world_mtx, make_float4(camera_sample.x, camera_sample.y, -1.0f, 0.0f)));
		F32x3 focal_point = ray.origin + (ray.direction * g_camera->properties.thinlens.focal_length);

		ray.origin = Transform(&camera_to_world_mtx, make_float4(MakeF32x3(lens_uv.x, lens_uv.y, 0.0f), 1.0f));
		ray.direction = normalize(focal_point - ray.origin);
		ray.t_max = FLT_MAX;
	}

	g_path_states[work_id].path_count++;
	g_extension_rays[work_id] = ray;
	g_extension_ray_intersection_datas[work_id].intersection_type = INTERSECTION_TYPE_BACKGROUND;
	g_extension_sample_infos[work_id].direction = ray.direction;
	g_extension_sample_infos[work_id].throughput = make_float4(1.0f, 1.0f, 1.0f, 0.0f);
	g_extension_sample_infos[work_id].pdf = 1.0f;
	g_rngs[work_id] = rng;
	g_path_states[work_id].path.throughput = make_float4(1.0f, 1.0f, 1.0f, 0.0f);
	g_path_states[work_id].path.radiance = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
	g_path_states[work_id].path.bounce_count = 0;
}

WR_KERNEL void Controller(INTEGRATOR_CTX_PARAMS, SCENE_CTX_PARAMS)
{
	U32 thread_id = GetGlobalIdX();
	SRenderWorkInfo work_info = *g_render_work_info;

	if (thread_id >= work_info.sample_count)
	{
		return;
	}

	SPathState path_state = g_path_states[thread_id];
	SRandomNumberGenerator rng = g_rngs[thread_id];

	SIntersectionData extension_ray_intersection_data = g_extension_ray_intersection_datas[thread_id];
	SIntersectionData bsdf_ray_intersection_data = g_bsdf_ray_intersection_datas[thread_id];
	SIntersectionData light_ray_intersection_data = g_light_ray_intersection_datas[thread_id];

	SSampleInfo extension_sample_info = g_extension_sample_infos[thread_id];
	SSampleInfo bsdf_sample_info = g_bsdf_sample_infos[thread_id];
	SSampleInfo light_sample_info = g_light_sample_infos[thread_id];

	if (bsdf_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
	{
		if (g_env_light)
		{
			F32x4 throughput = path_state.path.throughput * bsdf_sample_info.throughput;

			SEnvLight light = *g_env_light;
			// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(bsdf_sample_info.direction, 0.0f)));
			F32x3 w = bsdf_sample_info.direction;
			F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
			F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
			F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);

			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, bsdf_sample_info.pdf, 1, light_sample_info.pdf);
			path_state.path.radiance += throughput * mis_weight * l;

			g_bsdf_ray_intersection_datas[thread_id].intersection_type = INTERSECTION_TYPE_NULL;
		}
	}
	else if (bsdf_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
	{
		F32x4 throughput = path_state.path.throughput * bsdf_sample_info.throughput;
		SAreaLight light = g_area_lights[bsdf_ray_intersection_data.occluder_id];
		F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);
		F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, bsdf_sample_info.pdf, 1, light_sample_info.pdf);
		path_state.path.radiance += throughput * mis_weight * l;

		g_bsdf_ray_intersection_datas[thread_id].intersection_type = INTERSECTION_TYPE_NULL;
	}

	if (light_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
	{
		if (g_env_light)
		{
			F32x4 throughput = path_state.path.throughput * light_sample_info.throughput;

			SEnvLight light = *g_env_light;
			// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(light_sample_info.direction, 0.0f)));
			F32x3 w = light_sample_info.direction;
			F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
			F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
			F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);

			F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, light_sample_info.pdf, 1, bsdf_sample_info.pdf);
			path_state.path.radiance += throughput * mis_weight * l;

			g_light_ray_intersection_datas[thread_id].intersection_type = INTERSECTION_TYPE_NULL;
		}
	}
	else if (light_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
	{
		F32x4 throughput = path_state.path.throughput * light_sample_info.throughput;
		SAreaLight light = g_area_lights[light_ray_intersection_data.occluder_id];
		F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);
		F32 mis_weight = ComputeMisWeightBalanceHeuristic(1, light_sample_info.pdf, 1, bsdf_sample_info.pdf);
		path_state.path.radiance += throughput * mis_weight * l;

		g_light_ray_intersection_datas[thread_id].intersection_type = INTERSECTION_TYPE_NULL;
	}

	if (extension_ray_intersection_data.intersection_type == INTERSECTION_TYPE_BACKGROUND)
	{
		if (g_env_light)
		{
			F32x4 throughput = path_state.path.throughput * extension_sample_info.throughput;

			SEnvLight light = *g_env_light;
			// F32x3 w = normalize(Transform(&light.world_to_obj_mtx, make_float4(extension_sample_info.direction, 0.0f)));
			F32x3 w = extension_sample_info.direction;
			F32 s = CartesianToSphericalPhiUnit(w) * INV_2PI;
			F32 t = CartesianToSphericalThetaUnit(w) * INV_PI;
			F32x4 l = ReadTexture2dF32x4(g_env_map, s, t);

			path_state.path.radiance += throughput * l;
		}

		g_path_states[thread_id] = path_state;

		U32 work_id = atomic_inc(&g_path_gen_stream_info->counter);
		g_path_gen_stream[work_id] = thread_id;

		return;
	}
	else if (extension_ray_intersection_data.intersection_type == INTERSECTION_TYPE_LIGHT_SURFACE)
	{
		// Get emission and update radiance
		F32x4 throughput = path_state.path.throughput * extension_sample_info.throughput;
		SAreaLight light = g_area_lights[extension_ray_intersection_data.occluder_id];
		F32x4 l = light.color * light.intensity * pow(2.0f, light.exposure);

		path_state.path.radiance += throughput * l;

		g_path_states[thread_id] = path_state;

		U32 work_id = atomic_inc(&g_path_gen_stream_info->counter);
		g_path_gen_stream[work_id] = thread_id;

		return;
	}

	path_state.path.throughput *= extension_sample_info.throughput;

	if (path_state.path.bounce_count > g_render_settings->max_indirect_bounce_count)
	{
		g_path_states[thread_id] = path_state;

		U32 work_id = atomic_inc(&g_path_gen_stream_info->counter);
		g_path_gen_stream[work_id] = thread_id;

		return;
	}

	F32 cont_prob = fmin(0.5f, Luminance(path_state.path.throughput));

	if (GenerateF32(&rng) > cont_prob)
	{
		g_path_states[thread_id] = path_state;

		U32 work_id = atomic_inc(&g_path_gen_stream_info->counter);
		g_path_gen_stream[work_id] = thread_id;

		return;
	}

	path_state.path.throughput /= cont_prob;

	path_state.path.bounce_count++;

	SShaderGlobals sg;
	sg.pos = extension_ray_intersection_data.pos;
	sg.normal = extension_ray_intersection_data.normal;
	sg.texcoord = extension_ray_intersection_data.texcoord;
	sg.wo = -extension_sample_info.direction;
	sg.tangent_to_world_mtx = BuildMatrix4x4(
		make_float4(extension_ray_intersection_data.tangent, 0.0f),
		make_float4(extension_ray_intersection_data.binormal, 0.0f),
		make_float4(extension_ray_intersection_data.normal, 0.0f),
		make_float4(0.0f, 0.0f, 0.0f, 1.0f));
	sg.tangent_to_world_mtx = TransposeMatrix4x4(&sg.tangent_to_world_mtx);
	sg.mesh_id = extension_ray_intersection_data.occluder_id;
	sg.material_id = g_meshes[sg.mesh_id].material_id;

	SSceneMetadata scene_metadata = *g_scene_metadata;
	SampleLight(&rng, &sg,
		&scene_metadata,
		g_area_lights, g_env_light, g_env_map, g_env_light_conditional_cdfs, g_env_light_marginal_cdf,
		&light_sample_info);

	g_shader_globals[thread_id] = sg;
	g_direct_illum_rand_samples[thread_id] = make_float2(GenerateF32(&rng), GenerateF32(&rng));
	g_indirect_illum_rand_samples[thread_id] = make_float2(GenerateF32(&rng), GenerateF32(&rng));
	g_rngs[thread_id] = rng;
	g_light_sample_infos[thread_id] = light_sample_info;
	g_path_states[thread_id] = path_state;

	U32 shadow_raycast_work_id = atomic_inc(&g_shadow_ray_stream_info->counter);
	g_shadow_ray_stream[shadow_raycast_work_id] = thread_id;

	U32 shading_work_id = atomic_inc(&g_shading_substream_infos[sg.material_id].counter);
	shading_work_id += sg.material_id * work_info.sample_count;

	g_shading_stream[shading_work_id] = thread_id;
}

WR_KERNEL void PrepareShadowRays(INTEGRATOR_CTX_PARAMS, SCENE_CTX_PARAMS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_shadow_ray_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_shadow_ray_stream[thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	SSampleInfo bsdf_sample_info = g_bsdf_sample_infos[work_id];
	SSampleInfo light_sample_info = g_light_sample_infos[work_id];

	SRay bsdf_ray;
	bsdf_ray.origin = sg.pos;
	bsdf_ray.direction = bsdf_sample_info.direction;
	bsdf_ray.t_max = FLT_MAX;

	SRay light_ray;
	light_ray.origin = sg.pos;
	light_ray.direction = light_sample_info.direction;
	light_ray.t_max = FLT_MAX;

	g_bsdf_rays[work_id] = bsdf_ray;
	g_light_rays[work_id] = light_ray;
}

WR_KERNEL void PrepareExtensionRays(INTEGRATOR_CTX_PARAMS, SCENE_CTX_PARAMS)
{
	U32 thread_id = GetGlobalIdX();

	if (thread_id >= g_extension_ray_stream_info->counter)
	{
		return;
	}

	U32 work_id = g_extension_ray_stream[thread_id];

	SShaderGlobals sg = g_shader_globals[work_id];

	SSampleInfo extension_sample_info = g_extension_sample_infos[work_id];

	SRay extension_ray;
	extension_ray.origin = sg.pos;
	extension_ray.direction = extension_sample_info.direction;
	extension_ray.t_max = FLT_MAX;

	g_extension_rays[work_id] = extension_ray;
}

WR_KERNEL void AccumulatePixelValues(INTEGRATOR_CTX_PARAMS, SCENE_CTX_PARAMS)
{
	U32 thread_id = GetGlobalIdX();
	SRenderWorkInfo work_info = *g_render_work_info;

	if (thread_id >= work_info.sample_count)
	{
		return;
	}

	SPathState path_state = g_path_states[thread_id];
	F32x4 pixel_color = g_accumulation_buffer[thread_id];

	pixel_color += clamp(Pow(path_state.path.radiance, 1.0f / 2.2f), make_float4(0.0f, 0.0f, 0.0f, 0.0f), make_float4(1.0f, 1.0f, 1.0f, 1.0f));
	g_accumulation_buffer[thread_id] = pixel_color;

	pixel_color /= path_state.path_count;
	pixel_color = Pow(pixel_color, 1.0f / 2.2f);

	g_framebuffer[thread_id] = clamp(pixel_color, make_float4(0.0f, 0.0f, 0.0f, 0.0f), make_float4(1.0f, 1.0f, 1.0f, 1.0f));
}

WR_PROGRAM_END

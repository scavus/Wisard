/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KMath.h"
#include "KGeometry.h"
#include "KRandom.h"
#include "KSampling.h"
#include "KCamera.h"
#include "KScene.h"
#include "KSceneMetadata.h"
#include "KMesh.h"
#include "KLight.h"
#include "KMaterial.h"
#include "KShading.h"
#include "KDeferredIntegratorCommon.h"
#include "KRenderSettingsCommon.h"

WR_PROGRAM_BEGIN

typedef struct SRenderSettings
{
	SRenderSettingsCommon common;
	U32 max_indirect_bounce_count;
	U32 iteration_count;
	bool enable_mis;
	bool enable_rr;

} SRenderSettings;

typedef struct SPath
{
	F32x4 throughput;
	F32x4 radiance;
	U8 bounce_count;

} SPath;

typedef struct SPathState
{
	U32 path_count;
	SPath path;
	U32x2 pixel_coord;

} SPathState;

typedef struct SRenderWorkInfo
{
	U32 sample_offset;
	U32 sample_count;
} SRenderWorkInfo;

#ifndef WR_BACKEND_DEBUG

#define INTEGRATOR_CTX_PARAMS \
		WR_GMEM_PARAM SRenderWorkInfo*	g_render_work_info, \
		WR_GMEM_PARAM SRenderSettings*	g_render_settings, \
		WR_GMEM_PARAM SPathState*		g_path_states, \
		WR_GMEM_PARAM SSceneMetadata*		g_scene_metadata, \
		WR_GMEM_PARAM U32*				g_path_gen_stream, \
		WR_GMEM_PARAM SStreamInfo*		g_path_gen_stream_info, \
		WR_GMEM_PARAM U32*                g_shadow_ray_stream, \
		WR_GMEM_PARAM SStreamInfo*		g_shadow_ray_stream_info, \
		WR_GMEM_PARAM U32*                g_extension_ray_stream, \
		WR_GMEM_PARAM SStreamInfo*		g_extension_ray_stream_info, \
		WR_GMEM_PARAM U32*                g_shading_stream, \
		WR_GMEM_PARAM SSubstreamInfo*		g_shading_substream_infos, \
		WR_GMEM_PARAM SShaderGlobals*		g_shader_globals, \
		WR_GMEM_PARAM SRandomNumberGenerator* g_rngs, \
		WR_GMEM_PARAM F32x2* 				g_direct_illum_rand_samples, \
		WR_GMEM_PARAM F32x2* 			  g_indirect_illum_rand_samples, \
		WR_GMEM_PARAM SSampleInfo*			g_bsdf_sample_infos, \
		WR_GMEM_PARAM SSampleInfo* 			g_light_sample_infos, \
		WR_GMEM_PARAM SSampleInfo* 			g_extension_sample_infos, \
		WR_GMEM_PARAM SRay* 				g_extension_rays, \
		WR_GMEM_PARAM SRay* 				g_bsdf_rays, \
		WR_GMEM_PARAM SRay* 				g_light_rays, \
		WR_GMEM_PARAM SIntersectionData*  g_extension_ray_intersection_datas, \
		WR_GMEM_PARAM SIntersectionData*  g_bsdf_ray_intersection_datas, \
		WR_GMEM_PARAM SIntersectionData*  g_light_ray_intersection_datas, \
		WR_GMEM_PARAM F32x4*				g_framebuffer, \
		WR_GMEM_PARAM F32x4*				g_accumulation_buffer

#define SCENE_CTX_PARAMS \
		WR_GMEM_PARAM SCamera*						g_camera, \
		WR_GMEM_PARAM SMesh*							g_meshes, \
		WR_GMEM_PARAM SAreaLight*						g_area_lights, \
		WR_GMEM_PARAM SEnvLight*						g_env_light, \
		WR_READONLY_TEXTURE2D										g_env_map, \
		WR_GMEM_PARAM F32*								g_env_light_conditional_cdfs, \
		WR_GMEM_PARAM F32* 								g_env_light_marginal_cdf

#endif

#ifdef WR_BACKEND_DEBUG
	#define WR_KERNEL_BEGIN(KERNEL_NAME) void KERNEL_NAME(void** CtxParams, const SWorkDimension& GridDim, const SWorkDimension& BlockDim) { \
			__pragma(omp parallel for) \
			for (S32 It = 0; It < GridDim.X * BlockDim.X; It++) {
	#define WR_KERNEL_END }}

	#define WR_KERNEL_DECLARE(KERNEL_NAME) void KERNEL_NAME(void** CtxParams, const SWorkDimension& GridDim, const SWorkDimension& BlockDim)

	#define	g_render_work_info					static_cast<SRenderWorkInfo*>			(CtxParams[0])
	#define	g_render_settings					static_cast<SRenderSettings*>			(CtxParams[1])
	#define	g_path_states						static_cast<SPathState*>				(CtxParams[2])
	#define	g_scene_metadata					static_cast<SSceneMetadata*>			(CtxParams[3])
	#define	g_direct_illum_datas					static_cast<SDirectIllumData*>			(CtxParams[4])
	#define	g_path_gen_stream				static_cast<U32*>						(CtxParams[5])
	#define	g_path_gen_stream_info		static_cast<SStreamInfo*>				(CtxParams[6])
	#define g_shadow_ray_stream			static_cast<U32*>						(CtxParams[7])
	#define	g_shadow_ray_stream_info	static_cast<SStreamInfo*>				(CtxParams[8])
	#define g_shading_stream					static_cast<U32*>						(CtxParams[9])
	#define	g_shading_substream_infos			static_cast<SSubstreamInfo*>			(CtxParams[10])
	#define g_extension_ray_intersection_datas	static_cast<SIntersectionData*>			(CtxParams[11])
	#define g_bsdf_ray_intersection_datas			static_cast<SIntersectionData*>			(CtxParams[12])
	#define	g_light_ray_intersection_datas		static_cast<SIntersectionData*>			(CtxParams[13])
	#define	g_framebuffer						static_cast<F32x4*>						(CtxParams[14])
	#define	g_accumulation_buffer				static_cast<F32x4*>						(CtxParams[15])

	#define	g_camera							static_cast<SCamera*>					(CtxParams[16])
	#define	g_meshes							static_cast<SMesh*>						(CtxParams[17])
	#define	g_triangles						static_cast<STriangle*>					(CtxParams[18])
	#define	g_bvh_nodes							static_cast<SBvhNode*>					(CtxParams[19])
	#define	g_area_lights						static_cast<SAreaLight*>				(CtxParams[20])
	#define	g_light_quads						static_cast<SQuad*>						(CtxParams[21])
	#define	g_light_disks						static_cast<SDisk*>						(CtxParams[22])
	#define	g_light_spheres						static_cast<SSphere*>					(CtxParams[23])

	WR_KERNEL_DECLARE(Initialize);
	WR_KERNEL_DECLARE(GeneratePaths);
	WR_KERNEL_DECLARE(CastShadowRays);
	WR_KERNEL_DECLARE(CastExtensionRays);
	WR_KERNEL_DECLARE(ControlIntegration);
	WR_KERNEL_DECLARE(AccumulatePixelValues);

#endif

WR_PROGRAM_END

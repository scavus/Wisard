/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "KCommonTypes.h"
#include "KMath.h"
#include "KGeometry.h"

WR_PROGRAM_BEGIN

typedef struct SRayOptixPrime
{
	F32x3 origin;
	F32 t_min;
	F32x3 dir;
	F32 t_max;

} SRayOptixPrime;

typedef struct SHitOptixPrime
{
	F32 t;
	int triangle_id;
	F32 u;
	F32 v;

} SHitOptixPrime;

typedef struct SVertexOptixPrime
{
    F32x3 pos;
} SVertexOptixPrime;

WR_KERNEL void ConvertRays(
    U32 g_active_ray_count,
    WR_GMEM_PARAM SRay* g_rays,
    WR_GMEM_PARAM U32* g_active_ray_stream,
    WR_GMEM_PARAM SRayOptixPrime* g_api_rays)
{
    // Get active rays and convert them

    U32 thread_id = GetGlobalIdX();

    if (thread_id >= g_active_ray_count)
    {
        return;
    }

    U32 ray_id = g_active_ray_stream[thread_id];

    SRay ray = g_rays[ray_id];

    SRayOptixPrime api_ray;
    api_ray.origin = ray.origin;
    api_ray.dir = ray.direction;
    api_ray.t_min = 0.0f;
    api_ray.t_max = ray.t_max;

    g_api_rays[thread_id] = api_ray;
}

WR_KERNEL void ConvertGeometry(
    U32 g_triangle_count,
    WR_GMEM_PARAM STriangle* g_triangles,
    WR_GMEM_PARAM SVertexOptixPrime* g_api_vertices)
{
    // Extract vertices from triangles

    U32 thread_id = GetGlobalIdX();

    if (thread_id >= g_triangle_count)
    {
        return;
    }

    SVertex vtx[3];
    vtx[0] = g_triangles[thread_id].vtx[0];
    vtx[1] = g_triangles[thread_id].vtx[1];
    vtx[2] = g_triangles[thread_id].vtx[2];

    SVertexOptixPrime api_vtx[3];
    api_vtx[0].pos = vtx[0].pos;
    api_vtx[1].pos = vtx[1].pos;
    api_vtx[2].pos = vtx[2].pos;

    U32 vtx_offset = thread_id * 3;
    g_api_vertices[vtx_offset + 0] = api_vtx[0];
    g_api_vertices[vtx_offset + 1] = api_vtx[1];
    g_api_vertices[vtx_offset + 2] = api_vtx[2];
}

WR_KERNEL void ConvertQueryResult(
    U32 g_active_ray_count,
    WR_GMEM_PARAM SRayOptixPrime* g_api_rays,
    WR_GMEM_PARAM SHitOptixPrime* g_api_hits,
    WR_GMEM_PARAM STriangle* g_triangles,
    WR_GMEM_PARAM SRay* g_rays,
    WR_GMEM_PARAM U32* g_active_ray_stream,
    WR_GMEM_PARAM SIntersectionData* g_intersection_datas)
{
    // Convert the optix prime query result to wisard query result

    U32 thread_id = GetGlobalIdX();

    if (thread_id >= g_active_ray_count)
    {
        return;
    }

    U32 ray_id = g_active_ray_stream[thread_id];

	SRay ray = g_rays[ray_id];
    SRayOptixPrime api_ray = g_api_rays[thread_id];
    SHitOptixPrime api_hit = g_api_hits[thread_id];

    SIntersectionData intersection_data;

    if (api_hit.t < 0.0f)
    {
        // TODO:
        intersection_data.intersection_type = INTERSECTION_TYPE_BACKGROUND;
    }
    else
    {
        STriangle tri = g_triangles[api_hit.triangle_id];

        F32 u = api_hit.u;
        F32 v = api_hit.v;
        F32 w = 1.0f - u - v;

		F32x3 isect_point = ray.origin + (ray.direction * (api_hit.t - 1e-3f));
		g_rays[ray_id].t_max = api_hit.t - 1e-3f;

        intersection_data.intersection_type = INTERSECTION_TYPE_MESH_SURFACE;
        intersection_data.pos = isect_point;
        intersection_data.normal = w * tri.vtx[0].normal + u * tri.vtx[1].normal + v * tri.vtx[2].normal;
        intersection_data.tangent = w * tri.vtx[0].tangent + u * tri.vtx[1].tangent + v * tri.vtx[2].tangent;
        intersection_data.binormal = w * tri.vtx[0].binormal + u * tri.vtx[1].binormal + v * tri.vtx[2].binormal;
        intersection_data.texcoord = w * tri.vtx[0].texcoord + u * tri.vtx[1].texcoord + v * tri.vtx[2].texcoord;
        intersection_data.occluder_id = tri.mesh_id;
    }

    g_intersection_datas[ray_id] = intersection_data;
}

WR_PROGRAM_END

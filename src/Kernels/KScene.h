/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KSceneMetadata.h"
#include "KMesh.h"
#include "KGeometry.h"
#include "KMath.h"
#include "KLight.h"
#include "KAcceleratorBvh2.h"

WR_PROGRAM_BEGIN

WR_DEVICE_FN void TraverseBvh(SRay* ray_ptr, SSceneMetadata* scene_metadata_ptr,
	WR_GMEM_PARAM SMesh* meshes,
	WR_GMEM_PARAM const STriangle* WR_RESTRICT triangles,
	WR_GMEM_PARAM const SBvhNode* WR_RESTRICT bvh_nodes,
	SIntersectionData* intersection_data_ptr,
	F32* min_hit_distance_ptr)
{
	U32 TraversalStack[32];
	const S32 StackHeadIdxNull = -1;
	S32 StackHeadIdx = -1;

	SIntersectionData intersection_data;
	intersection_data.intersection_type = INTERSECTION_TYPE_BACKGROUND;
	intersection_data_ptr->intersection_type = INTERSECTION_TYPE_BACKGROUND;

	F32x3 InvDir = MakeF32x3(1.0f / ray_ptr->direction.x, 1.0f / ray_ptr->direction.y, 1.0f / ray_ptr->direction.z);
	// bool DirIsNeg[3] = {InvDir.x < 0, InvDir.y < 0, InvDir.z < 0};
	// FIXME: MSVC gives "Constant too big" error
	F32 min_hit_distance = FLT_MAX;

	bool bTerminated = false;
	U32 CurrentNodeID = 0;
	U32 PostponedNodeId = 0;
	SBvhNode CurrentNode = bvh_nodes[CurrentNodeID];

	while (!bTerminated)
	{
		while (CurrentNode.prim_count == 0)
		{
			SAabb aabb0;
			SAabb aabb1;

#ifndef WR_BACKEND_CUDA
			aabb0 = bvh_nodes[CurrentNodeID + 1].aabb;
			aabb1 = bvh_nodes[CurrentNode.offset].aabb;
#endif

#ifdef WR_BACKEND_CUDA
			aabb0.bound_max.x = __ldg(&bvh_nodes[CurrentNodeID + 1].aabb.bound_max.x);
			aabb0.bound_max.y = __ldg(&bvh_nodes[CurrentNodeID + 1].aabb.bound_max.y);
			aabb0.bound_max.z = __ldg(&bvh_nodes[CurrentNodeID + 1].aabb.bound_max.z);

			aabb0.bound_min.x = __ldg(&bvh_nodes[CurrentNodeID + 1].aabb.bound_min.x);
			aabb0.bound_min.y = __ldg(&bvh_nodes[CurrentNodeID + 1].aabb.bound_min.y);
			aabb0.bound_min.z = __ldg(&bvh_nodes[CurrentNodeID + 1].aabb.bound_min.z);

			aabb1.bound_max.x = __ldg(&bvh_nodes[CurrentNode.offset].aabb.bound_max.x);
			aabb1.bound_max.y = __ldg(&bvh_nodes[CurrentNode.offset].aabb.bound_max.y);
			aabb1.bound_max.z = __ldg(&bvh_nodes[CurrentNode.offset].aabb.bound_max.z);

			aabb1.bound_min.x = __ldg(&bvh_nodes[CurrentNode.offset].aabb.bound_min.x);
			aabb1.bound_min.y = __ldg(&bvh_nodes[CurrentNode.offset].aabb.bound_min.y);
			aabb1.bound_min.z = __ldg(&bvh_nodes[CurrentNode.offset].aabb.bound_min.z);
#endif

			bool bIntersectsChildNode0 = IntersectAabbFast2(ray_ptr, &aabb0, InvDir);
			bool bIntersectsChildNode1 = IntersectAabbFast2(ray_ptr, &aabb1, InvDir);

			if (!bIntersectsChildNode0 && !bIntersectsChildNode1)
			{
				if (StackHeadIdx != StackHeadIdxNull)
				{
					CurrentNodeID = TraversalStack[StackHeadIdx];

#ifndef WR_BACKEND_CUDA
					CurrentNode = bvh_nodes[CurrentNodeID];
#endif

#ifdef WR_BACKEND_CUDA
					CurrentNode.aabb.bound_max.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.x);
					CurrentNode.aabb.bound_max.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.y);
					CurrentNode.aabb.bound_max.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.z);

					CurrentNode.aabb.bound_min.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.x);
					CurrentNode.aabb.bound_min.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.y);
					CurrentNode.aabb.bound_min.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.z);

					CurrentNode.offset = __ldg(&bvh_nodes[CurrentNodeID].offset);
					CurrentNode.prim_count = __ldg(&bvh_nodes[CurrentNodeID].prim_count);
#endif

					StackHeadIdx--;
				}
				else
				{
					bTerminated = true;
					break;
				}
			}
			else if (bIntersectsChildNode0 && bIntersectsChildNode1)
			{
				// Push the farthest node
				/*
				if (DirIsNeg[CurrentNode.axis])
				{
					StackHeadIdx++;
					TraversalStack[StackHeadIdx] = CurrentNodeID + 1;

					CurrentNodeID = CurrentNode.offset;
					CurrentNode = bvh_nodes[CurrentNodeID];
				}
				else
				{
					StackHeadIdx++;
					TraversalStack[StackHeadIdx] = CurrentNode.offset;

					CurrentNodeID = CurrentNodeID + 1;
					CurrentNode = bvh_nodes[CurrentNodeID];
				}
				*/

				StackHeadIdx++;
				TraversalStack[StackHeadIdx] = CurrentNode.offset;

				CurrentNodeID = CurrentNodeID + 1;

#ifndef WR_BACKEND_CUDA
				CurrentNode = bvh_nodes[CurrentNodeID];
#endif

#ifdef WR_BACKEND_CUDA
				CurrentNode.aabb.bound_max.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.x);
				CurrentNode.aabb.bound_max.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.y);
				CurrentNode.aabb.bound_max.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.z);

				CurrentNode.aabb.bound_min.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.x);
				CurrentNode.aabb.bound_min.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.y);
				CurrentNode.aabb.bound_min.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.z);

				CurrentNode.offset = __ldg(&bvh_nodes[CurrentNodeID].offset);
				CurrentNode.prim_count = __ldg(&bvh_nodes[CurrentNodeID].prim_count);
#endif

			}
			else if (bIntersectsChildNode0 && !bIntersectsChildNode1)
			{
				CurrentNodeID = CurrentNodeID + 1;

#ifndef WR_BACKEND_CUDA
				CurrentNode = bvh_nodes[CurrentNodeID];
#endif

#ifdef WR_BACKEND_CUDA
				CurrentNode.aabb.bound_max.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.x);
				CurrentNode.aabb.bound_max.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.y);
				CurrentNode.aabb.bound_max.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.z);

				CurrentNode.aabb.bound_min.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.x);
				CurrentNode.aabb.bound_min.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.y);
				CurrentNode.aabb.bound_min.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.z);

				CurrentNode.offset = __ldg(&bvh_nodes[CurrentNodeID].offset);
				CurrentNode.prim_count = __ldg(&bvh_nodes[CurrentNodeID].prim_count);
#endif
			}
			else
			{
				CurrentNodeID = CurrentNode.offset;

#ifndef WR_BACKEND_CUDA

				CurrentNode = bvh_nodes[CurrentNodeID];
#endif

#ifdef WR_BACKEND_CUDA
				CurrentNode.aabb.bound_max.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.x);
				CurrentNode.aabb.bound_max.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.y);
				CurrentNode.aabb.bound_max.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.z);

				CurrentNode.aabb.bound_min.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.x);
				CurrentNode.aabb.bound_min.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.y);
				CurrentNode.aabb.bound_min.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.z);

				CurrentNode.offset = __ldg(&bvh_nodes[CurrentNodeID].offset);
				CurrentNode.prim_count = __ldg(&bvh_nodes[CurrentNodeID].prim_count);
#endif
			}
		}

		while (CurrentNode.prim_count > 0)
		{

			for (U32 prim_idx = 0; prim_idx < CurrentNode.prim_count; prim_idx++)
			{
				STriangle triangle = triangles[CurrentNode.offset + prim_idx];

				bool bIntersects;
				bIntersects = IntersectTriangle(ray_ptr, &triangle, &intersection_data);

				if (bIntersects)
				{
					F32 hit_distance = Distance(ray_ptr->origin, intersection_data.pos);

					if (hit_distance < min_hit_distance)
					{
						intersection_data_ptr->intersection_type = INTERSECTION_TYPE_MESH_SURFACE;
						intersection_data_ptr->pos = intersection_data.pos;
						intersection_data_ptr->normal = intersection_data.normal;
						intersection_data_ptr->binormal = intersection_data.binormal;
						intersection_data_ptr->tangent = intersection_data.tangent;
						intersection_data_ptr->texcoord = intersection_data.texcoord;
						intersection_data_ptr->occluder_id = triangle.mesh_id;

						min_hit_distance = hit_distance;
					}
				}
			}

			if (StackHeadIdx == StackHeadIdxNull)
			{
				bTerminated = true;
				break;
			}
			else
			{
				CurrentNodeID = TraversalStack[StackHeadIdx];

#ifndef WR_BACKEND_CUDA
				CurrentNode = bvh_nodes[CurrentNodeID];
#endif

#ifdef WR_BACKEND_CUDA
				CurrentNode.aabb.bound_max.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.x);
				CurrentNode.aabb.bound_max.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.y);
				CurrentNode.aabb.bound_max.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_max.z);

				CurrentNode.aabb.bound_min.x = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.x);
				CurrentNode.aabb.bound_min.y = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.y);
				CurrentNode.aabb.bound_min.z = __ldg(&bvh_nodes[CurrentNodeID].aabb.bound_min.z);

				CurrentNode.offset = __ldg(&bvh_nodes[CurrentNodeID].offset);
				CurrentNode.prim_count = __ldg(&bvh_nodes[CurrentNodeID].prim_count);
#endif
				StackHeadIdx--;
			}
		}
	}

	*min_hit_distance_ptr = min_hit_distance;
}


WR_DEVICE_FN void IntersectScene(SRay* ray_ptr, SSceneMetadata* scene_metadata_ptr,
	WR_GMEM_PARAM SMesh* meshes,
	WR_GMEM_PARAM const STriangle* WR_RESTRICT triangles,
	WR_GMEM_PARAM const SBvhNode* WR_RESTRICT bvh_nodes,
	WR_GMEM_PARAM SAreaLight* area_lights,
	WR_GMEM_PARAM SDisk* area_light_disks,
	WR_GMEM_PARAM SSphere* area_light_spheres,
	WR_GMEM_PARAM SQuad* area_light_quads,
	SIntersectionData* intersection_data_ptr)
{
	F32 min_hit_distance = FLT_MAX;

	SIntersectionData intersection_data;

	TraverseBvh(ray_ptr, scene_metadata_ptr, meshes, triangles, bvh_nodes, intersection_data_ptr, &min_hit_distance);

	for (U32 AreaLightIdx = 0; AreaLightIdx < scene_metadata_ptr->area_light_count; AreaLightIdx++)
	{
		SAreaLight area_light = area_lights[AreaLightIdx];
		bool bIntersects = false;
		switch (area_light.type)
		{
			case LIGHT_TYPE_QUAD:
			{
				// SQuad quad = area_light_quads[area_light.shape_id];
				SQuad quad = area_light.shape.quad;
				bIntersects = IntersectQuad(ray_ptr, &quad, &intersection_data);
				break;
			}
			case LIGHT_TYPE_DISK:
			{
				// SDisk disk = area_light_disks[area_light.shape_id];
				SDisk disk = area_light.shape.disk;
				bIntersects = IntersectDisk(ray_ptr, &disk, &intersection_data);
				break;
			}
			case LIGHT_TYPE_SPHERE:
			{
				// SSphere sphere = area_light_spheres[area_light.shape_id];
				SSphere sphere = area_light.shape.sphere;
				bIntersects = IntersectSphere(ray_ptr, &sphere, &intersection_data);
				break;
			}
			case LIGHT_TYPE_TUBE:
			{

				break;
			}
		}

		F32 hit_distance = Distance(ray_ptr->origin, intersection_data.pos);

		if (bIntersects && hit_distance < min_hit_distance)
		{
			intersection_data_ptr->intersection_type = INTERSECTION_TYPE_LIGHT_SURFACE;
			intersection_data_ptr->pos = intersection_data.pos;
			intersection_data_ptr->occluder_id = AreaLightIdx;

			min_hit_distance = hit_distance;
		}
	}
}

WR_PROGRAM_END

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"

WR_PROGRAM_BEGIN

#define PI 3.141592653589793238462643383f
#define INV_PI 1.0f / PI
#define INV_2PI 1.0f / (2.0f * PI)

#define Sqr(val) val * val

typedef struct SMatrix3x3
{
	F32x3 R0;
	F32x3 R1;
	F32x3 R2;

} SMatrix3x3;

typedef struct SMatrix4x4
{
	F32x4 R0;
	F32x4 R1;
	F32x4 R2;
	F32x4 R3;

} SMatrix4x4;

WR_DEVICE_FN F32 Saturate(F32 val)
{
	return clamp(val, 0.0f, 1.0f);
}

WR_DEVICE_FN F32x3 Reflect(F32x3 i, F32x3 n)
{
	// return i - 2.0f * dot(n, i) * n;
	return 2.0f * dot(n, i) * n - i;
}

WR_DEVICE_FN F32 Distance(F32x3 v0, F32x3 v1)
{
	F32x3 v;

	v.x = v0.x - v1.x;
	v.y = v0.y - v1.y;
	v.z = v0.z - v1.z;

	return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

WR_DEVICE_FN F32 DistanceSq(F32x3 v0, F32x3 v1)
{
	F32x3 v;

	v.x = v0.x - v1.x;
	v.y = v0.y - v1.y;
	v.z = v0.z - v1.z;

	return v.x * v.x + v.y * v.y + v.z * v.z;
}

WR_DEVICE_FN F32x4 Pow(F32x4 vec, F32 val)
{
	vec.x = pow(vec.x, val);
	vec.y = pow(vec.y, val);
	vec.z = pow(vec.z, val);
	vec.w = pow(vec.w, val);

	return vec;
}

WR_DEVICE_FN F32x3 SphericalToCartesian(F32 r, F32 theta, F32 phi)
{
	F32 sin_theta = sin(theta);
	F32 sin_phi = sin(phi);
	F32 cos_theta = cos(theta);
	F32 cos_phi = cos(phi);

	F32x3 v;
	v.x = r * sin_theta * cos_phi;
	v.y = r * sin_theta * sin_phi;
	v.z = r * cos_theta;
	return v;
}

WR_DEVICE_FN F32x3 SphericalToCartesianUnit(F32 theta, F32 phi)
{
	F32 sin_theta = sin(theta);
	F32 sin_phi = sin(phi);
	F32 cos_theta = cos(theta);
	F32 cos_phi = cos(phi);

	F32x3 v;
	v.x = sin_theta * cos_phi;
	v.y = sin_theta * sin_phi;
	v.z = cos_theta;
	return v;
}

WR_DEVICE_FN F32x2 CartesianToSphericalUnit(F32x3 v)
{
	F32x2 sc;
	sc.x = acos(v.z);
	sc.y = atan(v.y / v.x);

	return sc;
}

WR_DEVICE_FN F32 CartesianToSphericalThetaUnit(F32x3 v)
{
	return acos(clamp(v.z, -1.0f, 1.0f));
}

WR_DEVICE_FN F32 CartesianToSphericalPhiUnit(F32x3 v)
{
	F32 p = atan2(v.y, v.x);
	return (p < 0.0f) ? p + 2.0f * PI : p;
}

WR_DEVICE_FN SMatrix3x3 BuildMatrix3x3(F32x3 v0, F32x3 v1, F32x3 v2)
{
	SMatrix3x3 mtx;

	mtx.R0 = v0;
	mtx.R1 = v1;
	mtx.R2 = v2;

	return mtx;
}

WR_DEVICE_FN SMatrix4x4 BuildMatrix4x4(F32x4 v0, F32x4 v1, F32x4 v2, F32x4 v3)
{
	SMatrix4x4 mtx;

	mtx.R0 = v0;
	mtx.R1 = v1;
	mtx.R2 = v2;
	mtx.R3 = v3;

	return mtx;
}

WR_DEVICE_FN F32x4 MultiplyM4V4(SMatrix4x4* m0, F32x4 v0)
{
	F32x4 oVector;

	oVector.x = dot(m0->R0, v0);
	oVector.y = dot(m0->R1, v0);
	oVector.z = dot(m0->R2, v0);
	oVector.w = dot(m0->R3, v0);

	return oVector;
}

WR_DEVICE_FN F32x3 Transform(SMatrix4x4* m0, F32x4 v0)
{
	F32x4 v = MultiplyM4V4(m0, v0);
	return MakeF32x3(v.x, v.y, v.z);
}

WR_DEVICE_FN SMatrix4x4 TransposeMatrix4x4(SMatrix4x4* m0)
{
	SMatrix4x4 mtx;

	mtx.R0 = make_float4(m0->R0.x, m0->R1.x, m0->R2.x, m0->R3.x);
	mtx.R1 = make_float4(m0->R0.y, m0->R1.y, m0->R2.y, m0->R3.y);
	mtx.R2 = make_float4(m0->R0.z, m0->R1.z, m0->R2.z, m0->R3.z);
	mtx.R3 = make_float4(m0->R0.w, m0->R1.w, m0->R2.w, m0->R3.w);

	return mtx;
}

WR_PROGRAM_END

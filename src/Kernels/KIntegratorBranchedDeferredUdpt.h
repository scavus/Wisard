/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KMath.h"
#include "KGeometry.h"
#include "KRandom.h"
#include "KSampling.h"
#include "KCamera.h"
#include "KScene.h"
#include "KSceneMetadata.h"
#include "KMesh.h"
#include "KLight.h"
#include "KMaterial.h"
#include "KShading.h"
#include "KDeferredIntegratorCommon.h"
#include "KRenderSettingsCommon.h"

WR_PROGRAM_BEGIN

typedef struct SRenderSettings
{
	SRenderSettingsCommon common;
	U32 max_indirect_bounce_count;
	U32 aa_sample_count;
	U32 direct_illum_sample_count;
	U32 indirect_illum_sample_count;

	bool enable_mis;
	bool enable_rr;
} SRenderSettings;

typedef struct SPathState
{
	F32x4 throughput;
	F32x4 radiance;
	U8 bounce_count;
	U32x2 pixel_coord;
} SPathState;

typedef struct SRenderWorkInfo
{
	U32 tile_offset_x;
	U32 tile_offset_y;
	U32 tile_width;
	U32 tile_height;
} SRenderWorkInfo;

WR_PROGRAM_END

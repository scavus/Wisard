/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "KCommonTypes.h"
#include "KMath.h"

WR_PROGRAM_BEGIN

typedef enum ECameraType
{
	PINHOLE,
	THINLENS

} ECameraType;

typedef struct SPinholeCameraProperties
{
	F32 fov;

} SPinholeCameraProperties;

typedef struct SThinLensCameraProperties
{
	F32 aperture_size;
	F32 focal_length;
	F32 fov;

} SThinLensCameraProperties;

typedef struct SCamera
{
	ECameraType type;

	union UProperties
	{
		SPinholeCameraProperties pinhole;
		SThinLensCameraProperties thinlens;

	} properties;

	SMatrix4x4 camera_to_world_mtx;

} SCamera;

WR_PROGRAM_END

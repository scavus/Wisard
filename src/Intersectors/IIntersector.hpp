/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"
#include "Scene/SceneContext.hpp"

struct SRayIntersectionQueryData
{
	SBufferHdl ray_buffer;
	U32 ray_count;
	SBufferHdl ray_stream;
	SBufferHdl ray_stream_info;
	SBufferHdl result;
};

struct SRayOcclusionQueryData
{
	SBufferHdl ray_buffer;
	U32 ray_count;
	SBufferHdl ray_stream;
	SBufferHdl ray_stream_info;
};

class IIntersector
{
	public:
		virtual std::string GetAcceleratorName() = 0;
		virtual void Update(const SSceneContext* scene_ctx_ptr) = 0;
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data) = 0;
		virtual void QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data) = 0;

	protected:

	private:
};

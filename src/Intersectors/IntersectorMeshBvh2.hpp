/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Intersectors/IIntersector.hpp"
#include "Intersectors/IIntersectorFactory.hpp"

class CAcceleratorBvh2;

template<typename DeviceManagerBackend_T>
class TIntersectorMeshBvh2 : public IIntersector
{
	static const U32 INTERSECTION_KERNEL_PARAM_COUNT = 6;
	static const U32 OCCLUSION_KERNEL_PARAM_COUNT = 0;
	// static const std::string ACCELERATOR_NAME;

	public:
		TIntersectorMeshBvh2(){};

		TIntersectorMeshBvh2(DeviceManagerBackend_T* device_manager_ptr);

		virtual ~TIntersectorMeshBvh2()
		{
			_device_manager_ptr->DestroyBuffer(_mesh_geom_buffer_hdl);
			_device_manager_ptr->DestroyBuffer(_mesh_bvh2_node_buffer_hdl);

			// TODO:
			// _device_manager_ptr->DestroyKernel(_intersection_kernel_hdl);
			// _device_manager_ptr->DestroyKernel(_occlusion_kernel_hdl);
			// _device_manager_ptr->DestroyProgram(_intersector_program_hdl);
		};

		virtual std::string GetAcceleratorName() override { return "Bvh2"; };
		virtual void Update(const SSceneContext* scene_ctx_ptr) override;
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data) override;
		virtual void QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data) override;

		void SetAccelerator(const CAcceleratorBvh2* accelerator_ptr) { _accelerator_ptr = accelerator_ptr; };

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;

		const CAcceleratorBvh2* _accelerator_ptr;

		SBufferHdl _mesh_geom_buffer_hdl;
		SBufferHdl _mesh_bvh2_node_buffer_hdl;

		SProgramHdl _intersector_program_hdl;
		SKernelHdl _intersection_kernel_hdl;
		SKernelHdl _occlusion_kernel_hdl;
};

template<>
TIntersectorMeshBvh2<CDeviceManagerCuda>::TIntersectorMeshBvh2(CDeviceManagerCuda* device_manager_ptr);

template<>
TIntersectorMeshBvh2<CDeviceManagerOcl>::TIntersectorMeshBvh2(CDeviceManagerOcl* device_manager_ptr);

class CIntersectorMeshBvh2Factory : public IIntersectorFactory
{
	public:
		CIntersectorMeshBvh2Factory(){};
		~CIntersectorMeshBvh2Factory(){};

		virtual IIntersector* Create(CDeviceManagerCuda* device_manager_ptr) override
		{
			return new TIntersectorMeshBvh2 < CDeviceManagerCuda > { device_manager_ptr };
		};

		virtual IIntersector* Create(CDeviceManagerOcl* device_manager_ptr) override
		{
			return new TIntersectorMeshBvh2 < CDeviceManagerOcl > { device_manager_ptr };
		};

	protected:

	private:

};

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "firerays.h"
#include "firerays_cl.h"

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/DeviceManagerOcl.hpp"
#include "Intersectors/IIntersector.hpp"
#include "Intersectors/IIntersectorFactory.hpp"

class CIntersectorFireRays : public IIntersector
{
	friend class CAcceleratorFireRays;

	public:
		CIntersectorFireRays(){};
		~CIntersectorFireRays(){};

		CIntersectorFireRays(CDeviceManagerOcl* device_manager_ptr);

		virtual std::string GetAcceleratorName() override { return "FireRays"; };
		virtual void Update(const SSceneContext* scene_ctx_ptr) override;
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data) override;
		virtual void QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data) override;

		CDeviceManagerOcl* GetDeviceManagerPtr() { return _device_manager_ptr; };
		FireRays::IntersectionApiCL* GetApi() { return _api; };
		FireRays::Shape* GetShape() { return _api_shape; };
		void SetShape(FireRays::Shape* shape) { _api_shape = shape; };

		SKernelHdl GetConvertGeometryKernelHdl() { return _convert_geometry_kernel_hdl; };

	protected:

	private:
		CDeviceManagerOcl* _device_manager_ptr;
		FireRays::IntersectionApiCL* _api;
		FireRays::Shape* _api_shape;

		SBufferHdl _geom_buffer_hdl;

		SProgramHdl _intersector_program_hdl;
		SKernelHdl _convert_rays_kernel_hdl;
		SKernelHdl _convert_geometry_kernel_hdl;
		SKernelHdl _convert_query_result_kernel_hdl;
};

class CIntersectorFireRaysFactory : public IIntersectorFactory
{
	public:
		CIntersectorFireRaysFactory(){};
		~CIntersectorFireRaysFactory(){};

		virtual IIntersector* Create(CDeviceManagerCuda* device_manager_ptr) override
		{
			// Not implemented
			return nullptr;
		};

		virtual IIntersector* Create(CDeviceManagerOcl* device_manager_ptr) override
		{
			return new CIntersectorFireRays{ device_manager_ptr };
		}

	protected:

	private:

};

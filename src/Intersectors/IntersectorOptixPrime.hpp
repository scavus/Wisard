/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <optix_prime/optix_prime.h>

#include "Utility/CommonTypes.hpp"
#include "Utility/Math.hpp"
#include "RenderDevice/DeviceManagerCuda.hpp"
#include "Intersectors/IIntersector.hpp"
#include "Intersectors/IIntersectorFactory.hpp"

class CIntersectorOptixPrime : public IIntersector
{
	friend class CAcceleratorOptixPrime;

	static const U32 INTERSECTION_KERNEL_PARAM_COUNT = 6;
	static const U32 OCCLUSION_KERNEL_PARAM_COUNT = 0;

	public:
		CIntersectorOptixPrime()
		{
			rtpContextCreate(RTP_CONTEXT_TYPE_CUDA, &_api_ctx);
		};

		~CIntersectorOptixPrime(){};

		CIntersectorOptixPrime(CDeviceManagerCuda* device_manager_ptr);

		void SetDeviceManager(CDeviceManagerCuda* device_manager_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			U32 device_num = _device_manager_ptr->GetId();
			rtpContextSetCudaDeviceNumbers(_api_ctx, 1, &device_num);

			rtpModelCreate(_api_ctx, &_api_model);
			rtpQueryCreate(_api_model, RTP_QUERY_TYPE_CLOSEST, &_api_query);
		};

		void Init();

		virtual std::string GetAcceleratorName() override { return "OptixPrime"; };
		virtual void Update(const SSceneContext* scene_ctx_ptr) override {};
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data) override;
		virtual void QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data) override;

		RTPcontext GetContext() { return _api_ctx; };
		RTPmodel GetModel() { return _api_model; };
		SKernelHdl GetConvertGeometryKernelHdl(){ return _convert_geometry_kernel_hdl; };
		CDeviceManagerCuda* GetDeviceManagerPtr() { return _device_manager_ptr; };

	protected:

	private:
		struct SRayOptixPrime
		{
			F32x3 origin;
			F32 t_min;
			F32x3 dir;
			F32 t_max;
		};

		static const RTPbufferformat OPTIX_PRIME_RAY_FORMAT = RTP_BUFFER_FORMAT_RAY_ORIGIN_TMIN_DIRECTION_TMAX;

		struct SHitOptixPrime
		{
			F32 t;
			int triangle_id;
			F32 u;
			F32 v;
		};

		static const RTPbufferformat OPTIX_PRIME_HIT_FORMAT = RTP_BUFFER_FORMAT_HIT_T_TRIID_U_V;

		struct SVertexOptixPrime
		{
			F32x3 pos;
		};

		static const RTPbufferformat OPTIX_PRIME_VERTEX_FORMAT = RTP_BUFFER_FORMAT_VERTEX_FLOAT3;

		CDeviceManagerCuda* _device_manager_ptr;
		RTPcontext _api_ctx;
		RTPmodel _api_model;
		RTPquery _api_query;

		SBufferHdl _geom_buffer_hdl;

		SProgramHdl _intersector_program_hdl;
		SKernelHdl _convert_rays_kernel_hdl;
		SKernelHdl _convert_geometry_kernel_hdl;
		SKernelHdl _convert_query_result_kernel_hdl;
};

class CIntersectorOptixPrimeFactory : public IIntersectorFactory
{
	public:
		CIntersectorOptixPrimeFactory(){};
		~CIntersectorOptixPrimeFactory(){};

		virtual IIntersector* Create(CDeviceManagerCuda* device_manager_ptr) override
		{
			return new CIntersectorOptixPrime{ device_manager_ptr };
		};

		virtual IIntersector* Create(CDeviceManagerOcl* device_manager_ptr) override
		{
			// Not implemented
			return nullptr;
		};

	protected:

	private:

};

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "IntersectorMeshBvh2.hpp"
#include "Accelerators/AcceleratorBvh2.hpp"

template class TIntersectorMeshBvh2<CDeviceManagerCuda>;
template class TIntersectorMeshBvh2<CDeviceManagerOcl>;

template<>
TIntersectorMeshBvh2<CDeviceManagerCuda>::TIntersectorMeshBvh2(CDeviceManagerCuda* device_manager_ptr)
{
    _device_manager_ptr = device_manager_ptr;

    _intersector_program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KIntersectorTriangleBvh2Default.cubin");
    _intersection_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "IntersectionTriangleBvh2Default", INTERSECTION_KERNEL_PARAM_COUNT);
    // _occlusion_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "OcclusionTriangleBvh2Default", OCCLUSION_KERNEL_PARAM_COUNT);

    _mesh_geom_buffer_hdl = 0;
    _mesh_bvh2_node_buffer_hdl = 0;
};

template<>
TIntersectorMeshBvh2<CDeviceManagerOcl>::TIntersectorMeshBvh2(CDeviceManagerOcl* device_manager_ptr)
{
    _device_manager_ptr = device_manager_ptr;

    std::vector<const char*> filenames;
    filenames.push_back("data/Kernels/KIntersectorTriangleBvh2Default.c");

    const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;
    _intersector_program_hdl = _device_manager_ptr->CompileProgram(filenames, compiler_options);

    _intersection_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "IntersectionTriangleBvh2Default", INTERSECTION_KERNEL_PARAM_COUNT);
    // _occlusion_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "OcclusionTriangleBvh2Default", OCCLUSION_KERNEL_PARAM_COUNT);

    _mesh_geom_buffer_hdl = 0;
    _mesh_bvh2_node_buffer_hdl = 0;
};

template<typename DeviceManagerBackend_T>
void TIntersectorMeshBvh2<DeviceManagerBackend_T>::Update(const SSceneContext* scene_ctx_ptr)
{
	CAcceleratorBvh2* accel = static_cast<CAcceleratorBvh2*>(const_cast<SSceneContext*>(scene_ctx_ptr)->accelerators["Bvh2"]);

	const auto& nodes = accel->GetStorageConstRef().mesh_bvh2_nodes;
	const auto& geom = accel->GetStorageConstRef().temp_mesh_geom;

	size_t buffer_size = 0;

	if (_mesh_geom_buffer_hdl == 0)
	{
		buffer_size = sizeof(SLinearBvh2Node) * nodes.size();
		_mesh_bvh2_node_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &nodes[0]);

		buffer_size = sizeof(WrTriangle) * geom.size();
		_mesh_geom_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &geom[0]);
	}
	else
	{
		_device_manager_ptr->DestroyBuffer(_mesh_bvh2_node_buffer_hdl);
		_device_manager_ptr->DestroyBuffer(_mesh_geom_buffer_hdl);

		buffer_size = sizeof(SLinearBvh2Node) * nodes.size();
		_mesh_bvh2_node_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &nodes[0]);

		buffer_size = sizeof(WrTriangle) * geom.size();
		_mesh_geom_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &geom[0]);
	}
}

template<typename DeviceManagerBackend_T>
void TIntersectorMeshBvh2<DeviceManagerBackend_T>::QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data)
{
	size_t param_idx = 0;
	_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_buffer);
	_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_stream);
	_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_stream_info);
	_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, _mesh_geom_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, _mesh_bvh2_node_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.result);

	auto block_size = _device_manager_ptr->GetWarpSize();

	_device_manager_ptr->DispatchKernel(_intersection_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(query_data.ray_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });
}

template<typename DeviceManagerBackend_T>
void TIntersectorMeshBvh2<DeviceManagerBackend_T>::QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data)
{

}

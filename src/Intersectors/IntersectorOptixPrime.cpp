/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "cuda_runtime.h"

#include "IntersectorOptixPrime.hpp"

CIntersectorOptixPrime::CIntersectorOptixPrime(CDeviceManagerCuda* device_manager_ptr)
{
	_device_manager_ptr = device_manager_ptr;
	rtpContextCreate(RTP_CONTEXT_TYPE_CUDA, &_api_ctx);
	
	_intersector_program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KIntersectorOptixPrime.cubin");

	_convert_geometry_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertGeometry", 3);
	_convert_rays_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertRays", 4);
	_convert_query_result_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertQueryResult", 7);
}

void CIntersectorOptixPrime::QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data)
{
	auto geom_manager = scene_ctx_ptr->mesh_geom_manager_ptr;

	size_t buffer_size = 0;

	buffer_size = sizeof(SRayOptixPrime) * query_data.ray_count;
	SBufferHdl api_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	SRayOptixPrime* rays_rt;
	cudaMalloc(&rays_rt, buffer_size);
	auto rays = new SRayOptixPrime[query_data.ray_count];

	buffer_size = sizeof(SHitOptixPrime) * query_data.ray_count;
	SBufferHdl api_hit_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	SHitOptixPrime* hits_rt;
	cudaMalloc(&hits_rt, buffer_size);
	auto hits = new SHitOptixPrime[query_data.ray_count];

	U32 block_size = _device_manager_ptr->GetWarpSize();

	U32 arg_idx = 0;

	arg_idx = 0;
	_device_manager_ptr->SetKernelArgValue(_convert_rays_kernel_hdl, arg_idx++, sizeof(U32), &query_data.ray_count);
	_device_manager_ptr->SetKernelArgBuffer(_convert_rays_kernel_hdl, arg_idx++, query_data.ray_buffer);
	_device_manager_ptr->SetKernelArgBuffer(_convert_rays_kernel_hdl, arg_idx++, query_data.ray_stream);
	_device_manager_ptr->SetKernelArgBuffer(_convert_rays_kernel_hdl, arg_idx++, api_ray_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_convert_rays_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(query_data.ray_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->Synchronize();

	_device_manager_ptr->ReadBuffer(api_ray_buffer_hdl, rays);

	cudaMemcpy(rays_rt, rays, sizeof(SRayOptixPrime) * query_data.ray_count, cudaMemcpyKind::cudaMemcpyHostToDevice);

	RTPbufferdesc rays_desc;
	( rtpBufferDescCreate(_api_ctx, OPTIX_PRIME_RAY_FORMAT, RTP_BUFFER_TYPE_CUDA_LINEAR, rays_rt, &rays_desc) );
	( rtpBufferDescSetRange(rays_desc, 0, query_data.ray_count) );

	RTPbufferdesc hits_desc;
	( rtpBufferDescCreate(_api_ctx, OPTIX_PRIME_HIT_FORMAT, RTP_BUFFER_TYPE_CUDA_LINEAR, hits_rt, &hits_desc) );
	( rtpBufferDescSetRange(hits_desc, 0, query_data.ray_count) );
	
	( rtpQuerySetRays(_api_query, rays_desc) );
	( rtpQuerySetHits(_api_query, hits_desc) );
	( rtpQueryExecute(_api_query, 0) );

	cudaMemcpy(hits, hits_rt, sizeof(SHitOptixPrime) * query_data.ray_count, cudaMemcpyKind::cudaMemcpyDeviceToHost);

	_device_manager_ptr->WriteBuffer(api_hit_buffer_hdl, hits);

	arg_idx = 0;
	_device_manager_ptr->SetKernelArgValue(_convert_query_result_kernel_hdl, arg_idx++, sizeof(U32), &query_data.ray_count);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, api_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, api_hit_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, _geom_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, query_data.ray_buffer);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, query_data.ray_stream);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, query_data.result);
	_device_manager_ptr->DispatchKernel(_convert_query_result_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(query_data.ray_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });
	
	_device_manager_ptr->Synchronize();

	( rtpBufferDescDestroy(rays_desc) );
	( rtpBufferDescDestroy(hits_desc) );
	
	delete[] rays;
	delete[] hits;

	cudaFree(rays_rt);
	cudaFree(hits_rt);

	_device_manager_ptr->DestroyBuffer(api_ray_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(api_hit_buffer_hdl);
}

void CIntersectorOptixPrime::QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data)
{

}

void CIntersectorOptixPrime::Init()
{
	_intersector_program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KIntersectorOptixPrime.cubin");

	_convert_geometry_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertGeometry", 3);
	_convert_rays_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertRays", 4);
	_convert_query_result_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertQueryResult", 7);
}

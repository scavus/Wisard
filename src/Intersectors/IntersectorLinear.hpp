/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Intersectors/IIntersector.hpp"

template<class DeviceManagerBackend_T, typename Traits_T>
class TLinearIntersector : public IIntersector
{
	using GeomManagerT = TIntersectorTraits<TLinearIntersector>::GeomManagerT;
	using AcceleratorT = TIntersectorTraits<TLinearIntersector>::AcceleratorT;

	public:
		TLinearIntersector();

		template<>
		TLinearIntersector<CDeviceManagerCuda>::TLinearIntersector(DeviceManagerBackend_T* device_manager_ptr)
		{
			_device_manager_ptr = device_manager_ptr;

			_intersector_program_hdl = _device_manager_ptr->CreateProgramFromCubin(Traits_T::IntersectorProgramName);
			_intersection_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, Traits_T::IntersectionKernelName, 0);
			_occlusion_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, Traits_T::OcclusionKernelName, 0);
		};

		template<>
		TLinearIntersector<CDeviceManagerOcl>::TLinearIntersector(DeviceManagerBackend_T* device_manager_ptr)
		{
			_device_manager_ptr = device_manager_ptr;

			std::vector<const char*> filenames;
			filenames.push_back(Traits_T::IntersectorProgramName);

			const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;
			auto program_hdl = _device_manager_ptr->CompileProgram(filenames, compiler_options);

			_intersection_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, Traits_T::IntersectionKernelName, 0);
			_occlusion_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, Traits_T::OcclusionKernelName, 0);
		};


		virtual ~TLinearIntersector();

		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data) override
		{
			auto& geom_manager = static_cast<GeomManagerT&>(scene_ctx_ptr->GeomManagers[typeid(GeomManagerT)]);

			auto geom_buffer_hdl = geom_manager.GetBackendManagerPtr(_device_manager_ptr)->GetGeomBufferHdl();

			size_t param_idx = 0;

			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_buffer);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_stream);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_stream_info);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, geom_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.result);

			auto block_size = _device_manager_ptr->GetWarpSize();

			_device_manager_ptr->DispatchKernel(_intersection_kernel_hdl
			{ static_cast<U32>(std::ceil(static_cast<F32>(query_data.ray_count) / static_cast<F32>(block_size))), 1, 1 },
			{ block_size, 1, 1 });
		};

		virtual void QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data) override
		{
		};

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;

		SProgramHdl _intersector_program_hdl;
		SKernelHdl _intersection_kernel_hdl;
		SKernelHdl _occlusion_kernel_hdl;

};

template<typename GeomManagerT, char* ProgramName, char* IntersectionKernelName, char* OcclusionKernelName>
struct TLinearIntersectorTraits
{
    using GeomManagerT = GeomManagerT;
    static const char* IntersectorProgramName = ProgramName;
    static const char* IntersectionKernelName = IntersectionKernelName;
    static const char* OcclusionKernelName = OcclusionKernelName;
};

using TIntersectorQuadLinearTraits = TLinearIntersectorTraits<CQuadGeomManager, "KIntersectorQuadLinear", "IntersectionQuadLinear", "OcclusionQuadLinear">;
using TIntersectorDiskLinearTraits = TLinearIntersectorTraits<CDiskGeomManager, "KIntersectorDiskLinear", "IntersectionDiskLinear", "OcclusionDiskLinear">;
using TIntersectorSphereLinearTraits = TLinearIntersectorTraits<CSphereGeomManager, "KIntersectorSphereLinear", "IntersectionSphereLinear", "OcclusionSphereLinear">;

template<typename DeviceManagerBackend_T>
using TIntersectorQuadLinear<DeviceManagerBackend_T> = TIntersectorLinear<DeviceManagerBackend_T, TIntersectorQuadLinearTraits>;

template<typename DeviceManagerBackend_T>
using TIntersectorDiskLinear<DeviceManagerBackend_T> = TIntersectorLinear<DeviceManagerBackend_T, TIntersectorDiskLinearTraits>;

template<typename DeviceManagerBackend_T>
using TIntersectorSphereLinear<DeviceManagerBackend_T> = TIntersectorLinear<DeviceManagerBackend_T, TIntersectorSphereLinearTraits>;

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Intersectors/IIntersector.hpp"
#include "Intersectors/IIntersectorFactory.hpp"

struct SSceneContext;

template<typename DeviceManagerBackend_T>
class TIntersectorLightLinear : public IIntersector
{
	static const U32 INTERSECTION_KERNEL_PARAM_COUNT = 6;
	static const U32 OCCLUSION_KERNEL_PARAM_COUNT = 0;

	public:
		TIntersectorLightLinear(){};
		~TIntersectorLightLinear(){};

		TIntersectorLightLinear(DeviceManagerBackend_T* device_manager_ptr);

		virtual std::string GetAcceleratorName() { return ""; };

		virtual void Update(const SSceneContext* scene_ctx_ptr) override
		{

		};

		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data) override
        {
			auto area_light_manager_ptr = scene_ctx_ptr->area_light_manager_ptr;

			auto metadata = area_light_manager_ptr->GetMetadata();

			auto area_light_buffer_hdl = area_light_manager_ptr->GetBackendManagerPtr(_device_manager_ptr)->GetAreaLightsBufferHdl();

			size_t param_idx = 0;
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_buffer);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_stream);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.ray_stream_info);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, area_light_buffer_hdl);
			_device_manager_ptr->SetKernelArgValue(_intersection_kernel_hdl, param_idx++, sizeof(U32), &metadata.light_count);
			_device_manager_ptr->SetKernelArgBuffer(_intersection_kernel_hdl, param_idx++, query_data.result);

			auto block_size = _device_manager_ptr->GetWarpSize();

			_device_manager_ptr->DispatchKernel(_intersection_kernel_hdl,
			{ static_cast<U32>(std::ceil(static_cast<F32>(query_data.ray_count) / static_cast<F32>(block_size))), 1, 1 },
			{ block_size, 1, 1 });
        };

		virtual void QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data) override
        {
        };

	protected:

	private:
        DeviceManagerBackend_T* _device_manager_ptr;
		SProgramHdl _intersector_program_hdl;
		SKernelHdl _intersection_kernel_hdl;
		SKernelHdl _occlusion_kernel_hdl;
};

template<>
TIntersectorLightLinear<CDeviceManagerCuda>::TIntersectorLightLinear(CDeviceManagerCuda* device_manager_ptr)
{
    _device_manager_ptr = device_manager_ptr;

    _intersector_program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KIntersectorLightLinear.cubin");
    _intersection_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "IntersectionLightLinear", INTERSECTION_KERNEL_PARAM_COUNT);
    // _occlusion_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "OcclusionLightLinear", OCCLUSION_KERNEL_PARAM_COUNT);
};

template<>
TIntersectorLightLinear<CDeviceManagerOcl>::TIntersectorLightLinear(CDeviceManagerOcl* device_manager_ptr)
{
    _device_manager_ptr = device_manager_ptr;

    std::vector<const char*> filenames;
    filenames.push_back("data/Kernels/KIntersectorLightLinear.c");

    const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;
    _intersector_program_hdl = _device_manager_ptr->CompileProgram(filenames, compiler_options);

    _intersection_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "IntersectionLightLinear", INTERSECTION_KERNEL_PARAM_COUNT);
    // _occlusion_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "OcclusionLightLinear", OCCLUSION_KERNEL_PARAM_COUNT);
};

class CIntersectorLightLinearFactory : public IIntersectorFactory
{
	public:
		CIntersectorLightLinearFactory(){};
		~CIntersectorLightLinearFactory(){};

		virtual IIntersector* Create(CDeviceManagerCuda* device_manager_ptr) override
		{
			return new TIntersectorLightLinear < CDeviceManagerCuda > { device_manager_ptr };
		};

		virtual IIntersector* Create(CDeviceManagerOcl* device_manager_ptr) override
		{
			return new TIntersectorLightLinear < CDeviceManagerOcl > { device_manager_ptr };
		};

	protected:

	private:

};

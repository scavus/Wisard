/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "IntersectorFireRays.hpp"
#include "Scene/SceneContext.hpp"

CIntersectorFireRays::CIntersectorFireRays(CDeviceManagerOcl* device_manager_ptr)
{
	_device_manager_ptr = device_manager_ptr;

	cl_context ocl_context = _device_manager_ptr->GetContext();
	cl_device_id ocl_device_id = _device_manager_ptr->GetDeviceId();
	cl_command_queue ocl_queue = _device_manager_ptr->GetCommandQueue();
	
	_api = FireRays::IntersectionApiCL::CreateFromOpenClContext(ocl_context, ocl_device_id, ocl_queue);
	_api_shape = nullptr;

	std::vector<const char*> filenames;
	filenames.push_back("data/Kernels/KIntersectorFireRays.c");
	const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;

	_intersector_program_hdl = _device_manager_ptr->CompileProgram(filenames, compiler_options);

	_convert_geometry_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertGeometry", 4);
	_convert_rays_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertRays", 4);
	_convert_query_result_kernel_hdl = _device_manager_ptr->CreateKernel(_intersector_program_hdl, "ConvertQueryResult", 7);
}

void CIntersectorFireRays::QueryIntersection(const SSceneContext* scene_ctx_ptr, SRayIntersectionQueryData& query_data)
{
	auto geom_manager = scene_ctx_ptr->mesh_geom_manager_ptr;

	size_t triangle_count = geom_manager->GetTransformedMeshGeomConstRef().size();
	size_t buffer_size = 0;

	buffer_size = sizeof(FireRays::ray) * query_data.ray_count;
	SBufferHdl api_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(FireRays::Intersection) * query_data.ray_count;
	SBufferHdl api_hit_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	FireRays::Buffer* api_ray_buffer = _api->CreateFromOpenClBuffer(_device_manager_ptr->GetBufferDesc(api_ray_buffer_hdl).id);
	FireRays::Buffer* api_hit_buffer = _api->CreateFromOpenClBuffer(_device_manager_ptr->GetBufferDesc(api_hit_buffer_hdl).id);

	U32 block_size = _device_manager_ptr->GetWarpSize();
	U32 arg_idx = 0;

	arg_idx = 0;
	_device_manager_ptr->SetKernelArgValue(_convert_rays_kernel_hdl, arg_idx++, sizeof(U32), &query_data.ray_count);
	_device_manager_ptr->SetKernelArgBuffer(_convert_rays_kernel_hdl, arg_idx++, query_data.ray_buffer);
	_device_manager_ptr->SetKernelArgBuffer(_convert_rays_kernel_hdl, arg_idx++, query_data.ray_stream);
	_device_manager_ptr->SetKernelArgBuffer(_convert_rays_kernel_hdl, arg_idx++, api_ray_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_convert_rays_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(query_data.ray_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	_api->QueryIntersection(api_ray_buffer, query_data.ray_count, api_hit_buffer, nullptr, nullptr);

	arg_idx = 0;
	_device_manager_ptr->SetKernelArgValue(_convert_query_result_kernel_hdl, arg_idx++, sizeof(U32), &query_data.ray_count);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, api_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, api_hit_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, _geom_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, query_data.ray_buffer);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, query_data.ray_stream);
	_device_manager_ptr->SetKernelArgBuffer(_convert_query_result_kernel_hdl, arg_idx++, query_data.result);
	_device_manager_ptr->DispatchKernel(_convert_query_result_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(query_data.ray_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->Synchronize();

	_device_manager_ptr->DestroyBuffer(api_ray_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(api_hit_buffer_hdl);

	_api->DeleteBuffer(api_ray_buffer);
	_api->DeleteBuffer(api_hit_buffer);
}

void CIntersectorFireRays::QueryOcclusion(const SSceneContext* scene_ctx_ptr, SRayOcclusionQueryData& query_data)
{

}

void CIntersectorFireRays::Update(const SSceneContext* scene_ctx_ptr)
{
	// Not implemented
}

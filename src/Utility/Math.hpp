/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <algorithm>

#include "Api/WrCommon.h"
#include "Api/WrTypes.h"

namespace MathUtils
{
	const F32 WR_PI = 3.14159265f;

	inline F32 ToRad(F32 a);

	inline F32 ToDeg(F32 a);

	inline F32 Clamp(F32 n, F32 minRange, F32 maxRange);

	inline F32 Saturate(F32 n);

	template<typename T, U32 n>
	struct TVector
	{
		T elements[n];
	};

	WrVector2f MakeVec2(F32 x, F32 y);

	WrVector3f MakeVec3(F32 x, F32 y, F32 z);

	F32 GetVec3Elem(const WrVector3f v, S32 i);


	F32 Dot(const WrVector3f& v0, const WrVector3f& v1);
	WrVector3f Cross(const WrVector3f& v0, const WrVector3f& v1);
	WrVector3f Normalize(const WrVector3f& v0);
	F32 Length(const WrVector3f& v0);

	WrVector4f MakeVec4(F32 x, F32 y, F32 z, F32 w);

	typedef WrVector4f ColorRGBA;

	F32 Dot(const WrVector4f& v0, const WrVector4f& v1);

	WrVector4f Normalize(const WrVector4f& v0);

	F32 Length(const WrVector4f& v0);

	template <typename T, U32 dimX, U32 dimY>
	struct Matrix
	{
		T elements[dimX][dimY];
	};

	template <>
	struct Matrix<F32, 3, 3>
	{
		union
		{
			F32 elements[3][3];

			struct
			{
				F32 m00, m01, m02;
				F32 m10, m11, m12;
				F32 m20, m21, m22;
			};
		};
	};

	typedef Matrix<F32, 3, 3> Matrix3x3;

	WrMatrix4x4 MakeMatrix4x4(const WrVector4f& row0, const WrVector4f& row1, const WrVector4f& row2, const WrVector4f& row3);

	// WrVector4f Transform();
	WrVector3f TransformPos(const WrMatrix4x4& m, const WrVector3f& v);;
	WrVector3f TransformDir(const WrMatrix4x4& m, const WrVector3f& v);;

	WrMatrix4x4 Transpose(const WrMatrix4x4& mat0);
	WrMatrix4x4 Translation(F32 Tx, F32 Ty, F32 Tz);
	WrMatrix4x4 Scale(F32 Sx, F32 Sy, F32 Sz);
	WrMatrix4x4 RotationX(F32 deg);
	WrMatrix4x4 RotationY(F32 deg);
	WrMatrix4x4 RotationZ(F32 deg);
	WrMatrix4x4 Rotation(const WrVector3f& v0, F32 deg);
	WrMatrix4x4 Perspective_GL(F32 fovDeg, F32 aspect, F32 zNear, F32 zFar);
	WrMatrix4x4 PerspectiveFovMatrix(F32 FovYDeg, F32 AspectRatio, F32 ZNear, F32 ZFar);
	WrMatrix4x4 PerspectiveFovMatrixLH(F32 FovYDeg, F32 AspectRatio, F32 ZNear, F32 ZFar);
	WrMatrix4x4 LookAtMatrix(const WrVector3f& Eye, const WrVector3f &Target, const WrVector3f& Up);
	WrMatrix4x4 LookAtMatrixLH(const WrVector3f& Eye, const WrVector3f &Target, const WrVector3f& Up);
};

typedef MathUtils::TVector<U8, 2> U8x2;
typedef MathUtils::TVector<U8, 3> U8x3;
typedef MathUtils::TVector<U8, 4> U8x4;

WrVector3f operator+(const WrVector3f& v0, const WrVector3f& v1);
WrVector3f operator-(const WrVector3f& v0, const WrVector3f& v1);
WrVector3f operator*(const WrVector3f& v0, F32 f);
WrVector3f operator/(const WrVector3f& v0, F32 f);

WrVector4f operator+(const WrVector4f& v0, const WrVector4f& v1);
WrVector4f operator-(const WrVector4f& v0, const WrVector4f& v1);
WrVector4f operator*(const WrVector4f& v0, F32 f);
WrVector4f operator/(const WrVector4f& v0, F32 f);

WrMatrix4x4 operator*(const WrMatrix4x4& mat0, const WrMatrix4x4& mat1);
WrVector4f operator*(const WrMatrix4x4& mat0, const WrVector4f& v0);

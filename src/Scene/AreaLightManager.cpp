/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "AreaLightManager.hpp"
#include "Scene/SceneContext.hpp"

template class TAreaLightBackendManager<CDeviceManagerCuda>;
template class TAreaLightBackendManager<CDeviceManagerOcl>;
template class TAreaLightBackendManager<CDeviceManagerDebug>;

namespace AreaLightUtils
{
	void Transform(SAreaLightContainer* area_light_container_ptr)
	{
		for (WrAreaLight& area_light : area_light_container_ptr->area_lights_arr.data)
		{
			switch (area_light.type)
			{
				case WrLightType::LIGHT_TYPE_DISK:
				{
					// SDisk& disk = area_light_container_ptr->area_light_disks_arr.data[area_light.shape_id];
					WrDisk& disk = area_light.shape.disk;

					WrDisk pt_disk;

					pt_disk.center = MathUtils::TransformPos(area_light.obj_to_world_mtx, MathUtils::MakeVec3(0.0f, 0.0f, 0.0f));
					pt_disk.normal =  MathUtils::Normalize(MathUtils::TransformDir(area_light.obj_to_world_mtx, disk.normal));
					pt_disk.radius = disk.radius;

					WrAreaLight pt_area_light;
					pt_area_light = area_light;
					pt_area_light.shape.disk = pt_disk;

					area_light_container_ptr->transformed_area_lights_arr.push_back(pt_area_light);
					// area_light_container_ptr->transformed_area_light_disks_arr.Append(pt_disk);

					break;
				}
				/*
				case ELightType::LIGHT_TYPE_SPHERE:
				{
					SSphere& Sphere = LightSpheres[area_light.shape_id];

					SSphere PtSphere;

					PtSphere.center = (area_light.obj_to_world_mtx * Math::Vector4(Sphere.center, 1.0f)).xyz();
					// PtSphere.radius =

					PtLightSpheres.push_back(PtSphere);

					break;
				}
				*/
				case WrLightType::LIGHT_TYPE_QUAD:
				{
					WrQuad pt_quad;

					pt_quad.base = MathUtils::TransformPos(area_light.obj_to_world_mtx, MathUtils::MakeVec3(-0.5f, 0.0f, 0.5f));
					// pt_quad.u = MathUtils::TransformDir(area_light.obj_to_world_mtx, MathUtils::MakeVec3(1.0f, 0.0f, 0.0f));
					// pt_quad.v = MathUtils::TransformDir(area_light.obj_to_world_mtx, MathUtils::MakeVec3(0.0f, 0.0f, -1.0f));
					pt_quad.u = MathUtils::TransformDir(area_light.obj_to_world_mtx, MathUtils::MakeVec3(1.0f, 0.0f, 0.0f));
					pt_quad.v = MathUtils::TransformDir(area_light.obj_to_world_mtx, MathUtils::MakeVec3(0.0f, 0.0f, 1.0f));

					pt_quad.normal = MathUtils::Normalize(MathUtils::Cross(pt_quad.u, pt_quad.v));
					// pt_quad.normal = WrVector3f{ 0.0f, 1.0f, 0.0f };

					pt_quad.inv_length_sqr_u = 1.0f / (pt_quad.u.x * pt_quad.u.x + pt_quad.u.y * pt_quad.u.y + pt_quad.u.z * pt_quad.u.z);
					pt_quad.inv_length_sqr_v = 1.0f / (pt_quad.v.x * pt_quad.v.x + pt_quad.v.y * pt_quad.v.y + pt_quad.v.z * pt_quad.v.z);

					WrAreaLight pt_area_light;
					pt_area_light = area_light;
					pt_area_light.shape.quad = pt_quad;
					area_light_container_ptr->transformed_area_lights_arr.push_back(pt_area_light);
					// area_light_container_ptr->transformed_area_light_quads_arr.Append(pt_quad);

					break;
				}
				/*
				case ELightType::LIGHT_TYPE_TUBE:
				{

					break;
				}
				*/
				default:
					break;
			}
		}
	}
}

SAreaLightId CAreaLightManager::CreateAreaLight(WrAreaLight& area_light)
{
	SAreaLightId id;
	id = _resource_container_ptr->area_lights_arr.Append(area_light);

	_resource_container_ptr->area_lights_arr.update_flag = true;

	return id;
}

void CAreaLightManager::Update()
{
	if (_resource_container_ptr->area_lights_arr.update_flag)
	{
		AreaLightUtils::Transform(_resource_container_ptr);
		UpdateBackends();

		_resource_container_ptr->area_lights_arr.update_flag = false;
	}
}

void CAreaLightManager::DestroyAreaLight(SAreaLightId resource_id)
{

}

void CAreaLightManager::SetAreaLight(SAreaLightId area_light_id, WrAreaLight& area_light)
{
	_resource_container_ptr->area_lights_arr[area_light_id] = std::move(area_light);
	_resource_container_ptr->area_lights_arr.update_flag = true;
}

template<class DeviceManagerBackend_T>
void TAreaLightBackendManager<DeviceManagerBackend_T>::Update()
{
	auto& area_lights_arr = _resource_container_ptr->transformed_area_lights_arr;

	if (_area_light_buffer_hdl == 0)
	{
		CreateBuffers();
		return;
	}

	_device_manager_ptr->WriteBuffer(_area_light_buffer_hdl, &area_lights_arr[0]);
}

template<class DeviceManagerBackend_T>
void TAreaLightBackendManager<DeviceManagerBackend_T>::CreateBuffers()
{
	size_t buffer_size;

	const std::vector<WrAreaLight>& transformed_area_lights_arr = _resource_container_ptr->transformed_area_lights_arr;
	buffer_size = sizeof(WrAreaLight) * transformed_area_lights_arr.size();

	if (buffer_size)
	{
		_area_light_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &transformed_area_lights_arr[0]);
	}
	else
	{
		_area_light_buffer_hdl = 0;
	}
}


/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "MeshGeomManager.hpp"
#include "Utility/Math.hpp"
#include "Scene/SceneContext.hpp"

namespace MeshGeomUtils
{
	void Transform(const std::vector<SMesh>& meshes, const std::vector<WrTriangle>& primitives, std::vector<WrTriangle>& transformed_primitives)
	{
		for (const WrTriangle& triangle : primitives)
		{
			WrTriangle pt_triangle;

			const WrMatrix4x4& obj_to_world_mtx = meshes[triangle.mesh_id].obj_to_world_mtx;

			pt_triangle.vtx[0].pos = MathUtils::TransformPos(obj_to_world_mtx, triangle.vtx[0].pos);
			pt_triangle.vtx[1].pos = MathUtils::TransformPos(obj_to_world_mtx, triangle.vtx[1].pos);
			pt_triangle.vtx[2].pos = MathUtils::TransformPos(obj_to_world_mtx, triangle.vtx[2].pos);

			pt_triangle.vtx[0].normal = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[0].normal) );
			pt_triangle.vtx[1].normal = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[1].normal) );
			pt_triangle.vtx[2].normal = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[2].normal) );

			pt_triangle.vtx[0].tangent = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[0].tangent) );
			pt_triangle.vtx[1].tangent = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[1].tangent) );
			pt_triangle.vtx[2].tangent = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[2].tangent) );

			pt_triangle.vtx[0].binormal = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[0].binormal) );
			pt_triangle.vtx[1].binormal = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[1].binormal) );
			pt_triangle.vtx[2].binormal = MathUtils::Normalize( MathUtils::TransformDir(obj_to_world_mtx, triangle.vtx[2].binormal) );

			pt_triangle.vtx[0].texcoord = triangle.vtx[0].texcoord;
			pt_triangle.vtx[1].texcoord = triangle.vtx[1].texcoord;
			pt_triangle.vtx[2].texcoord = triangle.vtx[2].texcoord;

			pt_triangle.mesh_id = triangle.mesh_id;

			transformed_primitives.push_back(pt_triangle);
		}
	}
};

void CMeshGeomManager::Update()
{
	if (_resource_container_ptr->transformed_mesh_geom_update_flag)
	{
		MeshGeomUtils::Transform(_scene_ctx_ptr->mesh_manager_ptr->GetMeshArrayConstRef().data, 
								 _resource_container_ptr->mesh_geom, _resource_container_ptr->transformed_geom);

		_resource_container_ptr->transformed_mesh_geom_update_flag = false;
	}
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "BasicMtlManager.hpp"

template class TBasicMtlBackendManager<CDeviceManagerCuda>;
template class TBasicMtlBackendManager<CDeviceManagerOcl>;
template class TBasicMtlBackendManager<CDeviceManagerDebug>;


void CBasicMtlManager::Update()
{
	UpdateBackends();
}

SBasicMtlId CBasicMtlManager::AddBasicMtl(SBasicMtl& resource)
{
	SBasicMtlId id;
	id = _resource_container_ptr->basic_mtl_arr.Append(resource);

	return id;
}

void CBasicMtlManager::DeleteBasicMtl(SBasicMtlId resource_id)
{
	_resource_container_ptr->basic_mtl_arr.Delete(resource_id);
}


template <class DeviceManagerBackend_T>
void TBasicMtlBackendManager<DeviceManagerBackend_T>::CreateBuffers()
{
	size_t buffer_size;

	const TTrackedResourceArray<SBasicMtl>& mtl_arr = _resource_container_ptr->basic_mtl_arr;

	buffer_size = sizeof(SBasicMtl) * mtl_arr.data.size();
	_basic_mtl_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &mtl_arr.data[0]);
}


template <class DeviceManagerBackend_T>
void TBasicMtlBackendManager<DeviceManagerBackend_T>::Update()
{

}

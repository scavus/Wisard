/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "MeshManager.hpp"
#include "Scene/SceneContext.hpp"

template class TMeshBackendManager<CDeviceManagerCuda>;
template class TMeshBackendManager<CDeviceManagerOcl>;
template class TMeshBackendManager<CDeviceManagerDebug>;

template<class DeviceManagerBackend_T>
void TMeshBackendManager<DeviceManagerBackend_T>::CreateBuffers()
{
	size_t buffer_size;

	const TTrackedResourceArray<SMesh>& mesh_arr = _resource_container_ptr->mesh_arr;
	buffer_size = sizeof(SMesh) * mesh_arr.data.size();
	_mesh_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &mesh_arr.data[0]);
}

template<class DeviceManagerBackend_T>
void TMeshBackendManager<DeviceManagerBackend_T>::Update()
{
	if (_mesh_buffer_hdl == 0)
	{
		CreateBuffers();
	}
	else
	{
		auto& mesh_arr = _resource_container_ptr->mesh_arr;

		_device_manager_ptr->DestroyBuffer(_mesh_buffer_hdl);

		auto buffer_size = sizeof(SMesh) * mesh_arr.data.size();
		_mesh_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &mesh_arr.data[0]);
	}
}

void CMeshManager::Update()
{
	if (_resource_container_ptr->mesh_arr.update_flag)
	{
		_scene_ctx_ptr->mesh_geom_manager_ptr->Update();
		UpdateBackends();
		_resource_container_ptr->mesh_arr.update_flag = false;
	}
}

void CMeshManager::SetMesh(SMeshId resource_id, SMesh& resource)
{
	SMesh& mesh = _resource_container_ptr->mesh_arr[resource_id];
	// _scene_ctx_ptr->deferred_shading_system_ptr->UnbindMaterial(&mesh.material_id);

	_resource_container_ptr->mesh_arr[resource_id] = std::move(resource);
	// _scene_ctx_ptr->deferred_shading_system_ptr->BindMaterial(&mesh.material_id);

	_resource_container_ptr->mesh_arr.update_flag = true;
}

void CMeshManager::SetMeshMaterial(SMeshId mesh_id, SDeferredMtlId mtl_id)
{
	_resource_container_ptr->mesh_arr[mesh_id].material_id = mtl_id;
}

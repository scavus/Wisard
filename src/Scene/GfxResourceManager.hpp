/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <unordered_map>

#include "Api/WrTypes.h"
#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"
#include "RenderDevice/DeviceBackend.hpp"

/*
template<typename Tag>
struct TResourceId
{
	U32 idx;
	// U32 gen;
};
*/

template<typename T>
struct TTrackedResourceArray
{
	WrResourceId Append(const T& resource);
	void Delete(const WrResourceId resource_id);
	void Clear();

	T& operator[](const WrResourceId resource_id);
	const T& operator[](const WrResourceId resource_id) const;

	std::vector<T> data;
	std::vector<std::unordered_map<WrResourceId*, WrResourceId*>> trackers;

	bool update_flag;
};

template<typename T>
void TTrackedResourceArray<T>::Clear()
{
	data.clear();
	// TODO: Broadcast to trackers
	trackers.clear();
}

template<typename T>
inline T& TTrackedResourceArray<T>::operator[](const WrResourceId resource_id)
{
	return data[resource_id.idx];
}

template<typename T>
inline const T& TTrackedResourceArray<T>::operator[](const WrResourceId resource_id) const
{
	return data[resource_id.idx];
}

template<typename T>
WrResourceId TTrackedResourceArray<T>::Append(const T& resource)
{
	WrResourceId id;
	data.push_back(resource);
	// gens.push_back(0);

	// std::unordered_map<&TResourceId<Tag>, &TResourceId<Tag> tracker_map;
	trackers.push_back(std::unordered_map<WrResourceId*, WrResourceId*>());
	// trackers.reserve(data.size());

	id.idx = data.size() - 1;
	return id;
}

template<typename T>
void TTrackedResourceArray<T>::Delete(const WrResourceId resource_id)
{
	std::swap(data[resource_id.idx], data.back());
	std::swap(trackers[resource_id.idx], trackers.back());

	for (auto tracker_map : trackers[resource_id.idx])
	{
		*tracker_map.second = resource_id;
	}

	for (auto tracker_map : trackers.back())
	{
		*tracker_map.second = { 0 };
	}

	data.pop_back();
	trackers.pop_back();
}


template<class ResourceContainer, template<class DeviceManagerBackend_T> class ResourceBackendManager>
class TGfxResourceManager
{
	public:
		TGfxResourceManager(){};

		~TGfxResourceManager()
		{
			for (auto backend_manager_ptr : _cuda_backend_manager_ptrs)
			{
				delete backend_manager_ptr.second;
			}

			for (auto backend_manager_ptr : _ocl_backend_manager_ptrs)
			{
				delete backend_manager_ptr.second;
			}

			for (auto backend_manager_ptr : _debug_backend_manager_ptrs)
			{
				delete backend_manager_ptr.second;
			}
		};

		void CreateBackendManager(CDeviceManagerCuda* device_manager_ptr)
		{
			if (_cuda_backend_manager_ptrs.find(device_manager_ptr->GetId()) == _cuda_backend_manager_ptrs.end())
			{
				auto resource_backend_manager_ptr = new ResourceBackendManager<CDeviceManagerCuda>{ device_manager_ptr, _resource_container_ptr };
				_cuda_backend_manager_ptrs.insert({ device_manager_ptr->GetId(), resource_backend_manager_ptr });
			}
		};

		void CreateBackendManager(CDeviceManagerOcl* device_manager_ptr)
		{
			if (_ocl_backend_manager_ptrs.find(device_manager_ptr->GetId()) == _ocl_backend_manager_ptrs.end())
			{
				auto resource_backend_manager_ptr = new ResourceBackendManager<CDeviceManagerOcl>{ device_manager_ptr, _resource_container_ptr };
				_ocl_backend_manager_ptrs.insert({ device_manager_ptr->GetId(), resource_backend_manager_ptr });
			}
		};

		void CreateBackendManager(CDeviceManagerDebug* device_manager_ptr)
		{
			if (_debug_backend_manager_ptrs.find(device_manager_ptr->GetId()) == _debug_backend_manager_ptrs.end())
			{
				auto resource_backend_manager_ptr = new ResourceBackendManager < CDeviceManagerDebug > { device_manager_ptr, _resource_container_ptr };
				_debug_backend_manager_ptrs.insert({ device_manager_ptr->GetId(), resource_backend_manager_ptr });
			}
		};

		void DestroyBackendManager(CDeviceManagerCuda* device_manager_ptr)
		{
			_cuda_backend_manager_ptrs.erase(device_manager_ptr->GetId());
		};

		void DestroyBackendManager(CDeviceManagerOcl* device_manager_ptr)
		{
			_ocl_backend_manager_ptrs.erase(device_manager_ptr->GetId());
		};

		void DestroyBackendManager(CDeviceManagerDebug* device_manager_ptr)
		{
			_debug_backend_manager_ptrs.erase(device_manager_ptr->GetId());
		};

		void CreateBackendManagers(SDeviceRegistry* device_registry_ptr)
		{
			for (auto device_manager_ptr : device_registry_ptr->cuda_device_managers)
			{
				if (_cuda_backend_manager_ptrs.find(device_manager_ptr.second->GetId()) == _cuda_backend_manager_ptrs.end())
				{
					auto resource_backend_manager_ptr = new ResourceBackendManager<CDeviceManagerCuda>{ device_manager_ptr.second, _resource_container_ptr };
					_cuda_backend_manager_ptrs.insert({ device_manager_ptr.second->GetId(), resource_backend_manager_ptr });
				}
			}

			for (auto device_manager_ptr : device_registry_ptr->ocl_device_managers)
			{
				if (_ocl_backend_manager_ptrs.find(device_manager_ptr.second->GetId()) == _ocl_backend_manager_ptrs.end())
				{
					auto resource_backend_manager_ptr = new ResourceBackendManager<CDeviceManagerOcl>{ device_manager_ptr.second, _resource_container_ptr };
					_ocl_backend_manager_ptrs.insert({ device_manager_ptr.second->GetId(), resource_backend_manager_ptr });
				}
			}

			for (auto device_manager_ptr : device_registry_ptr->debug_device_managers)
			{
				if (_debug_backend_manager_ptrs.find(device_manager_ptr.second->GetId()) == _debug_backend_manager_ptrs.end())
				{
					auto resource_backend_manager_ptr = new ResourceBackendManager < CDeviceManagerDebug > { device_manager_ptr.second, _resource_container_ptr };
					_debug_backend_manager_ptrs.insert({ device_manager_ptr.second->GetId(), resource_backend_manager_ptr });
				}
			}

		};

		template<class DeviceManagerBackend_T>
		ResourceBackendManager<DeviceManagerBackend_T>* GetBackendManagerPtr(const DeviceManagerBackend_T* device_manager_ptr);

		inline ResourceBackendManager<CDeviceManagerCuda>* GetBackendManagerPtr(const CDeviceManagerCuda* device_manager_ptr)
		{
			return _cuda_backend_manager_ptrs[device_manager_ptr->GetId()];
		};

		inline ResourceBackendManager<CDeviceManagerOcl>* GetBackendManagerPtr(const CDeviceManagerOcl* device_manager_ptr)
		{
			return _ocl_backend_manager_ptrs[device_manager_ptr->GetId()];
		};

		inline ResourceBackendManager<CDeviceManagerDebug>* GetBackendManagerPtr(const CDeviceManagerDebug* device_manager_ptr)
		{
			return _debug_backend_manager_ptrs[device_manager_ptr->GetId()];
		};

		void CreateBackendBuffers()
		{
			/*
			for (auto backend_manager_ptr : _cuda_backend_manager_ptrs)
			{
				backend_manager_ptr.second->CreateBuffers();
			}

			for (auto backend_manager_ptr : _ocl_backend_manager_ptrs)
			{
				backend_manager_ptr.second->CreateBuffers();
			}

			for (auto backend_manager_ptr : _debug_backend_manager_ptrs)
			{
				backend_manager_ptr.second->CreateBuffers();
			}
			*/
		};

	protected:
		ResourceContainer* _resource_container_ptr;

		std::unordered_map<U32, ResourceBackendManager<CDeviceManagerCuda>*> _cuda_backend_manager_ptrs;
		std::unordered_map<U32, ResourceBackendManager<CDeviceManagerOcl>*> _ocl_backend_manager_ptrs;
		std::unordered_map<U32, ResourceBackendManager<CDeviceManagerDebug>*> _debug_backend_manager_ptrs;

		void UpdateBackends()
		{
			for (auto backend_manager_ptr : _cuda_backend_manager_ptrs)
			{
				backend_manager_ptr.second->Update();
			}

			for (auto backend_manager_ptr : _ocl_backend_manager_ptrs)
			{
				backend_manager_ptr.second->Update();
			}

			for (auto backend_manager_ptr : _debug_backend_manager_ptrs)
			{
				backend_manager_ptr.second->Update();
			}
		};

	private:
};

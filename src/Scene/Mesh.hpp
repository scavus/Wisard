/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Utility/Math.hpp"
#include "Scene/Geometry.hpp"
#include "GfxResourceManager.hpp"
#include "Scene/DeferredShadingSystem.hpp"

struct SMesh
{
	WrMatrix4x4 obj_to_world_mtx;
	WrMatrix4x4 world_to_obj_mtx;

	SDeferredMtlId material_id;
	U32 polygon_count;
};

struct SMeshContainer
{
	TTrackedResourceArray<SMesh> mesh_arr;
};

/*
struct SMeshGeomBuffer
{
	std::vector<STriangle> MeshGeomBuffer;
};
*/

// TODO: Use triangle buffers to hold mesh geometry
struct SMeshGeomContainer
{
	std::vector<WrTriangle> mesh_geom;
	std::vector<WrTriangle> transformed_geom;
	bool transformed_mesh_geom_update_flag;
};

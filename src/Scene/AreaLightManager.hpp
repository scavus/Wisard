/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>

#include "Api/WrCommon.h"
#include "Api/WrTypes.h"
#include "RenderDevice/ComputeDeviceManager.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "GfxResourceManager.hpp"

struct SSceneContext;

using SAreaLightId = WrResourceId;

struct SAreaLightContainer
{
	TTrackedResourceArray<WrAreaLight> area_lights_arr;
	std::vector<WrAreaLight> transformed_area_lights_arr;
};

template<class DeviceManagerBackend_T>
class TAreaLightBackendManager
{
	friend class CAreaLightManager;

	public:
		TAreaLightBackendManager(){};
		
		~TAreaLightBackendManager()
		{
			_device_manager_ptr->DestroyBuffer(_area_light_buffer_hdl);
		};

		TAreaLightBackendManager(DeviceManagerBackend_T* device_manager_ptr,
											const SAreaLightContainer* resource_container_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_resource_container_ptr = resource_container_ptr;
			_area_light_buffer_hdl = 0;
		};

		SBufferHdl GetAreaLightsBufferHdl() { return _area_light_buffer_hdl; };

		void Update();

		void CreateBuffers();

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;

		SBufferHdl _area_light_buffer_hdl;

		const SAreaLightContainer* _resource_container_ptr;
};

class CAreaLightManager : public TGfxResourceManager<SAreaLightContainer, TAreaLightBackendManager>
{
	public:
		using SStorage = SAreaLightContainer;

		struct SMetadata
		{
			U32 light_count;
		};

		CAreaLightManager(){};
		
		virtual ~CAreaLightManager()
		{
			_resource_container_ptr->area_lights_arr.Clear();
			_resource_container_ptr->transformed_area_lights_arr.clear();
		};

		CAreaLightManager(SDeviceRegistry* device_registry_ptr, SAreaLightContainer* resource_container_ptr, SSceneContext* scene_ctx_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			CreateBackendManagers(device_registry_ptr);
		};

		const TTrackedResourceArray<WrAreaLight>& GetAreaLightsArrayConstRef() const { return _resource_container_ptr->area_lights_arr; };

		SAreaLightId CreateAreaLight(WrAreaLight& area_light);
		void SetAreaLight(SAreaLightId area_light_id, WrAreaLight& area_light);

		void DestroyAreaLight(SAreaLightId resource_id);

		void Update();

		SMetadata GetMetadata()
		{
			SMetadata metadata;
			metadata.light_count = _resource_container_ptr->area_lights_arr.data.size();

			return metadata;
		};

	protected:

	private:
		SSceneContext* _scene_ctx_ptr;
};

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "GfxResourceManager.hpp"
#include "Scene/Material.hpp"
#include "Integrators/IntegratorDeferred.hpp"

struct SSceneContext;

enum ESurfaceShaderType
{
	SURFACESHADER_TYPE_STANDARD,
	SURFACESHADER_TYPE_LAMBERT,
	SURFACESHADER_TYPE_GGX,
	SURFACESHADER_TYPE_BLINN,
	SURFACESHADER_TYPE_PLASTIC
};

struct SDeferredMtlTag{};

using SDeferredMtlId = WrResourceId;

struct SDeferredMtlContainer
{
	TTrackedResourceArray<WrMaterial> mtl_array;
};

struct SShaderTag{};

using SShaderId = WrResourceId;

struct SShaderEvalData
{
	SBufferHdl shader_globals_buf;
	SBufferHdl rngs_buf;
	SBufferHdl shading_stream_buf;
	SBufferHdl shading_substream_infos_buf;
	SBufferHdl sample_infos_buf;
	SSubstreamInfo* shading_substream_infos;
};

struct SShaderSampleData
{
	SBufferHdl shader_globals_buf;
	SBufferHdl rngs_buf;
	SBufferHdl rand_samples_buf;
	SBufferHdl shading_stream_buf;
	SBufferHdl shading_substream_infos_buf;
	SBufferHdl sample_infos_buf;
	SSubstreamInfo* shading_substream_infos;
};

template <class DeviceManagerBackend_T>
class TDeferredShadingSystemBackend
{
	friend class CDeferredShadingSystem;

	public:
		TDeferredShadingSystemBackend(){};

		~TDeferredShadingSystemBackend()
		{
			for (auto h : _uniform_buffer_hdls.data)
			{
				_device_manager_ptr->DestroyBuffer(h);
			}

			// TODO:
			/*
			for (auto h : _shading_kernels)
			{
				_device_manager_ptr->DestroyKernel(h);
			}
			*/

			_uniform_buffer_hdls.Clear();
			_shading_kernels.Clear();
		};

		TDeferredShadingSystemBackend(DeviceManagerBackend_T* device_manager_ptr, const SDeferredMtlContainer* resource_container_ptr);

		void Eval(SShaderEvalData& eval_data);

		void Sample(SShaderSampleData& sample_data);

		void Update();

		void SetSceneCtx(const SSceneContext* scene_ctx_ptr) { _scene_ctx_ptr = scene_ctx_ptr; };

	protected:

	private:
		struct SShaderKernelBackend
		{
			SKernelHdl eval_kernel_hdl;
			SKernelHdl sample_kernel_hdl;
		};

		DeviceManagerBackend_T* _device_manager_ptr;
		TTrackedResourceArray<SBufferHdl> _uniform_buffer_hdls;
		TTrackedResourceArray<SShaderKernelBackend> _shading_kernels;

		const SDeferredMtlContainer* _resource_container_ptr;
		const SSceneContext* _scene_ctx_ptr;
};

class CDeferredShadingSystem: public TGfxResourceManager<SDeferredMtlContainer, TDeferredShadingSystemBackend>
{
	public:
		using SStorage = SDeferredMtlContainer;

		struct SMetadata
		{
			U32 mtl_count;
			U32 shader_count;
		};

		CDeferredShadingSystem(){};

		virtual ~CDeferredShadingSystem()
		{
			_resource_container_ptr->mtl_array.Clear();
		};

		CDeferredShadingSystem(SDeviceRegistry* device_registry_ptr, SDeferredMtlContainer* resource_container_ptr, SSceneContext* scene_ctx_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			CreateBackendManagers(device_registry_ptr);

			auto default_ub = new WrLambertShaderUniformBufferDesc;
			default_ub->albedo_color = { 1.0f, 1.0f, 1.0f, 1.0f };
			default_ub->albedo_color_tex_slot = -1;

			WrMaterial default_mtl =
			{
				{
					WR_RESOURCE_ID_NULL,
					WR_RESOURCE_ID_NULL,
					WR_RESOURCE_ID_NULL,
					WR_RESOURCE_ID_NULL,
					WR_RESOURCE_ID_NULL,
					WR_RESOURCE_ID_NULL,
					WR_RESOURCE_ID_NULL,
					WR_RESOURCE_ID_NULL
				},
				default_ub,
				sizeof(WrLambertShaderUniformBufferDesc),
				WR_LAMBERT_SHADER_ID
			};

			CreateMaterial(default_mtl);

			for (auto backend : _cuda_backend_manager_ptrs)
			{
				backend.second->SetSceneCtx(_scene_ctx_ptr);
			}

			for (auto backend : _ocl_backend_manager_ptrs)
			{
				backend.second->SetSceneCtx(_scene_ctx_ptr);
			}

			for (auto backend : _debug_backend_manager_ptrs)
			{
				backend.second->SetSceneCtx(_scene_ctx_ptr);
			}
		};

		const WrMaterial& GetDeferredMtlConstRef(SDeferredMtlId resource_id) const
		{
			return _resource_container_ptr->mtl_array[resource_id];
		};

		const TTrackedResourceArray<WrMaterial>& GetDeferredMtlArrayConstRef() const
		{
			return _resource_container_ptr->mtl_array;
		};

		void Update();

		SDeferredMtlId CreateMaterial(WrMaterial& resource);
		void DestroyMaterial(SDeferredMtlId& resource_id);
		void SetMaterial(SDeferredMtlId resource_id, WrMaterial& resource);

		void BindMaterial(SDeferredMtlId* resource_id_ptr);
		void UnbindMaterial(SDeferredMtlId* resource_id_ptr);

		SMetadata GetMetadata()
		{
			SMetadata metadata;
			metadata.mtl_count = _resource_container_ptr->mtl_array.data.size();

			return metadata;
		};

		// SShaderId RegisterShader();
		// void UnregisterShader(SShaderId resource_id);

	protected:

	private:
		SSceneContext* _scene_ctx_ptr;
};

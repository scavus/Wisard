/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "Geometry.hpp"


SAabb ComputeBounds(const WrTriangle& triangle)
{
	SAabb aabb;

	F32x3 vtx0_pos = triangle.vtx[0].pos;
	F32x3 vtx1_pos = triangle.vtx[1].pos;
	F32x3 vtx2_pos = triangle.vtx[2].pos;

	aabb.bound_min.x = std::min({ vtx0_pos.x, vtx1_pos.x, vtx2_pos.x });
	aabb.bound_min.y = std::min({ vtx0_pos.y, vtx1_pos.y, vtx2_pos.y });
	aabb.bound_min.z = std::min({ vtx0_pos.z, vtx1_pos.z, vtx2_pos.z });

	aabb.bound_max.x = std::max({ vtx0_pos.x, vtx1_pos.x, vtx2_pos.x });
	aabb.bound_max.y = std::max({ vtx0_pos.y, vtx1_pos.y, vtx2_pos.y });
	aabb.bound_max.z = std::max({ vtx0_pos.z, vtx1_pos.z, vtx2_pos.z });

	return aabb;
}

SAabb Combine(const SAabb& aabb0, const SAabb& aabb1)
{
	SAabb combined_aabb;

	combined_aabb.bound_min.x = std::min(aabb0.bound_min.x, aabb1.bound_min.x);
	combined_aabb.bound_min.y = std::min(aabb0.bound_min.y, aabb1.bound_min.y);
	combined_aabb.bound_min.z = std::min(aabb0.bound_min.z, aabb1.bound_min.z);

	combined_aabb.bound_max.x = std::max(aabb0.bound_max.x, aabb1.bound_max.x);
	combined_aabb.bound_max.y = std::max(aabb0.bound_max.y, aabb1.bound_max.y);
	combined_aabb.bound_max.z = std::max(aabb0.bound_max.z, aabb1.bound_max.z);

	return combined_aabb;
}

SAabb Combine(const SAabb& aabb, const F32x3& point)
{
	SAabb combined_aabb;

	combined_aabb.bound_min.x = std::min(aabb.bound_min.x, point.x);
	combined_aabb.bound_min.y = std::min(aabb.bound_min.y, point.y);
	combined_aabb.bound_min.z = std::min(aabb.bound_min.z, point.z);

	combined_aabb.bound_max.x = std::max(aabb.bound_max.x, point.x);
	combined_aabb.bound_max.y = std::max(aabb.bound_max.y, point.y);
	combined_aabb.bound_max.z = std::max(aabb.bound_max.z, point.z);

	return combined_aabb;
}


F32 ComputeSurfaceArea(const SAabb& aabb)
{
	F32x3 v = aabb.bound_max - aabb.bound_min;

	return 2.0f * (v.x * v.y + v.x * v.z + v.y * v.z);
}

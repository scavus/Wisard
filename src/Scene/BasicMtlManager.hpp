/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "GfxResourceManager.hpp"
#include "Material.hpp"

using SBasicMtlId = WrResourceId;

struct SBasicMtlContainer
{
		TTrackedResourceArray<SBasicMtl> basic_mtl_arr;
};

template <class DeviceManagerBackend_T>
class TBasicMtlBackendManager
{
	friend class CBasicMtlManager;

	public:
		TBasicMtlBackendManager(){};
		~TBasicMtlBackendManager(){};

		TBasicMtlBackendManager(DeviceManagerBackend_T* device_manager_ptr, const SBasicMtlContainer* resource_container_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_resource_container_ptr = resource_container_ptr;
		};

		SBufferHdl GetBasicMtlBufferHdl() { return _basic_mtl_buffer_hdl; };

		void CreateBuffers();

		void Update();

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;

		SBufferHdl _basic_mtl_buffer_hdl;

		const SBasicMtlContainer* _resource_container_ptr;
};


class CBasicMtlManager : public TGfxResourceManager<SBasicMtlContainer, TBasicMtlBackendManager>
{
	public:
		CBasicMtlManager();
		~CBasicMtlManager();

		CBasicMtlManager(SDeviceRegistry* device_registry_ptr, SBasicMtlContainer* resource_container_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			CreateBackendManagers(device_registry_ptr);
		};

		void Init()
		{
			CreateBackendBuffers();
		};

		// SBasicMtl& GetBasicMtlRef(SBasicMtlId resource_id);
		const TTrackedResourceArray<SBasicMtl>& GetBasicMtlArrayConstRef() const { return _resource_container_ptr->basic_mtl_arr; };

		SBasicMtlId AddBasicMtl(SBasicMtl& iBasicMtl);
		void DeleteBasicMtl(SBasicMtlId resource_id);

		void Update();

	protected:

	private:

};

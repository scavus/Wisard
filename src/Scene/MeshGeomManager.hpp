/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "Scene/Mesh.hpp"
#include "Scene/Geometry.hpp"
#include "Scene/GfxResourceManager.hpp"

struct SSceneContext;

class CMeshGeomManager
{
	public:
		using SStorage = SMeshGeomContainer;

		CMeshGeomManager(){};
		
		CMeshGeomManager(SMeshGeomContainer* resource_container_ptr, SSceneContext* scene_ctx_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
		};

		virtual ~CMeshGeomManager()
		{
			_resource_container_ptr->mesh_geom.clear();
			_resource_container_ptr->transformed_geom.clear();
		};

		void Update();

		const std::vector<WrTriangle>& GetTransformedMeshGeomConstRef() const { return _resource_container_ptr->transformed_geom; };

	protected:

	private:
		SMeshGeomContainer* _resource_container_ptr;
		SSceneContext* _scene_ctx_ptr;
};

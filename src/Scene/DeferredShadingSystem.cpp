/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "DeferredShadingSystem.hpp"
#include "Api/WrTypes.h"
#include "Scene/SceneContext.hpp"

template class TDeferredShadingSystemBackend<CDeviceManagerCuda>;
template class TDeferredShadingSystemBackend<CDeviceManagerOcl>;
template class TDeferredShadingSystemBackend<CDeviceManagerDebug>;

static const size_t SHADER_EVAL_PARAM_COUNT = 7 + WR_SHADER_TEX2D_SLOT_COUNT;
static const size_t SHADER_SAMPLE_PARAM_COUNT = 8 + WR_SHADER_TEX2D_SLOT_COUNT;

template<>
TDeferredShadingSystemBackend<CDeviceManagerCuda>::TDeferredShadingSystemBackend(
	CDeviceManagerCuda* device_manager_ptr, const SDeferredMtlContainer* resource_container_ptr)
{
	_device_manager_ptr = device_manager_ptr;
	_resource_container_ptr = resource_container_ptr;

	// TODO: Save program_hdl
	auto program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KBuiltinShaders.cubin");

	SKernelHdl eval_kernel_hdl=	_device_manager_ptr->CreateKernel(program_hdl, "LambertShaderEval", SHADER_EVAL_PARAM_COUNT);
	SKernelHdl sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "LambertShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });

	eval_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "GgxShaderEval", SHADER_EVAL_PARAM_COUNT);
	sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "GgxShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });

	eval_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "MirrorShaderEval", SHADER_EVAL_PARAM_COUNT);
	sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "MirrorShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });

	eval_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "BeckmannShaderEval", SHADER_EVAL_PARAM_COUNT);
	sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "BeckmannShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });
}

template<>
TDeferredShadingSystemBackend<CDeviceManagerOcl>::TDeferredShadingSystemBackend(
	CDeviceManagerOcl* device_manager_ptr, const SDeferredMtlContainer* resource_container_ptr)
{
	_device_manager_ptr = device_manager_ptr;
	_resource_container_ptr = resource_container_ptr;

	std::vector<const char*> filenames;
	filenames.push_back("data/Kernels/KBuiltinShaders.c");

	const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;
	auto program_hdl = _device_manager_ptr->CompileProgram(filenames, compiler_options);

	SKernelHdl eval_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "LambertShaderEval", SHADER_EVAL_PARAM_COUNT);
	SKernelHdl sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "LambertShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });

	eval_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "GgxShaderEval", SHADER_EVAL_PARAM_COUNT);
	sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "GgxShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });

	eval_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "MirrorShaderEval", SHADER_EVAL_PARAM_COUNT);
	sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "MirrorShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });

	eval_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "BeckmannShaderEval", SHADER_EVAL_PARAM_COUNT);
	sample_kernel_hdl = _device_manager_ptr->CreateKernel(program_hdl, "BeckmannShaderSample", SHADER_SAMPLE_PARAM_COUNT);
	_shading_kernels.Append({ eval_kernel_hdl, sample_kernel_hdl });
}

template<>
TDeferredShadingSystemBackend<CDeviceManagerDebug>::TDeferredShadingSystemBackend(
	CDeviceManagerDebug* device_manager_ptr, const SDeferredMtlContainer* resource_container_ptr)
{
	_device_manager_ptr = device_manager_ptr;
	_resource_container_ptr = resource_container_ptr;
}

template <class DeviceManagerBackend_T>
void TDeferredShadingSystemBackend<DeviceManagerBackend_T>::Eval(SShaderEvalData& eval_data)
{
	auto& mtls = _resource_container_ptr->mtl_array.data;
	auto texture2d_backend_manager_ptr = _scene_ctx_ptr->texture2d_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	U32 block_size = _device_manager_ptr->GetWarpSize();

	for (U32 i = 0; i < mtls.size(); i++)
	{
		SSubstreamInfo substream_info = eval_data.shading_substream_infos[i];

		if (substream_info.counter != 0)
		{
			auto& mtl = mtls[i];
			auto shading_kernel_hdl = _shading_kernels[mtl.shader_id].eval_kernel_hdl;
			auto uniform_buffer_hdl = _uniform_buffer_hdls.data[i];

			U32 arg_idx = 0;
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, eval_data.shader_globals_buf);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, eval_data.rngs_buf);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, eval_data.sample_infos_buf);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, eval_data.shading_stream_buf);
			_device_manager_ptr->SetKernelArgValue(shading_kernel_hdl, arg_idx++, sizeof(U32), &substream_info.counter);
			_device_manager_ptr->SetKernelArgValue(shading_kernel_hdl, arg_idx++, sizeof(U32), &substream_info.offset);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, uniform_buffer_hdl);

			for (U32 i = 0; i < WR_SHADER_TEX2D_SLOT_COUNT; i++)
			{
				STexture2dId tex_id = mtl.tex_slots[i];
				STextureHdl tex_hdl = 0;

				if (tex_id.idx != WR_RESOURCE_ID_NULL.idx)
				{
					tex_hdl = texture2d_backend_manager_ptr->GetTextureHdl(tex_id);
				}

				_device_manager_ptr->SetKernelArgTexture(shading_kernel_hdl, arg_idx++, tex_hdl);
			}

			_device_manager_ptr->DispatchKernel(shading_kernel_hdl,
			{ static_cast<U32>(std::ceil(static_cast<F32>(substream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
			{ block_size, 1, 1 });

			_device_manager_ptr->Synchronize();
		}
	}
}

template <class DeviceManagerBackend_T>
void TDeferredShadingSystemBackend<DeviceManagerBackend_T>::Sample(SShaderSampleData& sample_data)
{
	auto& mtls = _resource_container_ptr->mtl_array.data;
	auto texture2d_backend_manager_ptr = _scene_ctx_ptr->texture2d_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	U32 block_size = _device_manager_ptr->GetWarpSize();

	for (U32 i = 0; i < mtls.size(); i++)
	{
		SSubstreamInfo substream_info = sample_data.shading_substream_infos[i];

		if (substream_info.counter != 0)
		{
			auto& mtl = mtls[i];
			auto shading_kernel_hdl = _shading_kernels[mtl.shader_id].sample_kernel_hdl;
			auto uniform_buffer_hdl = _uniform_buffer_hdls.data[i];

			U32 arg_idx = 0;
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, sample_data.shader_globals_buf);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, sample_data.rngs_buf);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, sample_data.rand_samples_buf);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, sample_data.sample_infos_buf);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, sample_data.shading_stream_buf);
			_device_manager_ptr->SetKernelArgValue(shading_kernel_hdl, arg_idx++, sizeof(U32), &substream_info.counter);
			_device_manager_ptr->SetKernelArgValue(shading_kernel_hdl, arg_idx++, sizeof(U32), &substream_info.offset);
			_device_manager_ptr->SetKernelArgBuffer(shading_kernel_hdl, arg_idx++, uniform_buffer_hdl);

			for (U32 i = 0; i < WR_SHADER_TEX2D_SLOT_COUNT; i++)
			{
				STexture2dId tex_id = mtl.tex_slots[i];
				STextureHdl tex_hdl = 0;

				if (tex_id.idx != WR_RESOURCE_ID_NULL.idx)
				{
					tex_hdl = texture2d_backend_manager_ptr->GetTextureHdl(tex_id);
				}

				_device_manager_ptr->SetKernelArgTexture(shading_kernel_hdl, arg_idx++, tex_hdl);
			}

			_device_manager_ptr->DispatchKernel(shading_kernel_hdl,
			{ static_cast<U32>(std::ceil(static_cast<F32>(substream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
			{ block_size, 1, 1 });

			_device_manager_ptr->Synchronize();
		}
	}
}

template <class DeviceManagerBackend_T>
void TDeferredShadingSystemBackend<DeviceManagerBackend_T>::Update()
{
	for (U32 i = 0; i < _resource_container_ptr->mtl_array.data.size(); i++)
	{
		SBufferHdl& h = _uniform_buffer_hdls.data[i];
		WrMaterial mtl = _resource_container_ptr->mtl_array.data[i];

		if (h == 0)
		{
			h = _device_manager_ptr->CreateBuffer(mtl.uniform_buffer_size, mtl.uniform_buffer_data);
		}
		else
		{
			_device_manager_ptr->DestroyBuffer(h);
			h = _device_manager_ptr->CreateBuffer(mtl.uniform_buffer_size, mtl.uniform_buffer_data);
		}

	}
}

SDeferredMtlId CDeferredShadingSystem::CreateMaterial(WrMaterial& resource)
{
	SDeferredMtlId id = _resource_container_ptr->mtl_array.Append(resource);

	WrMaterial& mtl = _resource_container_ptr->mtl_array[id];

	// _scene_ctx_ptr->deferred_shading_system_ptr->Bind(&mtl.shader_id);

	for (U32 i = 0; i < WR_SHADER_TEX2D_SLOT_COUNT; i++)
	{
		if (mtl.tex_slots[i].idx != WR_RESOURCE_ID_NULL.idx)
		{
			_scene_ctx_ptr->texture2d_manager_ptr->BindTexture2d(&mtl.tex_slots[i]);
		}
	}

	for (auto cuda_backend_manager : _cuda_backend_manager_ptrs)
	{
		cuda_backend_manager.second->_uniform_buffer_hdls.Append(0);
	}

	for (auto ocl_backend_manager : _ocl_backend_manager_ptrs)
	{
		ocl_backend_manager.second->_uniform_buffer_hdls.Append(0);
	}

	_resource_container_ptr->mtl_array.update_flag = true;

	return id;
}

void CDeferredShadingSystem::DestroyMaterial(SDeferredMtlId& resource_id)
{
	WrMaterial& mtl = _resource_container_ptr->mtl_array[resource_id];

	// _scene_ctx_ptr->deferred_shading_system_ptr->Unbind(&mtl.shader_id);

	for (U32 i = 0; i < WR_SHADER_TEX2D_SLOT_COUNT; i++)
	{
		if (mtl.tex_slots[i].idx != WR_RESOURCE_ID_NULL.idx)
		{
			_scene_ctx_ptr->texture2d_manager_ptr->UnbindTexture2d(&mtl.tex_slots[i]);
		}
	}

	_resource_container_ptr->mtl_array.Delete(resource_id);

	for (auto cuda_backend_manager : _cuda_backend_manager_ptrs)
	{
		cuda_backend_manager.second->_uniform_buffer_hdls.Delete(resource_id);
	}

	for (auto ocl_backend_manager : _ocl_backend_manager_ptrs)
	{
		ocl_backend_manager.second->_uniform_buffer_hdls.Delete(resource_id);
	}

}

void CDeferredShadingSystem::SetMaterial(SDeferredMtlId resource_id, WrMaterial& resource)
{
	WrMaterial& mtl = _resource_container_ptr->mtl_array[resource_id];

	for (U32 i = 0; i < WR_SHADER_TEX2D_SLOT_COUNT; i++)
	{
		if (mtl.tex_slots[i].idx != WR_RESOURCE_ID_NULL.idx)
		{
			_scene_ctx_ptr->texture2d_manager_ptr->UnbindTexture2d(&mtl.tex_slots[i]);
		}
	}

	mtl = std::move(resource);

	for (U32 i = 0; i < WR_SHADER_TEX2D_SLOT_COUNT; i++)
	{
		if (mtl.tex_slots[i].idx != WR_RESOURCE_ID_NULL.idx)
		{
			_scene_ctx_ptr->texture2d_manager_ptr->BindTexture2d(&mtl.tex_slots[i]);
		}
	}

	_resource_container_ptr->mtl_array.update_flag = true;
}

void CDeferredShadingSystem::Update()
{
	if (_resource_container_ptr->mtl_array.update_flag)
	{
		UpdateBackends();

		_resource_container_ptr->mtl_array.update_flag = false;
	}
}

void CDeferredShadingSystem::BindMaterial(SDeferredMtlId* resource_id_ptr)
{
	_resource_container_ptr->mtl_array.trackers[resource_id_ptr->idx].insert({ resource_id_ptr, resource_id_ptr });
}

void CDeferredShadingSystem::UnbindMaterial(SDeferredMtlId* resource_id_ptr)
{
	_resource_container_ptr->mtl_array.trackers[resource_id_ptr->idx].erase(resource_id_ptr);
}

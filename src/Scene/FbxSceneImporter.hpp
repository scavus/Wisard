/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "fbxsdk.h"
#include "Scene.hpp"
#include "Geometry.hpp"
#include "Utility/CommonTypes.hpp"
#include "Utility/Math.hpp"
#include "Mesh.hpp"

namespace FBX
{
	class CSceneImporter
	{
		public:

			CSceneImporter();
			~CSceneImporter(){};

			void ImportScene(const char* filename, SMeshContainer* oMeshContainer, SMeshGeomContainer* oMeshGeomContainer);

		private:

			void TraverseSceneGraph_R(FbxNode* InFBXNode, SMeshContainer* oMeshContainer, SMeshGeomContainer* oMeshGeomContainer);

			void LoadMesh(FbxNode* InFBXNode, U32 InMeshID, SMeshContainer* oMeshContainer, SMeshGeomContainer* oMeshGeomContainer);

			// void LoadLight(FbxNode* InFBXNode, FScene* OutScene);
			// void LoadCamera(FbxNode* InFBXNode, FScene* OutScene);

			FbxManager* mpFBXManager;
			FbxImporter* mpFBXImporter;
			FbxScene* mpFBXScene;
	};

}

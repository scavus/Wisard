/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>

#include "Api/WrCommon.h"
#include "Api/WrTypes.h"
#include "RenderDevice/ComputeDeviceManager.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "Scene/GfxResourceManager.hpp"

struct SSceneContext;

struct SCameraContainer
{
	WrCamera camera;
	bool update_flag;
};

template<class DeviceManagerBackend_T>
class TCameraBackendManager
{
	friend class CCameraManager;

	public:
		TCameraBackendManager(){};

		~TCameraBackendManager()
		{
			_device_manager_ptr->DestroyBuffer(_camera_buffer_hdl);
		};

		TCameraBackendManager(DeviceManagerBackend_T* device_manager_ptr, const SCameraContainer* resource_container_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_resource_container_ptr = resource_container_ptr;
			_camera_buffer_hdl = 0;
		};

		void SetResourceContainerPtr(const SCameraContainer* resource_container_ptr) { _resource_container_ptr = resource_container_ptr; };

		SBufferHdl GetCameraBufferHdl()	{ return _camera_buffer_hdl; };

		void Update();

		void CreateBuffers();

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;

		SBufferHdl _camera_buffer_hdl;

		const SCameraContainer* _resource_container_ptr;
};

class CCameraManager : public TGfxResourceManager<SCameraContainer, TCameraBackendManager>
{
	public:
		using SStorage = SCameraContainer;

		CCameraManager(){};
		virtual ~CCameraManager(){};

		CCameraManager(SDeviceRegistry* device_registry_ptr, SCameraContainer* resource_container_ptr, SSceneContext* scene_ctx_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			CreateBackendManagers(device_registry_ptr);
		};

		WrCamera& GetCameraRef();
		WrCamera& GetCameraConstRef();

		void SetCamera(WrCamera& camera);

		void Update();

	protected:

	private:
		SSceneContext* _scene_ctx_ptr;
};

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "EnvLightManager.hpp"
#include "Scene/SceneContext.hpp"

namespace EnvLightUtils
{
	void Preprocess(WrEnvLight& env_light)
	{
		if (!env_light.data)
		{
			return;
		}

		F32* data = static_cast<F32*>(env_light.data);
		U32 width = env_light.width;
		U32 height = env_light.height;
		U32 channel_count = 4;

		U32 k = 0;
		F32 angle_frac = MathUtils::WR_PI / static_cast<F32>(height);
		F32 theta = angle_frac * 0.5f;
		F32* sin_thetas = new F32[height];

		for (U32 i = 0; i < height; i++, theta += angle_frac)
		{
			sin_thetas[i] = std::sin(theta);
		}

		F32* buffer = new F32[width * (height + 1)];
		F32* marginal_cdf = &buffer[width * height];
		F32* conditional_cdfs = buffer;

		for (U32 i = 0, m = 0; i < width; i++, m += height)
		{
			F32* conditional_cdf = &conditional_cdfs[m];
			k = i * channel_count;
			conditional_cdf[0] = 0.2126f * data[k + 0] + 0.7152f * data[k + 1] + 0.0722f * data[k + 2];
			conditional_cdf[0] *= sin_thetas[0];

			for (U32 j = 1, k = (width + i) * channel_count; j < height; j++, k += width * channel_count)
			{
				F32 luminance = 0.2126f * data[k + 0] + 0.7152f * data[k + 1] + 0.0722f * data[k + 2];
				luminance *= sin_thetas[j];
				conditional_cdf[j] = conditional_cdf[j - 1] + luminance;
			}

			if (i == 0)
			{
				marginal_cdf[i] = conditional_cdf[height - 1];
			}
			else
			{
				marginal_cdf[i] = marginal_cdf[i - 1] + conditional_cdf[height - 1];
			}
		}

		env_light.conditional_cdfs = conditional_cdfs;
		env_light.marginal_cdf = marginal_cdf;
	}
};

template<class DeviceManagerBackend_T>
void TEnvLightBackendManager<DeviceManagerBackend_T>::Update()
{
	CreateBuffers();
}

template<class DeviceManagerBackend_T>
void TEnvLightBackendManager<DeviceManagerBackend_T>::CreateBuffers()
{
	struct SEnvLightGpu
	{
		WrMatrix4x4 obj_to_world_mtx;
		WrMatrix4x4 world_to_obj_mtx;

		U32 width;
		U32 height;
	};

	size_t buffer_size = 0;

	WrEnvLight env_light = _resource_container_ptr->env_light;

	if (!env_light.data)
	{
		_env_light_buffer_hdl = 0;
		_env_map_texture_hdl = 0;
		_env_light_conditional_cdf_buffer_hdl = 0;
		_env_light_marginal_cdf_buffer_hdl = 0;
		return;
	}

	SEnvLightGpu env_light_gpu;
	env_light_gpu.width = env_light.width;
	env_light_gpu.height = env_light.height;

	buffer_size = sizeof(SEnvLightGpu);
	_env_light_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &env_light_gpu);

	buffer_size = sizeof(F32) * env_light.width * env_light.height;
	_env_light_conditional_cdf_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, env_light.conditional_cdfs);

	buffer_size = sizeof(F32) * env_light.width;
	_env_light_marginal_cdf_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, env_light.marginal_cdf);

	STexture2dDesc desc;
	desc.width = env_light.width;
	desc.height = env_light.height;
	desc.usage = ETextureUsage::READ_ONLY;
	desc.format = ETextureFormat::RGBA_F32;

	_env_map_texture_hdl = _device_manager_ptr->CreateTexture2D(desc, env_light.data);
}

void CEnvLightManager::Update()
{
	if (_resource_container_ptr->update_flag)
	{
		EnvLightUtils::Preprocess(_resource_container_ptr->env_light);
		UpdateBackends();
		_resource_container_ptr->update_flag = false;
	}
}

void CEnvLightManager::SetEnvLight(const WrEnvLight& env_light)
{
	_resource_container_ptr->env_light = std::move(env_light);
	_resource_container_ptr->update_flag = true;
}

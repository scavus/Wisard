/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Api/WrCommon.h"
#include "Api/WrTypes.h"
#include "Scene/GfxResourceManager.hpp"

struct SSceneContext;

struct SEnvLightContainer
{
    WrEnvLight env_light;
	bool update_flag;

	SEnvLightContainer()
	{
		env_light.data = nullptr;
		update_flag = true;
	};
};

template<class DeviceManagerBackend_T>
class TEnvLightBackendManager
{
    public:
		TEnvLightBackendManager(){};

		~TEnvLightBackendManager()
		{
			_device_manager_ptr->DestroyBuffer(_env_light_buffer_hdl);
			_device_manager_ptr->DestroyTexture2D(_env_map_texture_hdl);
			_device_manager_ptr->DestroyBuffer(_env_light_conditional_cdf_buffer_hdl);
			_device_manager_ptr->DestroyBuffer(_env_light_marginal_cdf_buffer_hdl);
		};

		TEnvLightBackendManager(DeviceManagerBackend_T* device_manager_ptr, const SEnvLightContainer* resource_container_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_resource_container_ptr = resource_container_ptr;
		};

		void CreateBuffers();
		void Update();

		SBufferHdl GetEnvLightBufferHdl() { return _env_light_buffer_hdl; };
		STextureHdl GetEnvMapTextureHdl() { return _env_map_texture_hdl; };
		SBufferHdl GetEnvLightConditionalCdfBufferHdl() { return _env_light_conditional_cdf_buffer_hdl; };
		SBufferHdl GetEnvLightMarginalCdfBufferHdl() { return _env_light_marginal_cdf_buffer_hdl; };

    protected:

    private:
		DeviceManagerBackend_T* _device_manager_ptr;
		const SEnvLightContainer* _resource_container_ptr;

		SBufferHdl _env_light_buffer_hdl;
		STextureHdl _env_map_texture_hdl;
		SBufferHdl _env_light_conditional_cdf_buffer_hdl;
		SBufferHdl _env_light_marginal_cdf_buffer_hdl;
};

class CEnvLightManager : public TGfxResourceManager<SEnvLightContainer, TEnvLightBackendManager>
{
	public:
		using SStorage = SEnvLightContainer;

		struct SMetadata
		{
			U32 env_light_count;
		};

		CEnvLightManager(){};

		virtual ~CEnvLightManager()
		{
			delete[] _resource_container_ptr->env_light.data;
			delete[] _resource_container_ptr->env_light.marginal_cdf;
			delete[] _resource_container_ptr->env_light.conditional_cdfs;
		};

		CEnvLightManager(SDeviceRegistry* device_registry_ptr, SEnvLightContainer* resource_container_ptr, SSceneContext* scene_ctx_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			CreateBackendManagers(device_registry_ptr);
		};
			
		void Update();

		void SetEnvLight(const WrEnvLight& env_light);

		SMetadata GetMetadata() 
		{ 
			SMetadata metadata;

			if (_resource_container_ptr->env_light.data)
			{
				metadata.env_light_count = 1;
			}
			else
			{
				metadata.env_light_count = 0;
			}

			return metadata;
		};



	protected:

	private:
		SSceneContext* _scene_ctx_ptr;
};

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "CameraManager.hpp"
#include "Scene/SceneContext.hpp"

template class TCameraBackendManager<CDeviceManagerCuda>;
template class TCameraBackendManager<CDeviceManagerOcl>;
template class TCameraBackendManager<CDeviceManagerDebug>;

template<class DeviceManagerBackend_T>
void TCameraBackendManager<DeviceManagerBackend_T>::CreateBuffers()
{
	size_t buffer_size;

	const WrCamera& camera = _resource_container_ptr->camera;

	buffer_size = sizeof(WrCamera);
	_camera_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &camera);
}

template<class DeviceManagerBackend_T>
void TCameraBackendManager<DeviceManagerBackend_T>::Update()
{
	if (_camera_buffer_hdl == 0)
	{
		CreateBuffers();
	}
	else
	{
		const WrCamera& camera = _resource_container_ptr->camera;
		_device_manager_ptr->WriteBuffer(_camera_buffer_hdl, &camera);
	}
}

WrCamera& CCameraManager::GetCameraRef()
{
	_resource_container_ptr->update_flag = true;

	return _resource_container_ptr->camera;
}

void CCameraManager::Update()
{
	if (_resource_container_ptr->update_flag)
	{
		UpdateBackends();
		_resource_container_ptr->update_flag = false;
	}
}

void CCameraManager::SetCamera(WrCamera& camera)
{
	_resource_container_ptr->camera = std::move(camera);
	_resource_container_ptr->update_flag = true;
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "Texture2dManager.hpp"
#include "Scene/SceneContext.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"

template class TTexture2dBackendManager<CDeviceManagerCuda>;
template class TTexture2dBackendManager<CDeviceManagerOcl>;
template class TTexture2dBackendManager<CDeviceManagerDebug>;

STexture2dId CTexture2dManager::CreateTexture2d(WrTexture2d& resource)
{
	STexture2dId id;
	id = _resource_container_ptr->tex2d_arr.Append(resource);

	for (auto backend_manager_ptr : _cuda_backend_manager_ptrs)
	{
		auto hdl = backend_manager_ptr.second->_device_manager_ptr->CreateTexture2D(resource.desc, resource.data);

		backend_manager_ptr.second->_tex2d_hdls.push_back(hdl);
	}

	for (auto backend_manager_ptr : _ocl_backend_manager_ptrs)
	{
		auto hdl = backend_manager_ptr.second->_device_manager_ptr->CreateTexture2D(resource.desc, resource.data);
		backend_manager_ptr.second->_tex2d_hdls.push_back(hdl);
	}

	return id;
}

template <class DeviceManagerBackend_T>
void TTexture2dBackendManager<DeviceManagerBackend_T>::DestroyTexture2d(STexture2dId resource_id)
{
	_device_manager_ptr->DestroyTexture2D(_tex2d_hdls[resource_id.idx]);
	std::swap(_tex2d_hdls[resource_id.idx], _tex2d_hdls.back());
	_tex2d_hdls.pop_back();
}

void CTexture2dManager::DestroyTexture2d(STexture2dId resource_id)
{
	_resource_container_ptr->tex2d_arr.Delete(resource_id);

	for (auto backend_manager_ptr: _cuda_backend_manager_ptrs)
	{
		backend_manager_ptr.second->DestroyTexture2d(resource_id);
	}

	for (auto backend_manager_ptr : _ocl_backend_manager_ptrs)
	{
		backend_manager_ptr.second->DestroyTexture2d(resource_id);
	}
}

void CTexture2dManager::BindTexture2d(STexture2dId* resource_id_ptr)
{
	_resource_container_ptr->tex2d_arr.trackers[resource_id_ptr->idx].insert({ resource_id_ptr, resource_id_ptr });
}

void CTexture2dManager::UnbindTexture2d(STexture2dId* resource_id_ptr)
{
	_resource_container_ptr->tex2d_arr.trackers[resource_id_ptr->idx].erase(resource_id_ptr);
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Mesh.hpp"
#include "GfxResourceManager.hpp"
#include "MeshGeomManager.hpp"

struct SSceneContext;

using SMeshId = WrResourceId;

template<class DeviceManagerBackend_T>
class TMeshBackendManager
{
	friend class CMeshManager;

	public:
		TMeshBackendManager(){};
		
		TMeshBackendManager(DeviceManagerBackend_T* device_manager_ptr, const SMeshContainer* resource_container_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_resource_container_ptr = resource_container_ptr;
			_mesh_buffer_hdl = 0;
		};

		~TMeshBackendManager()
		{
			_device_manager_ptr->DestroyBuffer(_mesh_buffer_hdl);
		};

		SBufferHdl GetMeshBufferHdl() { return _mesh_buffer_hdl; };

		void CreateBuffers();
		void Update();

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;
		SBufferHdl _mesh_buffer_hdl;
		const SMeshContainer* _resource_container_ptr;
};


class CMeshManager : public TGfxResourceManager<SMeshContainer, TMeshBackendManager>
{
	public:
		using SStorage = SMeshContainer;

		CMeshManager(){};

		CMeshManager(SDeviceRegistry* device_registry_ptr, SMeshContainer* resource_container_ptr, SSceneContext* scene_ctx_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			CreateBackendManagers(device_registry_ptr);
		};
		
		virtual ~CMeshManager()
		{
			_resource_container_ptr->mesh_arr.Clear();
		};

		const TTrackedResourceArray<SMesh>& GetMeshArrayConstRef() const { return _resource_container_ptr->mesh_arr; };

		void Update();

		void SetMesh(SMeshId resource_id, SMesh& resource);
		void SetMeshMaterial(SMeshId mesh_id, SDeferredMtlId mtl_id);

	protected:

	private:
		CMeshGeomManager* _mesh_geom_manager_ptr;
		SSceneContext* _scene_ctx_ptr;
};

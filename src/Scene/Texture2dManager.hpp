/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Api/WrCommon.h"
#include "Api/WrTypes.h"
#include "Scene/GfxResourceManager.hpp"

struct SSceneContext;

struct STexture2dTag{};

using STexture2dId = WrResourceId;

struct STexture2dContainer
{
	TTrackedResourceArray<WrTexture2d> tex2d_arr;
};

template <class DeviceManagerBackend_T>
class TTexture2dBackendManager
{
	friend class CTexture2dManager;
	public:

		TTexture2dBackendManager(){};

		TTexture2dBackendManager(DeviceManagerBackend_T* device_manager_ptr, const STexture2dContainer* resource_container_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_resource_container_ptr = resource_container_ptr;
		};

		~TTexture2dBackendManager()
		{
			for (auto h : _tex2d_hdls)
			{
				_device_manager_ptr->DestroyTexture2D(h);
			}
		};

		STextureHdl GetTextureHdl(STexture2dId resource_id)
		{
			return _tex2d_hdls[resource_id.idx];
		};

		void CreateBuffers(){};
		void Update(){};

		void DestroyTexture2d(STexture2dId resource_id);

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;

		std::vector<STextureHdl> _tex2d_hdls;

		const STexture2dContainer* _resource_container_ptr;
};


class CTexture2dManager : public TGfxResourceManager<STexture2dContainer, TTexture2dBackendManager>
{
	public:
		using SStorage = STexture2dContainer;

		CTexture2dManager(){};

		CTexture2dManager(SDeviceRegistry* device_registry_ptr, STexture2dContainer* resource_container_ptr, SSceneContext* scene_ctx_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			CreateBackendManagers(device_registry_ptr);
		};

		virtual ~CTexture2dManager()
		{
			_resource_container_ptr->tex2d_arr.Clear();
		};

		STexture2dId CreateTexture2d(WrTexture2d& resource);
		void DestroyTexture2d(STexture2dId resource_id);
		void BindTexture2d(STexture2dId* resource_id_ptr);
		void UnbindTexture2d(STexture2dId* resource_id_ptr);

	protected:

	private:
		SSceneContext* _scene_ctx_ptr;
};

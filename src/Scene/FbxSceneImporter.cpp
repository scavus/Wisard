/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "FbxSceneImporter.hpp"
#include "fbxsdk.h"

namespace FBX
{
	CSceneImporter::CSceneImporter()
	{
		mpFBXManager = FbxManager::Create();

		FbxIOSettings* pFBXIOSettings = FbxIOSettings::Create(mpFBXManager, IOSROOT);
		mpFBXManager->SetIOSettings(pFBXIOSettings);

		mpFBXImporter = FbxImporter::Create(mpFBXManager, "");
		mpFBXScene = FbxScene::Create(mpFBXManager, "");
	}

	void CSceneImporter::ImportScene(const char* filename, SMeshContainer* oMeshContainer, SMeshGeomContainer* oMeshGeomContainer)
	{
		bool bSuccess;
		bSuccess = mpFBXImporter->Initialize(filename, -1, mpFBXManager->GetIOSettings());

		if (!bSuccess)
		{
			// TODO: Return Code
			return;
		}

		bSuccess = mpFBXImporter->Import(mpFBXScene);
		if (!bSuccess)
		{
			// TODO: Return Code
			return;
		}

		FbxAxisSystem::OpenGL.ConvertScene(mpFBXScene);
		// FbxAxisSystem::MayaYUp.ConvertScene(mpFBXScene);
		FbxNode* pFBXRootNode = mpFBXScene->GetRootNode();

		if (pFBXRootNode)
		{
			for (int ChildIdx = 0; ChildIdx < pFBXRootNode->GetChildCount(); ChildIdx++)
			{
				TraverseSceneGraph_R(pFBXRootNode->GetChild(ChildIdx), oMeshContainer, oMeshGeomContainer);
			}
		}
	}

	// Traverse the scenegraph and load scene data
	void CSceneImporter::TraverseSceneGraph_R(FbxNode* iFbxNode, SMeshContainer* oMeshContainer, SMeshGeomContainer* oMeshGeomContainer)
	{
		// TODO: Load Lights
		// TODO: Load Camera
		// TODO: Get custom user attributes

		FbxNodeAttribute::EType NodeType = iFbxNode->GetNodeAttribute()->GetAttributeType();

		static U32 mesh_id = 0;

		switch (NodeType)
		{
		case FbxNodeAttribute::eUnknown:
			break;

		case FbxNodeAttribute::eNull:
			break;

		case FbxNodeAttribute::eMarker:
			break;

		case FbxNodeAttribute::eSkeleton:
			break;

		case FbxNodeAttribute::eMesh:
		{
			LoadMesh(iFbxNode, mesh_id, oMeshContainer, oMeshGeomContainer);

			mesh_id++;

			break;
		}

		case FbxNodeAttribute::eNurbs:
			break;

		case FbxNodeAttribute::ePatch:
			break;

		case FbxNodeAttribute::eCamera:
		{
			// LoadCamera(iFbxNode, OutScene);
			break;
		}

		case FbxNodeAttribute::eCameraStereo:
			break;

		case FbxNodeAttribute::eCameraSwitcher:
			break;

		case FbxNodeAttribute::eLight:
		{
			// LoadLight(iFbxNode, OutScene);
			break;
		}

		case FbxNodeAttribute::eOpticalReference:
			break;

		case FbxNodeAttribute::eOpticalMarker:
			break;

		case FbxNodeAttribute::eNurbsCurve:
			break;

		case FbxNodeAttribute::eTrimNurbsSurface:
			break;

		case FbxNodeAttribute::eBoundary:
			break;

		case FbxNodeAttribute::eNurbsSurface:
			break;

		case FbxNodeAttribute::eShape:
			break;

		case FbxNodeAttribute::eLODGroup:
			break;

		case FbxNodeAttribute::eSubDiv:
			break;

		case FbxNodeAttribute::eCachedEffect:
			break;

		case FbxNodeAttribute::eLine:
			break;

		default:
			break;
		}


	}

	void CSceneImporter::LoadMesh(FbxNode* iFbxNode, U32 iMeshId, SMeshContainer* oMeshContainer, SMeshGeomContainer* oMeshGeomContainer)
	{
		FbxMesh* pFBXMesh = iFbxNode->GetMesh();


		FbxMatrix GlobalTransform = pFBXMesh->GetNode()->GetScene()->GetAnimationEvaluator()->GetNodeGlobalTransform(pFBXMesh->GetNode());

		FbxMatrix InvGlobalTransform = GlobalTransform.Inverse();

		FbxVector4 Row0 = GlobalTransform.GetRow(0);
		FbxVector4 Row1 = GlobalTransform.GetRow(1);
		FbxVector4 Row2 = GlobalTransform.GetRow(2);
		FbxVector4 Row3 = GlobalTransform.GetRow(3);

		WrVector4f R0{ (F32)Row0.mData[0], (F32)Row0.mData[1], (F32)Row0.mData[2], (F32)Row0.mData[3] };
		WrVector4f R1{ (F32)Row1.mData[0], (F32)Row1.mData[1], (F32)Row1.mData[2], (F32)Row1.mData[3] };
		WrVector4f R2{ (F32)Row2.mData[0], (F32)Row2.mData[1], (F32)Row2.mData[2], (F32)Row2.mData[3] };
		WrVector4f R3{ (F32)Row3.mData[0], (F32)Row3.mData[1], (F32)Row3.mData[2], (F32)Row3.mData[3] };

		WrMatrix4x4 obj_to_world_mtx = MathUtils::MakeMatrix4x4(R0, R1, R2, R3);

		FbxVector4 InvRow0 = InvGlobalTransform.GetRow(0);
		FbxVector4 InvRow1 = InvGlobalTransform.GetRow(1);
		FbxVector4 InvRow2 = InvGlobalTransform.GetRow(2);
		FbxVector4 InvRow3 = InvGlobalTransform.GetRow(3);

		WrVector4f InvR0{ (F32)InvRow0.mData[0], (F32)InvRow0.mData[1], (F32)InvRow0.mData[2], (F32)InvRow0.mData[3] };
		WrVector4f InvR1{ (F32)InvRow1.mData[0], (F32)InvRow1.mData[1], (F32)InvRow1.mData[2], (F32)InvRow1.mData[3] };
		WrVector4f InvR2{ (F32)InvRow2.mData[0], (F32)InvRow2.mData[1], (F32)InvRow2.mData[2], (F32)InvRow2.mData[3] };
		WrVector4f InvR3{ (F32)InvRow3.mData[0], (F32)InvRow3.mData[1], (F32)InvRow3.mData[2], (F32)InvRow3.mData[3] };

		WrMatrix4x4 world_to_obj_mtx = MathUtils::MakeMatrix4x4(InvR0, InvR1, InvR2, InvR3);



		/*
		dvec4 c0 = glm::make_vec4((double*)globalTransform.GetColumn(0).Buffer());
		dvec4 c1 = glm::make_vec4((double*)globalTransform.GetColumn(1).Buffer());
		dvec4 c2 = glm::make_vec4((double*)globalTransform.GetColumn(2).Buffer());
		dvec4 c3 = glm::make_vec4((double*)globalTransform.GetColumn(3).Buffer());
		glm::mat4 convertMatr = mat4(c0, c1, c2, c3);
		convertMatr = inverse(convertMatr);
		*/


		/*
		F32 RotationDegX = iFbxNode->LclRotation.Get().mData[0];
		F32 RotationDegY = iFbxNode->LclRotation.Get().mData[1];
		F32 RotationDegZ = iFbxNode->LclRotation.Get().mData[2];

		F32 ScaleX = iFbxNode->LclScaling.Get().mData[0];
		F32 ScaleY = iFbxNode->LclScaling.Get().mData[1];
		F32 ScaleZ = iFbxNode->LclScaling.Get().mData[2];

		F32 TranslationX = iFbxNode->LclTranslation.Get().mData[0];
		F32 TranslationY = iFbxNode->LclTranslation.Get().mData[1];
		F32 TranslationZ = iFbxNode->LclTranslation.Get().mData[2];

		Math::Matrix4x4 RotationMatrix = Math::RotationX(RotationDegX) * Math::RotationY(RotationDegY) * Math::RotationZ(RotationDegZ);
		Math::Matrix4x4 InvRotationMatrix = Math::Transpose(RotationMatrix);

		Math::Matrix4x4 TranslationMatrix = Math::Translation(TranslationX, TranslationY, TranslationZ);
		Math::Matrix4x4 InvTranslationMatrix = Math::Translation(-TranslationX, -TranslationY, -TranslationZ);

		Math::Matrix4x4 ScaleMatrix = Math::Scale(ScaleX, ScaleY, ScaleZ);
		Math::Matrix4x4 InvScaleMatrix = Math::Scale(1.0f / ScaleX, 1.0f / ScaleY, 1.0f / ScaleZ);
		*/

		SMesh Mesh;
		Mesh.obj_to_world_mtx = MathUtils::Transpose(obj_to_world_mtx);
		Mesh.world_to_obj_mtx = MathUtils::Transpose(world_to_obj_mtx);

		/*
		Mesh.obj_to_world_mtx = Math::Transpose(ScaleMatrix * RotationMatrix * TranslationMatrix);
		Mesh.world_to_obj_mtx = Math::Transpose(InvTranslationMatrix * InvRotationMatrix * InvScaleMatrix);
		*/

		/*
		Mesh.obj_to_world_mtx = Math::Transpose(InvTranslationMatrix * InvRotationMatrix * InvScaleMatrix);
		Mesh.world_to_obj_mtx = Math::Transpose(ScaleMatrix * RotationMatrix * TranslationMatrix);
		*/



		oMeshContainer->mesh_arr.Append(Mesh);

		U32 prim_count = pFBXMesh->GetPolygonCount();
		U32 VertexIdx = 0;

		for (U32 TriangleIdx = 0; TriangleIdx < prim_count; TriangleIdx++)
		{
			WrTriangle triangle;
			triangle.mesh_id = iMeshId;

			for (U32 TriangleVertexIdx = 0; TriangleVertexIdx < 3; TriangleVertexIdx++)
			{
				WrVertex Vertex;

				U32 ControlPointIdx = pFBXMesh->GetPolygonVertex(TriangleIdx, TriangleVertexIdx);

				// Get Vertex position
				WrVector3f VertexPos;
				VertexPos.x = static_cast<F32>(pFBXMesh->GetControlPointAt(ControlPointIdx).mData[0]);
				VertexPos.y = static_cast<F32>(pFBXMesh->GetControlPointAt(ControlPointIdx).mData[1]);
				VertexPos.z = static_cast<F32>(pFBXMesh->GetControlPointAt(ControlPointIdx).mData[2]);

				Vertex.pos = VertexPos;

				// Math::Vector4 WSVertexPos = Math::Transpose(obj_to_world_mtx) * VertexPos;

				// Get Vertex normal
				fbxsdk_2015_1::FbxGeometryElementNormal* pFBXVertexNormal = pFBXMesh->GetElementNormal();

				fbxsdk_2015_1::FbxLayerElement::EMappingMode MappingMode = pFBXVertexNormal->GetMappingMode();
				fbxsdk_2015_1::FbxLayerElement::EReferenceMode ReferenceMode = pFBXVertexNormal->GetReferenceMode();

				switch (MappingMode)
				{
				case fbxsdk_2015_1::FbxLayerElement::eNone:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByControlPoint:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						WrVector3f VertexNormal;
						VertexNormal.x = static_cast<F32>(pFBXVertexNormal->GetDirectArray().GetAt(ControlPointIdx).mData[0]);
						VertexNormal.y = static_cast<F32>(pFBXVertexNormal->GetDirectArray().GetAt(ControlPointIdx).mData[1]);
						VertexNormal.z = static_cast<F32>(pFBXVertexNormal->GetDirectArray().GetAt(ControlPointIdx).mData[2]);

						Vertex.normal = VertexNormal;

						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
						break;

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygonVertex:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						WrVector3f VertexNormal;
						VertexNormal.x = static_cast<F32>(pFBXVertexNormal->GetDirectArray().GetAt(VertexIdx).mData[0]);
						VertexNormal.y = static_cast<F32>(pFBXVertexNormal->GetDirectArray().GetAt(VertexIdx).mData[1]);
						VertexNormal.z = static_cast<F32>(pFBXVertexNormal->GetDirectArray().GetAt(VertexIdx).mData[2]);

						Vertex.normal = VertexNormal;

						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
						break;

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygon:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByEdge:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eAllSame:
					break;

				default:
					break;
				}

				// Get Vertex tangent
				fbxsdk_2015_1::FbxGeometryElementTangent* pFBXVertexTangent = pFBXMesh->GetElementTangent(0);

				MappingMode = pFBXVertexTangent->GetMappingMode();
				ReferenceMode = pFBXVertexTangent->GetReferenceMode();

				switch (MappingMode)
				{
				case fbxsdk_2015_1::FbxLayerElement::eNone:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByControlPoint:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						WrVector3f VertexTangent;
						VertexTangent.x = static_cast<F32>(pFBXVertexTangent->GetDirectArray().GetAt(ControlPointIdx).mData[0]);
						VertexTangent.y = static_cast<F32>(pFBXVertexTangent->GetDirectArray().GetAt(ControlPointIdx).mData[1]);
						VertexTangent.z = static_cast<F32>(pFBXVertexTangent->GetDirectArray().GetAt(ControlPointIdx).mData[2]);

						Vertex.tangent = VertexTangent;

						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
						break;

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygonVertex:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						WrVector3f VertexTangent;
						VertexTangent.x = static_cast<F32>(pFBXVertexTangent->GetDirectArray().GetAt(VertexIdx).mData[0]);
						VertexTangent.y = static_cast<F32>(pFBXVertexTangent->GetDirectArray().GetAt(VertexIdx).mData[1]);
						VertexTangent.z = static_cast<F32>(pFBXVertexTangent->GetDirectArray().GetAt(VertexIdx).mData[2]);

						Vertex.tangent = VertexTangent;

						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
						break;

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygon:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByEdge:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eAllSame:
					break;

				default:
					break;
				}

				// Get Vertex binormal
				fbxsdk_2015_1::FbxGeometryElementBinormal* pFBXVertexBinormal = pFBXMesh->GetElementBinormal();

				MappingMode = pFBXVertexBinormal->GetMappingMode();
				ReferenceMode = pFBXVertexBinormal->GetReferenceMode();

				switch (MappingMode)
				{
				case fbxsdk_2015_1::FbxLayerElement::eNone:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByControlPoint:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						WrVector3f VertexBinormal;
						VertexBinormal.x = static_cast<F32>(pFBXVertexBinormal->GetDirectArray().GetAt(ControlPointIdx).mData[0]);
						VertexBinormal.y = static_cast<F32>(pFBXVertexBinormal->GetDirectArray().GetAt(ControlPointIdx).mData[1]);
						VertexBinormal.z = static_cast<F32>(pFBXVertexBinormal->GetDirectArray().GetAt(ControlPointIdx).mData[2]);

						Vertex.binormal = VertexBinormal;

						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
						break;

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygonVertex:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						WrVector3f VertexBinormal;
						VertexBinormal.x = static_cast<F32>(pFBXVertexBinormal->GetDirectArray().GetAt(VertexIdx).mData[0]);
						VertexBinormal.y = static_cast<F32>(pFBXVertexBinormal->GetDirectArray().GetAt(VertexIdx).mData[1]);
						VertexBinormal.z = static_cast<F32>(pFBXVertexBinormal->GetDirectArray().GetAt(VertexIdx).mData[2]);

						Vertex.binormal = VertexBinormal;

						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
						break;

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygon:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByEdge:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eAllSame:
					break;

				default:
					break;
				}

				// Get Vertex TexCoords
				U32 TexCoordIdx = pFBXMesh->GetTextureUVIndex(TriangleIdx, TriangleVertexIdx);

				fbxsdk_2015_1::FbxGeometryElementUV*  pFBXVertexTexCoord = pFBXMesh->GetElementUV();

				MappingMode = pFBXVertexTexCoord->GetMappingMode();
				ReferenceMode = pFBXVertexTexCoord->GetReferenceMode();

				switch (MappingMode)
				{
				case fbxsdk_2015_1::FbxLayerElement::eNone:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByControlPoint:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						WrVector2f VertexTexCoord;
						VertexTexCoord.x = static_cast<F32>(pFBXVertexTexCoord->GetDirectArray().GetAt(ControlPointIdx).mData[0]);
						VertexTexCoord.y = static_cast<F32>(pFBXVertexTexCoord->GetDirectArray().GetAt(ControlPointIdx).mData[1]);

						Vertex.texcoord = VertexTexCoord;

						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
						break;

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygonVertex:
				{
					switch (ReferenceMode)
					{
					case fbxsdk_2015_1::FbxLayerElement::eDirect:
					{
						break;
					}

					case fbxsdk_2015_1::FbxLayerElement::eIndexToDirect:
					{
						WrVector2f VertexTexCoord;
						VertexTexCoord.x = static_cast<F32>(pFBXVertexTexCoord->GetDirectArray().GetAt(TexCoordIdx).mData[0]);
						VertexTexCoord.y = static_cast<F32>(pFBXVertexTexCoord->GetDirectArray().GetAt(TexCoordIdx).mData[1]);

						Vertex.texcoord = VertexTexCoord;

						break;
					}

					default:
						break;
					}

					break;
				}

				case fbxsdk_2015_1::FbxLayerElement::eByPolygon:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eByEdge:
					break;

				case fbxsdk_2015_1::FbxLayerElement::eAllSame:
					break;

				default:
					break;
				}

				triangle.vtx[TriangleVertexIdx] = Vertex;

				VertexIdx++;
			}


			oMeshGeomContainer->mesh_geom.push_back(triangle);
		}

		// TODO: Load Materials for Meshes
		// TODO: Load Textures for Meshes

		/*
		iFbxNode->GetMaterialCount();
		FbxSurfaceMaterial* pFBXMaterial = iFbxNode->GetMaterial(0);

		FbxProperty FBXMaterialProperty = pFBXMaterial->FindProperty(FbxLayerElement::sTextureChannelNames[0]);

		if (FBXMaterialProperty.IsValid())
		{
			U32 TextureCount = FBXMaterialProperty.GetSrcObjectCount<FbxTexture>();

			for (U32 TextureIdx = 0; TextureIdx < TextureCount; TextureIdx++)
			{
				FbxTexture* pFBXTexture = FBXMaterialProperty.GetSrcObject<FbxTexture>(TextureIdx);



			}

		}
		*/

	}

	/*
	void CSceneImporter::LoadLight(FbxNode* iFbxNode, FScene* OutScene)
	{
		FbxLight* pFBXLight = iFbxNode->GetLight();

		iFbxNode->LclRotation.Get();
		iFbxNode->LclTranslation.Get();

		pFBXLight->intensity.Get();
		pFBXLight->color.Get();
	}
	*/

	/*
	void CSceneImporter::LoadCamera(FbxNode* iFbxNode, FScene* OutScene)
	{
		FbxCamera* pFBXCamera = iFbxNode->GetCamera();

		FbxMatrix GlobalTransform = pFBXCamera->GetNode()->GetScene()->GetAnimationEvaluator()->GetNodeGlobalTransform(pFBXCamera->GetNode());

		FbxMatrix InvGlobalTransform = GlobalTransform.Inverse();

		FbxVector4 Row0 = GlobalTransform.GetRow(0);
		FbxVector4 Row1 = GlobalTransform.GetRow(1);
		FbxVector4 Row2 = GlobalTransform.GetRow(2);
		FbxVector4 Row3 = GlobalTransform.GetRow(3);

		Math::Vector4 R0{ (F32)Row0.mData[0], (F32)Row0.mData[1], (F32)Row0.mData[2], (F32)Row0.mData[3] };
		Math::Vector4 R1{ (F32)Row1.mData[0], (F32)Row1.mData[1], (F32)Row1.mData[2], (F32)Row1.mData[3] };
		Math::Vector4 R2{ (F32)Row2.mData[0], (F32)Row2.mData[1], (F32)Row2.mData[2], (F32)Row2.mData[3] };
		Math::Vector4 R3{ (F32)Row3.mData[0], (F32)Row3.mData[1], (F32)Row3.mData[2], (F32)Row3.mData[3] };

		Math::Matrix4x4 obj_to_world_mtx{ R0, R1, R2, R3 };

		FbxVector4 InvRow0 = InvGlobalTransform.GetRow(0);
		FbxVector4 InvRow1 = InvGlobalTransform.GetRow(1);
		FbxVector4 InvRow2 = InvGlobalTransform.GetRow(2);
		FbxVector4 InvRow3 = InvGlobalTransform.GetRow(3);

		Math::Vector4 InvR0{ (F32)InvRow0.mData[0], (F32)InvRow0.mData[1], (F32)InvRow0.mData[2], (F32)InvRow0.mData[3] };
		Math::Vector4 InvR1{ (F32)InvRow1.mData[0], (F32)InvRow1.mData[1], (F32)InvRow1.mData[2], (F32)InvRow1.mData[3] };
		Math::Vector4 InvR2{ (F32)InvRow2.mData[0], (F32)InvRow2.mData[1], (F32)InvRow2.mData[2], (F32)InvRow2.mData[3] };
		Math::Vector4 InvR3{ (F32)InvRow3.mData[0], (F32)InvRow3.mData[1], (F32)InvRow3.mData[2], (F32)InvRow3.mData[3] };

		Math::Matrix4x4 world_to_obj_mtx{ InvR0, InvR1, InvR2, InvR3 };

		OutScene->Camera.camera_to_world_mtx = Math::Transpose(obj_to_world_mtx);
		return;

		F32 RotationDegX = static_cast<F32>(iFbxNode->LclRotation.Get().mData[0]);
		F32 RotationDegY = static_cast<F32>(iFbxNode->LclRotation.Get().mData[1]);
		F32 RotationDegZ = static_cast<F32>(iFbxNode->LclRotation.Get().mData[2]);

		F32 TranslationX = static_cast<F32>(iFbxNode->LclTranslation.Get().mData[0]);
		F32 TranslationY = static_cast<F32>(iFbxNode->LclTranslation.Get().mData[1]);
		F32 TranslationZ = static_cast<F32>(iFbxNode->LclTranslation.Get().mData[2]);
		// TODO: Ad-hoc solution;
		Math::Matrix4x4 RotationMatrix = Math::RotationX(RotationDegX) * Math::RotationY(RotationDegY) * Math::RotationZ(RotationDegZ);
		Math::Matrix4x4 InvRotationMatrix = Math::Transpose(RotationMatrix);

		Math::Matrix4x4 TranslationMatrix = Math::Translation(TranslationX, TranslationY, TranslationZ);
		Math::Matrix4x4 InvTranslationMatrix = Math::Translation(-TranslationX, -TranslationY, -TranslationZ);

		// OutScene->Camera.camera_to_world_mtx = Math::Transpose(RotationMatrix * TranslationMatrix);

		OutScene->Camera.camera_to_world_mtx = Math::Transpose(InvTranslationMatrix * InvRotationMatrix);



		OutScene->Camera.fov.x = static_cast<F32>(pFBXCamera->FieldOfViewX.Get());
		OutScene->Camera.fov.y = static_cast<F32>(pFBXCamera->FieldOfViewY.Get());
		OutScene->Camera.aspect_width = static_cast<F32>(pFBXCamera->aspect_width.Get());
		OutScene->Camera.aspect_height = static_cast<F32>(pFBXCamera->aspect_height.Get());
		OutScene->Camera.AspectRatio = OutScene->Camera.aspect_width / OutScene->Camera.aspect_height;
	}
	*/
}

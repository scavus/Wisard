/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Scene/CameraManager.hpp"
#include "Scene/MeshManager.hpp"
#include "Scene/MeshGeomManager.hpp"
#include "Scene/AreaLightManager.hpp"
#include "Scene/EnvLightManager.hpp"
#include "Scene/BasicMtlManager.hpp"
#include "Scene/DeferredShadingSystem.hpp"
#include "Scene/Texture2dManager.hpp"
#include "Accelerators/IAccelerator.hpp"
/*
#include "Accelerators/AcceleratorBvh2.hpp"
#include "Accelerators/AcceleratorOptixPrime.hpp"
#include "Accelerators/AcceleratorFireRays.hpp"
*/

struct SSceneContext
{
	CCameraManager* camera_manager_ptr;
	CMeshManager* mesh_manager_ptr;
	CMeshGeomManager* mesh_geom_manager_ptr;
	CAreaLightManager* area_light_manager_ptr;
	CEnvLightManager* env_light_manager_ptr;
	CTexture2dManager* texture2d_manager_ptr;
	CDeferredShadingSystem* deferred_shading_system_ptr;
	std::unordered_map<std::string, IAccelerator*> accelerators;

	/*
	SSceneContext()
	{
		accelerators.insert({ "Bvh2", new CAcceleratorBvh2{ this } });
		accelerators.insert({ "OptixPrime", new CAcceleratorOptixPrime{ this } });
		accelerators.insert({ "FireRays", new CAcceleratorFireRays{ this } });
	}
	*/
};

struct SSceneStorage
{
	CCameraManager::SStorage* camera_storage;
	CMeshManager::SStorage* mesh_storage;
	CMeshGeomManager::SStorage* mesh_geom_storage;
	CAreaLightManager::SStorage* area_light_storage;
	CEnvLightManager::SStorage* env_light_storage;
	CTexture2dManager::SStorage* texture2d_storage;
	CDeferredShadingSystem::SStorage* shader_storage;
};

namespace SceneCtxUtils
{
	inline void UpdateSceneCtx(SSceneContext* scene_ctx_ptr)
	{
		scene_ctx_ptr->camera_manager_ptr->Update();
		scene_ctx_ptr->mesh_manager_ptr->Update();
		scene_ctx_ptr->mesh_geom_manager_ptr->Update();
		scene_ctx_ptr->area_light_manager_ptr->Update();
		scene_ctx_ptr->env_light_manager_ptr->Update();
		// scene_ctx_ptr->texture2d_manager_ptr->Update();
		scene_ctx_ptr->deferred_shading_system_ptr->Update();

		for (auto accelerator: scene_ctx_ptr->accelerators)
		{
			accelerator.second->Update();
		}
	}
}

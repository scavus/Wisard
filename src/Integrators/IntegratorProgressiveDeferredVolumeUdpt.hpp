/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.h"
#include "Scene/GfxResourceManager.h"
#include "RenderDevice/DeviceBackend.h"
#include "Scheduling.h"
#include "SceneContext.h"
#include "RmRenderSettingsProgressiveUdpt.h"

template<class DeviceManagerBackend_T = nullptr_t>
class TRenderNodeProgressiveDeferredVolumeUdpt
{
	public:
		using DistributionPolicy = CDistributionPolicyDefault;
		using SceneContext = SSceneContextDeferred;
		using RenderSettingsManager = CRenderSettingsManagerProgressiveUdpt;

		TRenderNodeProgressiveDeferredVolumeUdpt(){};
		~TRenderNodeProgressiveDeferredVolumeUdpt(){};

		void SetDeviceManagerPtr(DeviceManagerBackend_T* device_manager_ptr) { _device_manager_ptr = device_manager_ptr; };
		void SetWorkInfo(const DistributionPolicy::SRenderWorkInfo* work_info_ptr) { _work_info_ptr = work_info_ptr; };
		void SetRenderSettingsManager(RenderSettingsManager* render_settings_manager_ptr) { _render_settings_manager_ptr = render_settings_manager_ptr; };
		void SetSceneCtxPtr(const SceneContext* scene_ctx_ptr) { _scene_ctx_ptr = scene_ctx_ptr; };

		void AllocBuffers();

		void CreateProgram();

		void CreateKernels();

		void Init();

		void Execute();

	protected:
		void SetKernelIntegratorContext(SKernelHdl kernel_hdl);
		void SetKernelGfxContext(SKernelHdl kernel_hdl);

	private:
		DeviceManagerBackend_T* _device_manager_ptr;
		const SceneContext* _scene_ctx_ptr;
		const DistributionPolicy::SRenderWorkInfo* _work_info_ptr;
		RenderSettingsManager* _render_settings_manager_ptr;


		SBufferHdl _path_state_buffer_hdl;
		SBufferHdl _direct_illum_data_buffer_hdl;
		SBufferHdl _accumulation_buffer_hdl;
		SBufferHdl _framebuffer_hdl;
		SBufferHdl _render_settings_buffer_hdl;
		SBufferHdl _bsdf_ray_intersection_data_buffer_hdl;
		SBufferHdl _light_ray_intersection_data_buffer_hdl;
		SBufferHdl _extension_ray_intersection_data_buffer_hdl;
		SBufferHdl _shading_stream_buffer_hdl;
		SBufferHdl _shading_substream_info_buffer_hdl;
		SBufferHdl _shadow_ray_casting_stream_buffer_hdl;
		SBufferHdl _shadow_ray_casting_stream_info_buffer_hdl;
		SBufferHdl _path_gen_stream_buffer_hdl;
		SBufferHdl _path_gen_stream_info_buffer_hdl;
		SBufferHdl _scene_metadata_buffer_hdl;
		SBufferHdl _render_work_info_buffer_hdl;
		SBufferHdl mStratifiedSampleBufferHdl;

		SProgramHdl _integrator_program_hdl;
		SKernelHdl _init_kernel_hdl;
		SKernelHdl _generate_paths_kernel_hdl;
		SKernelHdl _cast_shadow_rays_kernel_hdl;
		SKernelHdl _cast_extension_rays_kernel_hdl;
		SKernelHdl _controller_kernel_hdl;
		SKernelHdl _postprocessing_kernel_hdl;
};

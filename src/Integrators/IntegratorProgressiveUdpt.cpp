/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

/*
#include "Integrators/IntegratorCommon.hpp"

#include "IntegratorProgressiveUdpt.hpp"

template class TIntegratorProgressiveUdpt<CDeviceManagerCuda>;
template class TIntegratorProgressiveUdpt<CDeviceManagerOcl>;

struct SRenderSettings
{
	U8 MaxBounceCount;
};

struct SDirectIllumData
{
	SSampleInfo bsdf_sample;
	SSampleInfo light_sample;
	Math::Vector4 throughput;
};

struct SPath
{
	Math::Vector4 throughput;
	Math::Vector4 radiance;
	SRay ray;
	U8 bounce_count;
};

enum EThreadState
{
	THREAD_STATE_IDLE,
	THREAD_STATE_ACTIVE
};

struct SPathState
{
	EThreadState state;
	U32 path_count;
	SPath path;
	SRandomNumberGenerator rng;
	SShaderGlobals sg;
};

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveUdpt<DeviceManagerBackend_T>::Init()
{
	SKernelHdl Hdl;

	auto MeshRmBackendPtr = _scene_ctx_ptr->mesh_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto MeshGeomRmBackendPtr = _scene_ctx_ptr->mesh_geom_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto MeshBvh2RmBackendPtr = _scene_ctx_ptr->mesh_bvh2_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto CameraRmBackendPtr = _scene_ctx_ptr->camera_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto AreaLightRmBackendPtr = _scene_ctx_ptr->area_light_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto BasicMtlRmBackendPtr = _scene_ctx_ptr->BasicMtlRmPtr->GetBackendManagerPtr(_device_manager_ptr);

	TRenderSettingsManagerProgressiveUdptBackend<DeviceManagerBackend_T>* RenderSettingsRmBackendPtr = _render_settings_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);

	Hdl = _init_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 0, _path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 1, _accumulation_buffer_hdl);

	Hdl = _generate_paths_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 0, _path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 1, CameraRmBackendPtr->GetCameraBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 2, RenderSettingsRmBackendPtr->GetRenderSettingsBufferHdl());

	Hdl = _extend_paths_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 0, _path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 1, RenderSettingsRmBackendPtr->GetRenderSettingsBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 2, _scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 3, MeshRmBackendPtr->GetMeshBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 4, MeshGeomRmBackendPtr->GetMeshGeomBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 5, MeshBvh2RmBackendPtr->GetMeshBvh2NodeBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 6, AreaLightRmBackendPtr->GetAreaLightsBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 7, AreaLightRmBackendPtr->GetAreaLightQuadsBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 8, AreaLightRmBackendPtr->GetAreaLightDisksBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 9, AreaLightRmBackendPtr->GetAreaLightSpheresBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 10, BasicMtlRmBackendPtr->GetBasicMtlBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 11, _direct_illum_data_buffer_hdl);

	Hdl = _eval_direct_illum_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 0, _path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 1, _direct_illum_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 2, _scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 3, MeshRmBackendPtr->GetMeshBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 4, MeshGeomRmBackendPtr->GetMeshGeomBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 5, MeshBvh2RmBackendPtr->GetMeshBvh2NodeBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 6, AreaLightRmBackendPtr->GetAreaLightsBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 7, AreaLightRmBackendPtr->GetAreaLightQuadsBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 8, AreaLightRmBackendPtr->GetAreaLightDisksBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 9, AreaLightRmBackendPtr->GetAreaLightSpheresBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 10, BasicMtlRmBackendPtr->GetBasicMtlBufferHdl());

	Hdl = _postprocessing_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 0, _path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 1, _accumulation_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(Hdl, 2, _framebuffer_hdl);

	U32 block_size = _device_manager_ptr->GetWarpSize();

	_device_manager_ptr->DispatchKernel(_init_kernel_hdl,
	{ _work_info_ptr->sample_count / block_size, 1, 1 },
	{ block_size, 1, 1 });
}

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveUdpt<DeviceManagerBackend_T>::Execute()
{
	// TODO: Super::Execute();
	_device_manager_ptr->SwitchContext();

	U32 block_size = _device_manager_ptr->GetWarpSize();

	auto& render_settings = _render_settings_manager_ptr->GetRenderSettingsConstRef();

	// FIXME: Convert this to 1D kernel, also start from pixel work_info.sample_offset

	// _device_manager_ptr->DispatchKernel(_generate_paths_kernel_hdl,
	// { render_settings.render_width / (block_size / 2), render_settings.render_height / (block_size / 2), 1 },
	// { block_size / 2, block_size / 2, 1 });

	_device_manager_ptr->DispatchKernel(_generate_paths_kernel_hdl,
	{ _work_info_ptr->sample_count / (block_size), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->DispatchKernel(_extend_paths_kernel_hdl,
	{ _work_info_ptr->sample_count / (block_size), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->DispatchKernel(_eval_direct_illum_kernel_hdl,
	{ _work_info_ptr->sample_count / block_size, 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->DispatchKernel(_postprocessing_kernel_hdl,
	{ _work_info_ptr->sample_count / block_size, 1, 1 },
	{ block_size, 1, 1 });


	_device_manager_ptr->ReadBuffer(_framebuffer_hdl, &_work_info_ptr->result[0]);
}

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveUdpt<DeviceManagerBackend_T>::AllocBuffers()
{
	size_t buffer_size;

	buffer_size = _work_info_ptr->sample_count * sizeof(SPathState);
	_path_state_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(SDirectIllumData);
	_direct_illum_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(F32x4);
	_framebuffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_accumulation_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	SSceneMetadata metadata;
	metadata.mesh_count = _scene_ctx_ptr->mesh_manager_ptr->GetMeshArrayConstRef().data.size();
	metadata.prim_count = _scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef().size();
	metadata.area_light_count = _scene_ctx_ptr->area_light_manager_ptr->GetAreaLightsArrayConstRef().data.size();
	metadata.area_light_disk_count = _scene_ctx_ptr->area_light_manager_ptr->GetAreaLightDisksArrayConstRef().data.size();
	metadata.area_light_quad_count = _scene_ctx_ptr->area_light_manager_ptr->GetAreaLightQuadsArrayConstRef().data.size();
	metadata.area_light_sphere_count = _scene_ctx_ptr->area_light_manager_ptr->GetAreaLightSpheresArrayConstRef().data.size();
	metadata.material_count = _scene_ctx_ptr->BasicMtlRmPtr->GetBasicMtlArrayConstRef().data.size();
	buffer_size = sizeof(SSceneMetadata);
	_scene_metadata_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &metadata);
}

template<>
void TIntegratorProgressiveUdpt<CDeviceManagerCuda>::CreateProgram()
{
	_integrator_program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KIntegratorProgressiveUdpt.cubin");
};

template<>
void TIntegratorProgressiveUdpt<CDeviceManagerOcl>::CreateProgram()
{
	std::vector<const char*> filenames;

	filenames.push_back("data/Kernels/KIntegratorProgressiveUdpt.cl");

	const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;
	_device_manager_ptr->CompileProgram(filenames, compiler_options);
};

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveUdpt<DeviceManagerBackend_T>::CreateKernels()
{
	_init_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "Initialize", 2);
	_generate_paths_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "GeneratePaths", 2);
	_extend_paths_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "ExtendPaths", 12);
	_eval_direct_illum_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "EvaluateDirectIllumination", 11);
	_postprocessing_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "AccumulatePixelValues", 3);
}
*/

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Utility/Math.hpp"

struct SRandomNumberGenerator
{
    S32 state;
};

struct SShaderGlobals
{
    F32x3 pos;
    F32x3 normal;
    F32x2 texcoord;

    F32x3 wo;

    WrMatrix4x4 tangent_to_world_mtx;

    U32 mesh_id;
    U32 material_id;
};

enum EIntersectionType
{
    INTERSECTION_TYPE_NULL,
    INTERSECTION_TYPE_BACKGROUND,
    INTERSECTION_TYPE_MESH_SURFACE,
    INTERSECTION_TYPE_LIGHT_SURFACE,
    INTERSECTION_TYPE_MESH_VOLUME,
    INTERSECTION_TYPE_LIGHT_VOLUME
};

struct SIntersectionData
{
    F32x3 pos;
    F32x3 normal;
    F32x3 tangent;
    F32x3 binormal;
    F32x2 texcoord;

    EIntersectionType intersection_type;
    U32 occluder_id;
};

struct SSampleInfo
{
	F32x3 direction;
	F32x4 throughput;
	F32 pdf;
};

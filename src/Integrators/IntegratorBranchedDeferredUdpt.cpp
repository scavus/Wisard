/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "Scene/Scene.hpp"
#include "Integrators/IntegratorCommon.hpp"
#include "Integrators/IntegratorDeferred.hpp"

#include "IntegratorBranchedDeferredUdpt.hpp"

template class TIntegratorBranchedDeferredUdpt<CDeviceManagerCuda>;
template class TIntegratorBranchedDeferredUdpt<CDeviceManagerOcl>;
template class TIntegratorBranchedDeferredUdpt<CDeviceManagerDebug>;

struct SPathState
{
	F32x4 throughput;
	F32x4 radiance;
	U8 bounce_count;

	WrVector2u pixel_coord;
};

struct SRenderWorkInfo
{
	U32 tile_offset_x;
	U32 tile_offset_y;
	U32 tile_width;
	U32 tile_height;
};

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::Init()
{
	auto mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;
	base_shading_substream_infos = new SSubstreamInfo[mtl_count];
	branched_shading_substream_infos = new SSubstreamInfo[mtl_count];
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::Update()
{
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::ExecuteTile()
{
	AllocBaseBuffers();

	U32 work_size = 0;
	U32 block_size = _device_manager_ptr->GetWarpSize();

	SKernelHdl kh;
	U32 arg_idx = 0;
	size_t buffer_size = 0;

	auto render_settings = _render_settings_manager_ptr->GetRenderSettingsConstRef();
	auto shading_system = _scene_ctx_ptr->deferred_shading_system_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;

	size_t pixel_count = _tile_it->tile_width * _tile_it->tile_height;
	size_t total_aa_sample_count = render_settings.aa_sample_count * render_settings.aa_sample_count;
	size_t total_direct_illum_sample_count = render_settings.direct_illum_sample_count * render_settings.direct_illum_sample_count;
	size_t total_indirect_illum_sample_count = render_settings.indirect_illum_sample_count * render_settings.indirect_illum_sample_count;
	size_t base_path_count = pixel_count * total_aa_sample_count;

	render_settings_buffer_hdl = _render_settings_manager_ptr->GetBackendManagerPtr(_device_manager_ptr)->GetRenderSettingsBufferHdl();
	camera_buffer_hdl = _scene_ctx_ptr->camera_manager_ptr->GetBackendManagerPtr(_device_manager_ptr)->GetCameraBufferHdl();
	mesh_buffer_hdl = _scene_ctx_ptr->mesh_manager_ptr->GetBackendManagerPtr(_device_manager_ptr)->GetMeshBufferHdl();

	auto area_light_backend_manager_ptr = _scene_ctx_ptr->area_light_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	area_light_buffer_hdl = area_light_backend_manager_ptr->GetAreaLightsBufferHdl();

	auto env_light_backend_manager_ptr = _scene_ctx_ptr->env_light_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	env_light_buffer_hdl = env_light_backend_manager_ptr->GetEnvLightBufferHdl();
	env_map_texture_hdl = env_light_backend_manager_ptr->GetEnvMapTextureHdl();
	env_light_conditional_cdf_buffer_hdl = env_light_backend_manager_ptr->GetEnvLightConditionalCdfBufferHdl();
	env_light_marginal_cdf_buffer_hdl = env_light_backend_manager_ptr->GetEnvLightMarginalCdfBufferHdl();

	arg_idx = 0;
	kh = _init_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, extension_ray_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_substream_info_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_init_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(_tile_it->tile_width * _tile_it->tile_height) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	buffer_size = base_path_count * sizeof(SRandomNumberGenerator);
	branched_rng_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	arg_idx = 0;
	kh = _generate_paths_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, camera_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, aa_rand_sample_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_extension_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_extension_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_extension_ray_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, extension_ray_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_extension_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_info_buffer_hdl);
	work_size = static_cast<U32>(std::ceil(static_cast<F32>(pixel_count) / static_cast<F32>(block_size)));
	_device_manager_ptr->DispatchKernel(_generate_paths_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

	{
		SRayIntersectionQueryData query_data;
		query_data.ray_buffer = base_extension_ray_buffer_hdl;
		query_data.ray_count = pixel_count * total_aa_sample_count;
		query_data.ray_stream = base_extension_ray_stream_buffer_hdl;
		query_data.ray_stream_info = extension_ray_stream_info_buffer_hdl;
		query_data.result = base_extension_ray_intersection_data_buffer_hdl;
		_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);
	}

	_device_manager_ptr->ReadBuffer(base_active_path_stream_info_buffer_hdl, &active_path_stream_info);
	U32 active_path_count = active_path_stream_info.counter;
	active_path_stream_info.counter = 0;
	_device_manager_ptr->WriteBuffer(base_active_path_stream_info_buffer_hdl, &active_path_stream_info);

	_device_manager_ptr->DestroyBuffer(base_rng_buffer_hdl);
	base_rng_buffer_hdl = branched_rng_buffer_hdl;

	arg_idx = 0;
	kh = _controller_kernel_hdl;
	_device_manager_ptr->SetKernelArgValue(kh, arg_idx++, sizeof(U32), &active_path_count);
	_device_manager_ptr->SetKernelArgValue(kh, arg_idx++, sizeof(U32), &base_path_count);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_extension_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_extension_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, mesh_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, area_light_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_buffer_hdl);
	_device_manager_ptr->SetKernelArgTexture(kh, arg_idx++, env_map_texture_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_substream_info_buffer_hdl);
	work_size = static_cast<U32>(std::ceil(static_cast<F32>(active_path_count) / static_cast<F32>(block_size)));
	_device_manager_ptr->DispatchKernel(_controller_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

	_device_manager_ptr->ReadBuffer(base_active_path_stream_info_buffer_hdl, &active_path_stream_info);

	if (active_path_stream_info.counter > 0)
	{
		DirectIllumPass();

		if (render_settings.max_indirect_bounce_count > 0)
		{
			IndirectIllumPass();
		}	
	}

	arg_idx = 0;
	kh = _postprocessing_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, framebuffer_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_postprocessing_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(_tile_it->tile_width * _tile_it->tile_height) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->ReadBuffer(framebuffer_buffer_hdl, &_tile_it->result[0]);

	ReleaseBaseBuffers();
	return;
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::Execute()
{
	_device_manager_ptr->SwitchContext();

	U32 i = 0;
	U32 tile_count = _work_info_ptr->tiles.size();

	for (_tile_it; _tile_it < _work_info_ptr->tiles.end(); _tile_it++, i++)
	{
		ExecuteTile();
		std::cout << "Rendering tile [" << i + 1 << "/" << tile_count << "] " << "\r";
		std::cout.flush();
		_work_info_ptr->finished_tile_count++;
	}
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::DirectIllumPass()
{
	U32 work_size = 0;
	U32 block_size = _device_manager_ptr->GetWarpSize();

	SKernelHdl kh;
	U32 arg_idx = 0;

	auto render_settings = _render_settings_manager_ptr->GetRenderSettingsConstRef();
	auto shading_system = _scene_ctx_ptr->deferred_shading_system_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;

	size_t pixel_count = _tile_it->tile_width * _tile_it->tile_height;
	size_t total_aa_sample_count = render_settings.aa_sample_count * render_settings.aa_sample_count;
	size_t total_direct_illum_sample_count = render_settings.direct_illum_sample_count * render_settings.direct_illum_sample_count;
	size_t total_indirect_illum_sample_count = render_settings.indirect_illum_sample_count * render_settings.indirect_illum_sample_count;
	size_t base_path_count = pixel_count * total_aa_sample_count;

	size_t branched_path_count = base_path_count * total_direct_illum_sample_count;
	AllocPostBranchBuffers(EIllumPass::DIRECT, branched_path_count);

	for (U32 i = 0; i < mtl_count; i++)
	{
		branched_shading_substream_infos[i].counter = 0;
		branched_shading_substream_infos[i].offset = i * branched_path_count;
	}

	_device_manager_ptr->WriteBuffer(branched_shading_substream_info_buffer_hdl, branched_shading_substream_infos);

	arg_idx = 0;
	kh = _prepare_direct_illum_branched_samples_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_substream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_substream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, direct_illum_rand_sample_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_info_buffer_hdl);
	work_size = static_cast<U32>(std::ceil(static_cast<F32>(active_path_stream_info.counter) / static_cast<F32>(block_size)));
	_device_manager_ptr->DispatchKernel(_prepare_direct_illum_branched_samples_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

	arg_idx = 0;
	kh = _prepare_direct_illum_branched_shading_stream_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_substream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_substream_info_buffer_hdl);
	work_size = static_cast<U32>(std::ceil(static_cast<F32>(branched_path_count) / static_cast<F32>(block_size)));
	_device_manager_ptr->DispatchKernel(_prepare_direct_illum_branched_shading_stream_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

	_device_manager_ptr->ReadBuffer(shadow_stream_info_buffer_hdl, &shadow_ray_stream_info);
	_device_manager_ptr->ReadBuffer(branched_shading_substream_info_buffer_hdl, branched_shading_substream_infos);

	{
		SShaderSampleData sample_data;
		sample_data.shader_globals_buf = branched_shader_globals_buffer_hdl;
		sample_data.rngs_buf = branched_rng_buffer_hdl;
		sample_data.rand_samples_buf = direct_illum_rand_sample_buffer_hdl;
		sample_data.shading_stream_buf = branched_shading_stream_buffer_hdl;
		sample_data.shading_substream_infos_buf = branched_shading_substream_info_buffer_hdl;
		sample_data.sample_infos_buf = bsdf_sample_info_buffer_hdl;
		sample_data.shading_substream_infos = branched_shading_substream_infos;
		shading_system->Sample(sample_data);
	}

	arg_idx = 0;
	kh = _sample_lights_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, direct_illum_rand_sample_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, area_light_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_buffer_hdl);
	_device_manager_ptr->SetKernelArgTexture(kh, arg_idx++, env_map_texture_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_conditional_cdf_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_marginal_cdf_buffer_hdl);
	work_size = static_cast<U32>(std::ceil(static_cast<F32>(shadow_ray_stream_info.counter) / static_cast<F32>(block_size)));
	_device_manager_ptr->DispatchKernel(_sample_lights_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

	{
		SShaderEvalData eval_data;
		eval_data.shader_globals_buf = branched_shader_globals_buffer_hdl;
		eval_data.rngs_buf = branched_rng_buffer_hdl;
		eval_data.shading_stream_buf = branched_shading_stream_buffer_hdl;
		eval_data.shading_substream_infos_buf = branched_shading_substream_info_buffer_hdl;
		eval_data.sample_infos_buf = light_sample_info_buffer_hdl;
		eval_data.shading_substream_infos = branched_shading_substream_infos;
		shading_system->Eval(eval_data);
	}

	arg_idx = 0;
	kh = _prepare_shadow_rays_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_info_buffer_hdl);
	work_size = static_cast<U32>(std::ceil(static_cast<F32>(shadow_ray_stream_info.counter) / static_cast<F32>(block_size)));
	_device_manager_ptr->DispatchKernel(_prepare_shadow_rays_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

	if (shadow_ray_stream_info.counter > 0)
	{
		{
			SRayIntersectionQueryData query_data;
			query_data.ray_buffer = bsdf_ray_buffer_hdl;
			query_data.ray_count = shadow_ray_stream_info.counter;
			query_data.ray_stream = shadow_stream_buffer_hdl;
			query_data.ray_stream_info = shadow_stream_info_buffer_hdl;
			query_data.result = bsdf_ray_intersection_data_buffer_hdl;
			_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);
		}

		{
			SRayIntersectionQueryData query_data;
			query_data.ray_buffer = light_ray_buffer_hdl;
			query_data.ray_count = shadow_ray_stream_info.counter;
			query_data.ray_stream = shadow_stream_buffer_hdl;
			query_data.ray_stream_info = shadow_stream_info_buffer_hdl;
			query_data.result = light_ray_intersection_data_buffer_hdl;
			_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);
		}

		shadow_ray_stream_info.counter = 0;
		_device_manager_ptr->WriteBuffer(shadow_stream_info_buffer_hdl, &shadow_ray_stream_info);
	}

	arg_idx = 0;
	kh = _contrib_direct_illum_branched_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, area_light_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_buffer_hdl);
	_device_manager_ptr->SetKernelArgTexture(kh, arg_idx++, env_map_texture_hdl);
	work_size = static_cast<U32>(std::ceil(static_cast<F32>(active_path_stream_info.counter) / static_cast<F32>(block_size)));
	_device_manager_ptr->DispatchKernel(_contrib_direct_illum_branched_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

	ReleasePostBranchBuffers(EIllumPass::DIRECT);
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::IndirectIllumPass()
{
	U32 work_size = 0;
	U32 block_size = _device_manager_ptr->GetWarpSize();

	SKernelHdl kh;
	U32 arg_idx = 0;

	auto render_settings = _render_settings_manager_ptr->GetRenderSettingsConstRef();
	auto shading_system = _scene_ctx_ptr->deferred_shading_system_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;

	size_t pixel_count = _tile_it->tile_width * _tile_it->tile_height;
	size_t total_aa_sample_count = render_settings.aa_sample_count * render_settings.aa_sample_count;
	size_t total_direct_illum_sample_count = render_settings.direct_illum_sample_count * render_settings.direct_illum_sample_count;
	size_t total_indirect_illum_sample_count = render_settings.indirect_illum_sample_count * render_settings.indirect_illum_sample_count;
	size_t base_path_count = pixel_count * total_aa_sample_count;
	U32 active_path_count;

	size_t branched_path_count = base_path_count * total_indirect_illum_sample_count;
	AllocPostBranchBuffers(EIllumPass::INDIRECT, branched_path_count);

	for (U32 i = 0; i < mtl_count; i++)
	{
		branched_shading_substream_infos[i].counter = 0;
		branched_shading_substream_infos[i].offset = i * branched_path_count;
	}

	_device_manager_ptr->WriteBuffer(branched_shading_substream_info_buffer_hdl, branched_shading_substream_infos);

	arg_idx = 0;
	kh = _prepare_indirect_illum_branched_samples_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_substream_info_buffer_hdl);

	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_substream_info_buffer_hdl);

	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, extension_ray_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, indirect_illum_rand_sample_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_prepare_indirect_illum_branched_samples_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(active_path_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	arg_idx = 0;
	kh = _prepare_indirect_illum_branched_shading_stream_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_shading_substream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_substream_info_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_prepare_indirect_illum_branched_shading_stream_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(branched_path_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->ReadBuffer(branched_shading_substream_info_buffer_hdl, branched_shading_substream_infos);

	{
		SShaderSampleData sample_data;
		sample_data.shader_globals_buf = branched_shader_globals_buffer_hdl;
		sample_data.rngs_buf = branched_rng_buffer_hdl;
		sample_data.rand_samples_buf = indirect_illum_rand_sample_buffer_hdl;
		sample_data.shading_stream_buf = branched_shading_stream_buffer_hdl;
		sample_data.shading_substream_infos_buf = branched_shading_substream_info_buffer_hdl;
		sample_data.sample_infos_buf = branched_extension_sample_info_buffer_hdl;
		sample_data.shading_substream_infos = branched_shading_substream_infos;
		shading_system->Sample(sample_data);
	}

	_device_manager_ptr->ReadBuffer(extension_ray_stream_info_buffer_hdl, &extension_ray_stream_info);

	arg_idx = 0;
	kh = _prepare_extension_rays_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, extension_ray_stream_info_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_prepare_extension_rays_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(extension_ray_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	if (extension_ray_stream_info.counter > 0)
	{
		SRayIntersectionQueryData query_data;
		query_data.ray_buffer = branched_extension_ray_buffer_hdl;
		query_data.ray_count = extension_ray_stream_info.counter;
		query_data.ray_stream = branched_extension_ray_stream_buffer_hdl;
		query_data.ray_stream_info = extension_ray_stream_info_buffer_hdl;
		query_data.result = branched_extension_ray_intersection_data_buffer_hdl;

		_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);

		extension_ray_stream_info.counter = 0;
		_device_manager_ptr->WriteBuffer(extension_ray_stream_info_buffer_hdl, &extension_ray_stream_info);
	}

	while (true)
	{
		_device_manager_ptr->ReadBuffer(branched_active_path_stream_info_buffer_hdl, &active_path_stream_info);
		active_path_count = active_path_stream_info.counter;
		active_path_stream_info.counter = 0;
		_device_manager_ptr->WriteBuffer(branched_active_path_stream_info_buffer_hdl, &active_path_stream_info);

		for (U32 i = 0; i < mtl_count; i++)
		{
			branched_shading_substream_infos[i].counter = 0;
			branched_shading_substream_infos[i].offset = i * branched_path_count;
		}

		_device_manager_ptr->WriteBuffer(branched_shading_substream_info_buffer_hdl, branched_shading_substream_infos);

		arg_idx = 0;
		kh = _controller_kernel_hdl;
		_device_manager_ptr->SetKernelArgValue(kh, arg_idx++, sizeof(U32), &active_path_count);
		_device_manager_ptr->SetKernelArgValue(kh, arg_idx++, sizeof(U32), &branched_path_count);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_path_state_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_rng_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_intersection_data_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, mesh_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, area_light_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_buffer_hdl);
		_device_manager_ptr->SetKernelArgTexture(kh, arg_idx++, env_map_texture_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shading_substream_info_buffer_hdl);
		_device_manager_ptr->DispatchKernel(_controller_kernel_hdl,
		{ static_cast<U32>(std::ceil(static_cast<F32>(active_path_count) / static_cast<F32>(block_size))), 1, 1 },
		{ block_size, 1, 1 });

		_device_manager_ptr->ReadBuffer(branched_active_path_stream_info_buffer_hdl, &active_path_stream_info);

		if (active_path_stream_info.counter == 0)
		{
			break;
		}

		arg_idx = 0;
		kh = _prepare_generic_illum_samples_kernel_hdl;
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_path_state_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_rng_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, direct_illum_rand_sample_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, indirect_illum_rand_sample_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, extension_ray_stream_info_buffer_hdl);
		_device_manager_ptr->DispatchKernel(_prepare_generic_illum_samples_kernel_hdl,
		{ static_cast<U32>(std::ceil(static_cast<F32>(active_path_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
		{ block_size, 1, 1 });

		arg_idx = 0;
		kh = _sample_lights_kernel_hdl;
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_rng_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, direct_illum_rand_sample_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, scene_metadata_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, area_light_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_buffer_hdl);
		_device_manager_ptr->SetKernelArgTexture(kh, arg_idx++, env_map_texture_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_conditional_cdf_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_marginal_cdf_buffer_hdl);
		_device_manager_ptr->DispatchKernel(_sample_lights_kernel_hdl,
		{ static_cast<U32>(std::ceil(static_cast<F32>(active_path_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
		{ block_size, 1, 1 });

		_device_manager_ptr->ReadBuffer(branched_shading_substream_info_buffer_hdl, branched_shading_substream_infos);

		{
			SShaderEvalData eval_data;
			eval_data.shader_globals_buf = branched_shader_globals_buffer_hdl;
			eval_data.rngs_buf = branched_rng_buffer_hdl;
			eval_data.shading_stream_buf = branched_shading_stream_buffer_hdl;
			eval_data.shading_substream_infos_buf = branched_shading_substream_info_buffer_hdl;
			eval_data.sample_infos_buf = light_sample_info_buffer_hdl;
			eval_data.shading_substream_infos = branched_shading_substream_infos;
			shading_system->Eval(eval_data);
		}

		{
			SShaderSampleData sample_data;
			sample_data.shader_globals_buf = branched_shader_globals_buffer_hdl;
			sample_data.rngs_buf = branched_rng_buffer_hdl;
			sample_data.rand_samples_buf = direct_illum_rand_sample_buffer_hdl;
			sample_data.shading_stream_buf = branched_shading_stream_buffer_hdl;
			sample_data.shading_substream_infos_buf = branched_shading_substream_info_buffer_hdl;
			sample_data.sample_infos_buf = bsdf_sample_info_buffer_hdl;
			sample_data.shading_substream_infos = branched_shading_substream_infos;
			shading_system->Sample(sample_data);
		}

		{
			SShaderSampleData sample_data;
			sample_data.shader_globals_buf = branched_shader_globals_buffer_hdl;
			sample_data.rngs_buf = branched_rng_buffer_hdl;
			sample_data.rand_samples_buf = indirect_illum_rand_sample_buffer_hdl;
			sample_data.shading_stream_buf = branched_shading_stream_buffer_hdl;
			sample_data.shading_substream_infos_buf = branched_shading_substream_info_buffer_hdl;
			sample_data.sample_infos_buf = branched_extension_sample_info_buffer_hdl;
			sample_data.shading_substream_infos = branched_shading_substream_infos;
			shading_system->Sample(sample_data);
		}

		_device_manager_ptr->ReadBuffer(shadow_stream_info_buffer_hdl, &shadow_ray_stream_info);

		arg_idx = 0;
		kh = _prepare_shadow_rays_kernel_hdl;
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_ray_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_ray_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_ray_intersection_data_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_ray_intersection_data_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, shadow_stream_info_buffer_hdl);
		work_size = static_cast<U32>(std::ceil(static_cast<F32>(shadow_ray_stream_info.counter) / static_cast<F32>(block_size)));
		_device_manager_ptr->DispatchKernel(_prepare_shadow_rays_kernel_hdl, { work_size, 1, 1 }, { block_size, 1, 1 });

		if (shadow_ray_stream_info.counter > 0)
		{
			{
				SRayIntersectionQueryData query_data;
				query_data.ray_buffer = bsdf_ray_buffer_hdl;
				query_data.ray_count = shadow_ray_stream_info.counter;
				query_data.ray_stream = shadow_stream_buffer_hdl;
				query_data.ray_stream_info = shadow_stream_info_buffer_hdl;
				query_data.result = bsdf_ray_intersection_data_buffer_hdl;
				_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);
			}

			{
				SRayIntersectionQueryData query_data;
				query_data.ray_buffer = light_ray_buffer_hdl;
				query_data.ray_count = shadow_ray_stream_info.counter;
				query_data.ray_stream = shadow_stream_buffer_hdl;
				query_data.ray_stream_info = shadow_stream_info_buffer_hdl;
				query_data.result = light_ray_intersection_data_buffer_hdl;
				_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);
			}

			arg_idx = 0;
			kh = _contrib_direct_illum_single_kernel_hdl;
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_path_state_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_active_path_stream_info_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_ray_intersection_data_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_ray_intersection_data_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, bsdf_sample_info_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, light_sample_info_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, area_light_buffer_hdl);
			_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, env_light_buffer_hdl);
			_device_manager_ptr->SetKernelArgTexture(kh, arg_idx++, env_map_texture_hdl);
			_device_manager_ptr->DispatchKernel(_contrib_direct_illum_single_kernel_hdl,
			{ static_cast<U32>(std::ceil(static_cast<F32>(active_path_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
			{ block_size, 1, 1 });

			shadow_ray_stream_info.counter = 0;
			_device_manager_ptr->WriteBuffer(shadow_stream_info_buffer_hdl, &shadow_ray_stream_info);
		}

		_device_manager_ptr->ReadBuffer(extension_ray_stream_info_buffer_hdl, &extension_ray_stream_info);

		arg_idx = 0;
		kh = _prepare_extension_rays_kernel_hdl;
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_shader_globals_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_intersection_data_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_sample_info_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_extension_ray_stream_buffer_hdl);
		_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, extension_ray_stream_info_buffer_hdl);
		_device_manager_ptr->DispatchKernel(_prepare_extension_rays_kernel_hdl,
		{ static_cast<U32>(std::ceil(static_cast<F32>(extension_ray_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
		{ block_size, 1, 1 });

		if (extension_ray_stream_info.counter > 0)
		{
			SRayIntersectionQueryData query_data;
			query_data.ray_buffer = branched_extension_ray_buffer_hdl;
			query_data.ray_count = extension_ray_stream_info.counter;
			query_data.ray_stream = branched_extension_ray_stream_buffer_hdl;
			query_data.ray_stream_info = extension_ray_stream_info_buffer_hdl;
			query_data.result = branched_extension_ray_intersection_data_buffer_hdl;
			_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);

			extension_ray_stream_info.counter = 0;
			_device_manager_ptr->WriteBuffer(extension_ray_stream_info_buffer_hdl, &extension_ray_stream_info);
		}
	}

	arg_idx = 0;
	kh = _contrib_indirect_illum_kernel_hdl;
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, base_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, branched_path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kh, arg_idx++, render_settings_buffer_hdl);
	_device_manager_ptr->DispatchKernel(_contrib_indirect_illum_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(pixel_count * total_aa_sample_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	ReleasePostBranchBuffers(EIllumPass::INDIRECT);
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::AllocBaseBuffers()
{
	auto render_settings = _render_settings_manager_ptr->GetRenderSettingsConstRef();
	size_t pixel_count = _tile_it->tile_width * _tile_it->tile_height;
	size_t total_aa_sample_count = render_settings.aa_sample_count * render_settings.aa_sample_count;
	size_t base_path_count = pixel_count * total_aa_sample_count;

	auto mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;

	size_t buffer_size = 0;

	SRenderWorkInfo work_info;
	work_info.tile_width = _tile_it->tile_width;
	work_info.tile_height = _tile_it->tile_height;
	work_info.tile_offset_x = _tile_it->tile_offset_x;
	work_info.tile_offset_y = _tile_it->tile_offset_y;
	buffer_size = sizeof(SRenderWorkInfo);
	render_work_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &work_info);

	buffer_size = sizeof(F32x4) * pixel_count;
	framebuffer_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SPathState) * base_path_count;
	base_path_state_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(U32) * base_path_count;
	base_active_path_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SRandomNumberGenerator) * pixel_count;
	base_rng_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(F32x2) * base_path_count;
	aa_rand_sample_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SShaderGlobals) * base_path_count;
	base_shader_globals_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(U32) * base_path_count * mtl_count;
	base_shading_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SSampleInfo) * base_path_count;
	base_extension_sample_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SRay) * base_path_count;
	base_extension_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(U32) * base_path_count;
	base_extension_ray_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SIntersectionData) * base_path_count;
	base_extension_ray_intersection_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::ReleaseBaseBuffers()
{
	_device_manager_ptr->DestroyBuffer(render_work_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(framebuffer_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_path_state_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_active_path_stream_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_rng_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(aa_rand_sample_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_shader_globals_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_shading_stream_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_extension_sample_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_extension_ray_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_extension_ray_stream_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_extension_ray_intersection_data_buffer_hdl);
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::AllocPostBranchBuffers(EIllumPass pass, size_t branched_path_count)
{
	size_t buffer_size = 0;

	auto mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;

	buffer_size = sizeof(SRandomNumberGenerator) * branched_path_count;
	branched_rng_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SShaderGlobals) * branched_path_count;
	branched_shader_globals_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(U32) * branched_path_count * mtl_count;
	branched_shading_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SSampleInfo) * branched_path_count;
	bsdf_sample_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	light_sample_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(F32x2) * branched_path_count;
	direct_illum_rand_sample_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SRay) * branched_path_count;
	bsdf_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	light_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SIntersectionData) * branched_path_count;
	bsdf_ray_intersection_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	light_ray_intersection_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(U32) * branched_path_count;
	shadow_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	if (pass == EIllumPass::INDIRECT)
	{
		buffer_size = sizeof(SPathState) * branched_path_count;
		branched_path_state_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

		buffer_size = sizeof(U32) * branched_path_count;
		branched_active_path_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

		buffer_size = sizeof(F32x2) * branched_path_count;
		indirect_illum_rand_sample_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

		buffer_size = sizeof(SSampleInfo) * branched_path_count;
		branched_extension_sample_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

		buffer_size = sizeof(SRay) * branched_path_count;
		branched_extension_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

		buffer_size = sizeof(SIntersectionData) * branched_path_count;
		branched_extension_ray_intersection_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

		buffer_size = sizeof(U32) * branched_path_count;
		branched_extension_ray_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	}
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::ReleasePostBranchBuffers(EIllumPass pass)
{
	_device_manager_ptr->DestroyBuffer(branched_rng_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(branched_shader_globals_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(branched_shading_stream_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(direct_illum_rand_sample_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(bsdf_sample_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(light_sample_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(bsdf_ray_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(light_ray_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(bsdf_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(light_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(shadow_stream_buffer_hdl);

	if (pass == INDIRECT)
	{
		_device_manager_ptr->DestroyBuffer(branched_path_state_buffer_hdl);
		_device_manager_ptr->DestroyBuffer(branched_active_path_stream_buffer_hdl);
		_device_manager_ptr->DestroyBuffer(indirect_illum_rand_sample_buffer_hdl);
		_device_manager_ptr->DestroyBuffer(branched_extension_sample_info_buffer_hdl);
		_device_manager_ptr->DestroyBuffer(branched_extension_ray_buffer_hdl);
		_device_manager_ptr->DestroyBuffer(branched_extension_ray_stream_buffer_hdl);
		_device_manager_ptr->DestroyBuffer(branched_extension_ray_intersection_data_buffer_hdl);
	}
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::AllocPersistentBuffers()
{
	size_t buffer_size = 0;

	SSceneMetadata metadata;
	metadata.mesh_count = _scene_ctx_ptr->mesh_manager_ptr->GetMeshArrayConstRef().data.size();
	metadata.prim_count = _scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef().size();
	metadata.area_light_count = _scene_ctx_ptr->area_light_manager_ptr->GetMetadata().light_count;
	metadata.env_light_count = _scene_ctx_ptr->env_light_manager_ptr->GetMetadata().env_light_count;
	metadata.material_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;
	buffer_size = sizeof(SSceneMetadata);
	scene_metadata_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &metadata);

	buffer_size = sizeof(SSubstreamInfo) * metadata.material_count;
	base_shading_substream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SSubstreamInfo) * metadata.material_count;
	branched_shading_substream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SStreamInfo);
	base_active_path_stream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SStreamInfo);
	branched_active_path_stream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SStreamInfo);
	extension_ray_stream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SStreamInfo);
	shadow_stream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::ReleasePersistentBuffers()
{
	_device_manager_ptr->DestroyBuffer(scene_metadata_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_shading_substream_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(base_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(branched_active_path_stream_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(extension_ray_stream_info_buffer_hdl);
	_device_manager_ptr->DestroyBuffer(shadow_stream_info_buffer_hdl);
}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::AllocBuffers()
{
	AllocPersistentBuffers();
}

template<>
void TIntegratorBranchedDeferredUdpt<CDeviceManagerCuda>::CreateProgram()
{
	_integrator_program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KIntegratorBranchedDeferredUdpt.cubin");
}

template<>
void TIntegratorBranchedDeferredUdpt<CDeviceManagerOcl>::CreateProgram()
{
	std::vector<const char*> filenames;

	filenames.push_back("data/Kernels/KIntegratorBranchedDeferredUdpt.c");

	const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;
	_integrator_program_hdl = _device_manager_ptr->CompileProgram(filenames, compiler_options);
}

template<>
void TIntegratorBranchedDeferredUdpt<CDeviceManagerDebug>::CreateProgram(){}

template<>
void TIntegratorBranchedDeferredUdpt<CDeviceManagerDebug>::CreateKernels(){}

template<class DeviceManagerBackend_T>
void TIntegratorBranchedDeferredUdpt<DeviceManagerBackend_T>::CreateKernels()
{
	_init_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "Initialize", 8);
	_generate_paths_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "GeneratePaths", 14);
	_prepare_generic_illum_samples_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareGenericIllumSamples", 24);
	_prepare_direct_illum_branched_samples_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareDirectIllumBranchedSamples", 19);
	_prepare_indirect_illum_branched_samples_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareIndirectIllumBranchedSamples", 24);
	_prepare_direct_illum_branched_shading_stream_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareDirectIllumBranchedShadingStream", 24);
	_prepare_indirect_illum_branched_shading_stream_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareIndirectIllumBranchedShadingStream", 24);
	_contrib_direct_illum_single_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "ContribDirectIllumSingle", 24);
	_contrib_direct_illum_branched_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "ContribDirectIllumBranched", 24);
	_contrib_indirect_illum_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "ContribIndirectIllum", 24);
	_prepare_shadow_rays_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareShadowRays", 24);
	_prepare_extension_rays_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareExtensionRays", 24);
	_controller_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "Controller", 16);
	_sample_lights_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "SampleLights", 24);
	_postprocessing_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "AccumulatePixelValues", 24);
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "Scene/Scene.hpp"
#include "Integrators/IntegratorCommon.hpp"
#include "Integrators/IntegratorDeferred.hpp"

#include "IntegratorProgressiveDeferredUdpt.hpp"
// #include "Kernels/KIntegratorProgressiveDeferredUdpt.h"

template class TIntegratorProgressiveDeferredUdpt<CDeviceManagerCuda>;
template class TIntegratorProgressiveDeferredUdpt<CDeviceManagerOcl>;
template class TIntegratorProgressiveDeferredUdpt<CDeviceManagerDebug>;

struct SPath
{
	WrVector4f throughput;
	WrVector4f radiance;
	U8 bounce_count;
};

struct SPathState
{
	U32 path_count;
	SPath path;
	WrVector2u pixel_coord;
};

struct SRenderWorkInfo
{
	U32 sample_offset;
	U32 sample_count;
};

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveDeferredUdpt<DeviceManagerBackend_T>::Init()
{
	_finished_iteration_count = 0;

	SetKernelIntegratorContext(_init_kernel_hdl);
	SetKernelIntegratorContext(_generate_paths_kernel_hdl);
	SetKernelIntegratorContext(_controller_kernel_hdl);
	SetKernelIntegratorContext(_prepare_shadow_rays_kernel_hdl);
	SetKernelIntegratorContext(_prepare_extension_rays_kernel_hdl);
	SetKernelIntegratorContext(_postprocessing_kernel_hdl);

	SetKernelGfxContext(_init_kernel_hdl);
	SetKernelGfxContext(_generate_paths_kernel_hdl);
	SetKernelGfxContext(_controller_kernel_hdl);
	SetKernelGfxContext(_prepare_shadow_rays_kernel_hdl);
	SetKernelGfxContext(_prepare_extension_rays_kernel_hdl);
	SetKernelGfxContext(_postprocessing_kernel_hdl);

	U32 block_size = _device_manager_ptr->GetWarpSize();

	_device_manager_ptr->DispatchKernel(_init_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(_work_info_ptr->sample_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });
}

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveDeferredUdpt<DeviceManagerBackend_T>::Update()
{

}

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveDeferredUdpt<DeviceManagerBackend_T>::Execute()
{
	_device_manager_ptr->SwitchContext();

	U32 work_size = 0;
	U32 block_size = _device_manager_ptr->GetWarpSize();

	auto shading_system = _scene_ctx_ptr->deferred_shading_system_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;

	SStreamInfo path_gen_stream_info;
	SStreamInfo shadow_ray_casting_stream_info;
	SSubstreamInfo* shading_substream_infos = new SSubstreamInfo[mtl_count];

	_device_manager_ptr->ReadBuffer(_path_gen_stream_info_buffer_hdl, &path_gen_stream_info);

	if (path_gen_stream_info.counter > 0)
	{
		_device_manager_ptr->DispatchKernel(_generate_paths_kernel_hdl,
		{ static_cast<U32>(std::ceil(static_cast<F32>(path_gen_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
		{ block_size, 1, 1 });

		path_gen_stream_info.counter = 0;
		_device_manager_ptr->WriteBuffer(_path_gen_stream_info_buffer_hdl, &path_gen_stream_info);
	}

	_device_manager_ptr->ReadBuffer(_shadow_ray_casting_stream_info_buffer_hdl, &shadow_ray_casting_stream_info);

	if (shadow_ray_casting_stream_info.counter > 0)
	{
		SRayIntersectionQueryData query_data;
		query_data.ray_buffer = _bsdf_ray_buffer_hdl;
		query_data.ray_count = shadow_ray_casting_stream_info.counter;
		query_data.ray_stream = _shadow_ray_casting_stream_buffer_hdl;
		query_data.ray_stream_info = _shadow_ray_casting_stream_info_buffer_hdl;
		query_data.result = _bsdf_ray_intersection_data_buffer_hdl;
		_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);

		query_data.ray_buffer = _light_ray_buffer_hdl;
		query_data.result = _light_ray_intersection_data_buffer_hdl;
		_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);

		shadow_ray_casting_stream_info.counter = 0;
		_device_manager_ptr->WriteBuffer(_shadow_ray_casting_stream_info_buffer_hdl, &shadow_ray_casting_stream_info);
	}

	{
		SRayIntersectionQueryData query_data;
		query_data.ray_buffer = _extension_ray_buffer_hdl;
		query_data.ray_count = _work_info_ptr->sample_count;
		query_data.ray_stream = _extension_ray_casting_stream_buffer_hdl;
		query_data.ray_stream_info = _extension_ray_casting_stream_info_buffer_hdl;
		query_data.result = _extension_ray_intersection_data_buffer_hdl;
		_intersector_ptr->QueryIntersection(_scene_ctx_ptr, query_data);
	}

	_device_manager_ptr->DispatchKernel(_controller_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(_work_info_ptr->sample_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->ReadBuffer(_shading_substream_info_buffer_hdl, shading_substream_infos);

	{
		SShaderEvalData eval_data;
		eval_data.shader_globals_buf = _shader_globals_buffer_hdl;
		eval_data.rngs_buf = _rng_buffer_hdl;
		eval_data.shading_stream_buf = _shading_stream_buffer_hdl;
		eval_data.shading_substream_infos_buf = _shading_substream_info_buffer_hdl;
		eval_data.sample_infos_buf = _light_sample_info_buffer_hdl;
		eval_data.shading_substream_infos = shading_substream_infos;
		shading_system->Eval(eval_data);
	}

	{
		SShaderSampleData sample_data;
		sample_data.shader_globals_buf = _shader_globals_buffer_hdl;
		sample_data.rngs_buf = _rng_buffer_hdl;
		sample_data.rand_samples_buf = _direct_illum_rand_sample_buffer_hdl;
		sample_data.shading_stream_buf = _shading_stream_buffer_hdl;
		sample_data.shading_substream_infos_buf = _shading_substream_info_buffer_hdl;
		sample_data.sample_infos_buf = _bsdf_sample_info_buffer_hdl;
		sample_data.shading_substream_infos = shading_substream_infos;
		shading_system->Sample(sample_data);
	}

	{
		SShaderSampleData sample_data;
		sample_data.shader_globals_buf = _shader_globals_buffer_hdl;
		sample_data.rngs_buf = _rng_buffer_hdl;
		sample_data.rand_samples_buf = _indirect_illum_rand_sample_buffer_hdl;
		sample_data.shading_stream_buf = _shading_stream_buffer_hdl;
		sample_data.shading_substream_infos_buf = _shading_substream_info_buffer_hdl;
		sample_data.sample_infos_buf = _extension_sample_info_buffer_hdl;
		sample_data.shading_substream_infos = shading_substream_infos;
		shading_system->Sample(sample_data);
	}

	_device_manager_ptr->ReadBuffer(_shadow_ray_casting_stream_info_buffer_hdl, &shadow_ray_casting_stream_info);

	if (shadow_ray_casting_stream_info.counter > 0)
	{
		_device_manager_ptr->DispatchKernel(_prepare_shadow_rays_kernel_hdl,
		{ static_cast<U32>(std::ceil(static_cast<F32>(shadow_ray_casting_stream_info.counter) / static_cast<F32>(block_size))), 1, 1 },
		{ block_size, 1, 1 });
	}

	_device_manager_ptr->DispatchKernel(_prepare_extension_rays_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(_work_info_ptr->sample_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });


	for (U32 i = 0; i < mtl_count; i++)
	{
		shading_substream_infos[i].counter = 0;
	}

	_device_manager_ptr->WriteBuffer(_shading_substream_info_buffer_hdl, shading_substream_infos);

	_device_manager_ptr->DispatchKernel(_postprocessing_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(_work_info_ptr->sample_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	_device_manager_ptr->ReadBuffer(_framebuffer_hdl, &_work_info_ptr->result[0]);

	_finished_iteration_count++;

	if (_finished_iteration_count == _render_settings_manager_ptr->GetRenderSettingsConstRef().iteration_count)
	{
		_work_info_ptr->finished = true;
	}

	delete[] shading_substream_infos;
}

template <class DeviceManagerBackend_T>
void TIntegratorProgressiveDeferredUdpt<DeviceManagerBackend_T>::SetKernelIntegratorContext(SKernelHdl kernel_hdl)
{
	auto render_settings_manager_backend_ptr = _render_settings_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);

	U32 arg_idx = 0;
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _render_work_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, render_settings_manager_backend_ptr->GetRenderSettingsBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _path_state_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _scene_metadata_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _path_gen_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _path_gen_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _shadow_ray_casting_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _shadow_ray_casting_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _extension_ray_casting_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _extension_ray_casting_stream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _shading_stream_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _shading_substream_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _shader_globals_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _rng_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _direct_illum_rand_sample_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _indirect_illum_rand_sample_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _bsdf_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _light_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _extension_sample_info_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _extension_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _bsdf_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _light_ray_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _extension_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _bsdf_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _light_ray_intersection_data_buffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _framebuffer_hdl);
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, _accumulation_buffer_hdl);

	_gfx_context_arg_start_idx = arg_idx;
}

template <class DeviceManagerBackend_T>
void TIntegratorProgressiveDeferredUdpt<DeviceManagerBackend_T>::SetKernelGfxContext(SKernelHdl kernel_hdl)
{
	auto MeshBackendManagerPtr = _scene_ctx_ptr->mesh_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto AreaLightBackendManagerPtr = _scene_ctx_ptr->area_light_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto EnvLightBackendManagerPtr = _scene_ctx_ptr->env_light_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);
	auto CameraBackendManagerPtr = _scene_ctx_ptr->camera_manager_ptr->GetBackendManagerPtr(_device_manager_ptr);

	U32 arg_idx = _gfx_context_arg_start_idx;
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, CameraBackendManagerPtr->GetCameraBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, MeshBackendManagerPtr->GetMeshBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, AreaLightBackendManagerPtr->GetAreaLightsBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, EnvLightBackendManagerPtr->GetEnvLightBufferHdl());
	_device_manager_ptr->SetKernelArgTexture(kernel_hdl, arg_idx++, EnvLightBackendManagerPtr->GetEnvMapTextureHdl());
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, EnvLightBackendManagerPtr->GetEnvLightConditionalCdfBufferHdl());
	_device_manager_ptr->SetKernelArgBuffer(kernel_hdl, arg_idx++, EnvLightBackendManagerPtr->GetEnvLightMarginalCdfBufferHdl());
}

template<class DeviceManagerBackend_T>
void TIntegratorProgressiveDeferredUdpt<DeviceManagerBackend_T>::AllocBuffers()
{
	size_t buffer_size = 0;

	buffer_size = _work_info_ptr->sample_count * sizeof(SPathState);
	_path_state_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(SRay);
	_extension_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_bsdf_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_light_ray_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(F32x4);
	_framebuffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_accumulation_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(SIntersectionData);
	_bsdf_ray_intersection_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_light_ray_intersection_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_extension_ray_intersection_data_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(SShaderGlobals);
	_shader_globals_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(SRandomNumberGenerator);
	_rng_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(F32x2);
	_direct_illum_rand_sample_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_indirect_illum_rand_sample_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(SSampleInfo);
	_extension_sample_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_bsdf_sample_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_light_sample_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = _work_info_ptr->sample_count * sizeof(U32);
	_shadow_ray_casting_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_extension_ray_casting_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_path_gen_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = sizeof(SStreamInfo);
	_shadow_ray_casting_stream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_extension_ray_casting_stream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);
	_path_gen_stream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	U32 mtl_count = _scene_ctx_ptr->deferred_shading_system_ptr->GetMetadata().mtl_count;
	buffer_size = _work_info_ptr->sample_count * sizeof(U32) * mtl_count;
	_shading_stream_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	buffer_size = mtl_count * sizeof(SSubstreamInfo);
	_shading_substream_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size);

	SSceneMetadata metadata;
	metadata.mesh_count = _scene_ctx_ptr->mesh_manager_ptr->GetMeshArrayConstRef().data.size();
	metadata.prim_count = _scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef().size();
	metadata.area_light_count = _scene_ctx_ptr->area_light_manager_ptr->GetMetadata().light_count;
	metadata.env_light_count = _scene_ctx_ptr->env_light_manager_ptr->GetMetadata().env_light_count;
	metadata.material_count = mtl_count;
	buffer_size = sizeof(SSceneMetadata);
	_scene_metadata_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &metadata);

	SRenderWorkInfo work_info;
	work_info.sample_offset = _work_info_ptr->sample_offset;
	work_info.sample_count = _work_info_ptr->sample_count;
	buffer_size = sizeof(SRenderWorkInfo);
	_render_work_info_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &work_info);
}


template<>
void TIntegratorProgressiveDeferredUdpt<CDeviceManagerCuda>::CreateProgram()
{
	_integrator_program_hdl = _device_manager_ptr->CreateProgramFromCubin("data/Kernels/KIntegratorProgressiveDeferredUdpt.cubin");
}


template<>
void TIntegratorProgressiveDeferredUdpt<CDeviceManagerOcl>::CreateProgram()
{
	std::vector<const char*> filenames;

	filenames.push_back("data/Kernels/KIntegratorProgressiveDeferredUdpt.c");

	const char* compiler_options = WR_OCL_DEFAULT_COMPILER_OPTIONS;
	_integrator_program_hdl = _device_manager_ptr->CompileProgram(filenames, compiler_options);
}


template<>
void TIntegratorProgressiveDeferredUdpt<CDeviceManagerDebug>::CreateProgram(){}


template<>
void TIntegratorProgressiveDeferredUdpt<CDeviceManagerDebug>::CreateKernels()
{
	/*
	_init_kernel_hdl = _device_manager_ptr->CreateKernel(LH::Initialize, 24);
	_generate_paths_kernel_hdl = _device_manager_ptr->CreateKernel(LH::GeneratePaths, 24);
	_controller_kernel_hdl = _device_manager_ptr->CreateKernel(LH::ControlIntegration, 24);
	_postprocessing_kernel_hdl = _device_manager_ptr->CreateKernel(LH::AccumulatePixelValues, 24);
	*/
}


template<class DeviceManagerBackend_T>
void TIntegratorProgressiveDeferredUdpt<DeviceManagerBackend_T>::CreateKernels()
{
	_init_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "Initialize", 34);
	_generate_paths_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "GeneratePaths", 34);
	_controller_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "Controller", 34);
	_prepare_shadow_rays_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareShadowRays", 34);
	_prepare_extension_rays_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "PrepareExtensionRays", 34);
	_postprocessing_kernel_hdl = _device_manager_ptr->CreateKernel(_integrator_program_hdl, "AccumulatePixelValues", 34);
}

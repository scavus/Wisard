/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "Scene/GfxResourceManager.hpp"
#include "Scene/SceneContext.hpp"
#include "Renderer/LoadBalancer.hpp"
#include "Renderer/RenderSettingsManagerProgressiveUdpt.hpp"

template<class DeviceManagerBackend_T = std::nullptr_t>
class TIntegratorProgressiveUdpt
{
	public:
		using DistributionPolicy = CDistributionPolicyDefault;
		using SceneContext = SSceneContext;
		using RenderSettingsManager = CRenderSettingsManagerProgressiveUdpt;

		TIntegratorProgressiveUdpt(){};
		~TIntegratorProgressiveUdpt(){};

		TIntegratorProgressiveUdpt(DeviceManagerBackend_T* device_manager_ptr, RenderSettingsManager* render_settings_manager_ptr,
			const DistributionPolicy::SRenderWorkInfo* work_info_ptr, const SceneContext* scene_ctx_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_render_settings_manager_ptr = render_settings_manager_ptr;
			_work_info_ptr = work_info_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
		};

		void SetDeviceManagerPtr(DeviceManagerBackend_T* device_manager_ptr) { _device_manager_ptr = device_manager_ptr; };
		void SetWorkInfo(const DistributionPolicy::SRenderWorkInfo* work_info_ptr) { _work_info_ptr = work_info_ptr; };
		void SetRenderSettingsManager(RenderSettingsManager* render_settings_manager_ptr) { _render_settings_manager_ptr = render_settings_manager_ptr; };
		void SetSceneCtxPtr(const SceneContext* scene_ctx_ptr) { _scene_ctx_ptr = scene_ctx_ptr; };

		const DeviceManagerBackend_T* GetDeviceManagerPtr() { return _device_manager_ptr; };

		void AllocBuffers();

		void CreateProgram();

		void CreateKernels();

		void Init();

		void Execute();

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;

		const SceneContext* _scene_ctx_ptr;

		const DistributionPolicy::SRenderWorkInfo* _work_info_ptr;
		RenderSettingsManager* _render_settings_manager_ptr;

		SBufferHdl _path_state_buffer_hdl;
		SBufferHdl _direct_illum_data_buffer_hdl;
		SBufferHdl _framebuffer_hdl;
		SBufferHdl _accumulation_buffer_hdl;
		SBufferHdl _scene_metadata_buffer_hdl;
		// SBufferHdl _render_settings_buffer_hdl;

		SProgramHdl _integrator_program_hdl;
		SKernelHdl _init_kernel_hdl;
		SKernelHdl _generate_paths_kernel_hdl;
		SKernelHdl _extend_paths_kernel_hdl;
		SKernelHdl _eval_direct_illum_kernel_hdl;
		SKernelHdl _postprocessing_kernel_hdl;
};

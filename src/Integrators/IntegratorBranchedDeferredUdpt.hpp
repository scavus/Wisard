/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Scene/GfxResourceManager.hpp"
#include "Scene/SceneContext.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "Renderer/LoadBalancer.hpp"
#include "Renderer/RenderSettingsManagerBranchedUdpt.hpp"
#include "Intersectors/ISceneIntersector.hpp"
#include "Integrators/IIntegrator.hpp"
#include "Integrators/IIntegratorFactory.hpp"

template<class DeviceManagerBackend_T = std::nullptr_t>
class TIntegratorBranchedDeferredUdpt : public IIntegrator
{
	public:
		using DistributionPolicy = CDistributionPolicyTiled;
		using SceneContext = SSceneContext;
		using RenderSettingsManager = CRenderSettingsManagerBranchedUdpt;

		TIntegratorBranchedDeferredUdpt(){};
		~TIntegratorBranchedDeferredUdpt(){};

		void SetDeviceManagerPtr(DeviceManagerBackend_T* device_manager_ptr) { _device_manager_ptr = device_manager_ptr; };
		void SetWorkInfo(DistributionPolicy::SRenderWorkInfo* work_info_ptr) { _work_info_ptr = work_info_ptr; _tile_it = _work_info_ptr->tiles.begin(); };
		void SetRenderSettingsManager(RenderSettingsManager* render_settings_manager_ptr) { _render_settings_manager_ptr = render_settings_manager_ptr; };
		void SetSceneCtxPtr(const SceneContext* scene_ctx_ptr) { _scene_ctx_ptr = scene_ctx_ptr; };
		void SetSceneIntersectorPtr(ISceneIntersector* intersector_ptr) { _intersector_ptr = intersector_ptr; };

		const DeviceManagerBackend_T* GetDeviceManagerPtr() { return _device_manager_ptr; };

		void AllocBuffers();
		void CreateProgram();
		void CreateKernels();

		virtual void Init() override;
		virtual void Update() override;
		virtual void Execute() override;

	protected:

	private:
		enum EIllumPass
		{
			DIRECT,
			INDIRECT
		};

		void AllocBaseBuffers();
		void ReleaseBaseBuffers();

		void AllocPostBranchBuffers(EIllumPass pass, size_t branched_path_count);
		void ReleasePostBranchBuffers(EIllumPass pass);

		void AllocPersistentBuffers();
		void ReleasePersistentBuffers();

		void SetKernelIntegratorContext(SKernelHdl kernel_hdl);
		void SetKernelGfxContext(SKernelHdl kernel_hdl);
		void ExecuteTile();

		void DirectIllumPass();
		void IndirectIllumPass();

		DeviceManagerBackend_T* _device_manager_ptr;

		const SceneContext* _scene_ctx_ptr;

		DistributionPolicy::SRenderWorkInfo* _work_info_ptr;
		std::vector<DistributionPolicy::STileInfo>::const_iterator _tile_it;

		DistributionPolicy::STileInfo* _current_tile_ptr;

		RenderSettingsManager* _render_settings_manager_ptr;

		ISceneIntersector* _intersector_ptr;

		SStreamInfo active_path_stream_info;
		SStreamInfo shadow_ray_stream_info;
		SStreamInfo extension_ray_stream_info;
		SSubstreamInfo* base_shading_substream_infos;
		SSubstreamInfo* branched_shading_substream_infos;

		// Buffers
		SBufferHdl render_work_info_buffer_hdl;
		SBufferHdl render_settings_buffer_hdl;

		SBufferHdl base_path_state_buffer_hdl;
		SBufferHdl base_active_path_stream_buffer_hdl;
		SBufferHdl base_active_path_stream_info_buffer_hdl;
		SBufferHdl base_rng_buffer_hdl;
		SBufferHdl base_shader_globals_buffer_hdl;
		SBufferHdl base_shading_stream_buffer_hdl;
		SBufferHdl base_shading_substream_info_buffer_hdl;

		SBufferHdl branched_path_state_buffer_hdl;
		SBufferHdl branched_active_path_stream_buffer_hdl;
		SBufferHdl branched_active_path_stream_info_buffer_hdl;
		SBufferHdl branched_rng_buffer_hdl;
		SBufferHdl branched_shader_globals_buffer_hdl;
		SBufferHdl branched_shading_stream_buffer_hdl;
		SBufferHdl branched_shading_substream_info_buffer_hdl;

		SBufferHdl aa_rand_sample_buffer_hdl;
		SBufferHdl direct_illum_rand_sample_buffer_hdl;
		SBufferHdl indirect_illum_rand_sample_buffer_hdl;
		SBufferHdl bsdf_sample_info_buffer_hdl;
		SBufferHdl light_sample_info_buffer_hdl;
		// SBufferHdl extension_sample_info_buffer_hdl;
		SBufferHdl bsdf_ray_buffer_hdl;
		SBufferHdl light_ray_buffer_hdl;
		// SBufferHdl extension_ray_buffer_hdl;



		SBufferHdl bsdf_ray_intersection_data_buffer_hdl;
		SBufferHdl light_ray_intersection_data_buffer_hdl;

		// SBufferHdl extension_ray_intersection_data_buffer_hdl;


		SBufferHdl shadow_stream_buffer_hdl;
		SBufferHdl shadow_stream_info_buffer_hdl;

		// SBufferHdl extension_ray_stream_buffer_hdl;
		SBufferHdl extension_ray_stream_info_buffer_hdl;


		SBufferHdl base_extension_sample_info_buffer_hdl;
		SBufferHdl base_extension_ray_buffer_hdl;
		SBufferHdl base_extension_ray_stream_buffer_hdl;
		SBufferHdl base_extension_ray_intersection_data_buffer_hdl;

		SBufferHdl branched_extension_sample_info_buffer_hdl;
		SBufferHdl branched_extension_ray_buffer_hdl;
		SBufferHdl branched_extension_ray_stream_buffer_hdl;
		SBufferHdl branched_extension_ray_intersection_data_buffer_hdl;



		SBufferHdl framebuffer_buffer_hdl;

		SBufferHdl scene_metadata_buffer_hdl;
		SBufferHdl camera_buffer_hdl;
		SBufferHdl mesh_buffer_hdl;
		SBufferHdl area_light_buffer_hdl;
		SBufferHdl area_light_disk_buffer_hdl;
		SBufferHdl area_light_sphere_buffer_hdl;
		SBufferHdl area_light_quad_buffer_hdl;
		SBufferHdl env_light_buffer_hdl;
		STextureHdl env_map_texture_hdl;
		SBufferHdl env_light_conditional_cdf_buffer_hdl;
		SBufferHdl env_light_marginal_cdf_buffer_hdl;

		// Program and Kernels
		SProgramHdl _integrator_program_hdl;

		SKernelHdl _init_kernel_hdl;
		SKernelHdl _generate_paths_kernel_hdl;
		SKernelHdl _prepare_direct_illum_branched_samples_kernel_hdl;
		SKernelHdl _prepare_indirect_illum_branched_samples_kernel_hdl;
		SKernelHdl _prepare_generic_illum_samples_kernel_hdl;
		SKernelHdl _prepare_direct_illum_branched_shading_stream_kernel_hdl;
		SKernelHdl _prepare_indirect_illum_branched_shading_stream_kernel_hdl;
		SKernelHdl _contrib_direct_illum_single_kernel_hdl;
		SKernelHdl _contrib_direct_illum_branched_kernel_hdl;
		SKernelHdl _contrib_indirect_illum_kernel_hdl;
		SKernelHdl _sample_lights_kernel_hdl;
		SKernelHdl _prepare_shadow_rays_kernel_hdl;
		SKernelHdl _prepare_extension_rays_kernel_hdl;
		SKernelHdl _controller_kernel_hdl;
		SKernelHdl _postprocessing_kernel_hdl;
};

class CIntegratorBranchedDeferredUdptFactory : public IIntegratorFactory
{
	public:
		CIntegratorBranchedDeferredUdptFactory() {};
		~CIntegratorBranchedDeferredUdptFactory() {};

		virtual IIntegrator* Create() override;

	protected:

	private:

};

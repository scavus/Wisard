/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>

#define STB_IMAGE_IMPLEMENTATION
#include "../thirdparty/stb/stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../thirdparty/stb/stb_image_write.h"

#include "../thirdparty/cJSON/cJSON.h"

#include "Api/Wr.h"

WrHandle h_scene_ctx;
WrHandle h_render_engine;

int main(int argc, char const *argv[])
{
	if (argc == 1)
	{
		return 0;
	}

    wrInitialize();

    U32 device_count = wrGetAvailableDeviceCount();
    WrDeviceId* device_ids = new WrDeviceId[device_count];
	wrGetAvailableDevices(device_ids);

    std::string settings_filename;
    std::string scene_filename;
    std::string output_filename;

	U32 i = 1;

    while (i < argc)
    {
		const std::string arg = argv[i];

        if (arg == "-r" || arg == "--render-settings")
        {
            settings_filename = std::string(argv[i + 1]);
        }
        else if (arg == "-s" || arg == "--scene")
        {
            scene_filename = std::string(argv[i + 1]);
        }
        else if (arg == "-o" || arg == "--output")
        {
            output_filename = std::string(argv[i + 1]);
        }
        else if (arg == "-d" || arg == "--list-devices")
        {
            for (U32 k = 0; k < device_count; k++)
            {
                WrDeviceInfo device_info;
                wrGetDeviceInfo(device_ids[k], &device_info);

                const std::string backend = argv[i + 1];

                if (backend == "cuda")
                {
                    if (std::string(device_info.backend_type) == "CUDA")
                    {
                        std::cout << "Device " << k << ": " << device_info.device_name << std::endl;
                    }
                }
                else if (backend == "ocl")
                {
                    if (std::string(device_info.backend_type) == "OCL")
                    {
                        std::cout << "Device " << k << ": " << device_info.device_name << std::endl;
                    }
                }
                else if (backend == "all")
                {
                    std::cout << "Device " << k << ": " << device_info.device_name << std::endl;
                }
            }

            wrDeinitialize();
            delete[] device_ids;
            return 0;
        }

        i += 2;
    }

    std::string settings_str;
    std::ifstream settings_file(settings_filename, std::ios::in);

	if (settings_file.is_open())
	{
		settings_file.seekg(0, std::ios::end);
		size_t filesize = settings_file.tellg();
		settings_str.resize(settings_file.tellg());
		settings_file.seekg(0, std::ios::beg);
		settings_file.read(&settings_str[0], settings_str.size());
		settings_file.close();
	}

	cJSON* json_obj = cJSON_Parse(settings_str.c_str());

	std::vector<WrDeviceId> enabled_device_ids;

	cJSON* json_arr = nullptr;
	U32 json_arr_size = 0;

	json_arr = cJSON_GetObjectItem(json_obj, "enabled_cuda_devices");
	json_arr_size = cJSON_GetArraySize(json_arr);

	for (U32 i = 0; i < json_arr_size; i++)
	{
		U32 device_num = cJSON_GetArrayItem(json_arr, i)->valueint;
		wrRegisterDevice(device_ids[device_num]);
		enabled_device_ids.push_back(device_ids[device_num]);
	}

	json_arr = cJSON_GetObjectItem(json_obj, "enabled_ocl_devices");
	json_arr_size = cJSON_GetArraySize(json_arr);

	for (U32 i = 0; i < json_arr_size; i++)
	{
		U32 device_num = cJSON_GetArrayItem(json_arr, i)->valueint;
		wrRegisterDevice(device_ids[device_num]);
		enabled_device_ids.push_back(device_ids[device_num]);
	}

	WrSceneCtxProperties scene_ctx_props;
	h_scene_ctx = wrCreateSceneCtx(scene_ctx_props, scene_filename.c_str());

    WrRenderSettingsCommon common_settings;
	common_settings.render_width = cJSON_GetObjectItem(json_obj, "render_width")->valueint;
	common_settings.render_height = cJSON_GetObjectItem(json_obj, "render_height")->valueint;
	common_settings.tile_width = cJSON_GetObjectItem(json_obj, "tile_width")->valueint;
	common_settings.tile_height = cJSON_GetObjectItem(json_obj, "tile_height")->valueint;

	std::string integrator_type = cJSON_GetObjectItem(json_obj, "integrator_type")->valuestring;

    if (integrator_type == "BranchedDeferredUdpt")
    {
        WrRenderSettingsBranchedUdpt settings;
        settings.common = common_settings;
		settings.aa_sample_count = cJSON_GetObjectItem(json_obj, "aa_sample_count")->valueint;
		settings.direct_illum_sample_count = cJSON_GetObjectItem(json_obj, "di_sample_count")->valueint;
		settings.indirect_illum_sample_count = cJSON_GetObjectItem(json_obj, "ii_sample_count")->valueint;
		settings.max_indirect_bounce_count = cJSON_GetObjectItem(json_obj, "max_indirect_bounce_count")->valueint;
		settings.enable_mis = cJSON_GetObjectItem(json_obj, "enable_mis")->type == cJSON_True;
		settings.enable_rr = cJSON_GetObjectItem(json_obj, "enable_rr")->type == cJSON_True;

        WrRenderEngineProperties props;
        props.integrator_type = integrator_type.c_str();
        props.render_settings = &settings;
        h_render_engine = wrCreateRenderEngine(h_scene_ctx, props);
    }
    else if (integrator_type == "ProgressiveDeferredUdpt")
    {
        WrRenderSettingsProgressiveUdpt settings;
        settings.common = common_settings;
		settings.iteration_count = cJSON_GetObjectItem(json_obj, "iteration_count")->valueint;
		settings.max_indirect_bounce_count = cJSON_GetObjectItem(json_obj, "max_indirect_bounce_count")->valueint;
		settings.enable_mis = cJSON_GetObjectItem(json_obj, "enable_mis")->type == cJSON_True;
		settings.enable_rr = cJSON_GetObjectItem(json_obj, "enable_rr")->type == cJSON_True;

        WrRenderEngineProperties props;
        props.integrator_type = integrator_type.c_str();
        props.render_settings = &settings;
        h_render_engine = wrCreateRenderEngine(h_scene_ctx, props);
    }

    for (WrDeviceId device_id : enabled_device_ids)
    {
        WrSceneIntersectorProperties scene_intersector_props;
        scene_intersector_props.device_id = device_id;
		scene_intersector_props.mesh_intersector_type = cJSON_GetObjectItem(json_obj, "mesh_intersector_type")->valuestring;
		scene_intersector_props.area_light_intersector_type = cJSON_GetObjectItem(json_obj, "area_light_intersector_type")->valuestring;
        WrHandle h_scene_intersector = wrCreateSceneIntersector(h_scene_ctx, scene_intersector_props);

		WrIntegratorProperties integrator_props;
        integrator_props.device_id = device_id;
        integrator_props.h_scene_intersector = h_scene_intersector;
        wrAllocIntegrator(h_render_engine, integrator_props);
    }

	while (true)
	{
		S32 status_flag = wrRender(h_render_engine);

		if (status_flag == WR_RENDER_STATUS_FLAG_HALT)
		{
			break;
		}
	}

	WrFramebuffer framebuffer;
	wrGetFramebuffer(h_render_engine, &framebuffer);

	U8* raw_image = new U8[common_settings.render_width * common_settings.render_height * 3];
	F32x4* const color_buffer = framebuffer.color_buffer;

	for (U32 j = 0; j < framebuffer.height; j++)
	{
		for (U32 i = 0; i < framebuffer.width; i++)
		{
			U32 k = j * common_settings.render_width + i;
			U32 p = k * 3;

			raw_image[p + 0] = (U8)(std::min(std::fabs(color_buffer[k].x) * 255 + (0.5f), (255.0f)));
			raw_image[p + 1] = (U8)(std::min(std::fabs(color_buffer[k].y) * 255 + (0.5f), (255.0f)));
			raw_image[p + 2] = (U8)(std::min(std::fabs(color_buffer[k].z) * 255 + (0.5f), (255.0f)));
		}
	}

	// TODO: Temporary
	stbi_write_bmp(output_filename.c_str(), common_settings.render_width, common_settings.render_height, 3, raw_image);

    wrDeinitialize();
    delete[] device_ids;
    return 0;
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "DeviceManagerOcl.hpp"

U32 CDeviceManagerOcl::id = 0;

static const cl_channel_order CHANNEL_ORDER_MAP[] =
{
	/* R_U8				*/ CL_R,
	/* R_S8				*/ CL_R,
	/* R_U16			*/ CL_R,
	/* R_S16			*/ CL_R,
	/* R_NORM_U8		*/ CL_R,
	/* R_NORM_S8		*/ CL_R,
	/* R_NORM_U16		*/ CL_R,
	/* R_NORM_S16		*/ CL_R,
	/* R_F32			*/ CL_R,

	/* RG_U8			*/ CL_RG,
	/* RG_S8			*/ CL_RG,
	/* RG_U16			*/ CL_RG,
	/* RG_S16			*/ CL_RG,
	/* RG_NORM_U8		*/ CL_RG,
	/* RG_NORM_S8		*/ CL_RG,
	/* RG_NORM_U16		*/ CL_RG,
	/* RG_NORM_S16		*/ CL_RG,
	/* RG_F32			*/ CL_RG,

	/* RGBA_U8			*/ CL_RGBA,
	/* RGBA_S8			*/ CL_RGBA,
	/* RGBA_U16			*/ CL_RGBA,
	/* RGBA_S16			*/ CL_RGBA,
	/* RGBA_NORM_U8		*/ CL_RGBA,
	/* RGBA_NORM_S8		*/ CL_RGBA,
	/* RGBA_NORM_U16	*/ CL_RGBA,
	/* RGBA_NORM_S16	*/ CL_RGBA,
	/* RGBA_F32			*/ CL_RGBA
};

static const cl_channel_type CHANNEL_TYPE_MAP[] =
{
	/* R_U8				*/ CL_UNSIGNED_INT8,
	/* R_S8				*/ CL_SIGNED_INT8,
	/* R_U16			*/ CL_UNSIGNED_INT16,
	/* R_S16			*/ CL_SIGNED_INT16,
	/* R_NORM_U8		*/ CL_UNORM_INT8,
	/* R_NORM_S8		*/ CL_SNORM_INT8,
	/* R_NORM_U16		*/ CL_UNORM_INT16,
	/* R_NORM_S16		*/ CL_SNORM_INT16,
	/* R_F32			*/ CL_FLOAT,

	/* RG_U8			*/ CL_UNSIGNED_INT8,
	/* RG_S8			*/ CL_SIGNED_INT8,
	/* RG_U16			*/ CL_UNSIGNED_INT16,
	/* RG_S16			*/ CL_SIGNED_INT16,
	/* RG_NORM_U8		*/ CL_UNORM_INT8,
	/* RG_NORM_S8		*/ CL_SNORM_INT8,
	/* RG_NORM_U16		*/ CL_UNORM_INT16,
	/* RG_NORM_S16		*/ CL_SNORM_INT16,
	/* RG_F32			*/ CL_FLOAT,

	/* RGBA_U8			*/ CL_UNSIGNED_INT8,
	/* RGBA_S8			*/ CL_SIGNED_INT8,
	/* RGBA_U16			*/ CL_UNSIGNED_INT16,
	/* RGBA_S16			*/ CL_SIGNED_INT16,
	/* RGBA_NORM_U8		*/ CL_UNORM_INT8,
	/* RGBA_NORM_S8		*/ CL_SNORM_INT8,
	/* RGBA_NORM_U16	*/ CL_UNORM_INT16,
	/* RGBA_NORM_S16	*/ CL_SNORM_INT16,
	/* RGBA_F32			*/ CL_FLOAT
};

CDeviceManagerOcl::CDeviceManagerOcl()
{
	/*
	cl_platform_id* ocl_platform_ids;
	U32 ocl_platform_count;

	clGetPlatformIDs(0, nullptr, &ocl_platform_count);

	printf("PlatformCount: %d\n", ocl_platform_count);

	ocl_platform_ids = new cl_platform_id[ocl_platform_count];

	clGetPlatformIDs(ocl_platform_count, ocl_platform_ids, nullptr);
	// 0 is intel
	// 2 is nvidia
	U32 DeviceIdx = 0;
	// TODO: Check for multiple devices
	U32 device_count;
	clGetDeviceIDs(ocl_platform_ids[DeviceIdx], CL_DEVICE_TYPE_GPU, 0, nullptr, &device_count);

	printf("device_count: %d\n", device_count);

	clGetDeviceIDs(ocl_platform_ids[DeviceIdx], CL_DEVICE_TYPE_GPU, 1, &_ocl_device_id, nullptr);

	size_t DeviceInfoSize;

	clGetDeviceInfo(_ocl_device_id, CL_DEVICE_NAME, 0, nullptr, &DeviceInfoSize);
	char* DeviceName = new char[DeviceInfoSize];
	clGetDeviceInfo(_ocl_device_id, CL_DEVICE_NAME, DeviceInfoSize, DeviceName, nullptr);

	clGetDeviceInfo(_ocl_device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, 0, nullptr, &DeviceInfoSize);
	size_t MaxWorkGroupSize;
	clGetDeviceInfo(_ocl_device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, DeviceInfoSize, &MaxWorkGroupSize, nullptr);

	clGetDeviceInfo(_ocl_device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, 0, nullptr, &DeviceInfoSize);
	size_t MaxWorkItemSizes[3];
	clGetDeviceInfo(_ocl_device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, DeviceInfoSize, MaxWorkItemSizes, nullptr);

	printf("DeviceName: %s\n", DeviceName);

	printf("MaxWorkGroupSize: %d\n", MaxWorkGroupSize);

	printf("MaxWorkItemSizes: [%d, %d, %d]\n", MaxWorkItemSizes[0], MaxWorkItemSizes[1], MaxWorkItemSizes[2]);

	cl_context_properties ocl_ctx_properties[] =
	{
		CL_CONTEXT_PLATFORM,
		(cl_context_properties)ocl_platform_ids[DeviceIdx],
		0,
	};

	S32 error_code;
	_device_ctx.ocl_ctx = clCreateContext(ocl_ctx_properties, 1, &_ocl_device_id, nullptr, nullptr, &error_code);

	_ocl_command_queue = clCreateCommandQueue(_device_ctx.ocl_ctx, _ocl_device_id, 0, nullptr);

	SBufferOcl null_buffer;
	null_buffer.id = 0;
	_device_ctx.buffers.insert(std::pair<SBufferHdl, SBufferOcl>(0, null_buffer));

	STexture1dOcl null_texture1d;
	null_texture1d.id = 0;
	_device_ctx.texture1ds.insert(std::pair<STextureHdl, STexture1dOcl>(0, null_texture1d));

	STexture2dOcl null_texture2d;
	null_texture2d.id = 0;
	_device_ctx.texture2ds.insert(std::pair<STextureHdl, STexture2dOcl>(0, null_texture2d));

	STexture3dOcl null_texture3d;
	null_texture3d.id = 0;
	_device_ctx.texture3ds.insert(std::pair<STextureHdl, STexture3dOcl>(0, null_texture3d));

	_device_ctx.created_program_count = 0;
	_device_ctx.created_kernel_count = 0;
	_device_ctx.created_buffer_count = 1;
	_device_ctx.created_texture1d_count = 1;
	_device_ctx.created_texture2d_count = 1;
	_device_ctx.created_texture3d_count = 1;

	delete[] ocl_platform_ids;

	_id = id++;
	*/
}

CDeviceManagerOcl::CDeviceManagerOcl(const SDeviceInfoOcl& device_info)
{
	_ocl_device_id = device_info.DeviceId;

	cl_context_properties ocl_ctx_properties[] =
	{
		CL_CONTEXT_PLATFORM,
		(cl_context_properties)device_info.PlatformId,
		0,
	};

	S32 error_code;
	_device_ctx.ocl_ctx = clCreateContext(ocl_ctx_properties, 1, &_ocl_device_id, nullptr, nullptr, &error_code);

	_ocl_command_queue = clCreateCommandQueue(_device_ctx.ocl_ctx, _ocl_device_id, 0, nullptr);

	_device_ctx.created_program_count = 0;
	_device_ctx.created_kernel_count = 0;
	_device_ctx.created_buffer_count = 0;
	_device_ctx.created_texture1d_count = 0;
	_device_ctx.created_texture2d_count = 0;
	_device_ctx.created_texture3d_count = 0;

	SBufferOcl null_buffer;
	null_buffer.id = 0;
	_device_ctx.buffers.insert(std::pair<SBufferHdl, SBufferOcl>(_device_ctx.created_buffer_count++, null_buffer));

	STexture1dDesc tex1d_desc;
	tex1d_desc.width = 4;
	tex1d_desc.format = WrTextureFormat::R_NORM_U8;
	tex1d_desc.usage = WrTextureUsage::READ_ONLY;
	U8* null_tex1d_data = new U8[4];
	// TODO: CreateTexture1D(tex1d_desc, null_tex1d_data);
	delete[] null_tex1d_data;

	STexture2dDesc tex2d_desc;
	tex2d_desc.width = 4;
	tex2d_desc.height = 4;
	tex2d_desc.format = WrTextureFormat::R_NORM_U8;
	tex2d_desc.usage = WrTextureUsage::READ_ONLY;
	U8* null_tex2d_data = new U8[4 * 4];
	CreateTexture2D(tex2d_desc, null_tex2d_data);
	delete[] null_tex2d_data;

	STexture3dDesc tex3d_desc;
	tex3d_desc.width = 4;
	tex3d_desc.height = 4;
	tex3d_desc.depth = 4;
	tex3d_desc.format = WrTextureFormat::R_NORM_U8;
	tex3d_desc.usage = WrTextureUsage::READ_ONLY;
	U8* null_tex3d_data = new U8[4 * 4 * 4];
	// TODO: CreateTexture3D(tex3d_desc, null_tex3d_data);
	delete[] null_tex3d_data;

	_id = id++;
}

CDeviceManagerOcl::~CDeviceManagerOcl()
{
	// clReleaseContext(mOCLContext);
	// clReleaseCommandQueue(_ocl_command_queue);
	// clReleaseProgram(mOCLProgram);
}

void CDeviceManagerOcl::GetAvailableDeviceInfos(std::vector<SDeviceInfoOcl>& device_infos)
{
	cl_platform_id* ocl_platform_ids;
	cl_device_id* OCLDeviceIDs;
	U32 ocl_platform_count;

	clGetPlatformIDs(0, nullptr, &ocl_platform_count);

	ocl_platform_ids = new cl_platform_id[ocl_platform_count];

	clGetPlatformIDs(ocl_platform_count, ocl_platform_ids, nullptr);

	for (U32 platform_idx = 0; platform_idx < ocl_platform_count; platform_idx++)
	{
		cl_platform_id PlatformId = ocl_platform_ids[platform_idx];

		U32 device_count;
		clGetDeviceIDs(PlatformId, CL_DEVICE_TYPE_GPU, 0, nullptr, &device_count);

		OCLDeviceIDs = new cl_device_id[device_count];

		clGetDeviceIDs(PlatformId, CL_DEVICE_TYPE_GPU, device_count, OCLDeviceIDs, nullptr);

		for (U32 DeviceIdx = 0; DeviceIdx < device_count; DeviceIdx++)
		{
			cl_device_id DeviceId = OCLDeviceIDs[DeviceIdx];
			SDeviceInfoOcl DeviceInfo;
			size_t DeviceInfoSize;

			DeviceInfo.PlatformId = PlatformId;
			DeviceInfo.DeviceId = DeviceId;

			clGetDeviceInfo(DeviceId, CL_DEVICE_NAME, 0, nullptr, &DeviceInfoSize);
			char* DeviceName = new char[DeviceInfoSize];
			clGetDeviceInfo(DeviceId, CL_DEVICE_NAME, DeviceInfoSize, DeviceName, nullptr);

			DeviceInfo.DeviceName = DeviceName;

			/*
			clGetDeviceInfo(DeviceId, CL_DEVICE_MAX_WORK_GROUP_SIZE, 0, nullptr, &DeviceInfoSize);
			U32 MaxWorkGroupSize;
			clGetDeviceInfo(DeviceId, CL_DEVICE_MAX_WORK_GROUP_SIZE, DeviceInfoSize, &MaxWorkGroupSize, nullptr);

			clGetDeviceInfo(DeviceId, CL_DEVICE_MAX_WORK_ITEM_SIZES, 0, nullptr, &DeviceInfoSize);
			U32 MaxWorkItemSizes[3];
			clGetDeviceInfo(DeviceId, CL_DEVICE_MAX_WORK_ITEM_SIZES, DeviceInfoSize, MaxWorkItemSizes, nullptr);
			*/
			device_infos.push_back(DeviceInfo);

		}

		delete[] OCLDeviceIDs;
	}

	delete[] ocl_platform_ids;
}

SKernelHdl CDeviceManagerOcl::CreateKernel(const SProgramHdl program_hdl, const char* kernel_name, const U32 arg_count)
{
	S32 error_code;
	SKernelOcl kernel;

	cl_kernel ApiKernelHandle = clCreateKernel(_device_ctx.programs[program_hdl], kernel_name, &error_code);

	CheckErrorCode(error_code);

	kernel.id = ApiKernelHandle;
	kernel.KernelInfo.ArgCount = arg_count;
	kernel.KernelInfo.Args = new void*[arg_count];

	_device_ctx.kernels.insert({ _device_ctx.created_kernel_count, kernel });

	return _device_ctx.created_kernel_count++;
}

SBufferHdl CDeviceManagerOcl::CreateBuffer(const size_t buffer_size)
{
	SBufferOcl buffer;

	S32 error_code;
	buffer.id = clCreateBuffer(_device_ctx.ocl_ctx, CL_MEM_READ_WRITE, buffer_size, nullptr, &error_code);
	CheckErrorCode(error_code);

	buffer.size = buffer_size;

	_device_ctx.buffers.insert({ _device_ctx.created_buffer_count, buffer });

	return _device_ctx.created_buffer_count++;
}

SBufferHdl CDeviceManagerOcl::CreateBuffer(const size_t buffer_size, const void* data_ptr)
{
	SBufferOcl buffer;

	S32 error_code;
	// buffer.BufferID = clCreateBuffer(_device_ctx.OCLContext, EMemUsage::READ_WRITE | CL_MEM_COPY_HOST_PTR, buffer_size, const_cast<void*>(data_ptr), &error_code);
	buffer.id = clCreateBuffer(_device_ctx.ocl_ctx, CL_MEM_READ_WRITE, buffer_size, nullptr, &error_code);
	CheckErrorCode(error_code);

	error_code = clEnqueueWriteBuffer(_ocl_command_queue, buffer.id, true, 0, buffer_size, const_cast<void*>(data_ptr), 0, nullptr, nullptr);
	CheckErrorCode(error_code);

	buffer.size = buffer_size;

	_device_ctx.buffers.insert({ _device_ctx.created_buffer_count, buffer });

	return _device_ctx.created_buffer_count++;
}

void CDeviceManagerOcl::DestroyBuffer(SBufferHdl handle)
{
	S32 error_code;
	error_code = clReleaseMemObject(_device_ctx.buffers[handle].id);
	CheckErrorCode(error_code);
}

STextureHdl CDeviceManagerOcl::CreateTexture2D(const STexture2dDesc& desc, const void* data_ptr)
{
	STexture2dOcl tex;

	S32 error_code;

	cl_image_format image_format;
	image_format.image_channel_order = CHANNEL_ORDER_MAP[desc.format];
	image_format.image_channel_data_type = CHANNEL_TYPE_MAP[desc.format];

	tex.id = clCreateImage2D(_device_ctx.ocl_ctx, desc.usage | CL_MEM_COPY_HOST_PTR,
		&image_format, desc.width, desc.height, 0, const_cast<void*>(data_ptr), &error_code);

	CheckErrorCode(error_code);

	tex.image_format = image_format;
	tex.width = desc.width;
	tex.height = desc.height;

	_device_ctx.texture2ds.insert({ _device_ctx.created_texture2d_count, tex });

	return _device_ctx.created_texture2d_count++;
}

void CDeviceManagerOcl::DestroyTexture2D(STextureHdl handle)
{
	S32 error_code = clReleaseMemObject(_device_ctx.texture2ds[handle].id);

	CheckErrorCode(error_code);
}

/*
U32 CDeviceManagerOcl::CreateSampler(const ESamplerAddrMode iAddrMode, const ESamplerFilterMode iFilterMode, const bool iNormalizedCoordsFlg)
{
	U32 oSamplerId;

	SSamplerOcl Sampler;

	S32 error_code;

	Sampler.SamplerId = clCreateSampler(_device_ctx.OCLContext, iNormalizedCoordsFlg, iAddrMode, iFilterMode, &error_code);

	CheckErrorCode(error_code);

	_device_ctx.Samplers.push_back(Sampler);

	oSamplerId = _device_ctx.Samplers.size() - 1;
	return oSamplerId;
}
*/

SProgramHdl CDeviceManagerOcl::CompileProgram(const std::vector<const char*>& filenames, const char* compiler_options)
{
	U32 FileCount = filenames.size();

	std::vector<std::string> ProgramSources;
	ProgramSources.reserve(FileCount);

	std::vector<size_t> ProgramSourceLengths;
	ProgramSourceLengths.reserve(FileCount);

	for (U32 FileIdx = 0; FileIdx < FileCount; FileIdx++)
	{
		std::ifstream filestream(filenames[FileIdx], std::ios::in | std::ios::binary);

		if (filestream)
		{
			std::string temp;

			filestream.seekg(0, std::ios::end);
			temp.resize(filestream.tellg());
			filestream.seekg(0, std::ios::beg);

			filestream.read(&temp[0], temp.size());

			filestream.close();

			ProgramSources.push_back(temp);
			ProgramSourceLengths.push_back(temp.size());
		}
	}

	std::vector<const char*> ProgramSourcesC;

	U32 ProgramSourceCount = ProgramSources.size();

	for (U32 idx = 0; idx < ProgramSourceCount; idx++)
	{
		ProgramSourcesC.push_back(ProgramSources[idx].c_str());
	}

	S32 error_code;

	cl_program Program;

	Program = clCreateProgramWithSource(_device_ctx.ocl_ctx, ProgramSources.size(), &ProgramSourcesC[0], &ProgramSourceLengths[0], &error_code);

	if (error_code != CL_SUCCESS)
	{
		printf("Error %d: Program Creation Failed\n", error_code);
	}

	/*
	U32 KernelSourceSize;
	clGetProgramInfo(_device_ctx.OCLRenderProgram, CL_PROGRAM_SOURCE, 0, NULL, &KernelSourceSize);
	char* KernelSource = new char[KernelSourceSize];
	clGetProgramInfo(_device_ctx.OCLRenderProgram, CL_PROGRAM_SOURCE, KernelSourceSize, KernelSource, NULL);

	printf("\n\n%s\n\n", KernelSource);
	*/

	error_code = clBuildProgram(Program, 1, &_ocl_device_id, compiler_options, nullptr, nullptr);

	size_t BuildLogSize;
	clGetProgramBuildInfo(Program, _ocl_device_id, CL_PROGRAM_BUILD_LOG, 0, nullptr, &BuildLogSize);
	char* BuildLog = new char[BuildLogSize];
	clGetProgramBuildInfo(Program, _ocl_device_id, CL_PROGRAM_BUILD_LOG, BuildLogSize, BuildLog, nullptr);

	std::ofstream BuildLogFile("KernelBuildLog.txt");

	BuildLogFile << BuildLog;

	// printf("\n\n%s\n\n", BuildLog);

	if (error_code != CL_SUCCESS)
	{
		printf("Error %d: Program Compilation Failed\n", error_code);
	}

	delete[] BuildLog;

	_device_ctx.programs.insert({ _device_ctx.created_program_count, Program });

	return _device_ctx.created_program_count++;
}

void CDeviceManagerOcl::ReadBuffer(const U32 handle, const U32 data_size, void* data_ptr)
{

	SBufferOcl buffer = _device_ctx.buffers[handle];

	S32 error_code = clEnqueueReadBuffer(_ocl_command_queue, buffer.id, true, 0, data_size, data_ptr, 0, nullptr, nullptr);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::ReadBuffer(SBufferHdl handle, void* data_ptr)
{
	SBufferOcl buffer = _device_ctx.buffers[handle];

	S32 error_code = clEnqueueReadBuffer(_ocl_command_queue, buffer.id, true, 0, buffer.size, data_ptr, 0, nullptr, nullptr);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::WriteBuffer(const U32 handle, const U32 data_size, const void* iData)
{
	SBufferOcl buffer = _device_ctx.buffers[handle];

	S32 error_code = clEnqueueWriteBuffer(_ocl_command_queue, buffer.id, true, 0, data_size, iData, 0, nullptr, nullptr);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::WriteBuffer(SBufferHdl handle, const void* data_ptr)
{
	SBufferOcl buffer = _device_ctx.buffers[handle];

	S32 error_code = clEnqueueWriteBuffer(_ocl_command_queue, buffer.id, true, 0, buffer.size, data_ptr, 0, nullptr, nullptr);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim)
{
	SKernelOcl kernel = _device_ctx.kernels[handle];

	U32 DimensionCount = static_cast<U32>(block_dim.x > 1) + static_cast<U32>(block_dim.y > 1) + static_cast<U32>(block_dim.z > 1);
	size_t LocalWorkSize[] = { block_dim.x, block_dim.y, block_dim.z };
	size_t GlobalWorkSize[] = { block_dim.x * grid_dim.x, block_dim.y * grid_dim.y, block_dim.z * grid_dim.z };

	S32 error_code = clEnqueueNDRangeKernel(_ocl_command_queue, kernel.id, DimensionCount, nullptr, GlobalWorkSize, LocalWorkSize, 0, nullptr, nullptr);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl)
{
	SKernelOcl kernel = _device_ctx.kernels[handle];
	SBufferOcl buffer = _device_ctx.buffers[buffer_arg_hdl];

	S32 error_code = clSetKernelArg(kernel.id, arg_idx, sizeof(cl_mem), &buffer.id);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl)
{
	SKernelOcl kernel = _device_ctx.kernels[handle];

	STexture2dOcl tex = _device_ctx.texture2ds[texture_arg_hdl];

	S32 error_code = clSetKernelArg(kernel.id, arg_idx, sizeof(cl_mem), &tex.id);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr)
{
	SKernelOcl kernel = _device_ctx.kernels[handle];

	S32 error_code = clSetKernelArg(kernel.id, arg_idx, value_size, value_arg_ptr);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::SynchronizeCommands()
{
	S32 error_code = clEnqueueBarrier(_ocl_command_queue);

	CheckErrorCode(error_code);
}

bool CDeviceManagerOcl::CheckErrorCode(S32 error_code)
{
	switch (error_code)
	{
		case CL_SUCCESS: return true;

		case CL_DEVICE_NOT_FOUND: printf("Error %d: CL_DEVICE_NOT_FOUND\n", error_code); break;
		case CL_DEVICE_NOT_AVAILABLE: printf("Error %d: CL_DEVICE_NOT_AVAILABLE\n", error_code); break;
		case CL_COMPILER_NOT_AVAILABLE: printf("Error %d: CL_COMPILER_NOT_AVAILABLE\n", error_code); break;
		case CL_MEM_OBJECT_ALLOCATION_FAILURE: printf("Error %d: CL_MEM_OBJECT_ALLOCATION_FAILURE\n", error_code); break;
		case CL_OUT_OF_RESOURCES: printf("Error %d: CL_OUT_OF_RESOURCES\n", error_code); break;
		case CL_OUT_OF_HOST_MEMORY: printf("Error %d: CL_OUT_OF_HOST_MEMORY\n", error_code); break;
		case CL_PROFILING_INFO_NOT_AVAILABLE: printf("Error %d: CL_PROFILING_INFO_NOT_AVAILABLE\n", error_code); break;
		case CL_MEM_COPY_OVERLAP: printf("Error %d: CL_MEM_COPY_OVERLAP\n", error_code); break;
		case CL_IMAGE_FORMAT_MISMATCH: printf("Error %d: CL_IMAGE_FORMAT_MISMATCH\n", error_code); break;
		case CL_IMAGE_FORMAT_NOT_SUPPORTED: printf("Error %d: CL_IMAGE_FORMAT_NOT_SUPPORTED\n", error_code); break;
		case CL_BUILD_PROGRAM_FAILURE: printf("Error %d: CL_BUILD_PROGRAM_FAILURE\n", error_code);
		case CL_MAP_FAILURE: printf("Error %d: CL_MAP_FAILURE\n", error_code); break;
		case CL_MISALIGNED_SUB_BUFFER_OFFSET: printf("Error %d: CL_MISALIGNED_SUB_BUFFER_OFFSET\n", error_code); break;
		case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST: printf("Error %d: CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST\n", error_code); break;

		case CL_INVALID_VALUE: printf("Error %d: CL_INVALID_VALUE\n", error_code); break;
		case CL_INVALID_DEVICE_TYPE: printf("Error %d: CL_INVALID_DEVICE_TYPE\n", error_code); break;
		case CL_INVALID_PLATFORM: printf("Error %d: CL_INVALID_PLATFORM\n", error_code); break;
		case CL_INVALID_DEVICE: printf("Error %d: CL_INVALID_DEVICE\n", error_code); break;
		case CL_INVALID_CONTEXT: printf("Error %d: CL_INVALID_CONTEXT\n", error_code); break;
		case CL_INVALID_QUEUE_PROPERTIES: printf("Error %d: CL_INVALID_QUEUE_PROPERTIES\n", error_code); break;
		case CL_INVALID_COMMAND_QUEUE: printf("Error %d: CL_INVALID_COMMAND_QUEUE\n", error_code); break;
		case CL_INVALID_HOST_PTR: printf("Error %d: CL_INVALID_HOST_PTR\n", error_code); break;
		case CL_INVALID_MEM_OBJECT: printf("Error %d: CL_INVALID_MEM_OBJECT\n", error_code); break;
		case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: printf("Error %d: CL_INVALID_IMAGE_FORMAT_DESCRIPTOR\n", error_code); break;
		case CL_INVALID_IMAGE_SIZE: printf("Error %d: CL_INVALID_IMAGE_SIZE\n", error_code); break;
		case CL_INVALID_SAMPLER: printf("Error %d: CL_INVALID_SAMPLER\n", error_code); break;
		case CL_INVALID_BINARY: printf("Error %d: CL_INVALID_BINARY\n", error_code); break;
		case CL_INVALID_BUILD_OPTIONS: printf("Error %d: CL_INVALID_BUILD_OPTIONS\n", error_code); break;
		case CL_INVALID_PROGRAM: printf("Error %d: CL_INVALID_PROGRAM\n", error_code); break;
		case CL_INVALID_PROGRAM_EXECUTABLE: printf("Error %d: CL_INVALID_PROGRAM_EXECUTABLE\n", error_code); break;
		case CL_INVALID_KERNEL_NAME: printf("Error %d: CL_INVALID_KERNEL_NAME\n", error_code); break;
		case CL_INVALID_KERNEL_DEFINITION: printf("Error %d: CL_INVALID_KERNEL_DEFINITION\n", error_code); break;
		case CL_INVALID_KERNEL: printf("Error %d: CL_INVALID_KERNEL\n", error_code); break;
		case CL_INVALID_ARG_INDEX: printf("Error %d: CL_INVALID_ARG_INDEX\n", error_code); break;
		case CL_INVALID_ARG_VALUE: printf("Error %d: CL_INVALID_ARG_VALUE\n", error_code); break;
		case CL_INVALID_ARG_SIZE: printf("Error %d: CL_INVALID_ARG_SIZE\n", error_code); break;
		case CL_INVALID_KERNEL_ARGS: printf("Error %d: CL_INVALID_KERNEL_ARGS\n", error_code); break;
		case CL_INVALID_WORK_DIMENSION: printf("Error %d: CL_INVALID_WORK_DIMENSION\n", error_code); break;
		case CL_INVALID_WORK_GROUP_SIZE: printf("Error %d: CL_INVALID_WORK_GROUP_SIZE\n", error_code); break;
		case CL_INVALID_WORK_ITEM_SIZE: printf("Error %d: CL_INVALID_WORK_ITEM_SIZE\n", error_code); break;
		case CL_INVALID_GLOBAL_OFFSET: printf("Error %d: CL_INVALID_GLOBAL_OFFSET\n", error_code); break;
		case CL_INVALID_EVENT_WAIT_LIST: printf("Error %d: CL_INVALID_EVENT_WAIT_LIST\n", error_code); break;
		case CL_INVALID_EVENT: printf("Error %d: CL_INVALID_EVENT\n", error_code); break;
		case CL_INVALID_OPERATION: printf("Error %d: CL_INVALID_OPERATION\n", error_code); break;
		case CL_INVALID_GL_OBJECT: printf("Error %d: CL_INVALID_GL_OBJECT\n", error_code); break;
		case CL_INVALID_BUFFER_SIZE: printf("Error %d: CL_INVALID_BUFFER_SIZE\n", error_code); break;
		case CL_INVALID_MIP_LEVEL: printf("Error %d: CL_INVALID_MIP_LEVEL\n", error_code); break;
		case CL_INVALID_GLOBAL_WORK_SIZE: printf("Error %d: CL_INVALID_GLOBAL_WORK_SIZE\n", error_code); break;
		case CL_INVALID_PROPERTY: printf("Error %d: CL_INVALID_PROPERTY\n", error_code); break;

		default:
			break;
	}

	return false;
}

void CDeviceManagerOcl::MapBuffer(const U32 handle, const EMapType iMapType, const bool iBlockingOpFlg, const U32 iMapRegionsOffset, const U32 iMapRegionSize, void* oMappedDataPtr)
{
	SBufferOcl buffer = _device_ctx.buffers[handle];

	S32 error_code;

	clEnqueueMapBuffer(_ocl_command_queue, buffer.id, iBlockingOpFlg, iMapType, iMapRegionsOffset, iMapRegionSize, 0, nullptr, nullptr, &error_code);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::UnmapBuffer(const U32 handle, void* mapped_data_ptr)
{
	SBufferOcl buffer = _device_ctx.buffers[handle];

	S32 error_code;

	error_code = clEnqueueUnmapMemObject(_ocl_command_queue, buffer.id, mapped_data_ptr, 0, nullptr, nullptr);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::Synchronize()
{
	S32 error_code = clFinish(_ocl_command_queue);

	CheckErrorCode(error_code);
}

void CDeviceManagerOcl::SwitchContext()
{
	return;
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include <cstring>

#include "DeviceManagerDebug.hpp"

std::atomic<U32> CDeviceManagerDebug::id = { 0 };

CDeviceManagerDebug::CDeviceManagerDebug()
{
	_id = id;
	++id;

	_device_ctx._created_kernel_count = 0;
	_device_ctx._created_buffer_count = 0;
	_device_ctx._created_texture_count = 0;
}

SBufferHdl CDeviceManagerDebug::CreateBuffer(const size_t buffer_size)
{
	SBufferDebug Buffer;
	Buffer.buffer_size = buffer_size;
	Buffer.BufferData = std::malloc(buffer_size);

	_device_ctx._buffers.insert({ _device_ctx._created_buffer_count, Buffer });

	return _device_ctx._created_buffer_count++;
}

SBufferHdl CDeviceManagerDebug::CreateBuffer(const size_t buffer_size, const void* data_ptr)
{
	SBufferDebug Buffer;
	Buffer.buffer_size = buffer_size;
	Buffer.BufferData = std::malloc(buffer_size);
	std::memcpy(Buffer.BufferData, data_ptr, buffer_size);

	_device_ctx._buffers.insert({ _device_ctx._created_buffer_count, Buffer });

	return _device_ctx._created_buffer_count++;
}

void CDeviceManagerDebug::ReadBuffer(SBufferHdl handle, void* data_ptr)
{
	auto& Buffer = _device_ctx._buffers[handle];
	std::memcpy(data_ptr, Buffer.BufferData, Buffer.buffer_size);
}

void CDeviceManagerDebug::WriteBuffer(SBufferHdl handle, const void* data_ptr)
{
	auto& Buffer = _device_ctx._buffers[handle];
	std::memcpy(Buffer.BufferData, data_ptr, Buffer.buffer_size);
}

SKernelHdl CDeviceManagerDebug::CreateKernel(const std::function<void(void**, const SWorkDimension&, const SWorkDimension&)>& iKernelFn, const U32 arg_count)
{
	SKernelDebug Kernel;
	Kernel.KernelFn = iKernelFn;
	Kernel.Args = new void*[arg_count];

	_device_ctx._kernels.insert({ _device_ctx._created_kernel_count, Kernel });

	return _device_ctx._created_kernel_count++;
}

void CDeviceManagerDebug::SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl)
{
	auto& Kernel = _device_ctx._kernels[handle];
	Kernel.Args[arg_idx] = _device_ctx._buffers[buffer_arg_hdl].BufferData;
}

void CDeviceManagerDebug::SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr)
{
	auto& Kernel = _device_ctx._kernels[handle];
	Kernel.Args[arg_idx] = const_cast<void*>(value_arg_ptr);
}

void CDeviceManagerDebug::DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim)
{
	auto& Kernel = _device_ctx._kernels[handle];

	SWorkDimension GridDim;
	GridDim.x = grid_dim.x;
	GridDim.y = grid_dim.y;
	GridDim.z = grid_dim.z;

	SWorkDimension BlockDim;
	BlockDim.x = block_dim.x;
	BlockDim.y = block_dim.y;
	BlockDim.z = block_dim.z;

	Kernel.KernelFn(Kernel.Args, GridDim, BlockDim);
}

void CDeviceManagerDebug::SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl)
{

}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <functional>
#include <atomic>
#include <limits>

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"
// #include "Kernels/KCommonTypes.h"

struct SBufferDebug
{
	size_t buffer_size;
	void* BufferData;
};

struct SKernelDebug
{
	std::function<void(void**, const /*LH::*/SWorkDimension&, const /*LH::*/SWorkDimension&)> KernelFn;
	void** Args;
};

struct SDeviceContextDebug
{
	std::map<SBufferHdl, SBufferDebug> _buffers;
	U32 _created_buffer_count;

	std::map<STextureHdl, void*> _textures;
	U32 _created_texture_count;

	std::map<SKernelHdl, SKernelDebug> _kernels;
	U32 _created_kernel_count;
};
/*
*/

class CDeviceManagerDebug : public TComputeDeviceManager<CDeviceManagerDebug>
{
	public:
		CDeviceManagerDebug();
		~CDeviceManagerDebug();

		static CDeviceManagerDebug* Create() { return new CDeviceManagerDebug; };

		SBufferHdl	CreateBuffer(const size_t buffer_size);
		SBufferHdl	CreateBuffer(const size_t buffer_size, const void* data_ptr);
		void		DestroyBuffer(SBufferHdl handle){};
		void		ReadBuffer(SBufferHdl handle, void* data_ptr);
		void		ReadBufferRange();
		void		WriteBuffer(SBufferHdl handle, const void* data_ptr);
		void		WriteBufferRange();

		STextureHdl CreateTexture1D(const STexture1dDesc& desc, const void* data_ptr){ return 0; };
		STextureHdl CreateTexture2D(const STexture2dDesc& desc, const void* data_ptr){ return 0; };
		STextureHdl CreateTexture3D(const STexture3dDesc& desc, const void* data_ptr){ return 0; };
		void		DestroyTexture1D(STextureHdl handle){};
		void		DestroyTexture2D(STextureHdl handle){};
		void		DestroyTexture3D(STextureHdl handle){};

		SKernelHdl	CreateKernel(const SProgramHdl program_hdl, const char* kernel_name, const U32 arg_count);
		SKernelHdl	CreateKernel(const std::function<void(void**, const /*LH::*/SWorkDimension&, const /*LH::*/SWorkDimension&)>& iKernelFn, const U32 arg_count);
		void		SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl);
		void		SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl);
		void		SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr);
		void		SetKernelArgs(const SKernelHdl handle);
		void		DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim);

		void		CompileProgram() {};
		void		CreateProgramFromPtx(const char* filename) {};
		SProgramHdl CreateProgramFromCubin(const char* filename) { return 0; };

		void Synchronize() {};
		void SwitchContext() {};

		U32 GetWarpSize() { return 1; };

		U32 GetDeviceComputePower() { return std::numeric_limits<U32>::max(); };

		U32 GetId() const { return _id; };

	protected:

	private:
		static std::atomic<U32> id;
		U32 _id;

		SDeviceContextDebug _device_ctx;
};

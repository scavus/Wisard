/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include <cstring>

#include "cuda_runtime.h"

#include "DeviceManagerCuda.hpp"

U32 CDeviceManagerCuda::id = 0;

CDeviceManagerCuda::CDeviceManagerCuda()
{
	/*
	CheckErrorCode(cuInit(0));

	S32 device_count = 0;

	CheckErrorCode(cuDeviceGetCount(&device_count));

	if (!device_count)
	{
		return;
	}

	CheckErrorCode(cuDeviceGet(&_device_id, 0));

	CheckErrorCode(cuDeviceGetName(_device_info.Name, 1024, _device_id));
	cuDeviceGetAttribute(&_device_info.MaxThreadsPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxBlockDimX, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxBlockDimY, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxBlockDimZ, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxGridDimX, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxGridDimY, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxGridDimZ, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxSharedMemoryPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxSharedMemoryPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR, _device_id);
	cuDeviceGetAttribute(&_device_info.TotalConstantMemory, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY, _device_id);
	cuDeviceGetAttribute(&_device_info.WarpSize, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_WARP_SIZE, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxPitch, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_PITCH, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxRegistersPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxRegistersPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR, _device_id);
	cuDeviceGetAttribute(&_device_info.ClockRate, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CLOCK_RATE, _device_id);
	cuDeviceGetAttribute(&_device_info.TextureAlignment, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT, _device_id);
	cuDeviceGetAttribute(&_device_info.TexturePitchAlignment, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TEXTURE_PITCH_ALIGNMENT, _device_id);
	cuDeviceGetAttribute(&_device_info.GpuOverlapSupport, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_GPU_OVERLAP, _device_id);
	cuDeviceGetAttribute(&_device_info.MultiprocessorCount, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT, _device_id);
	cuDeviceGetAttribute(&_device_info.KernelExecTimeout, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT, _device_id);
	cuDeviceGetAttribute(&_device_info.Integrated, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_INTEGRATED, _device_id);
	cuDeviceGetAttribute(&_device_info.CanMapHostMemory, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY, _device_id);
	cuDeviceGetAttribute(&_device_info.ConcurrentKernelsSupport, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS, _device_id);
	cuDeviceGetAttribute(&_device_info.MemoryClockRate, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE, _device_id);
	cuDeviceGetAttribute(&_device_info.GlobalMemoryBusWidth, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH, _device_id);
	cuDeviceGetAttribute(&_device_info.L2CacheSize, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxThreadsPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR, _device_id);
	cuDeviceGetAttribute(&_device_info.UnifiedAddressing, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING, _device_id);
	cuDeviceGetAttribute(&_device_info.ComputeCapabilityMaj, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, _device_id);
	cuDeviceGetAttribute(&_device_info.ComputeCapabilityMin, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, _device_id);

	cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);

	// CheckErrorCode(cuCtxCreate(&_context, 0, _device_id));
	cuCtxGetCurrent(&_context);

	SBufferCuda null_buffer;
	null_buffer.size = 0;
	null_buffer.id = 0;
	_buffers.insert(std::pair<SBufferHdl, SBufferCuda>(0, null_buffer));

	STexture1dCuda null_tex1d;
	null_tex1d.tex_obj = 0;
	_texture1ds.insert(std::pair<STextureHdl, STexture1dCuda>(0, null_tex1d));

	STexture2dCuda null_tex2d;
	null_tex2d.tex_obj = 0;
	null_tex2d.tex_array = 0;
	_texture2ds.insert(std::pair<STextureHdl, STexture2dCuda>(0, null_tex2d));

	STexture3dCuda null_tex3d;
	null_tex3d.tex_obj = 0;
	null_tex3d.tex_array = 0;
	_texture3ds.insert(std::pair<STextureHdl, STexture3dCuda>(0, null_tex3d));

	_created_program_count = 0;
	_created_kernel_count = 0;
	_created_buffer_count = 1;
	_created_texture1d_count = 1;
	_created_texture2d_count = 1;
	_created_texture3d_count = 1;

	_id = id++;
	*/
}

CDeviceManagerCuda::CDeviceManagerCuda(const SDeviceInfo& device_info)
{
	_device_id = device_info.device_id;
	CheckErrorCode(cuDeviceGetName(_device_info.Name, 1024, _device_id));
	cuDeviceGetAttribute(&_device_info.MaxThreadsPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxBlockDimX, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxBlockDimY, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxBlockDimZ, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxGridDimX, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxGridDimY, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxGridDimZ, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxSharedMemoryPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxSharedMemoryPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR, _device_id);
	cuDeviceGetAttribute(&_device_info.TotalConstantMemory, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY, _device_id);
	cuDeviceGetAttribute(&_device_info.WarpSize, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_WARP_SIZE, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxPitch, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_PITCH, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxRegistersPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxRegistersPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR, _device_id);
	cuDeviceGetAttribute(&_device_info.ClockRate, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CLOCK_RATE, _device_id);
	cuDeviceGetAttribute(&_device_info.TextureAlignment, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT, _device_id);
	cuDeviceGetAttribute(&_device_info.TexturePitchAlignment, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TEXTURE_PITCH_ALIGNMENT, _device_id);
	cuDeviceGetAttribute(&_device_info.GpuOverlapSupport, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_GPU_OVERLAP, _device_id);
	cuDeviceGetAttribute(&_device_info.MultiprocessorCount, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT, _device_id);
	cuDeviceGetAttribute(&_device_info.KernelExecTimeout, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT, _device_id);
	cuDeviceGetAttribute(&_device_info.Integrated, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_INTEGRATED, _device_id);
	cuDeviceGetAttribute(&_device_info.CanMapHostMemory, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY, _device_id);
	cuDeviceGetAttribute(&_device_info.ConcurrentKernelsSupport, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS, _device_id);
	cuDeviceGetAttribute(&_device_info.MemoryClockRate, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE, _device_id);
	cuDeviceGetAttribute(&_device_info.GlobalMemoryBusWidth, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH, _device_id);
	cuDeviceGetAttribute(&_device_info.L2CacheSize, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE, _device_id);
	cuDeviceGetAttribute(&_device_info.MaxThreadsPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR, _device_id);
	cuDeviceGetAttribute(&_device_info.UnifiedAddressing, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING, _device_id);
	cuDeviceGetAttribute(&_device_info.ComputeCapabilityMaj, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, _device_id);
	cuDeviceGetAttribute(&_device_info.ComputeCapabilityMin, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, _device_id);

	CheckErrorCode(cuCtxCreate(&_context, 0, device_info.device_id));
	// cuCtxGetCurrent(&_context);

	SBufferCuda null_buffer;
	null_buffer.size = 0;
	null_buffer.id = 0;
	_buffers.insert(std::pair<SBufferHdl, SBufferCuda>(0, null_buffer));

	STexture1dCuda null_tex1d;
	null_tex1d.tex_obj = 0;
	_texture1ds.insert(std::pair<STextureHdl, STexture1dCuda>(0, null_tex1d));

	STexture2dCuda null_tex2d;
	null_tex2d.tex_obj = 0;
	null_tex2d.tex_array = 0;
	_texture2ds.insert(std::pair<STextureHdl, STexture2dCuda>(0, null_tex2d));

	STexture3dCuda null_tex3d;
	null_tex3d.tex_obj = 0;
	null_tex3d.tex_array = 0;
	_texture3ds.insert(std::pair<STextureHdl, STexture3dCuda>(0, null_tex3d));

	_created_program_count = 0;
	_created_kernel_count = 0;
	_created_buffer_count = 1;
	_created_texture1d_count = 1;
	_created_texture2d_count = 1;
	_created_texture3d_count = 1;

	_id = id++;
}


CDeviceManagerCuda::~CDeviceManagerCuda()
{

}


SBufferHdl CDeviceManagerCuda::CreateBuffer(const size_t buffer_size)
{
	SBufferCuda buffer;
	buffer.size = buffer_size;

	CheckErrorCode(cuMemAlloc(&buffer.id, buffer_size));

	_buffers.insert(std::pair<SBufferHdl, SBufferCuda>(_created_buffer_count, buffer));

	return _created_buffer_count++;
}


SBufferHdl CDeviceManagerCuda::CreateBuffer(const size_t buffer_size, const void* data_ptr)
{
	SBufferCuda buffer;
	buffer.size = buffer_size;

	CheckErrorCode(cuMemAlloc(&buffer.id, buffer_size));

	CheckErrorCode(cuMemcpyHtoD(buffer.id, data_ptr, buffer_size));

	_buffers.insert(std::pair<SBufferHdl, SBufferCuda>(_created_buffer_count, buffer));

	return _created_buffer_count++;
}

void CDeviceManagerCuda::DestroyBuffer(SBufferHdl handle)
{
	CheckErrorCode(cuMemFree(_buffers[handle].id));
	_buffers.erase(handle);
}

void CDeviceManagerCuda::ReadBuffer(SBufferHdl handle, void* data_ptr)
{
	SBufferCuda& buffer = _buffers[handle];

	// TODO: Check cuMemGetAddressRange

	CheckErrorCode(cuMemcpyDtoH(data_ptr, buffer.id, buffer.size));
}


void CDeviceManagerCuda::WriteBuffer(SBufferHdl handle, const void* data_ptr)
{
	SBufferCuda& buffer = _buffers[handle];

	// TODO: Check cuMemGetAddressRange

	CheckErrorCode(cuMemcpyHtoD(buffer.id, data_ptr, buffer.size));
}


void CDeviceManagerCuda::DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim)
{
	SKernelCuda& kernel = _kernels[handle];

	CheckErrorCode(cuLaunchKernel(kernel.KernelId, grid_dim.x, grid_dim.y, grid_dim.z, block_dim.x, block_dim.y, block_dim.z, 0, 0, kernel.args, 0));
}


void CDeviceManagerCuda::CreateProgramFromPtx(const char* filename)
{
	std::ifstream filestream(filename);

	filestream.seekg(0, filestream.end);
	S32 length = filestream.tellg();
	filestream.seekg(0, filestream.beg);

	char* cubin_data = new char[length];

	filestream.read(cubin_data, length);

	filestream.close();

	CUmodule ProgramId;
}


SProgramHdl CDeviceManagerCuda::CreateProgramFromCubin(const char* filename)
{
    std::ifstream filestream(filename, std::ios::binary);

	filestream.seekg(0, filestream.end);
    S32 length = filestream.tellg();
	filestream.seekg(0, filestream.beg);

	char* cubin_data = new char[length];

	filestream.read(cubin_data, length);

	filestream.close();
	// CheckErrorCode(cuModuleLoad(&mProgramId, filename));

	CUmodule program_id;

	CheckErrorCode(cuModuleLoadData(&program_id, (void*)cubin_data));

	_programs.insert({ _created_program_count, program_id });

	delete[] cubin_data;

	return _created_program_count++;
}


void CDeviceManagerCuda::SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl)
{
	SKernelCuda& kernel = _kernels[handle];
	CUdeviceptr* buffer_id = &_buffers[buffer_arg_hdl].id;

	kernel.args[arg_idx] = buffer_id;
}

void CDeviceManagerCuda::SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl)
{
	SKernelCuda& kernel = _kernels[handle];
	cudaTextureObject_t* tex_obj = &_texture2ds[texture_arg_hdl].tex_obj;

	*(kernel.args + arg_idx) = tex_obj;
}

void CDeviceManagerCuda::SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr)
{
	SKernelCuda& kernel = _kernels[handle];

	*(kernel.args + arg_idx) = const_cast<void*>(value_arg_ptr);
}


STextureHdl CDeviceManagerCuda::CreateTexture1D(const STexture1dDesc& desc, const void* data_ptr)
{
	/*
	cudaChannelFormatDesc channel_desc;

	cudaResourceDesc resource_desc;
	memset(&resource_desc, 0, sizeof(resource_desc));
	resource_desc.resType = cudaResourceTypeLinear;
	resource_desc.res.linear.desc = channel_desc;
	resource_desc.res.linear.devPtr = buffer;
	resource_desc.res.linear.sizeInBytes = buffer_size;

	cudaTextureObject_t tex_obj;
	cudaCreateTextureObject(&tex_obj, &resource_desc, &tex_desc, nullptr);

	STexture1dCuda tex;
	tex.tex_obj = tex_obj;
	tex.tex_buffer = Buffer;

	_textures.insert({ _created_texture_count, tex });
	*/
	return _created_texture1d_count++;
}

STextureHdl CDeviceManagerCuda::CreateTexture2D(const STexture2dDesc& desc, const void* data_ptr)
{
	cudaChannelFormatDesc channel_desc;

	cudaTextureDesc tex_desc;
	memset(&tex_desc, 0, sizeof(tex_desc));
	tex_desc.addressMode[0] = cudaAddressModeClamp;
	tex_desc.addressMode[1] = cudaAddressModeClamp;
	tex_desc.filterMode = cudaFilterModePoint;
	tex_desc.normalizedCoords = 1;

	switch (desc.format)
	{
		case ETextureFormat::R_U8:
		{
			channel_desc = cudaCreateChannelDesc<uchar1>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::R_NORM_U8:
		{
			channel_desc = cudaCreateChannelDesc<uchar1>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::R_F32:
		{
			channel_desc = cudaCreateChannelDesc<float>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::RG_U8:
		{
			channel_desc = cudaCreateChannelDesc<uchar2>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::RG_NORM_U8:
		{
			channel_desc = cudaCreateChannelDesc<uchar2>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::RG_F32:
		{
			channel_desc = cudaCreateChannelDesc<float2>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::RGBA_U8:
		{
			channel_desc = cudaCreateChannelDesc<uchar4>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::RGBA_NORM_U8:
		{
			channel_desc = cudaCreateChannelDesc<uchar4>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}
		case ETextureFormat::RGBA_F32:
		{
			channel_desc = cudaCreateChannelDesc<float4>();
			tex_desc.readMode = cudaReadModeElementType;
			break;
		}

		default:
			break;
	}

	cudaArray* tex_array;
	cudaMallocArray(&tex_array, &channel_desc, desc.width, desc.height);

	cudaMemcpyToArray(tex_array, 0, 0, data_ptr, GetTextureSize(desc), cudaMemcpyHostToDevice);

	cudaResourceDesc resource_desc;
	memset(&resource_desc, 0, sizeof(resource_desc));
	resource_desc.resType = cudaResourceTypeArray;
	resource_desc.res.array.array = tex_array;

	cudaTextureObject_t tex_obj;
	cudaCreateTextureObject(&tex_obj, &resource_desc, &tex_desc, nullptr);

	STexture2dCuda tex;
	tex.tex_obj = tex_obj;
	tex.tex_array = tex_array;

	_texture2ds.insert({ _created_texture2d_count, tex });

	return _created_texture2d_count++;
}

void CDeviceManagerCuda::DestroyTexture2D(STextureHdl handle)
{
	auto& tex = _texture2ds[handle];
	cudaDestroyTextureObject(tex.tex_obj);
	cudaFreeArray(tex.tex_array);

	_texture2ds.erase(handle);
}

SKernelHdl CDeviceManagerCuda::CreateKernel(const SProgramHdl program_hdl, const char* kernel_name, const U32 arg_count)
{
	SKernelCuda kernel;
	CUmodule ProgramId = _programs[program_hdl];

	CheckErrorCode(cuModuleGetFunction(&kernel.KernelId, ProgramId, kernel_name));

	kernel.args = new void*[arg_count];

	_kernels.insert(std::pair<SKernelHdl, SKernelCuda>(_created_kernel_count, kernel));

	return _created_kernel_count++;
}


bool CDeviceManagerCuda::CheckErrorCode(CUresult error_code)
{
	if (error_code == CUDA_SUCCESS)
	{
		// printf_s("CUDA_SUCCESS\n");
		return true;
	}

	char* error_string;
	cuGetErrorString(error_code, const_cast<const char**>(&error_string));
	std::cerr << "Error " << error_code << ": " << error_string << std::endl;

	return false;
}

void CDeviceManagerCuda::GetAvailableDeviceInfos(std::vector<SDeviceInfo>& device_infos)
{
	S32 device_count = 0;

	CheckErrorCode(cuDeviceGetCount(&device_count));

	if (!device_count)
	{
		return;
	}

	for (U32 i = 0; i < device_count; i++)
	{
		CUdevice device_id;
		SDeviceInfo device_info;
		CheckErrorCode(cuDeviceGet(&device_id, i));
		CheckErrorCode(cuDeviceGetName(device_info.Name, 1024, device_id));
		cuDeviceGetAttribute(&device_info.MaxThreadsPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, device_id);
		cuDeviceGetAttribute(&device_info.MaxBlockDimX, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X, device_id);
		cuDeviceGetAttribute(&device_info.MaxBlockDimY, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y, device_id);
		cuDeviceGetAttribute(&device_info.MaxBlockDimZ, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z, device_id);
		cuDeviceGetAttribute(&device_info.MaxGridDimX, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X, device_id);
		cuDeviceGetAttribute(&device_info.MaxGridDimY, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y, device_id);
		cuDeviceGetAttribute(&device_info.MaxGridDimZ, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z, device_id);
		cuDeviceGetAttribute(&device_info.MaxSharedMemoryPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, device_id);
		cuDeviceGetAttribute(&device_info.MaxSharedMemoryPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR, device_id);
		cuDeviceGetAttribute(&device_info.TotalConstantMemory, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY, device_id);
		cuDeviceGetAttribute(&device_info.WarpSize, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_WARP_SIZE, device_id);
		cuDeviceGetAttribute(&device_info.MaxPitch, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_PITCH, device_id);
		cuDeviceGetAttribute(&device_info.MaxRegistersPerBlock, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK, device_id);
		cuDeviceGetAttribute(&device_info.MaxRegistersPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR, device_id);
		cuDeviceGetAttribute(&device_info.ClockRate, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CLOCK_RATE, device_id);
		cuDeviceGetAttribute(&device_info.TextureAlignment, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT, device_id);
		cuDeviceGetAttribute(&device_info.TexturePitchAlignment, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_TEXTURE_PITCH_ALIGNMENT, device_id);
		cuDeviceGetAttribute(&device_info.GpuOverlapSupport, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_GPU_OVERLAP, device_id);
		cuDeviceGetAttribute(&device_info.MultiprocessorCount, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT, device_id);
		cuDeviceGetAttribute(&device_info.KernelExecTimeout, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT, device_id);
		cuDeviceGetAttribute(&device_info.Integrated, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_INTEGRATED, device_id);
		cuDeviceGetAttribute(&device_info.CanMapHostMemory, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY, device_id);
		cuDeviceGetAttribute(&device_info.ConcurrentKernelsSupport, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS, device_id);
		cuDeviceGetAttribute(&device_info.MemoryClockRate, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE, device_id);
		cuDeviceGetAttribute(&device_info.GlobalMemoryBusWidth, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH, device_id);
		cuDeviceGetAttribute(&device_info.L2CacheSize, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE, device_id);
		cuDeviceGetAttribute(&device_info.MaxThreadsPerMultiprocessor, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR, device_id);
		cuDeviceGetAttribute(&device_info.UnifiedAddressing, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING, device_id);
		cuDeviceGetAttribute(&device_info.ComputeCapabilityMaj, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, device_id);
		cuDeviceGetAttribute(&device_info.ComputeCapabilityMin, CUdevice_attribute::CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, device_id);

		device_info.device_id = device_id;

		device_infos.push_back(device_info);
	}
}

void CDeviceManagerCuda::Synchronize()
{
	auto Err = cudaDeviceSynchronize();
}

void CDeviceManagerCuda::SwitchContext()
{
	CheckErrorCode(cuCtxSetCurrent(_context));
}

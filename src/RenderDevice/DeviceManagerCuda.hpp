/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <map>
#include <vector>
#include <iostream>
#include <fstream>

#include "cuda.h"
#include "texture_types.h"

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"

struct SBufferCuda
{
	size_t size;

	CUdeviceptr id;
};

struct SKernelArg
{
	U32 idx;
	void* DataPtr;
};

struct SKernelCuda
{
	char* Name;

	CUfunction KernelId;

	void** args;
};

struct STexture1dCuda
{
	cudaTextureObject_t tex_obj;
	void* tex_buffer;
};

struct STexture2dCuda
{
	cudaTextureObject_t tex_obj;
	cudaArray* tex_array;
};

struct STexture3dCuda
{
	cudaTextureObject_t tex_obj;
	cudaArray* tex_array;
};

struct SDeviceInfoCuda
{
	CUdevice device_id;
	char Name[1024];

	S32 MaxThreadsPerBlock;

	S32 MaxBlockDimX;
	S32 MaxBlockDimY;
	S32 MaxBlockDimZ;

	S32 MaxGridDimX;
	S32 MaxGridDimY;
	S32 MaxGridDimZ;

	S32 MaxSharedMemoryPerBlock;
	S32 MaxSharedMemoryPerMultiprocessor;

	S32 TotalConstantMemory;

	S32 WarpSize;

	S32 MaxPitch;

	S32 MaxRegistersPerBlock;
	S32 MaxRegistersPerMultiprocessor;

	S32 ClockRate;

	S32 TextureAlignment;
	S32 TexturePitchAlignment;

	S32 GpuOverlapSupport;

	S32 MultiprocessorCount;

	S32 KernelExecTimeout;

	S32 Integrated;

	S32 CanMapHostMemory;

	S32 ConcurrentKernelsSupport;

	S32 MemoryClockRate;

	S32 GlobalMemoryBusWidth;
	S32 L2CacheSize;

	S32 MaxThreadsPerMultiprocessor;

	S32 UnifiedAddressing;

	S32 ComputeCapabilityMaj;
	S32 ComputeCapabilityMin;

};

typedef SDeviceInfoCuda SDeviceInfo;

class CDeviceManagerCuda : public TComputeDeviceManager<CDeviceManagerCuda>
{
	public:
		CDeviceManagerCuda();
		CDeviceManagerCuda(const SDeviceInfo& device_info);
		~CDeviceManagerCuda();

		// static void Init() { id = 0; };
		static U32 GetAvailableDeviceCount() { S32 c; cuDeviceGetCount(&c); return static_cast<U32>(c); };
		static void GetAvailableDeviceInfos(std::vector<SDeviceInfo>& device_infos);

		static CDeviceManagerCuda* Create() { return new CDeviceManagerCuda; };

		SBufferHdl	CreateBuffer(const size_t buffer_size);
		SBufferHdl	CreateBuffer(const size_t buffer_size, const void* data_ptr);
		void		DestroyBuffer(SBufferHdl handle);
		void		ReadBuffer(SBufferHdl handle, void* data_ptr);
		void		ReadBufferRange();
		void		WriteBuffer(SBufferHdl handle, const void* data_ptr);
		void		WriteBufferRange();

		STextureHdl CreateTexture1D(const STexture1dDesc& desc, const void* data_ptr);
		STextureHdl CreateTexture2D(const STexture2dDesc& desc, const void* data_ptr);
		STextureHdl CreateTexture3D(const STexture3dDesc& desc, const void* data_ptr);
		void		DestroyTexture1D(STextureHdl handle);
		void		DestroyTexture2D(STextureHdl handle);
		void		DestroyTexture3D(STextureHdl handle);

		SKernelHdl	CreateKernel(const SProgramHdl program_hdl, const char* kernel_name, const U32 arg_count);
		void		SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl);
		void		SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl);
		void		SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr);
		void		SetKernelArgs(const SKernelHdl handle);
		void		DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim);

		void		CompileProgram();
		void		CreateProgramFromPtx(const char* filename);
		SProgramHdl CreateProgramFromCubin(const char* filename);

		void Synchronize();
		void SwitchContext();

		U32 GetWarpSize() { return _device_info.WarpSize; };

		// TODO: Use a simple benchmark kernel to find FLOPS
		U32 GetDeviceComputePower() { return _device_info.ClockRate * _device_info.MultiprocessorCount; };
		std::string GetDeviceName() { return std::string(_device_info.Name); };
		

		U32 GetId() const { return _id; };

		SBufferCuda GetBufferDesc(SBufferHdl handle) { return _buffers[handle]; };

		CUcontext GetContext() { return _context; };

	protected:

	private:
		static U32 id;

		U32 _id;
		CUdevice _device_id;
		CUcontext _context;

		SDeviceInfoCuda _device_info;

		std::map<SProgramHdl, CUmodule> _programs;
		U32 _created_program_count;

		std::map<SBufferHdl, SBufferCuda> _buffers;
		U32 _created_buffer_count;

		std::map<STextureHdl, STexture1dCuda> _texture1ds;
		U32 _created_texture1d_count;

		std::map<STextureHdl, STexture2dCuda> _texture2ds;
		U32 _created_texture2d_count;

		std::map<STextureHdl, STexture3dCuda> _texture3ds;
		U32 _created_texture3d_count;

		std::map<SKernelHdl, SKernelCuda> _kernels;
		U32 _created_kernel_count;

		static bool CheckErrorCode(CUresult error_code);
};

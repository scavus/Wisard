/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>
#include <map>
#include <fstream>

#include "CL/opencl.h"

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"

static const char* WR_OCL_DEFAULT_COMPILER_OPTIONS = (char*)("-I data/Kernels/ -D WR_BACKEND_OCL -cl-fast-relaxed-math");

enum EBufferType
{
	LOCAL,
	GLOBAL
};

enum ESamplerFilterMode
{
	NEAREST = CL_FILTER_NEAREST,
	LINEAR = CL_FILTER_LINEAR
};

enum ESamplerAddrMode
{
	CLAMP = CL_ADDRESS_CLAMP,
	CLAMP_TO_EDGE = CL_ADDRESS_CLAMP_TO_EDGE,
	REPEAT = CL_ADDRESS_REPEAT
};

enum EMapType
{
	READ = CL_MAP_READ,
	WRITE = CL_MAP_WRITE
};

struct SBufferOcl
{
	EBufferType type;
	size_t size;
	cl_mem id;
};

struct STexture1dOcl
{
	U32 width;
	cl_image_format image_format;
	cl_mem id;
};

struct STexture2dOcl
{
	U32 width;
	U32 height;
	cl_image_format image_format;
	cl_mem id;
};

struct STexture3dOcl
{
	U32 width;
	U32 height;
	U32 depth;
	cl_image_format image_format;
	cl_mem id;
};

struct SSamplerOcl
{
	cl_sampler sampler_id;
};

struct SKernelInfoOcl
{
	std::string Name;
	void** Args;
	U32 ArgCount;
};

struct SKernelOcl
{
	cl_kernel id;

	SKernelInfoOcl KernelInfo;
};

struct SDeviceContextOcl
{
	static const U32 MAX_KERNEL_COUNT = 1024;
	static const U32 MAX_BUFFER_COUNT = 1024;

	cl_context ocl_ctx;

	std::map<SProgramHdl, cl_program> programs;
	U32 created_program_count;

	std::map<SKernelHdl, SKernelOcl> kernels;
	U32 created_kernel_count;

	std::map<SBufferHdl, SBufferOcl> buffers;
	U32 created_buffer_count;

	std::map<STextureHdl, STexture2dOcl> texture2ds;
	U32 created_texture2d_count;

	std::map<STextureHdl, STexture1dOcl> texture1ds;
	U32 created_texture1d_count;

	std::map<STextureHdl, STexture3dOcl> texture3ds;
	U32 created_texture3d_count;


	// std::map<SSamplerHdl, SSamplerOcl>	Samplers;
};

struct SDeviceInfoOcl
{
	cl_platform_id PlatformId;
	cl_device_id DeviceId;
	char* DeviceName;
	U32 GlobalMemSize;
	U32 MaxWorkGroupSize;
	U32 MaxWorkItemSize;
	U32 MaxReadImageArgCount;
	U32 MaxWriteImageArgCount;
	U32 MaxClockFreq;
	U32 MaxComputeUnits;
};

class CDeviceManagerOcl : public TComputeDeviceManager<CDeviceManagerOcl>
{
	public:

		CDeviceManagerOcl();
		~CDeviceManagerOcl();

		CDeviceManagerOcl(const SDeviceInfoOcl& device_info);

		static U32 GetAvailableDeviceCount()
		{
			cl_platform_id* ocl_platform_ids;
			U32 ocl_platform_count;

			clGetPlatformIDs(0, nullptr, &ocl_platform_count);

			ocl_platform_ids = new cl_platform_id[ocl_platform_count];

			clGetPlatformIDs(ocl_platform_count, ocl_platform_ids, nullptr);

			U32 total_device_count = 0;

			for (U32 i = 0; i < ocl_platform_count; i++)
			{
				U32 device_count;
				clGetDeviceIDs(ocl_platform_ids[i], CL_DEVICE_TYPE_GPU, 0, nullptr, &device_count);
				total_device_count += device_count;
			}

			return total_device_count;
		};
		static void GetAvailableDeviceInfos(std::vector<SDeviceInfoOcl>& device_infos);
		static CDeviceManagerOcl* Create() { return new CDeviceManagerOcl; };

		SBufferHdl	CreateBuffer(const size_t buffer_size);
		SBufferHdl	CreateBuffer(const size_t buffer_size, const void* data_ptr);
		void		DestroyBuffer(SBufferHdl handle);
		void		ReadBuffer(const U32 handle, const U32 data_size, void* data_ptr);
		void		ReadBuffer(SBufferHdl handle, void* data_ptr);
		void		ReadBufferRange();
		void		WriteBuffer(const U32 handle, const U32 data_size, const void* iData);
		void		WriteBuffer(SBufferHdl handle, const void* data_ptr);
		void		WriteBufferRange();
		void		MapBuffer(const U32 handle, const EMapType iMapType, const bool iBlockingOpFlg, const U32 iMapRegionsOffset, const U32 iMapRegionSize, void* oMappedDataPtr);
		void		UnmapBuffer(const U32 handle, void* mapped_data_ptr);

		STextureHdl CreateTexture1D(const STexture1dDesc& desc, const void* data_ptr);
		STextureHdl CreateTexture2D(const STexture2dDesc& desc, const void* data_ptr);
		STextureHdl CreateTexture3D(const STexture3dDesc& desc, const void* data_ptr);
		void		DestroyTexture1D(STextureHdl handle);
		void		DestroyTexture2D(STextureHdl handle);
		void		DestroyTexture3D(STextureHdl handle);
		U32			CreateSampler(const ESamplerAddrMode iAddrMode, const ESamplerFilterMode iFilterMode, const bool iNormalizedCoordsFlg);

		SKernelHdl	CreateKernel(const SProgramHdl program_hdl, const char* kernel_name, const U32 arg_count);
		void		SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl);
		void		SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl);
		void		SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr);
		void		SetKernelArgs(const SKernelHdl handle);
		void		DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim);

		SProgramHdl CompileProgram(const std::vector<const char*>& filenames, const char* compiler_options);
		void		CreateProgramFromPtx(const char* filename);
		void		CreateProgramFromCubin(const char* filename);

		void SynchronizeCommands();
		void Synchronize();
		void SwitchContext();

		// FIXME:
		U32 GetWarpSize() { return 64; };

		U32 GetDeviceComputePower() { return 100/*_device_info.MaxClockFreq * _device_info.MaxComputeUnits*/; };
		char* GetDeviceName() { return _device_info.DeviceName; };
		U32 GetId() const { return _id; };

		cl_context GetContext() { return _device_ctx.ocl_ctx; };
		cl_device_id GetDeviceId() { return _ocl_device_id; };
		cl_command_queue GetCommandQueue() { return _ocl_command_queue; };

		SBufferOcl GetBufferDesc(SBufferHdl handle) { return _device_ctx.buffers[handle]; };

	private:
		static U32 id;

		U32 _id;

		bool CheckErrorCode(S32 error_code);

		cl_device_id _ocl_device_id;
		cl_command_queue _ocl_command_queue;
		SDeviceContextOcl _device_ctx;
		SDeviceInfoOcl _device_info;
};

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>

#include "Api/WrCommon.h"
#include "Api/WrTypes.h"

typedef U32 SResourceHandle;
typedef U32 SBufferHdl;
typedef U32 STextureHdl;
typedef U32 SKernelHdl;
typedef U32 SProgramHdl;


using ETextureFormat = WrTextureFormat;
using ETextureUsage = WrTextureUsage;
using STexture1dDesc = WrTexture1dDesc;
using STexture2dDesc = WrTexture2dDesc;
using STexture3dDesc = WrTexture3dDesc;

struct SWorkDimension
{
	U32 x;
	U32 y;
	U32 z;
};

static const size_t TEXEL_SIZE_MAP[] =
{
	/* R_U8				*/ sizeof(U8),
	/* R_S8				*/ sizeof(S8),
	/* R_U16			*/ sizeof(U16),
	/* R_S16			*/ sizeof(S16),
	/* R_NORM_U8		*/ sizeof(U8),
	/* R_NORM_S8		*/ sizeof(S8),
	/* R_NORM_U16		*/ sizeof(U16),
	/* R_NORM_S16		*/ sizeof(S16),
	/* R_F32			*/ sizeof(F32),

	/* RG_U8			*/ sizeof(U8) * 2,
	/* RG_S8			*/ sizeof(S8) * 2,
	/* RG_U16			*/ sizeof(U16) * 2,
	/* RG_S16			*/ sizeof(S16) * 2,
	/* RG_NORM_U8		*/ sizeof(U8) * 2,
	/* RG_NORM_S8		*/ sizeof(S8) * 2,
	/* RG_NORM_U16		*/ sizeof(U16) * 2,
	/* RG_NORM_S16		*/ sizeof(S16) * 2,
	/* RG_F32			*/ sizeof(F32) * 2,

	/* RGBA_U8			*/ sizeof(U8) * 4,
	/* RGBA_S8			*/ sizeof(S8) * 4,
	/* RGBA_U16			*/ sizeof(U16) * 4,
	/* RGBA_S16			*/ sizeof(S16) * 4,
	/* RGBA_NORM_U8		*/ sizeof(U8) * 4,
	/* RGBA_NORM_S8		*/ sizeof(S8) * 4,
	/* RGBA_NORM_U16	*/ sizeof(U16) * 4,
	/* RGBA_NORM_S16	*/ sizeof(S16) * 4,
	/* RGBA_F32			*/ sizeof(F32) * 4
};


inline size_t GetTextureSize(const STexture1dDesc& desc)
{
	return desc.width * TEXEL_SIZE_MAP[desc.format];
}

inline size_t GetTextureSize(const STexture2dDesc& desc)
{
	return desc.width * desc.height * TEXEL_SIZE_MAP[desc.format];
}

inline size_t GetTextureSize(const STexture3dDesc& desc)
{
	return desc.width * desc.height * desc.depth * TEXEL_SIZE_MAP[desc.format];
}

template <class DeviceManagerBackend_T>
class TComputeDeviceManager
{
	public:
		TComputeDeviceManager(){};
		// ~TComputeDeviceManager();
		static DeviceManagerBackend_T* Create() { return DeviceManagerBackend_T::Create(); };

		SBufferHdl	CreateBuffer(const size_t buffer_size);
		SBufferHdl	CreateBuffer(const size_t buffer_size, const void* data_ptr);
		void		DestroyBuffer(SBufferHdl handle);
		void		ReadBuffer(SBufferHdl handle, void* data_ptr);
		void		ReadBufferRange();
		void		WriteBuffer(SBufferHdl handle, const void* data_ptr);
		void		WriteBufferRange();

		STextureHdl CreateTexture1D(const STexture1dDesc& desc, const void* data_ptr);
		STextureHdl CreateTexture2D(const STexture2dDesc& desc, const void* data_ptr);
		STextureHdl CreateTexture3D(const STexture3dDesc& desc, const void* data_ptr);
		void		DestroyTexture1D(STextureHdl handle);
		void		DestroyTexture2D(STextureHdl handle);
		void		DestroyTexture3D(STextureHdl handle);

		SKernelHdl	CreateKernel(const SProgramHdl program_hdl, const char* kernel_name, const U32 arg_count);
		void		SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl);
		void		SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl);
		void		SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr);
		void		DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim);

		SProgramHdl	CompileProgram(const std::vector<const char*>& filenames, const char* compiler_options);
		void		CreateProgramFromPtx(const char* filename);
		SProgramHdl CreateProgramFromCubin(const char* filename);

		void Synchronize();
		void SwitchContext();

		U32 GetWarpSize();
		U32 GetDeviceComputePower();
		U32 GetId() const;

	protected:

	private:
};

template <class DeviceManagerBackend_T>
U32 TComputeDeviceManager<DeviceManagerBackend_T>::GetId() const
{
	 return static_cast<DeviceManagerBackend_T&>(*this).GetId();
}

template <class DeviceManagerBackend_T>
U32 TComputeDeviceManager<DeviceManagerBackend_T>::GetDeviceComputePower()
{
	return static_cast<DeviceManagerBackend_T&>(*this).GetDeviceComputePower();
}

template <class DeviceManagerBackend_T>
U32 TComputeDeviceManager<DeviceManagerBackend_T>::GetWarpSize()
{
	return static_cast<DeviceManagerBackend_T&>(*this).GetWarpSize();
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::Synchronize()
{
	static_cast<DeviceManagerBackend_T&>(*this).Synchronize();
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::SwitchContext()
{
	static_cast<DeviceManagerBackend_T&>(*this).SwitchContext();
}

/*
template <class DeviceManagerBackend_T>
static DeviceManagerBackend_T* TComputeDeviceManager<DeviceManagerBackend_T>::Create()
{
	static_cast<DeviceManagerBackend_T&>(*this).Create();
}
*/

template <class DeviceManagerBackend_T>
SProgramHdl TComputeDeviceManager<DeviceManagerBackend_T>::CompileProgram(const std::vector<const char*>& filenames, const char* compiler_options)
{
	return static_cast<DeviceManagerBackend_T&>(*this).CompileProgram(filenames, compiler_options);
}

template <class DeviceManagerBackend_T>
SProgramHdl TComputeDeviceManager<DeviceManagerBackend_T>::CreateProgramFromCubin(const char* filename)
{
	return static_cast<DeviceManagerBackend_T&>(*this).CreateProgramFromCubin(filename);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::CreateProgramFromPtx(const char* filename)
{
	static_cast<DeviceManagerBackend_T&>(*this).CreateProgramFromPtx(filename);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::DispatchKernel(const SKernelHdl handle, const SWorkDimension& grid_dim, const SWorkDimension& block_dim)
{
	static_cast<DeviceManagerBackend_T&>(*this).DispatchKernel(handle, grid_dim, block_dim);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::SetKernelArgValue(const SKernelHdl handle, const U32 arg_idx, const size_t value_size, const void* value_arg_ptr)
{
	static_cast<DeviceManagerBackend_T&>(*this).SetKernelArgValue(handle, arg_idx, value_size, value_arg_ptr);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::SetKernelArgTexture(const SKernelHdl handle, const U32 arg_idx, const STextureHdl texture_arg_hdl)
{
	static_cast<DeviceManagerBackend_T&>(*this).SetKernelArgTexture(handle, arg_idx, texture_arg_hdl);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::SetKernelArgBuffer(const SKernelHdl handle, const U32 arg_idx, const SBufferHdl buffer_arg_hdl)
{
	static_cast<DeviceManagerBackend_T&>(*this).SetKernelArgBuffer(handle, arg_idx, buffer_arg_hdl);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::DestroyTexture2D(STextureHdl handle)
{
	static_cast<DeviceManagerBackend_T&>(*this).DestroyTexture2D(handle);
}

template <class DeviceManagerBackend_T>
SKernelHdl TComputeDeviceManager<DeviceManagerBackend_T>::CreateKernel(const SProgramHdl program_hdl, const char* kernel_name, const U32 arg_count)
{
	return static_cast<DeviceManagerBackend_T&>(*this).CreateKernel(program_hdl, kernel_name, arg_count);
}

template <class DeviceManagerBackend_T>
STextureHdl TComputeDeviceManager<DeviceManagerBackend_T>::CreateTexture2D(const STexture2dDesc& desc, const void* data_ptr)
{
	return static_cast<DeviceManagerBackend_T&>(*this).CreateTexture2D(desc, data_ptr);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::WriteBuffer(SBufferHdl handle, const void* data_ptr)
{
	static_cast<DeviceManagerBackend_T&>(*this).WriteBuffer(handle, data_ptr);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::ReadBuffer(SBufferHdl handle, void* data_ptr)
{
	static_cast<DeviceManagerBackend_T&>(*this).ReadBuffer(handle, data_ptr);
}

template <class DeviceManagerBackend_T>
SBufferHdl TComputeDeviceManager<DeviceManagerBackend_T>::CreateBuffer(const size_t buffer_size, const void* data_ptr)
{
	return static_cast<DeviceManagerBackend_T&>(*this).CreateBuffer(buffer_size, data_ptr);
}

template <class DeviceManagerBackend_T>
void TComputeDeviceManager<DeviceManagerBackend_T>::DestroyBuffer(SBufferHdl handle)
{
	static_cast<DeviceManagerBackend_T&>(*this).CreateBuffer(handle);
}

template <class DeviceManagerBackend_T>
SBufferHdl TComputeDeviceManager<DeviceManagerBackend_T>::CreateBuffer(const size_t buffer_size)
{
	return static_cast<DeviceManagerBackend_T&>(*this).CreateBuffer(buffer_size);
}

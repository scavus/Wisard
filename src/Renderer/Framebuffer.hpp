/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"

class CFramebuffer
{
    public:
        CFramebuffer(U32 width, U32 height)
        {
			_width = width;
			_height = height;
            _color_buffer = new F32x4[_width * _height];
        };

        ~CFramebuffer()
        {
            delete[] _color_buffer;
        };

        inline U32 GetWidth() { return _width; };
        inline U32 GetHeight() { return _height; };

        inline void SetColorBufferPx(U32 x, U32 y, const F32x4& color)
        {
            _color_buffer[_width * y + x] = color;
        };

        inline F32x4 GetColorBufferPx(U32 x, U32 y)
        {
            return _color_buffer[_height * y + x];
        };

        inline F32x4* GetColorBuffer()
        {
            return _color_buffer;
        };

    protected:

    private:
        U32 _width;
        U32 _height;

        F32x4* _color_buffer;
};


#pragma once

#include "Renderer/IRenderEngine.hpp"

class IRenderEngineFactory
{
    public:
		virtual IRenderEngine* Create(SSceneContext* scene_ctx_ptr, void* render_settings) = 0;

    protected:

    private:
};

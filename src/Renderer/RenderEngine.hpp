/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>
#include <unordered_map>
#include <thread>

#include "Utility/CommonTypes.hpp"
#include "Utility/Math.hpp"
#include "RenderDevice/DeviceBackend.hpp"

// #include "Integrators/IIntegrator.hpp"
// #include "Integrators/IIntegratorFactory.hpp"
// #include "Integrators/IntegratorProgressiveDeferredUdpt.hpp"
// #include "Integrators/IntegratorBranchedDeferredUdpt.hpp"

#include "Renderer/IRenderEngine.hpp"
#include "Renderer/IRenderEngineFactory.hpp"
#include "Renderer/Framebuffer.hpp"

template<template<class DeviceManagerBackend_T> class Integrator_T>
class TRenderEngine : public IRenderEngine
{
	public:
		using DistributionPolicy = typename Integrator_T<class DeviceManagerBackend_T>::DistributionPolicy;
		using SceneContext = typename Integrator_T<class DeviceManagerBackend_T>::SceneContext;
		using IntegratorCuda = Integrator_T<CDeviceManagerCuda>;
		using IntegratorOcl = Integrator_T<CDeviceManagerOcl>;
		using IntegratorDebug = Integrator_T<CDeviceManagerDebug>;
		using RenderSettingsManager = typename Integrator_T<class DeviceManagerBackend_T>::RenderSettingsManager;

		TRenderEngine(){};

		TRenderEngine(SDeviceRegistry* device_registry_ptr, RenderSettingsManager* render_settings_manager_ptr, SceneContext* scene_ctx_ptr)
		{
			_device_registry_ptr = device_registry_ptr;
			_render_settings_manager_ptr = render_settings_manager_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			_initialized = false;
		};

		TRenderEngine(SceneContext* scene_ctx_ptr, void* render_settings)
		{
			_scene_ctx_ptr = scene_ctx_ptr;

			SDeviceRegistry* null_device_registry = new SDeviceRegistry;
			typename RenderSettingsManager::SStorage* render_settings_storage = new typename RenderSettingsManager::SStorage;
			_render_settings_manager_ptr = new RenderSettingsManager( null_device_registry, render_settings_storage );

			_render_settings_manager_ptr->SetRenderSettings(render_settings);
			_initialized = false;
		};

		virtual ~TRenderEngine(){};

		virtual void Init() override
		{
			auto render_settings = _render_settings_manager_ptr->GetRenderSettingsConstRef();
			_framebuffer = new CFramebuffer{ render_settings.common.render_width, render_settings.common.render_height };

			SDeviceRegistry device_registry;

			// NOTE: Temporary
			for (auto integrator : _cuda_integrators)
			{
				device_registry.cuda_device_managers.insert({ integrator->GetDeviceManagerPtr()->GetId(), const_cast<CDeviceManagerCuda*>(integrator->GetDeviceManagerPtr()) });
			}

			for (auto integrator : _ocl_integrators)
			{
				device_registry.ocl_device_managers.insert({ integrator->GetDeviceManagerPtr()->GetId(), const_cast<CDeviceManagerOcl*>(integrator->GetDeviceManagerPtr()) });
			}

			DistributionPolicy::Distribute(&device_registry, render_settings.common, _cuda_work_infos, _ocl_work_infos, _debug_work_infos);

			for (auto integrator : _cuda_integrators)
			{
				integrator->SetRenderSettingsManager(_render_settings_manager_ptr);
				integrator->SetWorkInfo(&_cuda_work_infos[integrator->GetDeviceManagerPtr()->GetId()]);

				integrator->AllocBuffers();
				integrator->CreateProgram();
				integrator->CreateKernels();
				integrator->Init();
			}

			for (auto integrator : _ocl_integrators)
			{
				integrator->SetRenderSettingsManager(_render_settings_manager_ptr);
				integrator->SetWorkInfo(&_ocl_work_infos[integrator->GetDeviceManagerPtr()->GetId()]);

				integrator->AllocBuffers();
				integrator->CreateProgram();
				integrator->CreateKernels();
				integrator->Init();
			}

			for (auto integrator : _debug_integrators)
			{
				integrator->SetRenderSettingsManager(_render_settings_manager_ptr);
				integrator->SetWorkInfo(&_debug_work_infos[integrator->GetDeviceManagerPtr()->GetId()]);

				integrator->AllocBuffers();
				integrator->CreateProgram();
				integrator->CreateKernels();
				integrator->Init();
			}
		};


		virtual void Update() override
		{
			if (!_initialized)
			{
				SceneCtxUtils::UpdateSceneCtx(_scene_ctx_ptr);
				_render_settings_manager_ptr->Update();
				Init();
				_initialized = true;

				return;
			}


			/*
			bool rerender_flag;
			bool integrator_ctx_update_flag;

			rerender_flag = _scene_ctx_ptr->Update();
			integrator_ctx_update_flag = _render_settings_manager_ptr->Update();

			if (integrator_ctx_update_flag)
			{
				auto render_settings = _render_settings_manager_ptr->GetRenderSettingsConstRef();

				delete[] _framebuffer;
				_framebuffer = new F32x4[render_settings.render_width * render_settings.render_height];

				_cuda_work_infos.clear();
				_ocl_work_infos.clear();

				SDeviceRegistry device_registry;

				// NOTE: Temporary
				for (auto integrator : _cuda_integrators)
				{
					device_registry.cuda_device_managers.push_back(const_cast<CDeviceManagerCuda*>(integrator->GetDeviceManagerPtr()));
				}

				for (auto integrator : _ocl_integrators)
				{
					device_registry.ocl_device_managers.push_back(const_cast<CDeviceManagerOcl*>(integrator->GetDeviceManagerPtr()));
				}

				DistributionPolicy::Distribute(&device_registry, render_settings, _cuda_work_infos, _ocl_work_infos, _debug_work_infos);

				for (auto& integrator : _cuda_integrators)
				{
					integrator.SetWorkInfo(&_cuda_work_infos[device_manager_ptr->GetId()]);
					integrator.ReallocBuffers();
				}

				for (auto& integrator : _ocl_integrators)
				{
					integrator.SetWorkInfo(&_ocl_work_infos[device_manager_ptr->GetId()]);
					integrator.ReallocBuffers();
				}
			}

			if (rerender_flag || integrator_ctx_update_flag)
			{
				for (IntegratorCuda& integrator : _cuda_integrators)
				{
					integrator.Init();
				}

				for (IntegratorOcl& integrator : _ocl_integrators)
				{
					integrator.Init();
				}
			}
			*/
		};

		virtual S32 Render() override
		{
			std::vector<std::thread> cuda_threads;
			std::vector<std::thread> ocl_threads;
			std::vector<std::thread> debug_threads;

			for (auto integrator : _cuda_integrators)
			{
				cuda_threads.push_back(std::thread{ &IntegratorCuda::Execute, integrator });
			}

			for (auto integrator : _ocl_integrators)
			{
				ocl_threads.push_back(std::thread{ &IntegratorOcl::Execute, integrator });
			}

			for (auto integrator : _debug_integrators)
			{
				debug_threads.push_back(std::thread{ &IntegratorDebug::Execute, integrator });
			}

			for (auto& thread : cuda_threads)
			{
				thread.join();
			}

			for (auto& thread : ocl_threads)
			{
				thread.join();
			}

			for (auto& thread : debug_threads)
			{
				thread.join();
			}

			for (auto integrator_ptr : _cuda_integrators)
			{
				const_cast<CDeviceManagerCuda*>( integrator_ptr->GetDeviceManagerPtr() )->SwitchContext();
			}

			for (auto integrator_ptr : _ocl_integrators)
			{
				const_cast<CDeviceManagerOcl*>( integrator_ptr->GetDeviceManagerPtr() )->SwitchContext();
			}

			for (auto integrator_ptr : _debug_integrators)
			{
				const_cast<CDeviceManagerDebug*>( integrator_ptr->GetDeviceManagerPtr() )->SwitchContext();
			}

			DistributionPolicy::AssembleRenderResults(_cuda_work_infos, _ocl_work_infos, _debug_work_infos, _framebuffer);
			S32 status_flag = DistributionPolicy::GetStatusFlag(_cuda_work_infos, _ocl_work_infos, _debug_work_infos);

			return status_flag;
		};

		virtual void AllocIntegrator(CDeviceManagerCuda* device_manager_ptr, ISceneIntersector* scene_intersector_ptr) override
		{
			// IIntegrator* integrator_ptr = _integrator_factory_ptr->Create();

			IntegratorCuda* integrator_ptr = new IntegratorCuda;
			integrator_ptr->SetDeviceManagerPtr(device_manager_ptr);
			integrator_ptr->SetSceneCtxPtr(_scene_ctx_ptr);
			integrator_ptr->SetSceneIntersectorPtr(scene_intersector_ptr);
			_cuda_integrators.push_back(integrator_ptr);

			_render_settings_manager_ptr->CreateBackendManager(const_cast<CDeviceManagerCuda*>(integrator_ptr->GetDeviceManagerPtr()));
			// ...
		};

		virtual void AllocIntegrator(CDeviceManagerOcl* device_manager_ptr, ISceneIntersector* scene_intersector_ptr) override
		{
			// IIntegrator* integrator_ptr = _integrator_factory_ptr->Create();
			IntegratorOcl* integrator_ptr = new IntegratorOcl;
			integrator_ptr->SetDeviceManagerPtr(device_manager_ptr);
			integrator_ptr->SetSceneCtxPtr(_scene_ctx_ptr);
			integrator_ptr->SetSceneIntersectorPtr(scene_intersector_ptr);
			_ocl_integrators.push_back(integrator_ptr);

			_render_settings_manager_ptr->CreateBackendManager(const_cast<CDeviceManagerOcl*>(integrator_ptr->GetDeviceManagerPtr()));
			// ...

		};

		virtual void DeallocIntegrator(CDeviceManagerCuda* device_manager_ptr) override
		{

		};

		virtual void DeallocIntegrator(CDeviceManagerOcl* device_manager_ptr) override
		{

		};

		virtual CFramebuffer* GetFramebuffer() override { return _framebuffer; };

	protected:

	private:
		SDeviceRegistry* _device_registry_ptr;

		RenderSettingsManager* _render_settings_manager_ptr;
		SceneContext* _scene_ctx_ptr;

		// IIntegratorFactory* _integrator_factory_ptr;

		std::unordered_map<U32, typename DistributionPolicy::SRenderWorkInfo> _cuda_work_infos;
		std::unordered_map<U32, typename DistributionPolicy::SRenderWorkInfo> _ocl_work_infos;
		std::unordered_map<U32, typename DistributionPolicy::SRenderWorkInfo> _debug_work_infos;

		std::vector<IntegratorCuda*> _cuda_integrators;
		std::vector<IntegratorOcl*> _ocl_integrators;
		std::vector<IntegratorDebug*> _debug_integrators;

		CFramebuffer* _framebuffer;

		bool _initialized;
};

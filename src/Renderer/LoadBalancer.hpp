/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>
#include <unordered_map>

#include "Utility/CommonTypes.hpp"
#include "Utility/Math.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "Renderer/Framebuffer.hpp"
#include "Renderer/RenderEngine.hpp"

class CDistributionPolicyDefault
{
	public:
		struct SRenderWorkInfo
		{
			U32 sample_count;
			U32 sample_offset;

			bool finished;
			F32x4* result;
		};


		static void Distribute(SDeviceRegistry* device_registry_ptr, const WrRenderSettingsCommon& render_settings,
			std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
			std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
			std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos);

		static void AssembleRenderResults(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos,
			CFramebuffer* framebuffer);

		static S32 GetStatusFlag(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos);

	protected:

	private:
		CDistributionPolicyDefault();
		~CDistributionPolicyDefault();
};


class CDistributionPolicyTiled
{
	public:

		struct STileInfo
		{
			U32 tile_offset_x;
			U32 tile_offset_y;
			U32 tile_width;
			U32 tile_height;

			F32x4* result;
		};

		struct SRenderWorkInfo
		{
			std::vector<STileInfo> tiles;
			U32 finished_tile_count;
		};

		static void Distribute(SDeviceRegistry* device_registry_ptr, const WrRenderSettingsCommon& render_settings,
			std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
			std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
			std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos);

		static void AssembleRenderResults(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos,
			CFramebuffer* framebuffer);

		static S32 GetStatusFlag(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
			const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos);

	protected:

	private:
		CDistributionPolicyTiled();
		~CDistributionPolicyTiled();
};

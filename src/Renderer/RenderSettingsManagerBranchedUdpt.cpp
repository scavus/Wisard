/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "RenderSettingsManagerBranchedUdpt.hpp"

template class TRenderSettingsManagerBranchedUdptBackend<CDeviceManagerCuda>;
template class TRenderSettingsManagerBranchedUdptBackend<CDeviceManagerOcl>;
template class TRenderSettingsManagerBranchedUdptBackend<CDeviceManagerDebug>;

template <class DeviceManagerBackend_T>
void TRenderSettingsManagerBranchedUdptBackend<DeviceManagerBackend_T>::CreateBuffers()
{
	auto buffer_size = sizeof(WrRenderSettingsBranchedUdpt);
	_render_settings_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &_resource_container_ptr->render_settings);
}

template <class DeviceManagerBackend_T>
void TRenderSettingsManagerBranchedUdptBackend<DeviceManagerBackend_T>::Update()
{	
	if (_render_settings_buffer_hdl == 0)
	{
		CreateBuffers();
	}

	_device_manager_ptr->DestroyBuffer(_render_settings_buffer_hdl);

	auto buffer_size = sizeof(WrRenderSettingsBranchedUdpt);
	_render_settings_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &_resource_container_ptr->render_settings);
}

void CRenderSettingsManagerBranchedUdpt::SetRenderSettings(WrRenderSettingsBranchedUdpt& render_settings)
{
	_resource_container_ptr->render_settings = std::move(render_settings);
	_resource_container_ptr->update_flag = true;
}

void CRenderSettingsManagerBranchedUdpt::SetRenderSettings(void* render_settings)
{
	_resource_container_ptr->render_settings = *static_cast<WrRenderSettingsBranchedUdpt*>(render_settings);
	_resource_container_ptr->update_flag = true;
}

void CRenderSettingsManagerBranchedUdpt::Update()
{
	if (_resource_container_ptr->update_flag)
	{
		UpdateBackends();
		_resource_container_ptr->update_flag = false;
	}
}

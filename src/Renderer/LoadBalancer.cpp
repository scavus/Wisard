/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "LoadBalancer.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "Api/Wr.h"

void CDistributionPolicyDefault::Distribute(SDeviceRegistry* device_registry_ptr, const WrRenderSettingsCommon& render_settings,
	std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
	std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
	std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos)
{
	U32 total_compute_power = 0;
	U32 sample_offset = 0;

	for (auto device_manager_ptr : device_registry_ptr->cuda_device_managers)
	{
		total_compute_power += device_manager_ptr.second->GetDeviceComputePower();
	}

	for (auto device_manager_ptr : device_registry_ptr->ocl_device_managers)
	{
		total_compute_power += device_manager_ptr.second->GetDeviceComputePower();
	}

	for (auto device_manager_ptr : device_registry_ptr->debug_device_managers)
	{
		total_compute_power += device_manager_ptr.second->GetDeviceComputePower();
	}

	U32 total_sample_count = render_settings.render_width * render_settings.render_height;

	for (auto device_manager_ptr : device_registry_ptr->cuda_device_managers)
	{
		SRenderWorkInfo work_info;
		work_info.finished = false;
		work_info.sample_count = total_sample_count * (device_manager_ptr.second->GetDeviceComputePower() / total_compute_power);
		work_info.sample_offset = sample_offset;
		work_info.result = new F32x4[work_info.sample_count];
		sample_offset += work_info.sample_count;
		cuda_work_infos.insert({ device_manager_ptr.second->GetId(), work_info });
	}

	for (auto device_manager_ptr : device_registry_ptr->ocl_device_managers)
	{
		SRenderWorkInfo work_info;
		work_info.finished = false;
		work_info.sample_count = total_sample_count * (device_manager_ptr.second->GetDeviceComputePower() / total_compute_power);
		work_info.sample_offset = sample_offset;
		work_info.result = new F32x4[work_info.sample_count];
		sample_offset += work_info.sample_count;
		ocl_work_infos.insert({ device_manager_ptr.second->GetId(), work_info });
	}

	for (auto device_manager_ptr : device_registry_ptr->debug_device_managers)
	{
		SRenderWorkInfo work_info;
		work_info.finished = false;
		work_info.sample_count = total_sample_count * (device_manager_ptr.second->GetDeviceComputePower() / total_compute_power);
		work_info.sample_offset = sample_offset;
		work_info.result = new F32x4[work_info.sample_count];
		sample_offset += work_info.sample_count;
		debug_work_infos.insert({ device_manager_ptr.second->GetId(), work_info });
	}

}

void CDistributionPolicyDefault::AssembleRenderResults(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
	const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
	const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos,
	CFramebuffer* framebuffer)
{
	auto color_buffer = framebuffer->GetColorBuffer();

	for (auto work_info_map : cuda_work_infos)
	{
		SRenderWorkInfo& work_info = work_info_map.second;

		for (U32 idx = 0; idx < work_info.sample_count; idx++)
		{
			color_buffer[work_info.sample_offset + idx] = work_info.result[idx];
		}
	}

	for (auto work_info_map : ocl_work_infos)
	{
		SRenderWorkInfo& work_info = work_info_map.second;

		for (U32 idx = 0; idx < work_info.sample_count; idx++)
		{
			color_buffer[work_info.sample_offset + idx] = work_info.result[idx];
		}
	}

	for (auto work_info_map : debug_work_infos)
	{
		SRenderWorkInfo& work_info = work_info_map.second;

		for (U32 idx = 0; idx < work_info.sample_count; idx++)
		{
			color_buffer[work_info.sample_offset + idx] = work_info.result[idx];
		}
	}

}

S32 CDistributionPolicyDefault::GetStatusFlag(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos, 
	const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos, 
	const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos)
{
	S32 status_flag = WR_RENDER_STATUS_FLAG_ERROR;
	bool finished = true;

	for (auto work_info_map : cuda_work_infos)
	{
		SRenderWorkInfo& work_info = work_info_map.second;
		finished &= work_info.finished;
	}

	for (auto work_info_map : ocl_work_infos)
	{
		SRenderWorkInfo& work_info = work_info_map.second;
		finished &= work_info.finished;
	}

	if (finished)
	{
		status_flag = WR_RENDER_STATUS_FLAG_HALT;
	}
	else
	{
		status_flag = WR_RENDER_STATUS_FLAG_RUNNING;
	}

	return status_flag;
}

void CDistributionPolicyTiled::Distribute(SDeviceRegistry* device_registry_ptr, const WrRenderSettingsCommon& render_settings,
	std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
	std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
	std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos)
{
	U32 total_compute_power = 0;

	for (auto device_manager_ptr : device_registry_ptr->cuda_device_managers)
	{
		total_compute_power += device_manager_ptr.second->GetDeviceComputePower();
	}

	for (auto device_manager_ptr : device_registry_ptr->ocl_device_managers)
	{
		total_compute_power += device_manager_ptr.second->GetDeviceComputePower();
	}

	for (auto device_manager_ptr : device_registry_ptr->debug_device_managers)
	{
		total_compute_power += device_manager_ptr.second->GetDeviceComputePower();
	}

	// x = FrameBufferWidth / tile_width;
	// y = FrameBufferHeight / tile_height;

	// m = FrameBufferWidth % tile_width;
	// n = FrameBufferHeight % tile_height;

	// x * y tiles (tile_width, tile_height)
	// x tiles (tile_width, n)
	// y tiles (m, tile_height)
	// 1 tile (m, n)

	U32 x = render_settings.render_width / render_settings.tile_width;
	U32 y = render_settings.render_height / render_settings.tile_height;

	U32 m = render_settings.render_width % render_settings.tile_width;
	U32 n = render_settings.render_height % render_settings.tile_height;

	U32 total_tile_count = x * y;

	if (m > 0 && n > 0)
	{
		total_tile_count += x + y + 1;
	}
	else if (m == 0 && n > 0)
	{
		total_tile_count += x;
	}
	else if (m > 0 && n == 0)
	{
		total_tile_count += y;
	}

	std::vector<STileInfo> tile_infos;

	U32 tile_offset_x = 0;
	U32 tile_offset_y = 0;

	if (x * y > 0)
	{
		for (U32 tile_idx_y = 0; tile_idx_y < y; tile_idx_y++)
		{
			for (U32 tile_idx_x = 0; tile_idx_x < x; tile_idx_x++)
			{
				STileInfo tile_info;
				tile_info.tile_width = render_settings.tile_width;
				tile_info.tile_height = render_settings.tile_height;
				tile_info.tile_offset_x = tile_offset_x;
				tile_info.tile_offset_y = tile_offset_y;
				tile_info.result = new F32x4[tile_info.tile_width * tile_info.tile_height];
				tile_infos.push_back(tile_info);

				tile_offset_x += render_settings.tile_width;
			}

			tile_offset_x = 0;
			tile_offset_y += render_settings.tile_height;
		}
	}

	tile_offset_x = 0;
	tile_offset_y = render_settings.render_height - n;

	if (n > 0)
	{
		for (U32 tile_idx = 0; tile_idx < x; tile_idx++)
		{
			STileInfo tile_info;
			tile_info.tile_width = render_settings.tile_width;
			tile_info.tile_height = n;
			tile_info.tile_offset_x = tile_offset_x;
			tile_info.tile_offset_y = tile_offset_y;
			tile_info.result = new F32x4[tile_info.tile_width * tile_info.tile_height];
			tile_infos.push_back(tile_info);

			tile_offset_x += render_settings.tile_width;
		}
	}

	tile_offset_x = render_settings.render_width - m;
	tile_offset_y = 0;

	if (m > 0)
	{
		for (U32 tile_idx = 0; tile_idx < y; tile_idx++)
		{
			STileInfo tile_info;
			tile_info.tile_width = m;
			tile_info.tile_height = render_settings.tile_height;
			tile_info.tile_offset_x = tile_offset_x;
			tile_info.tile_offset_y = tile_offset_y;
			tile_info.result = new F32x4[tile_info.tile_width * tile_info.tile_height];
			tile_infos.push_back(tile_info);

			tile_offset_y += render_settings.tile_height;
		}
	}

	tile_offset_x = render_settings.render_width - m;
	tile_offset_y = render_settings.render_height - n;

	if (m > 0 && n > 0)
	{
		STileInfo tile_info;
		tile_info.tile_width = m;
		tile_info.tile_height = n;
		tile_info.tile_offset_x = tile_offset_x;
		tile_info.tile_offset_y = tile_offset_y;
		tile_info.result = new F32x4[tile_info.tile_width * tile_info.tile_height];
		tile_infos.push_back(tile_info);
	}

	for (auto device_manager_ptr : device_registry_ptr->cuda_device_managers)
	{
		SRenderWorkInfo work_info;
		work_info.finished_tile_count = 0;
		U32 tile_count = total_tile_count * (device_manager_ptr.second->GetDeviceComputePower() / total_compute_power);

		for (U32 tile_idx = 0; tile_idx < tile_count; tile_idx++)
		{
			work_info.tiles.push_back(tile_infos.back());
			tile_infos.pop_back();
		}

		cuda_work_infos.insert({ device_manager_ptr.second->GetId(), work_info });
	}

	for (auto device_manager_ptr : device_registry_ptr->ocl_device_managers)
	{
		SRenderWorkInfo work_info;
		work_info.finished_tile_count = 0;
		U32 tile_count = total_tile_count * (device_manager_ptr.second->GetDeviceComputePower() / total_compute_power);

		for (U32 tile_idx = 0; tile_idx < tile_count; tile_idx++)
		{
			work_info.tiles.push_back(tile_infos.back());
			tile_infos.pop_back();
		}

		ocl_work_infos.insert({ device_manager_ptr.second->GetId(), work_info });
	}

	for (auto device_manager_ptr : device_registry_ptr->debug_device_managers)
	{
		SRenderWorkInfo work_info;
		work_info.finished_tile_count = 0;
		U32 tile_count = total_tile_count * (device_manager_ptr.second->GetDeviceComputePower() / total_compute_power);

		for (U32 tile_idx = 0; tile_idx < tile_count; tile_idx++)
		{
			work_info.tiles.push_back(tile_infos.back());
			tile_infos.pop_back();
		}

		debug_work_infos.insert({ device_manager_ptr.second->GetId(), work_info });
	}
}

void CDistributionPolicyTiled::AssembleRenderResults(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos,
	const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos,
	const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos,
	CFramebuffer* framebuffer)
{
	for (auto& work_info_map : cuda_work_infos)
	{
		auto& work_info = work_info_map.second;

		for (auto& tile : work_info.tiles)
		{
			for (U32 y = 0; y < tile.tile_height; y++)
			for (U32 x = 0; x < tile.tile_width; x++)
			{
				U32 result_idx = y * tile.tile_width + x;
				framebuffer->SetColorBufferPx(x + tile.tile_offset_x, y + tile.tile_offset_y, tile.result[result_idx]);
			}
		}
	}

	for (auto& work_info_map : ocl_work_infos)
	{
		auto& work_info = work_info_map.second;

		for (auto& tile : work_info.tiles)
		{
			for (U32 y = 0; y < tile.tile_height; y++)
			for (U32 x = 0; x < tile.tile_width; x++)
			{
				U32 result_idx = y * tile.tile_width + x;

				framebuffer->SetColorBufferPx(x + tile.tile_offset_x, y + tile.tile_offset_y, tile.result[result_idx]);
			}
		}
	}

	for (auto& work_info_map : debug_work_infos)
	{
		auto& work_info = work_info_map.second;

		for (auto& tile : work_info.tiles)
		{
			for (U32 y = 0; y < tile.tile_height; y++)
			for (U32 x = 0; x < tile.tile_width; x++)
			{
				U32 result_idx = y * tile.tile_width + x;

				framebuffer->SetColorBufferPx(x + tile.tile_offset_x, y + tile.tile_offset_y, tile.result[result_idx]);
			}
		}
	}
}

S32 CDistributionPolicyTiled::GetStatusFlag(const std::unordered_map<U32, SRenderWorkInfo>& cuda_work_infos, 
	const std::unordered_map<U32, SRenderWorkInfo>& ocl_work_infos, 
	const std::unordered_map<U32, SRenderWorkInfo>& debug_work_infos)
{
	S32 status_flag = WR_RENDER_STATUS_FLAG_ERROR;
	bool finished = true;

	for (auto work_info_map : cuda_work_infos)
	{
		SRenderWorkInfo& work_info = work_info_map.second;
		finished &= work_info.finished_tile_count == work_info.tiles.size();
	}

	for (auto work_info_map : ocl_work_infos)
	{
		SRenderWorkInfo& work_info = work_info_map.second;
		finished &= work_info.finished_tile_count == work_info.tiles.size();
	}

	if (finished)
	{
		status_flag = WR_RENDER_STATUS_FLAG_HALT;
	}
	else
	{
		status_flag = WR_RENDER_STATUS_FLAG_RUNNING;
	}

	return status_flag;
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "RenderSettingsManagerProgressiveUdpt.hpp"

template class TRenderSettingsManagerProgressiveUdptBackend < CDeviceManagerCuda > ;
template class TRenderSettingsManagerProgressiveUdptBackend < CDeviceManagerOcl > ;
template class TRenderSettingsManagerProgressiveUdptBackend < CDeviceManagerDebug > ;

template <class DeviceManagerBackend_T>
void TRenderSettingsManagerProgressiveUdptBackend<DeviceManagerBackend_T>::CreateBuffers()
{
	auto buffer_size = sizeof(WrRenderSettingsProgressiveUdpt);
	_render_settings_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &_resource_container_ptr->render_settings);
}

template <class DeviceManagerBackend_T>
void TRenderSettingsManagerProgressiveUdptBackend<DeviceManagerBackend_T>::Update()
{
	if (_render_settings_buffer_hdl == 0)
	{
		CreateBuffers();
	}

	_device_manager_ptr->DestroyBuffer(_render_settings_buffer_hdl);

	auto buffer_size = sizeof(WrRenderSettingsProgressiveUdpt);
	_render_settings_buffer_hdl = _device_manager_ptr->CreateBuffer(buffer_size, &_resource_container_ptr->render_settings);
}

void CRenderSettingsManagerProgressiveUdpt::SetRenderSettings(WrRenderSettingsProgressiveUdpt& render_settings)
{
	_resource_container_ptr->render_settings = std::move(render_settings);
	_resource_container_ptr->update_flag = true;
}

void CRenderSettingsManagerProgressiveUdpt::SetRenderSettings(void* render_settings)
{
	_resource_container_ptr->render_settings = *static_cast<WrRenderSettingsProgressiveUdpt*>(render_settings);
	_resource_container_ptr->update_flag = true;
}

void CRenderSettingsManagerProgressiveUdpt::Update()
{
	if (_resource_container_ptr->update_flag)
	{
		UpdateBackends();
		_resource_container_ptr->update_flag = false;
	}
}

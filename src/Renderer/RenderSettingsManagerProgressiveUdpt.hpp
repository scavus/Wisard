/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Utility/CommonTypes.hpp"
#include "Scene/GfxResourceManager.hpp"

struct SRenderSettingsProgressiveUdptContainer
{
	WrRenderSettingsProgressiveUdpt render_settings;
	bool update_flag;
};

template<class DeviceManagerBackend_T>
class TRenderSettingsManagerProgressiveUdptBackend
{
	friend class CRenderSettingsManagerProgressiveUdpt;

	public:
		TRenderSettingsManagerProgressiveUdptBackend(){};
		~TRenderSettingsManagerProgressiveUdptBackend(){};

		TRenderSettingsManagerProgressiveUdptBackend(DeviceManagerBackend_T* device_manager_ptr, const SRenderSettingsProgressiveUdptContainer* resource_container_ptr)
		{
			_device_manager_ptr = device_manager_ptr;
			_resource_container_ptr = resource_container_ptr;
			_render_settings_buffer_hdl = 0;
		};

		SBufferHdl GetRenderSettingsBufferHdl() { return _render_settings_buffer_hdl; };

		void CreateBuffers();
		void Update();

	protected:

	private:
		DeviceManagerBackend_T* _device_manager_ptr;
		SBufferHdl _render_settings_buffer_hdl;
		const SRenderSettingsProgressiveUdptContainer* _resource_container_ptr;
};

class CRenderSettingsManagerProgressiveUdpt : public TGfxResourceManager<SRenderSettingsProgressiveUdptContainer, TRenderSettingsManagerProgressiveUdptBackend>
{
	public:
		using SStorage = SRenderSettingsProgressiveUdptContainer;

		CRenderSettingsManagerProgressiveUdpt(){};
		~CRenderSettingsManagerProgressiveUdpt(){};

		CRenderSettingsManagerProgressiveUdpt(SDeviceRegistry* device_registry_ptr, SRenderSettingsProgressiveUdptContainer* resource_container_ptr)
		{
			_resource_container_ptr = resource_container_ptr;
			CreateBackendManagers(device_registry_ptr);
		};

		WrRenderSettingsProgressiveUdpt& GetRenderSettingsRef() { return _resource_container_ptr->render_settings; };
		const WrRenderSettingsProgressiveUdpt& GetRenderSettingsConstRef() const { return _resource_container_ptr->render_settings; };

		void SetRenderSettings(WrRenderSettingsProgressiveUdpt& render_settings);
		void SetRenderSettings(void* render_settings);

		void Update();

	protected:

	private:
};

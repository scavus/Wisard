/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include <vector>

#include "Utility/CommonTypes.hpp"
#include "RenderDevice/ComputeDeviceManager.hpp"
#include "RenderDevice/DeviceBackend.hpp"
#include "Accelerators/BVH.hpp"
#include "Scene/Mesh.hpp"
#include "Scene/Geometry.hpp"
#include "Scene/GfxResourceManager.hpp"
#include "Accelerators/IAccelerator.hpp"
#include "Intersectors/IIntersector.hpp"

struct SSceneContext;

struct SAcceleratorBvh2Storage
{
	std::vector<WrTriangle> temp_mesh_geom;
	std::vector<SLinearBvh2Node> mesh_bvh2_nodes;
};

class CAcceleratorBvh2 : public IAccelerator
{
	public:
		CAcceleratorBvh2(SSceneContext* scene_ctx_ptr) { _scene_ctx_ptr = scene_ctx_ptr; };
		~CAcceleratorBvh2() {};

		virtual void Update() override;
		virtual void BindIntersector(IIntersector* intersector_ptr) override 
		{ 
			_intersectors.push_back(intersector_ptr); 
		};

		const SAcceleratorBvh2Storage& GetStorageConstRef() const { return _storage; };

	protected:

	private:
		SAcceleratorBvh2Storage _storage;
		bool _update_flag;

		std::vector<IIntersector*> _intersectors;

		SSceneContext* _scene_ctx_ptr;
};

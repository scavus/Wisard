/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "vector"

#include "Utility/CommonTypes.hpp"
#include "Scene/Geometry.hpp"
#include "Scene/Mesh.hpp"

// BVH2 Implementation based on PBRT

enum EPartitionTechnique
{
	MIDPOINT,
	SAH
};

struct SBvh2Node
{
	SAabb aabb;

	SBvh2Node* child_nodes[2];

	U32 split_axis;
	U32 first_prim_id;
	U32 prim_count;

	void InitLeafNode(U32 first_prim_id, U32 triangle_count, const SAabb& aabb);

	void InitInternalNode(U32 split_axis, SBvh2Node* child0_ptr, SBvh2Node* child1_ptr);
};


struct SBvh2PrimitiveInfo
{
	U32 prim_id;
	SAabb aabb;
	WrVector3f center;
};


struct SLinearBvh2Node
{
	SAabb aabb;
	U32 offset;
	U32 prim_count;
	U32 axis;
};

namespace Bvh2Utils
{
	void BuildBvh2(std::vector<WrTriangle>& triangles, std::vector<SLinearBvh2Node>& linear_nodes);
};

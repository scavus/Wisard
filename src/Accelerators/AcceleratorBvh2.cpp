/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "AcceleratorBvh2.hpp"
#include "Scene/SceneContext.hpp"

void CAcceleratorBvh2::Update()
{
	if (_update_flag && _intersectors.size() > 0)
	{
		_storage.mesh_bvh2_nodes.clear();
		_storage.temp_mesh_geom = _scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef();
		Bvh2Utils::BuildBvh2(_storage.temp_mesh_geom, _storage.mesh_bvh2_nodes);

		for (auto intersector : _intersectors)
		{
			intersector->Update(_scene_ctx_ptr);
		}

		_storage.temp_mesh_geom.clear();
		_update_flag = false;
	}
}

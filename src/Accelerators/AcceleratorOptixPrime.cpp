/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "AcceleratorOptixPrime.hpp"
#include "Scene/SceneContext.hpp"

void CAcceleratorOptixPrime::Update()
{
	if (_intersectors.size() == 0)
	{
		return;
	}

	RTPcontext api_ctx = _intersectors[0]->GetContext();
	RTPmodel api_model = _intersectors[0]->GetModel();
	CDeviceManagerCuda* device_manager_ptr = _intersectors[0]->GetDeviceManagerPtr();
	SKernelHdl convert_geometry_kernel_hdl = _intersectors[0]->GetConvertGeometryKernelHdl();

	auto geom_manager = _scene_ctx_ptr->mesh_geom_manager_ptr;
	auto geom = geom_manager->GetTransformedMeshGeomConstRef();
	size_t triangle_count = geom.size();

	size_t buffer_size = 0;

	buffer_size = sizeof(WrTriangle) * triangle_count;
	_intersectors[0]->_geom_buffer_hdl = device_manager_ptr->CreateBuffer(buffer_size, &geom[0]);

	U32 arg_idx = 0;
	U32 block_size = device_manager_ptr->GetWarpSize();

	buffer_size = sizeof(CIntersectorOptixPrime::SVertexOptixPrime) * triangle_count * 3;
	SBufferHdl vertex_buffer_hdl = device_manager_ptr->CreateBuffer(buffer_size);
	auto vertices = new CIntersectorOptixPrime::SVertexOptixPrime[triangle_count * 3];

	arg_idx = 0;
	device_manager_ptr->SetKernelArgValue(convert_geometry_kernel_hdl, arg_idx++, sizeof(U32), &triangle_count);
	device_manager_ptr->SetKernelArgBuffer(convert_geometry_kernel_hdl, arg_idx++, _intersectors[0]->_geom_buffer_hdl);
	device_manager_ptr->SetKernelArgBuffer(convert_geometry_kernel_hdl, arg_idx++, vertex_buffer_hdl);
	device_manager_ptr->DispatchKernel(convert_geometry_kernel_hdl,
	{ static_cast<U32>(std::ceil(static_cast<F32>(triangle_count) / static_cast<F32>(block_size))), 1, 1 },
	{ block_size, 1, 1 });

	SBufferCuda vertex_buffer = device_manager_ptr->GetBufferDesc(vertex_buffer_hdl);
	device_manager_ptr->ReadBuffer(vertex_buffer_hdl, vertices);

	RTPbufferdesc vertices_desc;
	(rtpBufferDescCreate(api_ctx, CIntersectorOptixPrime::OPTIX_PRIME_VERTEX_FORMAT, RTP_BUFFER_TYPE_HOST, vertices, &vertices_desc));
	(rtpBufferDescSetRange(vertices_desc, 0, triangle_count * 3));

	(rtpModelSetTriangles(api_model, 0, vertices_desc));
	(rtpModelUpdate(api_model, 0));

	for (U32 i = 1; i < _intersectors.size(); i++)
	{
		auto intersector = _intersectors[i];
		rtpModelCopy(api_model, intersector->GetModel());
	}

	delete[] vertices;
	(rtpBufferDescDestroy(vertices_desc));
	device_manager_ptr->DestroyBuffer(vertex_buffer_hdl);
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include "AcceleratorFireRays.hpp"
#include "Scene/SceneContext.hpp"

void CAcceleratorFireRays::Update()
{
	auto geom_manager = _scene_ctx_ptr->mesh_geom_manager_ptr;

	for (U32 i = 0; i < _intersectors.size(); i++)
	{
		auto device_manager_ptr = _intersectors[i]->GetDeviceManagerPtr();
		auto api = _intersectors[i]->GetApi();
		auto old_api_shape = _intersectors[i]->GetShape();

		if (old_api_shape)
		{
			api->DetachShape(old_api_shape);
		}

		auto geom = geom_manager->GetTransformedMeshGeomConstRef();
		U32 triangle_count = geom.size();

		size_t buffer_size = 0;
		
		buffer_size = sizeof(WrTriangle) * triangle_count;
		_intersectors[i]->_geom_buffer_hdl = device_manager_ptr->CreateBuffer(buffer_size, &geom[0]);

		auto convert_geometry_kernel_hdl = _intersectors[i]->GetConvertGeometryKernelHdl();

		U32 block_size = device_manager_ptr->GetWarpSize();
		U32 arg_idx = 0;

		F32* vertices = new F32[triangle_count * 3 * 3];
		buffer_size = sizeof(F32) * triangle_count * 3 * 3;
		SBufferHdl api_vertex_buffer_hdl = device_manager_ptr->CreateBuffer(buffer_size);

		int* indices = new int[triangle_count * 3];
		buffer_size = sizeof(int) * triangle_count * 3;
		SBufferHdl api_index_buffer_hdl = device_manager_ptr->CreateBuffer(buffer_size);

		arg_idx = 0;
		device_manager_ptr->SetKernelArgValue(convert_geometry_kernel_hdl, arg_idx++, sizeof(U32), &triangle_count);
		device_manager_ptr->SetKernelArgBuffer(convert_geometry_kernel_hdl, arg_idx++, _intersectors[i]->_geom_buffer_hdl);
		device_manager_ptr->SetKernelArgBuffer(convert_geometry_kernel_hdl, arg_idx++, api_vertex_buffer_hdl);
		device_manager_ptr->SetKernelArgBuffer(convert_geometry_kernel_hdl, arg_idx++, api_index_buffer_hdl);
		device_manager_ptr->DispatchKernel(convert_geometry_kernel_hdl,
		{ static_cast<U32>(std::ceil(static_cast<F32>(triangle_count) / static_cast<F32>(block_size))), 1, 1 },
		{ block_size, 1, 1 });

		device_manager_ptr->ReadBuffer(api_vertex_buffer_hdl, vertices);
		device_manager_ptr->ReadBuffer(api_index_buffer_hdl, indices);

		FireRays::Shape* api_shape = api->CreateMesh(vertices, triangle_count * 3, 3 * sizeof(F32), indices, 0, nullptr, triangle_count);
		api->AttachShape(api_shape);
		api->Commit();

		_intersectors[i]->SetShape(api_shape);

		delete[] vertices;
		delete[] indices;

		device_manager_ptr->DestroyBuffer(api_vertex_buffer_hdl);
		device_manager_ptr->DestroyBuffer(api_index_buffer_hdl);
	}
}

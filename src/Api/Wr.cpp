/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#include <unordered_map>

#include "Wr.h"
#include "Scene/SceneContext.hpp"
// #include "Renderer/IRenderEngine.hpp"
// #include "Integrators/IIntegrator.hpp"
#include "Renderer/RenderEngine.hpp"
#include "Intersectors/IIntersector.hpp"
#include "Intersectors/IIntersectorFactory.hpp"
#include "Intersectors/IntersectorOptixPrime.hpp"
#include "Intersectors/IntersectorFireRays.hpp"
#include "Intersectors/IntersectorMeshBvh2.hpp"
#include "Intersectors/IntersectorLightLinear.hpp"
#include "Intersectors/ISceneIntersector.hpp"
#include "Intersectors/SceneIntersectorDefault.hpp"
#include "Integrators/IIntegratorFactory.hpp"
#include "Integrators/IntegratorProgressiveDeferredUdpt.hpp"
#include "Integrators/IntegratorBranchedDeferredUdpt.hpp"
#include "Renderer/IRenderEngineFactory.hpp"
#include "Scene/WsfSceneImporter.hpp"

#include "Accelerators/AcceleratorBvh2.hpp"
#include "Accelerators/AcceleratorOptixPrime.hpp"
#include "Accelerators/AcceleratorFireRays.hpp"

/**********************************************************************************************
*	MAIN OBJECTS:																			  *
*																							  *
*    RenderEngine | --> Integrator0                                                           *
*                 | --> Integrator1                                                           *
*                 | ...                                                                       *
*                                                                                             *
*    Integrator | --> SceneIntersector                                                        *
*                                                                                             *
*    SceneIntersector | --> Intersector0                                                      *
*                     | --> Intersector1                                                      *
*                     | ...                                                                   *
*																							  *
*	 RenderEngine: Responsible of running light transport simulation algorithm.				  *
*		Composed of one or more integrator workers of one specific algorithm, RenderEngine	  *
*		distributes the work to these integrators using load balancing algorithms, gives      *
*		order, and assembles the results from integrators.									  *
*                                                                                             *
*    Intersector: Intersects only a single shape or specific shapes if implemented.           *
*        Example: CIntersectorOptixPrime tests only against mesh triangles,                   *
*        while TLightIntersectorLinear tests against all area light shapes.                   *
*                                                                                             *
*    SceneIntersector: Contains multiple intersectors, upon scene intersection query,         *
*        executes all bound intersectors to find scene-wide intersections.                    *
**********************************************************************************************/

#define WR_RENDER_ENGINE_FACTORY_DEFINE(iname) \
	class CRenderEngineFactory##iname : public IRenderEngineFactory \
				{ \
					public: \
						CRenderEngineFactory##iname(){}; \
						virtual ~CRenderEngineFactory##iname(){}; \
						virtual IRenderEngine* Create(SSceneContext* scene_ctx_ptr, void* render_settings) override \
						{ \
							return new TRenderEngine < iname > { scene_ctx_ptr, render_settings }; \
						}; \
				};

#define WR_RENDER_ENGINE_FACTORY_T(iname) CRenderEngineFactory##iname

WR_RENDER_ENGINE_FACTORY_DEFINE(TIntegratorProgressiveDeferredUdpt);
WR_RENDER_ENGINE_FACTORY_DEFINE(TIntegratorBranchedDeferredUdpt);

static std::unordered_map<std::string, IIntersectorFactory*> s_intersector_factories =
{
    { "OptixPrime", new CIntersectorOptixPrimeFactory },
    { "FireRays", new CIntersectorFireRaysFactory },
    { "MeshBvh2", new CIntersectorMeshBvh2Factory },
    { "AreaLightLinear", new CIntersectorLightLinearFactory }
};

static std::unordered_map<std::string, IRenderEngineFactory*> s_render_engine_factories =
{
	{ "ProgressiveDeferredUdpt", new WR_RENDER_ENGINE_FACTORY_T(TIntegratorProgressiveDeferredUdpt) },
	{ "BranchedDeferredUdpt", new WR_RENDER_ENGINE_FACTORY_T(TIntegratorBranchedDeferredUdpt) }
};

static std::unordered_map<U32, CSceneIntersectorDefault*> s_scene_intersectors;
static U32 s_created_scene_intersector_count = 0;

static std::unordered_map<U32, SSceneContext*> s_scene_ctxs;
static U32 s_created_scene_ctx_count = 0;

static std::unordered_map<U32, IRenderEngine*> s_render_engines;
static U32 s_created_render_engine_count = 0;

static CDeviceManagerCuda* s_cuda_device_managers;
static U32 s_cuda_device_manager_count = 0;
static CDeviceManagerOcl* s_ocl_device_managers;
static U32 s_ocl_device_manager_count = 0;
static SDeviceRegistry s_device_registry;

static const U32 WR_DEVICE_BACKEND_CUDA = 0;
static const U32 WR_DEVICE_BACKEND_OCL = 1;

static bool s_initialized_flag = false;

WR_API void wrInitialize()
{
	if (!s_initialized_flag)
	{
		cuInit(0);
		std::vector<SDeviceInfoCuda> cuda_device_infos;
		std::vector<SDeviceInfoOcl> ocl_device_infos;

		CDeviceManagerCuda::GetAvailableDeviceInfos(cuda_device_infos);
		CDeviceManagerOcl::GetAvailableDeviceInfos(ocl_device_infos);

		s_cuda_device_manager_count = cuda_device_infos.size();
		s_ocl_device_manager_count = ocl_device_infos.size();

		s_cuda_device_managers = new CDeviceManagerCuda[s_cuda_device_manager_count];
		s_ocl_device_managers = new CDeviceManagerOcl[s_ocl_device_manager_count];

		for (U32 i = 0; i < s_cuda_device_manager_count; i++)
		{
			s_cuda_device_managers[i] = CDeviceManagerCuda{ cuda_device_infos[i] };
		}

		for (U32 i = 0; i < s_ocl_device_manager_count; i++)
		{
			s_ocl_device_managers[i] = CDeviceManagerOcl{ ocl_device_infos[i] };
		}

		s_initialized_flag = true;
	}
}

WR_API void wrDeinitialize()
{

}

WR_API U32 wrGetAvailableDeviceCount()
{
	U32 total_device_count = 0;
	total_device_count += CDeviceManagerCuda::GetAvailableDeviceCount();
	total_device_count += CDeviceManagerOcl::GetAvailableDeviceCount();

	return total_device_count;
}

WR_API void wrGetAvailableDevices(WrDeviceId* device_ids)
{
	for (U32 i = 0; i < s_cuda_device_manager_count; i++)
	{
		WrDeviceId id;
		id.backend_type = WR_DEVICE_BACKEND_CUDA;
		id.idx = s_cuda_device_managers[i].GetId();
		device_ids[i] = id;
	}

	for (U32 i = 0, k = s_cuda_device_manager_count; i < s_ocl_device_manager_count; i++, k++)
	{
		WrDeviceId id;
		id.backend_type = WR_DEVICE_BACKEND_OCL;
		id.idx = s_ocl_device_managers[i].GetId();
		device_ids[k] = id;
	}
}

WR_API void wrGetDeviceInfo(WrDeviceId device_id, WrDeviceInfo* device_info_ptr)
{
    switch (device_id.backend_type)
	{
		case WR_DEVICE_BACKEND_CUDA:
		{
			strcpy(device_info_ptr->backend_type, "CUDA");
			// strncpy(device_info_ptr->device_name, s_device_registry.cuda_device_managers[device_id.idx]->GetDeviceName(), 64);
			// std::string asd = s_device_registry.cuda_device_managers[device_id.idx]->GetDeviceName();
			// device_info_ptr->device_name = const_cast<char*>(s_device_registry.cuda_device_managers[device_id.idx]->GetDeviceName().c_str());
			break;
		}

        case WR_DEVICE_BACKEND_OCL:
		{
			strcpy(device_info_ptr->backend_type, "OCL");
			// strcpy(device_info_ptr->device_name, s_device_registry.cuda_device_managers[device_id.idx]->GetDeviceName());
			break;
		}

        default:
			break;
	}
}

WR_API void wrRegisterDevice(WrDeviceId device_id)
{
	switch (device_id.backend_type)
	{
		case WR_DEVICE_BACKEND_CUDA:
		{
			s_device_registry.cuda_device_managers.insert({ device_id.idx, &s_cuda_device_managers[device_id.idx] });
			break;
		}

		case WR_DEVICE_BACKEND_OCL:
		{
			s_device_registry.ocl_device_managers.insert({ device_id.idx, &s_ocl_device_managers[device_id.idx] });
			break;
		}

		default:
			break;
	}
}

WR_API void wrUnregisterDevice(WrDeviceId device_id)
{
	switch (device_id.backend_type)
	{
		case WR_DEVICE_BACKEND_CUDA:
		{
			CDeviceManagerCuda* device_manager_ptr = s_device_registry.cuda_device_managers[device_id.idx];

			for (auto scene_ctx_pair : s_scene_ctxs)
			{
				SSceneContext* scene_ctx_ptr = scene_ctx_pair.second;
				scene_ctx_ptr->camera_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->mesh_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->area_light_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->env_light_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->deferred_shading_system_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->texture2d_manager_ptr->DestroyBackendManager(device_manager_ptr);
			}

			for (auto render_engine_pair : s_render_engines)
			{
				IRenderEngine* render_engine_ptr = render_engine_pair.second;
				render_engine_ptr->DeallocIntegrator(device_manager_ptr);
			}

			// device_ctx.scene_intersectors.clear();

			s_device_registry.cuda_device_managers.erase(device_id.idx);

			break;
		}

		case WR_DEVICE_BACKEND_OCL:
		{
			CDeviceManagerOcl* device_manager_ptr = s_device_registry.ocl_device_managers[device_id.idx];

			for (auto scene_ctx_pair : s_scene_ctxs)
			{
				SSceneContext* scene_ctx_ptr = scene_ctx_pair.second;
				scene_ctx_ptr->camera_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->mesh_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->area_light_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->env_light_manager_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->deferred_shading_system_ptr->DestroyBackendManager(device_manager_ptr);
				scene_ctx_ptr->texture2d_manager_ptr->DestroyBackendManager(device_manager_ptr);
			}

			for (auto render_engine_pair : s_render_engines)
			{
				IRenderEngine* render_engine_ptr = render_engine_pair.second;
				render_engine_ptr->DeallocIntegrator(device_manager_ptr);
			}

			// device_ctx.scene_intersectors.clear();

			s_device_registry.ocl_device_managers.erase(device_id.idx);

			break;
		}

		default:
			break;
	}
}

WR_API WrHandle wrCreateSceneCtx(WrSceneCtxProperties properties, const char* filename)
{
	WrHandle h;

    auto camera_storage = new CCameraManager::SStorage();
    auto mesh_storage = new CMeshManager::SStorage();
    auto mesh_geom_storage = new CMeshGeomManager::SStorage();
    auto area_light_storage = new CAreaLightManager::SStorage();
    auto env_light_storage = new CEnvLightManager::SStorage();
    auto texture2d_storage = new CTexture2dManager::SStorage();
    auto shader_storage = new CDeferredShadingSystem::SStorage();

    camera_storage->update_flag = true;
    mesh_storage->mesh_arr.update_flag = true;
    mesh_geom_storage->transformed_mesh_geom_update_flag = true;
    area_light_storage->area_lights_arr.update_flag = true;
    env_light_storage->update_flag = true;
    texture2d_storage->tex2d_arr.update_flag = true;
    shader_storage->mtl_array.update_flag = true;

	SSceneStorage scene_storage;
	scene_storage.camera_storage = camera_storage;
	scene_storage.mesh_storage = mesh_storage;
	scene_storage.mesh_geom_storage = mesh_geom_storage;
	scene_storage.area_light_storage = area_light_storage;
	scene_storage.env_light_storage = env_light_storage;
	scene_storage.shader_storage = shader_storage;
	scene_storage.texture2d_storage = texture2d_storage;

    if (filename)
    {
		WsfUtils::ImportWsf(filename, scene_storage);
    }

    SSceneContext* ctx = new SSceneContext;
    ctx->camera_manager_ptr = new CCameraManager{ &s_device_registry, camera_storage, ctx };
    ctx->mesh_manager_ptr = new CMeshManager{ &s_device_registry, mesh_storage, ctx };
    ctx->mesh_geom_manager_ptr = new CMeshGeomManager{ mesh_geom_storage, ctx };
    ctx->area_light_manager_ptr = new CAreaLightManager{ &s_device_registry, area_light_storage, ctx };
    ctx->env_light_manager_ptr = new CEnvLightManager{ &s_device_registry, env_light_storage, ctx };
    ctx->texture2d_manager_ptr = new CTexture2dManager{ &s_device_registry, texture2d_storage, ctx };
    ctx->deferred_shading_system_ptr = new CDeferredShadingSystem{ &s_device_registry, shader_storage, ctx };

	ctx->accelerators.insert({ "Bvh2", new CAcceleratorBvh2{ ctx } });
	ctx->accelerators.insert({ "OptixPrime", new CAcceleratorOptixPrime{ ctx } });
	ctx->accelerators.insert({ "FireRays", new CAcceleratorFireRays{ ctx } });

	h.idx = s_created_scene_ctx_count++;
	s_scene_ctxs.insert({ h.idx, ctx });

    return h;
}

WR_API void wrDestroySceneCtx(WrHandle h_scene_ctx)
{
    SSceneContext* ctx = s_scene_ctxs[h_scene_ctx.idx];
    delete ctx->camera_manager_ptr;
    delete ctx->mesh_manager_ptr;
    delete ctx->mesh_geom_manager_ptr;
    delete ctx->area_light_manager_ptr;
    delete ctx->env_light_manager_ptr;
    delete ctx->texture2d_manager_ptr;
    delete ctx->deferred_shading_system_ptr;
    ctx->accelerators.clear();

    delete ctx;

	s_scene_ctxs.erase(h_scene_ctx.idx);
}

WR_API WrSceneMetadata wrGetSceneMetadata(WrHandle h_scene_ctx)
{
	WrSceneMetadata metadata;
	metadata.triangle_count = s_scene_ctxs[h_scene_ctx.idx]->mesh_manager_ptr->GetMeshArrayConstRef().data.size();

	return metadata;
}

WR_API void wrSetMeshMaterial(WrHandle h_scene_ctx, WrResourceId mesh_id, WrResourceId mtl_id)
{
	SMeshId _mesh_id;
	_mesh_id.idx = mesh_id.idx;

	SDeferredMtlId _mtl_id;
	_mtl_id.idx = mtl_id.idx;

	s_scene_ctxs[h_scene_ctx.idx]->mesh_manager_ptr->SetMeshMaterial(_mesh_id, _mtl_id);
}

WR_API void wrSetCamera(WrHandle h_scene_ctx, WrCamera camera)
{
    s_scene_ctxs[h_scene_ctx.idx]->camera_manager_ptr->SetCamera(camera);
}

WR_API WrResourceId wrCreateTexture2d(WrHandle h_scene_ctx, WrTexture2d tex2d)
{
    WrResourceId wr_id;
    STexture2dId id = s_scene_ctxs[h_scene_ctx.idx]->texture2d_manager_ptr->CreateTexture2d(tex2d);
    wr_id.idx = id.idx;

    return wr_id;
}

WR_API void wrDestroyTexture2d(WrHandle h_scene_ctx, WrResourceId tex2d_id)
{
    s_scene_ctxs[h_scene_ctx.idx]->texture2d_manager_ptr->DestroyTexture2d(tex2d_id);
}

WR_API WrResourceId wrCreateAreaLight(WrHandle h_scene_ctx, WrAreaLight area_light)
{
    SSceneContext* scene_ctx_ptr = s_scene_ctxs[h_scene_ctx.idx];
	WrResourceId id = scene_ctx_ptr->area_light_manager_ptr->CreateAreaLight(area_light);

	return id;
}

WR_API void wrSetAreaLight(WrHandle h_scene_ctx, WrResourceId area_light_id, WrAreaLight area_light)
{
    SSceneContext* scene_ctx_ptr = s_scene_ctxs[h_scene_ctx.idx];
    scene_ctx_ptr->area_light_manager_ptr->SetAreaLight(area_light_id, area_light);
}

WR_API void wrDestroyAreaLight(WrHandle h_scene_ctx, WrResourceId area_light_id)
{
    SSceneContext* scene_ctx_ptr = s_scene_ctxs[h_scene_ctx.idx];
    scene_ctx_ptr->area_light_manager_ptr->DestroyAreaLight(area_light_id);
}

WR_API void wrSetEnvLight(WrHandle h_scene_ctx, WrEnvLight env_light)
{
    s_scene_ctxs[h_scene_ctx.idx]->env_light_manager_ptr->SetEnvLight(env_light);
}

WR_API WrResourceId wrCreateMaterial(WrHandle h_scene_ctx, WrMaterial mtl)
{
	WrResourceId mtl_id = s_scene_ctxs[h_scene_ctx.idx]->deferred_shading_system_ptr->CreateMaterial(mtl);

	return mtl_id;
}

WR_API void wrSetMaterial(WrHandle h_scene_ctx, WrResourceId mtl_id, WrMaterial mtl)
{
	s_scene_ctxs[h_scene_ctx.idx]->deferred_shading_system_ptr->SetMaterial(mtl_id, mtl);
}

WR_API void wrDestroyMaterial(WrHandle h_scene_ctx, WrResourceId mtl_id)
{
	s_scene_ctxs[h_scene_ctx.idx]->deferred_shading_system_ptr->DestroyMaterial(mtl_id);
}

void BindAcceleratorIntersector(SSceneContext* scene_ctx_ptr, IIntersector* intersector_ptr)
{
	std::string accelerator_name = intersector_ptr->GetAcceleratorName();

	if (!accelerator_name.empty())
	{
		// If we can find accelerator in map:
		scene_ctx_ptr->accelerators[accelerator_name]->BindIntersector(intersector_ptr);
	}
}

WR_API WrHandle wrCreateSceneIntersector(WrHandle h_scene_ctx, WrSceneIntersectorProperties properties)
{
	SSceneContext* scene_ctx_ptr =  s_scene_ctxs[h_scene_ctx.idx];

	IIntersector* mesh_intersector;
	IIntersector* area_light_intersector;

	if (properties.mesh_intersector_type)
	{
		switch (properties.device_id.backend_type)
		{
			case WR_DEVICE_BACKEND_CUDA:
			{
				CDeviceManagerCuda* device_manager_ptr = s_device_registry.cuda_device_managers[properties.device_id.idx];
				mesh_intersector = s_intersector_factories[properties.mesh_intersector_type]->Create(device_manager_ptr);
				break;
			}

			case WR_DEVICE_BACKEND_OCL:
			{
				CDeviceManagerOcl* device_manager_ptr = s_device_registry.ocl_device_managers[properties.device_id.idx];
				mesh_intersector = s_intersector_factories[properties.mesh_intersector_type]->Create(device_manager_ptr);
				break;
			}

			default:
				break;
		}
	}
	else
	{
		// mesh_intersector = s_intersector_factories["MeshBvh2"]->Create();
	}

	BindAcceleratorIntersector(scene_ctx_ptr, mesh_intersector);

	if (properties.area_light_intersector_type)
	{
		switch (properties.device_id.backend_type)
		{
			case WR_DEVICE_BACKEND_CUDA:
			{
				CDeviceManagerCuda* device_manager_ptr = s_device_registry.cuda_device_managers[properties.device_id.idx];
				area_light_intersector = s_intersector_factories[properties.area_light_intersector_type]->Create(device_manager_ptr);
				break;
			}

			case WR_DEVICE_BACKEND_OCL:
			{
				CDeviceManagerOcl* device_manager_ptr = s_device_registry.ocl_device_managers[properties.device_id.idx];
				area_light_intersector = s_intersector_factories[properties.area_light_intersector_type]->Create(device_manager_ptr);
				break;
			}

			default:
				break;
		}
	}
	else
	{
		// area_light_intersector = s_intersector_factories["AreaLightLinear"]->Create();
	}

	BindAcceleratorIntersector(scene_ctx_ptr, area_light_intersector);

	CSceneIntersectorDefault* scene_intersector = new CSceneIntersectorDefault;
	scene_intersector->SetMeshIntersector(mesh_intersector);
	scene_intersector->SetAreaLightIntersector(area_light_intersector);

	WrHandle h;
	h.idx = s_created_scene_intersector_count++;

	s_scene_intersectors.insert({ h.idx, scene_intersector });

	return h;
}

WR_API void wrDestroySceneIntersector(WrHandle h_scene_intersector)
{
	delete s_scene_intersectors[h_scene_intersector.idx];
}

WR_API void wrAllocIntegrator(WrHandle h_render_engine, WrIntegratorProperties properties)
{
    IRenderEngine* render_engine_ptr = s_render_engines[h_render_engine.idx];
	ISceneIntersector* scene_intersector_ptr = s_scene_intersectors[properties.h_scene_intersector.idx];

	switch (properties.device_id.backend_type)
	{
		case WR_DEVICE_BACKEND_CUDA:
		{
			CDeviceManagerCuda* device_manager_ptr = s_device_registry.cuda_device_managers[properties.device_id.idx];
			render_engine_ptr->AllocIntegrator(device_manager_ptr, scene_intersector_ptr);
			break;
		}

		case WR_DEVICE_BACKEND_OCL:
		{
			CDeviceManagerOcl* device_manager_ptr = s_device_registry.ocl_device_managers[properties.device_id.idx];
			render_engine_ptr->AllocIntegrator(device_manager_ptr, scene_intersector_ptr);
			break;
		}

		default:
			break;
	}
}

WR_API void wrDeallocIntegrator(WrHandle h_render_engine, WrDeviceId device_id)
{
	IRenderEngine* render_engine_ptr = s_render_engines[h_render_engine.idx];

	switch (device_id.backend_type)
	{
		case WR_DEVICE_BACKEND_CUDA:
		{
			CDeviceManagerCuda* device_manager_ptr = s_device_registry.cuda_device_managers[device_id.idx];
			render_engine_ptr->DeallocIntegrator(device_manager_ptr);
		}

		case WR_DEVICE_BACKEND_OCL:
		{
			CDeviceManagerOcl* device_manager_ptr = s_device_registry.ocl_device_managers[device_id.idx];
			render_engine_ptr->DeallocIntegrator(device_manager_ptr);
		}

		default:
			break;
	}
}

WR_API WrHandle wrCreateRenderEngine(WrHandle h_scene_ctx, WrRenderEngineProperties properties)
{
    SSceneContext* scene_ctx_ptr = s_scene_ctxs[h_scene_ctx.idx];
    IRenderEngine* render_engine_ptr = s_render_engine_factories[properties.integrator_type]->Create(scene_ctx_ptr, properties.render_settings);

    WrHandle h;
	h.idx = s_created_render_engine_count++;
	s_render_engines.insert({ h.idx, render_engine_ptr });

    return h;
}

WR_API void wrDestroyRenderEngine(WrHandle h_render_engine)
{
    delete s_render_engines[h_render_engine.idx];
}

WR_API S32 wrRender(WrHandle h_render_engine)
{
	S32 status_flag = 0;
    IRenderEngine* render_engine_ptr = s_render_engines[h_render_engine.idx];
    render_engine_ptr->Update();
	status_flag = render_engine_ptr->Render();

	return status_flag;
}

WR_API void wrGetFramebuffer(WrHandle h_render_engine, WrFramebuffer* framebuffer_ptr)
{
	CFramebuffer* framebuffer = s_render_engines[h_render_engine.idx]->GetFramebuffer();

    framebuffer_ptr->width = framebuffer->GetWidth();
    framebuffer_ptr->height = framebuffer->GetHeight();
    framebuffer_ptr->color_buffer = framebuffer->GetColorBuffer();
}

WR_API U32 WrGetSceneCtxCount()
{
    return s_scene_ctxs.size();
}

/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Api/WrCommon.h"

typedef struct WrResourceId
{
	// U32 type;
	U32 idx;
} WrResourceId;

static const WrResourceId WR_RESOURCE_ID_NULL = { UINT32_MAX };

#ifdef _MSC_VER
_declspec(align(8))
#endif
typedef struct
#ifdef __GNUC__
__attribute__((aligned(8)))
#endif
WrVector2f
{
	F32 x;
	F32	y;

} WrVector2f;

typedef WrVector2f F32x2;

WrVector2f wrMakeVec2f(F32 x, F32 y);

struct WrVector2u
{
	U32 x;
	U32 y;
};

#ifdef _MSC_VER
_declspec(align(16))
#endif
typedef struct
#ifdef __GNUC__
__attribute__((aligned(16)))
#endif
WrVector3f
{
	F32 x;
	F32 y;
	F32 z;

} WrVector3f;

typedef WrVector3f F32x3;

WrVector3f wrMakeVec3f(F32 x, F32 y, F32 z);

#ifdef _MSC_VER
_declspec(align(16))
#endif
typedef struct
#ifdef __GNUC__
__attribute__((aligned(16)))
#endif
WrVector4f
{
	F32 x;
	F32 y;
	F32 z;
	F32 w;

} WrVector4f;

typedef WrVector4f F32x4;

WrVector4f wrMakeVec4f(F32 x, F32 y, F32 z, F32 w);

struct WrMatrix4x4
{
	union
	{
		struct
		{
			WrVector4f r0;
			WrVector4f r1;
			WrVector4f r2;
			WrVector4f r3;
		};

		struct
		{
			F32 m00, m01, m02, m03;
			F32 m10, m11, m12, m13;
			F32 m20, m21, m22, m23;
			F32 m30, m31, m32, m33;
		};
	};

};

WrMatrix4x4 wrMakeMatrix4x4(const WrVector4f& row0, const WrVector4f& row1, const WrVector4f& row2, const WrVector4f& row3);

enum WrCameraType
{
	PINHOLE,
	THINLENS
};

struct WrPinholeCameraProperties
{
	F32 fov;
};

struct WrThinLensCameraProperties
{
	F32 aperture_size;
	F32 focal_length;
	F32 fov;
};

struct WrCamera
{
	WrCameraType type;

	union UProperties
	{
		WrPinholeCameraProperties pinhole;
		WrThinLensCameraProperties thinlens;

	} properties;

	WrMatrix4x4 camera_to_world_mtx;
};


struct WrQuad
{
	WrVector3f base;
	WrVector3f normal;
	WrVector3f u;
	WrVector3f v;
	F32 inv_length_sqr_u;
	F32 inv_length_sqr_v;
};

struct WrDisk
{
	WrVector3f center;
	WrVector3f normal;
	F32 radius;
};

struct WrSphere
{
	WrVector3f center;
	F32 radius;
};

struct WrVertex
{
	WrVector3f pos;
	WrVector3f normal;
	WrVector3f tangent;
	WrVector3f binormal;
	WrVector2f texcoord;

};

struct WrTriangle
{
	WrVertex vtx[3];
	
	U32 mesh_id;
};

enum WrLightType
{
	LIGHT_TYPE_QUAD,
	LIGHT_TYPE_DISK,
	LIGHT_TYPE_SPHERE,
	LIGHT_TYPE_TUBE
};

struct WrAreaLight
{
	WrLightType type;

	WrMatrix4x4 obj_to_world_mtx;
	WrMatrix4x4 world_to_obj_mtx;

	F32 intensity;
	F32 exposure;
	F32x4 color;

	union UShape
	{
		WrQuad quad;
		WrDisk disk;
		WrSphere sphere;

	} shape;
};

struct WrEnvLight
{
	WrMatrix4x4 obj_to_world_mtx;
	WrMatrix4x4 world_to_obj_mtx;

	U32 width;
	U32 height;
	void* data;

	F32* cdf;
	F32* conditional_cdfs;
	F32* marginal_cdf;
};

struct WrLambertShaderUniformBufferDesc
{
	F32x4 albedo_color;
	S32 albedo_color_tex_slot;
};

struct SBlinnOnlyUniformBufferDesc
{
	F32x4 specular_color;
	S8 specular_color_tex_slot;

	F32 ior;
	F32 roughness;
	S8 roughness_tex_slot;
};

struct WrGgxShaderUniformBufferDesc
{
	F32x4 specular_color;
	S32 specular_color_tex_slot;

	F32 ior;
	S32 ior_tex_slot;

	F32 alpha;
	S32 alpha_tex_slot;
};

typedef WrGgxShaderUniformBufferDesc WrBeckmannShaderUniformBufferDesc;

struct WrMirrorShaderUniformBufferDesc
{
	F32x4 pad;
};

static const WrResourceId WR_LAMBERT_SHADER_ID = { 0 };
static const WrResourceId WR_GGX_SHADER_ID = { 1 };
static const WrResourceId WR_MIRROR_SHADER_ID = { 2 };
static const WrResourceId WR_BECKMANN_SHADER_ID = { 3 };

enum WrTextureUsage
{
	READ_ONLY,
	WRITE_ONLY,
	READ_WRITE
};

enum WrTextureFormat
{
	R_U8,
	R_S8,
	R_U16,
	R_S16,
	R_NORM_U8,
	R_NORM_S8,
	R_NORM_U16,
	R_NORM_S16,
	R_F32,

	RG_U8,
	RG_S8,
	RG_U16,
	RG_S16,
	RG_NORM_U8,
	RG_NORM_S8,
	RG_NORM_U16,
	RG_NORM_S16,
	RG_F32,

	RGBA_U8,
	RGBA_S8,
	RGBA_U16,
	RGBA_S16,
	RGBA_NORM_U8,
	RGBA_NORM_S8,
	RGBA_NORM_U16,
	RGBA_NORM_S16,
	RGBA_F32
};

struct WrTexture1dDesc
{
	U32 width;
	WrTextureUsage usage;
	WrTextureFormat format;
};

struct WrTexture1d
{
	WrTexture1dDesc desc;
	void* data;
};

struct WrTexture2dDesc
{
	U32 width;
	U32 height;
	WrTextureUsage usage;
	WrTextureFormat format;
};

struct WrTexture2d
{
	WrTexture2dDesc desc;
	void* data;
};

struct WrTexture3dDesc
{
	U32 width;
	U32 height;
	U32 depth;
	WrTextureUsage usage;
	WrTextureFormat format;
};

struct WrTexture3d
{
	WrTexture3dDesc desc;
	void* data;
};

static const size_t WR_SHADER_TEX2D_SLOT_COUNT = 8;
static const size_t WR_BUILTIN_SHADER_COUNT = 4;

struct WrMaterial
{
	WrResourceId tex_slots[WR_SHADER_TEX2D_SLOT_COUNT];

	void* uniform_buffer_data;
	size_t uniform_buffer_size;

	WrResourceId shader_id;
};

static const WrMaterial WR_DEFAULT_MTL =
{
	{
		WR_RESOURCE_ID_NULL,
		WR_RESOURCE_ID_NULL,
		WR_RESOURCE_ID_NULL,
		WR_RESOURCE_ID_NULL,
		WR_RESOURCE_ID_NULL,
		WR_RESOURCE_ID_NULL,
		WR_RESOURCE_ID_NULL,
		WR_RESOURCE_ID_NULL
	},
	nullptr,
	0,
	WR_RESOURCE_ID_NULL
};

typedef struct WrRenderSettingsCommon
{
	U32 render_width;
	U32 render_height;

	U32 tile_width;
	U32 tile_height;
} WrRenderSettingsCommon;

typedef struct WrRenderSettingsProgressiveUdpt
{
	WrRenderSettingsCommon common;

	U32 max_indirect_bounce_count;
	U32 iteration_count;
	bool enable_mis;
	bool enable_rr;
} WrRenderSettingsProgressiveUdpt;

typedef struct WrRenderSettingsBranchedUdpt
{
	WrRenderSettingsCommon common;

	U32 max_indirect_bounce_count;
	U32 aa_sample_count;
	U32 direct_illum_sample_count;
	U32 indirect_illum_sample_count;

	bool enable_mis;
	bool enable_rr;
} WrRenderSettingsBranchedUdpt;

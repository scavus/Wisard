/******************************************************************************
* Copyright (C) 2016 Sencer Cavus
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#pragma once

#include "Api/WrCommon.h"
#include "Api/WrTypes.h"

#define WR_API extern "C"

WR_API typedef struct WrHandle
{
	U32 idx;
} WrHandle;

WR_API typedef struct WrDeviceId
{
	U32 backend_type;
	U32 idx;
} WrDeviceId;

WR_API typedef struct WrDeviceInfo
{
    char backend_type[8];
    char device_name[1024];
} WrDeviceInfo;

WR_API typedef struct WrSceneCtxProperties
{

} WrSceneCtxProperties;

// WR_API static const WrSceneCtxProperties WR_DEFAULT_SCENE_CTX_PROPERTIES = { 0 };

WR_API typedef struct WrSceneMetadata
{
	U32 triangle_count;
} WrSceneMetadata;

WR_API typedef struct WrSceneIntersectorProperties
{
	WrDeviceId device_id;
	char* mesh_intersector_type;
	char* area_light_intersector_type;
	// char* volume_intersector_type;
	// char* curve_intersector_type;
} WrSceneIntersectorProperties;

WR_API typedef struct WrIntegratorProperties
{
	WrDeviceId device_id;
	WrHandle h_scene_intersector;
} WrIntegratorProperties;

WR_API typedef struct WrRenderEngineProperties
{
	const char* integrator_type;
	void* render_settings;
} WrRenderEngineProperties;

static const S32 WR_RENDER_STATUS_FLAG_ERROR = -1;
static const S32 WR_RENDER_STATUS_FLAG_HALT = 0;
static const S32 WR_RENDER_STATUS_FLAG_RUNNING = 1;

WR_API typedef struct WrFramebuffer
{
	U32 width;
	U32 height;
	F32x4* color_buffer;
} WrFramebuffer;

static const WrRenderEngineProperties WR_DEFAULT_RENDER_ENGINE_PROPERTIES = { 0 };

static const WrSceneIntersectorProperties WR_DEFAULT_SCENE_INTERSECTOR_PROPERTIES = { 0, 0 };

WR_API typedef struct WrSurfaceShaderDesc
{
	char* filename;
	char* eval_kernel_name;
	char* sample_kernel_name;
} WrSurfaceShaderDesc;

WR_API void wrInitialize();
WR_API void wrDeinitialize();
WR_API U32 wrGetAvailableDeviceCount();
WR_API void wrGetAvailableDevices(WrDeviceId* device_ids);
WR_API void wrGetDeviceInfo(WrDeviceId device_id, WrDeviceInfo* device_info_ptr);
WR_API void wrRegisterDevice(WrDeviceId device_id);
WR_API void wrUnregisterDevice(WrDeviceId device_id);

WR_API WrHandle wrCreateSceneCtx(WrSceneCtxProperties properties /* = WR_DEFAULT_SCENE_CTX_PROPERTIES */, const char* filename = NULL);
WR_API void wrDestroySceneCtx(WrHandle h_scene_ctx);

WR_API WrSceneMetadata wrGetSceneMetadata(WrHandle h_scene_ctx);

WR_API void wrGetMeshIds(WrHandle h_scene_ctx, WrResourceId* ids);
WR_API void wrSetMeshMaterial(WrHandle h_scene_ctx, WrResourceId mesh_id, WrResourceId mtl_id);

WR_API void wrSetCamera(WrHandle h_scene_ctx, WrCamera camera);

WR_API WrResourceId wrCreateTexture2d(WrHandle h_scene_ctx, WrTexture2d tex2d);
WR_API void wrDestroyTexture2d(WrHandle h_scene_ctx, WrResourceId tex2d_id);

WR_API WrResourceId wrCreateAreaLight(WrHandle h_scene_ctx, WrAreaLight area_light);
WR_API void wrSetAreaLight(WrHandle h_scene_ctx, WrResourceId area_light_id, WrAreaLight area_light);
WR_API void wrDestroyAreaLight(WrHandle h_scene_ctx, WrResourceId area_light_id);

WR_API void wrSetEnvLight(WrHandle h_scene_ctx, WrEnvLight env_light);

WR_API WrResourceId wrCreateMaterial(WrHandle h_scene_ctx, WrMaterial mtl);
WR_API void wrSetMaterial(WrHandle h_scene_ctx, WrResourceId mtl_id, WrMaterial mtl);
WR_API void wrDestroyMaterial(WrHandle h_scene_ctx, WrResourceId mtl_id);

WR_API WrHandle wrCreateSceneIntersector(WrHandle h_scene_ctx, WrSceneIntersectorProperties properties = WR_DEFAULT_SCENE_INTERSECTOR_PROPERTIES);
WR_API void wrDestroySceneIntersector(WrHandle h_scene_intersector);

WR_API WrHandle wrCreateRenderEngine(WrHandle h_scene_ctx, WrRenderEngineProperties properties = WR_DEFAULT_RENDER_ENGINE_PROPERTIES);
WR_API void wrDestroyRenderEngine(WrHandle h_render_engine);
WR_API void wrAllocIntegrator(WrHandle h_render_engine, WrIntegratorProperties properties);
WR_API void wrDeallocIntegrator(WrHandle h_render_engine, WrDeviceId device_id);
WR_API S32 wrRender(WrHandle h_render_engine);
WR_API void wrGetFramebuffer(WrHandle h_render_engine, WrFramebuffer* framebuffer_ptr);

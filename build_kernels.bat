nvcc -x cu -O3 -w -cubin -arch=sm_50 -maxrregcount=96 -use_fast_math -cudart static src/Kernels/KIntegratorProgressiveDeferredUdpt.c -o data/Kernels/KIntegratorProgressiveDeferredUdpt.cubin
nvcc -x cu -O3 -w -cubin -arch=sm_50 -maxrregcount=96 -use_fast_math -cudart static src/Kernels/KIntegratorBranchedDeferredUdpt.c -o data/Kernels/KIntegratorBranchedDeferredUdpt.cubin
nvcc -x cu -O3 -w -cubin -arch=sm_50 -maxrregcount=96 -use_fast_math -cudart static src/Kernels/KBuiltinShaders.c -o data/Kernels/KBuiltinShaders.cubin
nvcc -x cu -O3 -w -cubin -arch=sm_50 -maxrregcount=96 -use_fast_math -cudart static src/Kernels/KIntersectorTriangleBvh2Default.c -o data/Kernels/KIntersectorTriangleBvh2Default.cubin
nvcc -x cu -O3 -w -cubin -arch=sm_50 -maxrregcount=96 -use_fast_math -cudart static src/Kernels/KIntersectorLightLinear.c -o data/Kernels/KIntersectorLightLinear.cubin
nvcc -x cu -O3 -w -cubin -arch=sm_50 -maxrregcount=96 -use_fast_math -cudart static src/Kernels/KIntersectorOptixPrime.c -o data/Kernels/KIntersectorOptixPrime.cubin
